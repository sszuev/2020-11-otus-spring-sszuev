package com.gitlab.sszuev.questions.shell;

import com.gitlab.sszuev.questions.NoShellTestBase;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.CommandNotCurrentlyAvailable;
import org.springframework.shell.ExitRequest;
import org.springframework.shell.Shell;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;

import java.util.stream.IntStream;

/**
 * Created by @ssz on 14.12.2020.
 */
@TestPropertySource("classpath:test.properties")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
// otherwise there may be wrong bean inside
public class QuizCommandsTest extends NoShellTestBase {

    @Autowired
    private Shell shell;

    @Test
    public void testSimpleFullCycle() {
        String name = "XXX";
        String first = (String) shell.evaluate(() -> "name " + name);
        Assertions.assertTrue(first.contains("Hello, <" + name + ">"));

        Assertions.assertNull(shell.evaluate(() -> "1"));
        Assertions.assertTrue(shell.evaluate(() -> "1") instanceof ExitRequest);
    }

    @Test
    public void testUnavailableCommands() {
        IntStream.rangeClosed(1, 10).mapToObj(String::valueOf)
                .forEach(s -> Assertions.assertTrue(shell.evaluate(() -> s) instanceof CommandNotCurrentlyAvailable));
        Assertions.assertTrue(shell.evaluate(() -> "r") instanceof CommandNotCurrentlyAvailable);
        Assertions.assertTrue(shell.evaluate(() -> "repeat") instanceof CommandNotCurrentlyAvailable);
    }
}
