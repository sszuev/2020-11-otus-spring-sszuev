package com.gitlab.sszuev.questions.dao;

import com.gitlab.sszuev.questions.NoShellTestBase;
import com.gitlab.sszuev.questions.domain.Answer;
import com.gitlab.sszuev.questions.domain.Question;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by @ssz on 13.12.2020.
 */
public class QuestionDAOMockTest extends NoShellTestBase {

    @Autowired
    private QuestionsDAO questionsDAO;

    @Test
    public void testDAO() {
        Assertions.assertEquals(2, questionsDAO.listAllQuestions().count());
        Assertions.assertEquals(0, questionsDAO.listAllAnswersByQuestion(null).count());

        Map<Long, Question> questions = questionsDAO.listAllQuestions().collect(Collectors.toMap(Question::getID, Function.identity()));
        Assertions.assertEquals(2, questions.size());
        Question q1 = questions.get(1L);
        Question q2 = questions.get(42L);
        Assertions.assertEquals("Q1", q1.getMessage());
        Assertions.assertEquals("Q2", q2.getMessage());

        Assertions.assertEquals(2, questionsDAO.listAllAnswersByQuestion(q1).count());
        Assertions.assertEquals(3, questionsDAO.listAllAnswersByQuestion(q2).count());

        Assertions.assertEquals(Arrays.asList("A", "B", "C"), questionsDAO.listAllAnswersByQuestion(q2)
                .filter(x -> !x.isCorrect()).map(Answer::getMessage).collect(Collectors.toList()));

        Assertions.assertEquals(Arrays.asList("D", "F"), questionsDAO.listAllAnswersByQuestion(q1)
                .filter(Answer::isCorrect).map(Answer::getMessage).collect(Collectors.toList()));
    }

    @TestConfiguration
    public static class TestConfig {

        @Bean
        @Primary
        public CSVResource csvResource() {
            CSVResource res = Mockito.mock(CSVResource.class);
            CSVResource.Record q1 = Mockito.mock(CSVResource.Record.class);
            CSVResource.Record q2 = Mockito.mock(CSVResource.Record.class);

            Mockito.when(q1.getId()).thenReturn(1L);
            Mockito.when(q2.getId()).thenReturn(42L);

            Mockito.when(q1.getQuestion()).thenReturn("Q1");
            Mockito.when(q2.getQuestion()).thenReturn("Q2");

            Mockito.when(q1.correctAnswers()).thenReturn(Stream.of("D", "F"));
            Mockito.when(q2.wrongAnswers()).thenReturn(Stream.of("A", "B", "C"));

            Mockito.when(res.load()).thenReturn(Stream.of(q1, q2));
            return res;
        }

    }

}
