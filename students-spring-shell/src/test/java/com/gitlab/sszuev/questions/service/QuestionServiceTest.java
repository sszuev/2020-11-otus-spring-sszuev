package com.gitlab.sszuev.questions.service;

import com.gitlab.sszuev.questions.NoShellTestBase;
import com.gitlab.sszuev.questions.domain.Question;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.TestPropertySource;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by @ssz on 13.12.2020.
 */
@TestPropertySource("classpath:test.properties")
public class QuestionServiceTest extends NoShellTestBase {

    @Autowired
    private QuestionService service;

    @Test
    public void testListQuestions() {
        Assertions.assertEquals(2, service.questions().peek(System.out::println).count());
    }

    @Test
    public void testListAnswers() {
        List<Question> questions = service.questions().collect(Collectors.toList());
        Assertions.assertEquals(2, questions.size());
        Question q1 = questions.stream().filter(x -> x.getID() == 3).findFirst().orElseThrow(AssertionError::new);
        Question q2 = questions.stream().filter(x -> x.getID() == 4).findFirst().orElseThrow(AssertionError::new);
        Assertions.assertEquals(6, service.answers(q1).count());
        Assertions.assertEquals(3, service.answers(q2).count());
    }
}
