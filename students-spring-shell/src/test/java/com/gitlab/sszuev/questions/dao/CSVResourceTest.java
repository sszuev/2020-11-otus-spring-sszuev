package com.gitlab.sszuev.questions.dao;

import com.gitlab.sszuev.questions.NoShellTestBase;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.TestPropertySource;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by @ssz on 13.12.2020.
 */
@TestPropertySource("classpath:test.properties")
public class CSVResourceTest extends NoShellTestBase {

    @Autowired
    private CSVResource resource;

    @Test
    public void testLoad() {
        Assertions.assertEquals(2, resource.load().peek(System.out::println).count());
        List<CSVResource.Record> actual = resource.load().collect(Collectors.toList());
        Assertions.assertEquals(2, actual.size());
        Assertions.assertEquals(Arrays.asList(3L, 4L), actual.stream()
                .map(CSVResource.Record::getId).collect(Collectors.toList()));
        Assertions.assertEquals(5, actual.get(0).correctAnswers().count());
        Assertions.assertEquals(1, actual.get(0).wrongAnswers().count());

        Assertions.assertEquals(1, actual.get(1).correctAnswers().count());
        Assertions.assertEquals(2, actual.get(1).wrongAnswers().count());
    }
}
