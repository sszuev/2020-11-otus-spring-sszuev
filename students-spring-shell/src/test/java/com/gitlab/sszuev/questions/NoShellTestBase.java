package com.gitlab.sszuev.questions;

import org.springframework.boot.test.context.SpringBootTest;

/**
 * Created by @ssz on 13.12.2020.
 *
 * @see <a href='https://otus.ru/nest/post/869/'>how to disable shell in tests (russian)</a>
 */
@SpringBootTest(properties = {
        org.springframework.shell.jline.InteractiveShellApplicationRunner.SPRING_SHELL_INTERACTIVE_ENABLED + "=false",
        org.springframework.shell.jline.ScriptShellApplicationRunner.SPRING_SHELL_SCRIPT_ENABLED + "=false"
})
public class NoShellTestBase {
}
