package com.gitlab.sszuev.questions.shell;

import com.gitlab.sszuev.questions.NoShellTestBase;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.TestPropertySource;

import java.util.Optional;

/**
 * Created by @ssz on 14.12.2020.
 */
@TestPropertySource("classpath:test.properties")
public class QuizHelperTest extends NoShellTestBase {
    @Autowired
    private QuizHelper helper;

    @Test
    public void testGiveAllAnswers() {
        Assertions.assertTrue(helper.current().isPresent());
        QuizHelper.Item first = helper.current().orElseThrow(AssertionError::new);
        int size = first.answersCount();
        Assertions.assertTrue(size > 0);
        QuizHelper.Item next = helper.next(first.getAnswer(size - 1)).orElseThrow(AssertionError::new);
        Assertions.assertNotEquals(first, next);
        Assertions.assertNotEquals(first.getQuestion(), next.getQuestion());

        Optional<QuizHelper.Item> last = helper.next(next.getAnswer(0));
        Assertions.assertTrue(last.isEmpty());
        Assertions.assertTrue(helper.current().isEmpty());
    }

    @Test
    public void testAnswerAndReset() {
        Assertions.assertTrue(helper.current().isPresent());
        QuizHelper.Item first = helper.current().orElseThrow(AssertionError::new);
        QuizHelper.Item next = helper.next(first.getAnswer(0)).orElseThrow(AssertionError::new);
        Assertions.assertNotEquals(first, next);
        Assertions.assertNotEquals(first.getQuestion(), next.getQuestion());

        helper.reset();
        Assertions.assertEquals(first.getQuestion(), helper.current().orElseThrow(AssertionError::new).getQuestion());
    }
}
