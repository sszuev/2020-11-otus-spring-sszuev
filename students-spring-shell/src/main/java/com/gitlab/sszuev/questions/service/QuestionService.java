package com.gitlab.sszuev.questions.service;

import com.gitlab.sszuev.questions.domain.Answer;
import com.gitlab.sszuev.questions.domain.Question;

import java.util.stream.Stream;

/**
 * The main service that provides access to the question data.
 * <p>
 * Created by @ssz on 13.12.2020.
 */
public interface QuestionService {

    /**
     * Lists all questions from the system.
     *
     * @return a {@code Stream} of {@link Question}s
     */
    Stream<Question> questions();

    /**
     * Lists all answers for the given question.
     *
     * @param question {@link Question}, not {@code null}
     * @return a {@code Stream} of {@link Answer}s (<b>order is not guaranteed</b>)
     */
    Stream<Answer> answers(Question question);
}
