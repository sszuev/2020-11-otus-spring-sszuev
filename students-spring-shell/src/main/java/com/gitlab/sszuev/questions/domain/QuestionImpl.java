package com.gitlab.sszuev.questions.domain;

import java.util.Objects;

/**
 * Created by @ssz on 13.12.2020.
 */
public class QuestionImpl implements Question {
    private final long id;
    private final String txt;

    public QuestionImpl(long id, String txt) {
        this.id = id;
        this.txt = Objects.requireNonNull(txt);
    }

    @Override
    public Long getID() {
        return id;
    }

    @Override
    public String getMessage() {
        return txt;
    }

    @Override
    public String toString() {
        return String.format("Question{id=%d, txt='%s'}", id, txt);
    }
}
