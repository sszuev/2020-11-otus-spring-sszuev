package com.gitlab.sszuev.questions.service;

import com.gitlab.sszuev.questions.dao.QuestionsDAO;
import com.gitlab.sszuev.questions.domain.Answer;
import com.gitlab.sszuev.questions.domain.Question;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by @ssz on 13.12.2020.
 */
@Service
public class QuestionServiceImpl implements QuestionService {

    private final QuestionsDAO repo;
    private final Random random;

    public QuestionServiceImpl(QuestionsDAO repo, @Value("#{new java.util.Random()}") Random random) {
        this.repo = Objects.requireNonNull(repo);
        this.random = Objects.requireNonNull(random);
    }

    @Override
    public Stream<Question> questions() {
        return repo.listAllQuestions();
    }

    @Override
    public Stream<Answer> answers(Question question) {
        List<Answer> res = repo.listAllAnswersByQuestion(question).collect(Collectors.toList());
        Collections.shuffle(res, random);
        return res.stream();
    }
}
