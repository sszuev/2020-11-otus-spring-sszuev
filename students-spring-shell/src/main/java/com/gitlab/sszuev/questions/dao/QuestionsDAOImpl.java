package com.gitlab.sszuev.questions.dao;

import com.gitlab.sszuev.questions.domain.Answer;
import com.gitlab.sszuev.questions.domain.AnswerImpl;
import com.gitlab.sszuev.questions.domain.Question;
import com.gitlab.sszuev.questions.domain.QuestionImpl;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by @ssz on 13.12.2020.
 */
@Repository
public class QuestionsDAOImpl implements QuestionsDAO {
    private final Map<Question, List<Answer>> data;

    public QuestionsDAOImpl(CSVResource resource) {
        this.data = toMap(resource);
    }

    private static Map<Question, List<Answer>> toMap(CSVResource resource) {
        try (Stream<CSVResource.Record> records = resource.load()) {
            return records.collect(Collectors.toMap(QuestionsDAOImpl::toQuestion, QuestionsDAOImpl::toAnswers));
        }
    }

    private static Question toQuestion(CSVResource.Record r) {
        return new QuestionImpl(r.getId(), r.getQuestion());
    }

    private static List<Answer> toAnswers(CSVResource.Record r) {
        List<String> good = r.correctAnswers().collect(Collectors.toList());
        List<String> bad = r.wrongAnswers().collect(Collectors.toList());
        List<Answer> res = new ArrayList<>();
        int id = 0;
        while (!good.isEmpty()) {
            res.add(new AnswerImpl(++id, good.remove(0), true));
        }
        while (!bad.isEmpty()) {
            res.add(new AnswerImpl(++id, bad.remove(0), false));
        }
        return res;
    }

    @Override
    public Stream<Question> listAllQuestions() {
        return data.keySet().stream();
    }

    @Override
    public Stream<Answer> listAllAnswersByQuestion(Question question) {
        return Optional.ofNullable(question).map(data::get).stream().flatMap(Collection::stream);
    }
}
