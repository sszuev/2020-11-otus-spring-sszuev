package com.gitlab.sszuev.questions.domain;

/**
 * A domain entity that describes a student testing answer item.
 * <p>
 * Created by @ssz on 13.12.2020.
 */
public interface Answer extends HasID, HasMessage {

    /**
     * Returns {@code true} if the answer is correct.
     *
     * @return {@code Boolean} ({@code null} if undefined)
     */
    Boolean isCorrect();
}
