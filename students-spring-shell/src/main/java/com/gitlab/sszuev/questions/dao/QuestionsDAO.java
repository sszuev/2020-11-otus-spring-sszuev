package com.gitlab.sszuev.questions.dao;

import com.gitlab.sszuev.questions.domain.Answer;
import com.gitlab.sszuev.questions.domain.Question;

import java.util.stream.Stream;

/**
 * A DAO that provides {@link Question}s and {@link Answer}s.
 * Created by @ssz on 13.12.2020.
 */
public interface QuestionsDAO {

    /**
     * Lists all question entities.
     *
     * @return a {@code Stream} over all {@link Question}s
     */
    Stream<Question> listAllQuestions();

    /**
     * Lists all (correct and wrong) answer entities for the given {@code question}.
     *
     * @param question {@link Question}, not {@code null}
     * @return a {@code Stream} over all {@link Answer}s
     */
    Stream<Answer> listAllAnswersByQuestion(Question question);

}
