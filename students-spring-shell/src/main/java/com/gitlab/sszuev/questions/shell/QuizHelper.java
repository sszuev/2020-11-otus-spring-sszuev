package com.gitlab.sszuev.questions.shell;

import com.gitlab.sszuev.questions.domain.Answer;
import com.gitlab.sszuev.questions.domain.Question;
import com.gitlab.sszuev.questions.service.QuestionService;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by @ssz on 14.12.2020.
 */
@Component
public class QuizHelper {
    private final QuestionService service;

    private List<Question> questions;
    private final List<Answer> selected = new ArrayList<>();
    private Item current;

    public QuizHelper(QuestionService service) {
        this.service = Objects.requireNonNull(service);
    }

    /**
     * Returns the currently selected item.
     *
     * @return an {@code Optional} around of <b>current</b> {@link Item}
     */
    public Optional<Item> current() {
        return Optional.ofNullable(current == null ? current = create(selected.size()) : current);
    }

    /**
     * Marks the given {@code answer}-object as answered and switch to the next question item.
     *
     * @param answer {@link Answer}, not {@code null}
     * @return an {@code Optional} around of <b>next</b> {@link Item}
     */
    public Optional<Item> next(Answer answer) {
        selected.add(answer);
        current = null;
        return current();
    }

    /**
     * Returns the current quiz score.
     * If the answer is correct then the interviewee gets {@code 1} point, otherwise {@code 0}.
     *
     * @return {@code int}
     */
    public int getCurrentScore() {
        return selected.stream().filter(Answer::isCorrect).mapToInt(x -> 1).sum();
    }

    /**
     * Returns total number of questions in the system.
     *
     * @return {@code int}
     */
    public int getTotalNumber() {
        return questions().size();
    }

    /**
     * Resets the state (to be able to reuse).
     */
    public void reset() {
        selected.clear();
        current = null;
    }

    private Item create(int index) {
        List<Question> list = questions();
        if (index >= list.size()) return null;
        Question question = list.get(index);
        List<Answer> answers = service.answers(question).collect(Collectors.toList());
        return new Item(index, question, answers);
    }

    private List<Question> questions() {
        return questions == null ? questions = service.questions().collect(Collectors.toList()) : questions;
    }

    /**
     * The question item.
     * A container that encapsulates {@link Question} and all its related {@link Answer}s.
     */
    public static class Item {
        private final int index;
        private final Question question;
        private final List<Answer> answers;

        protected Item(int index, Question question, List<Answer> answers) {
            this.index = index;
            this.question = Objects.requireNonNull(question);
            this.answers = Objects.requireNonNull(answers);
        }

        /**
         * Returns the question number (identifier).
         *
         * @return {@code int}
         */
        public int getNumber() {
            return index + 1;
        }

        /**
         * Gets question.
         *
         * @return {@link Question}
         */
        public Question getQuestion() {
            return question;
        }

        /**
         * Gets answer.
         *
         * @param index {@code int}
         * @return {@link Answer}
         * @throws IndexOutOfBoundsException if the index is out of range ({@code index < 0 || index >= answersCount()})
         */
        public Answer getAnswer(int index) {
            return answers.get(index);
        }

        /**
         * Returns the total number of answers binding to the question.
         *
         * @return {@code int}
         */
        public int answersCount() {
            return answers.size();
        }
    }
}
