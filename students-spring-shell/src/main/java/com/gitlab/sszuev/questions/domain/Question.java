package com.gitlab.sszuev.questions.domain;

/**
 * A domain entity that describes a student testing question item.
 * Created by @ssz on 13.12.2020.
 */
public interface Question extends HasID, HasMessage {
}
