package com.gitlab.sszuev.questions.shell;

import com.gitlab.sszuev.questions.domain.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.Availability;
import org.springframework.shell.ExitRequest;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellMethodAvailability;

import javax.annotation.PostConstruct;
import java.io.PrintStream;
import java.util.Objects;
import java.util.Optional;

/**
 * Created by @ssz on 13.12.2020.
 */
@ShellComponent
public class QuizCommands {

    static {
        // Workaround to suppress warnings.
        // checked on jdk11 (11.0.9), win10x64
        // see also https://stackoverflow.com/questions/46454995/
        try {
            java.lang.reflect.Field field = sun.misc.Unsafe.class.getDeclaredField("theUnsafe");
            field.setAccessible(true);
            sun.misc.Unsafe unsafe = (sun.misc.Unsafe) field.get(null);
            Class<?> clazz = Class.forName("jdk.internal.module.IllegalAccessLogger");
            java.lang.reflect.Field logger = clazz.getDeclaredField("logger");
            unsafe.putObjectVolatile(clazz, unsafe.staticFieldOffset(logger), null);
        } catch (Exception ignore) {
            // ignore
        }
    }

    private final QuizHelper service;
    private final PrintStream out;
    private String name;

    @Autowired
    public QuizCommands(QuizHelper service) {
        this(service, System.out);
    }

    public QuizCommands(QuizHelper service, PrintStream out) {
        this.service = Objects.requireNonNull(service);
        this.out = Objects.requireNonNull(out);
    }

    @PostConstruct
    public void sayHello() {
        String line = "=".repeat(42);
        out.println(line);
        out.println("Hi!");
        out.println("Please enter your name through 'name' command.");
        out.println("Or type 'help' for more info.");
        out.println(line + "\n");
    }

    @ShellMethod(value = "Ask name", key = {"s", "student", "n", "name"})
    public String sayName(String name) {
        this.name = Objects.requireNonNull(name);
        String q = service.current().map(this::print).orElse("There is no questions for you");
        return String.format("Hello, <%s>. \n\n%s", name, q);
    }

    @ShellMethod(value = "Repeat the last question", key = {"r", "repeat"})
    @ShellMethodAvailability(value = "isQuizReady")
    public String repeatQuestion() {
        return print(service.current().orElseThrow());
    }

    @ShellMethod(value = "Choose the option number 1", key = {"1", "a", "A"})
    @ShellMethodAvailability(value = "isQuizReady")
    public void answer1() {
        handleAnswer(1);
    }

    @ShellMethod(value = "Choose the option number 2", key = {"2", "b", "B"})
    @ShellMethodAvailability(value = "isQuizReady")
    public void answer2() {
        handleAnswer(2);
    }

    @ShellMethod(value = "Choose the option number 3", key = {"3", "c", "C"})
    @ShellMethodAvailability(value = "isQuizReady")
    public void answer3() {
        handleAnswer(3);
    }

    @ShellMethod(value = "Choose the option number 4", key = {"4", "d", "D"})
    @ShellMethodAvailability(value = "isQuizReady")
    public void answer4() {
        handleAnswer(4);
    }

    @ShellMethod(value = "Choose the option number 5", key = {"5", "e", "E"})
    @ShellMethodAvailability(value = "isQuizReady")
    public void answer5() {
        handleAnswer(5);
    }

    @ShellMethod(value = "Choose the option number 6", key = {"6", "f", "F"})
    @ShellMethodAvailability(value = "isQuizReady")
    public void answer6() {
        handleAnswer(6);
    }

    @ShellMethod(value = "Choose the option number 7", key = {"7", "g", "G"})
    @ShellMethodAvailability(value = "isQuizReady")
    public void answer7() {
        handleAnswer(7);
    }

    @ShellMethod(value = "Choose the option number 8", key = {"8", "h", "H"})
    @ShellMethodAvailability(value = "isQuizReady")
    public void answer8() {
        handleAnswer(8);
    }

    @ShellMethod(value = "Choose the option number 9", key = {"9", "i", "I"})
    @ShellMethodAvailability(value = "isQuizReady")
    public void answer9() {
        handleAnswer(9);
    }

    @ShellMethod(value = "Choose the option number 10", key = {"10", "j", "J"})
    @ShellMethodAvailability(value = "isQuizReady")
    public void answer10() {
        handleAnswer(10);
    }

    private void handleAnswer(int i) {
        QuizHelper.Item selected = service.current().orElseThrow();
        if (selected.answersCount() < i) {
            out.printf("Wrong input, %s." +
                    "%nPlease type a number between 1 and %d (inclusively)%n", name, selected.answersCount());
            return;
        }
        Answer a = selected.getAnswer(i - 1);
        out.printf("Your answer to the question #%d is '%s'.%n", selected.getNumber(), a.getMessage());
        // switch to the new item:
        Optional<QuizHelper.Item> next = service.next(a);
        if (next.isEmpty()) {
            out.printf("\nYour score is %d / %d.%n", service.getCurrentScore(), service.getTotalNumber());
            out.printf("Bye, %s%n.", name);
            throw new ExitRequest();
        }
        out.println(print(next.get()));
    }

    private String print(QuizHelper.Item item) {
        StringBuilder res = new StringBuilder("=".repeat(42))
                .append("\n").append("The question ")
                .append(item.getNumber()).append(" (of ").append(service.getTotalNumber())
                .append("): ")
                .append(item.getQuestion().getMessage()).append("\n");
        for (int i = 0; i < item.answersCount(); i++) {
            res.append("\t- ").append(i + 1).append(" : ").append(item.getAnswer(i).getMessage()).append("\n");
        }
        return res.toString();
    }

    @SuppressWarnings("unused")
    private Availability isQuizReady() {
        if (name == null) {
            return Availability.unavailable("no name specified.");
        }
        if (service.current().isEmpty()) {
            return Availability.unavailable("no more questions.");
        }
        return Availability.available();
    }
}
