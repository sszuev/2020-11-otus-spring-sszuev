package com.gitlab.sszuev.questions.domain;

import java.util.Objects;

/**
 * Created by @ssz on 13.12.2020.
 */
public class AnswerImpl implements Answer {
    private final long id;
    private final String txt;
    private final boolean correct;

    public AnswerImpl(long id, String txt, boolean correct) {
        this.id = id;
        this.txt = Objects.requireNonNull(txt);
        this.correct = correct;
    }

    @Override
    public Long getID() {
        return id;
    }

    @Override
    public String getMessage() {
        return txt;
    }

    @Override
    public Boolean isCorrect() {
        return correct;
    }

    @Override
    public String toString() {
        return String.format("Answer(%s){id=%d, txt='%s'}", correct ? "correct" : "wrong", id, txt);
    }


}
