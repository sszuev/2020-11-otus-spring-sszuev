package com.gitlab.sszuev.questions.dao;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * Created by @ssz on 13.12.2020.
 */
@Component
public class CSVResource {

    public static final int MAX_NUM_OF_ANSWERS = 10;

    private final Resource resource;

    @Autowired
    public CSVResource(ResourceLoader loader, @Value("${data.location}") String location) {
        this(loader.getResource(location));
    }

    public CSVResource(Resource resource) {
        this.resource = Objects.requireNonNull(resource);
    }

    private static Record parseRecord(CSVRecord record) {
        try {
            return new Record(Long.parseLong(record.get(0)), record.get(1).trim(), parseAnswers(record, 2), parseAnswers(record, 3));
        } catch (RuntimeException e) {
            throw new IllegalStateException("Can't parse record " + record, e);
        }
    }

    private static String[] parseAnswers(CSVRecord record, int x) {
        return record.get(x).trim().split(",\\s*");
    }

    /**
     * Loads records from the encapsulated resource.
     * A caller is responsible for {@code Stream} closing.
     *
     * @return a {@link Stream} of {@link Record}s (<b>please don't forget to call {@link Stream#close()}</b>)
     */
    public Stream<Record> load() {
        CSVParser p = createParser();
        return StreamSupport.stream(p.spliterator(), false)
                .map(CSVResource::parseRecord)
                .onClose(() -> {
                    try {
                        p.close();
                    } catch (IOException e) {
                        throw new UncheckedIOException(e);
                    }
                });
    }

    private CSVParser createParser() {
        try {
            return CSVParser.parse(resource.getInputStream(), StandardCharsets.UTF_8, CSVFormat.DEFAULT);
        } catch (IOException e) {
            throw new IllegalStateException("Can't parse " + resource, e);
        }
    }

    /**
     * Represents a csv application record.
     */
    public static class Record {
        private final Long id;
        private final String question;
        private final String[] correctAnswers;
        private final String[] wrongAnswers;

        private Record(Long id, String question, String[] correctAnswers, String[] wrongAnswers) {
            this.id = id;
            this.question = Objects.requireNonNull(question);
            this.correctAnswers = Objects.requireNonNull(correctAnswers);
            this.wrongAnswers = Objects.requireNonNull(wrongAnswers);
            long num = correctAnswers.length + wrongAnswers.length;
            if (num > MAX_NUM_OF_ANSWERS || num <= 0) {
                throw new IllegalArgumentException(String.format("Too many answers (%d), max allowed: %d",
                        num, MAX_NUM_OF_ANSWERS));
            }
        }

        public Long getId() {
            return id;
        }

        public String getQuestion() {
            return question;
        }

        public Stream<String> correctAnswers() {
            return Arrays.stream(correctAnswers);
        }

        public Stream<String> wrongAnswers() {
            return Arrays.stream(wrongAnswers);
        }

        @Override
        public String toString() {
            return String.format("Record{id=%d, question='%s', correct=%s, wrong=%s}",
                    id, question, Arrays.toString(correctAnswers), Arrays.toString(wrongAnswers));
        }
    }
}
