#### This is a home-work 'Application for testing students' (using [spring-shell](https://github.com/spring-projects/spring-shell))

A simple console application to perform students testing. There is nothing interesting here: if you are not
from [otus](https://otus.ru) team please ignore it.

##### Requirements:

- Git
- Java **11**
- Maven **3+**

##### Build and run:

```
$ git clone git@gitlab.com:sszuev/2020-11-otus-spring-sszuev.git
$ cd 2020-11-otus-spring-sszuev/students-testing-spring-shell
$ mvn clean package
$ java -jar target/students-testing.jar
```

##### See also:

- [students-testing-spring-boot](https://gitlab.com/sszuev/2020-11-otus-spring-sszuev/-/tree/master/students-testing-spring-boot)
- [students-testing](https://gitlab.com/sszuev/2020-11-otus-spring-sszuev/-/tree/master/students-testing)
- [otus-spring](https://github.com/OtusTeam/Spring)