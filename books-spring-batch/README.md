#### This is a home-work 'Spring Batch'

A simple ETL console utility to migrate data from relational db to NoSQL db (MongoDB). Supports [postgres](/docs/ps.sql)
and [h2](src/main/resources/data.sql). Example of command to copy data (from RDB to MongoDB):

```
copy jdbc:h2:mem:testdb --dst mongodb://localhost:27017 --dst-db FROM-H2-BOOKS-ETL
```

Example of command to clear data (MongoDB):

```
clean mongodb://localhost:27017 BOOKS-ETL
```

###### For [OtusTeam](https://otus.ru).

##### Requirements:

- Git
- Java **11**
- Maven **3+**

##### Build and run:

```
$ git clone git@gitlab.com:sszuev/2020-11-otus-spring-sszuev.git
$ cd 2020-11-otus-spring-sszuev/books-spring-batch
$ mvn clean package
$ java -jar target/books-library-etl.jar
```