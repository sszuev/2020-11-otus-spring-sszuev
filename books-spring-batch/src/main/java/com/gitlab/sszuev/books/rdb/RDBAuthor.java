package com.gitlab.sszuev.books.rdb;

import javax.persistence.*;

/**
 * A domain entity that describes an author item.
 * <p>
 * Created by @ssz on 14.03.2021.
 */
@Entity
@Table(name = "authors")
public class RDBAuthor implements HasID {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "name", nullable = false)
    private String name;

    @Override
    public Long getID() {
        return id;
    }

    @Override
    public void setID(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}

