package com.gitlab.sszuev.books.helpers;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * Created by @ssz on 20.03.2021.
 */
@Component
public class MongoConnectionHelper {
    public static final String EMBEDDED_MONGO_CONNECTION_URL = "EMBEDDED_MONGO_URL";

    private final MongoClient defaultClient;

    public MongoConnectionHelper(MongoClient client) {
        this.defaultClient = Objects.requireNonNull(client);
    }

    public MongoTemplate buildMongoTemplate(String url, String db) {
        MongoClient client = EMBEDDED_MONGO_CONNECTION_URL.equals(url) ? defaultClient : buildMongoClient(url);
        return new MongoTemplate(client, db);
    }

    public MongoClient buildMongoClient(String url) {
        MongoClientSettings args = MongoClientSettings.builder()
                .applyConnectionString(new ConnectionString(url))
                .build();
        return MongoClients.create(args);
    }
}
