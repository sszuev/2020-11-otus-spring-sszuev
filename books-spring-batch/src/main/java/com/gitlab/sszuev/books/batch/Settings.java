package com.gitlab.sszuev.books.batch;

/**
 * Created by @ssz on 13.03.2021.
 */
public class Settings {

    public static final String JOB_COPY = "job-copy-fromRDB-toMongoDB";
    public static final String JOB_CLEAN = "job-clean-MongoDB";
    static final String STEP_COPY = "step-fromRDB-toMongoDB";
    static final String STEP_CLEAN = "step-clean-MongoDB";
    static final String DB_READER_NAME = "fromRDB-itemReader";

    // Job Parameter keys
    public static final String RDB_URL = "rdb-url";
    public static final String RDB_LOGIN = "rdb-login";
    public static final String RDB_PWD = "rdb-pwd";
    public static final String RDB_DRIVER = "rdb-driver";
    public static final String MONGO_URL = "mongo-url";
    public static final String MONGO_DB = "mongo-db";
}
