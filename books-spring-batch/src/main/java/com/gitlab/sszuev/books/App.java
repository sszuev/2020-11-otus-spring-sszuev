package com.gitlab.sszuev.books;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.batch.core.configuration.JobRegistry;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.support.JobRegistryBeanPostProcessor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;

/**
 * Created by @ssz on 11.03.2021.
 */
@EnableBatchProcessing
@SpringBootApplication
public class App {

    static {
        forciblyDisableJDKWarnings();
    }

    public static void main(String... args) {
        SpringApplication.run(App.class, args).close();
    }

    @Bean
    public JobRegistryBeanPostProcessor jobRegistryPostProcessor(JobRegistry registry) {
        JobRegistryBeanPostProcessor res = new JobRegistryBeanPostProcessor();
        res.setJobRegistry(registry);
        return res;
    }

    @Primary
    @Bean
    public DataSourceProperties dataSourceProperties() {
        return new DataSourceProperties();
    }

    @Primary
    @Bean
    public DataSource dataSource(DataSourceProperties properties) {
        return properties.initializeDataSourceBuilder()
                .type(HikariDataSource.class)
                .build();
    }

    /**
     * Workaround to suppress warnings.
     * Checked on jdk11 (11.0.9), win10x64
     *
     * @see <a href='https://stackoverflow.com/questions/46454995/'>SO</a>
     */
    private static void forciblyDisableJDKWarnings() {
        try {
            java.lang.reflect.Field field = sun.misc.Unsafe.class.getDeclaredField("theUnsafe");
            field.setAccessible(true);
            sun.misc.Unsafe unsafe = (sun.misc.Unsafe) field.get(null);
            Class<?> clazz = Class.forName("jdk.internal.module.IllegalAccessLogger");
            java.lang.reflect.Field logger = clazz.getDeclaredField("logger");
            unsafe.putObjectVolatile(clazz, unsafe.staticFieldOffset(logger), null);
        } catch (Exception ignore) {
            // ignore
        }
    }
}