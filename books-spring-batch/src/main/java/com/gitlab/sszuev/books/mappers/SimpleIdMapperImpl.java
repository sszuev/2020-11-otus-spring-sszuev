package com.gitlab.sszuev.books.mappers;

import org.bson.types.ObjectId;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by @ssz on 20.03.2021.
 */
@Component
public class SimpleIdMapperImpl implements IdMapper {
    private static final AtomicLong NEXT_COUNTER = new AtomicLong(System.currentTimeMillis());
    private final Map<String, Long> collectionIds = new ConcurrentHashMap<>();

    @Override
    public String toObjectId(String collectionName, long id) {
        return newObjectId(id + getCollectionId(collectionName)).toHexString();
    }

    private long getCollectionId(String name) {
        return collectionIds.computeIfAbsent(Objects.requireNonNull(name), x -> NEXT_COUNTER.getAndIncrement());
    }

    /**
     * Creates an {@code ObjectId} from {@code long}.
     *
     * @param id {@code long}
     * @return {@link ObjectId}
     */
    public static ObjectId newObjectId(long id) {
        long timestamp = id >> 24;
        long counter = (-1L >>> 40) & id; // 3 bytes, required by ObjectId
        return new ObjectId((int) timestamp, (int) counter);
    }

}
