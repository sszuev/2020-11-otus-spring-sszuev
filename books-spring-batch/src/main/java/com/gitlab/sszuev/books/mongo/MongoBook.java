package com.gitlab.sszuev.books.mongo;

import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * Created by @ssz on 13.03.2021.
 */
@Document(value = MongoBook.COLLECTION)
public class MongoBook implements HasID {
    public static final String COLLECTION = "books";

    private String id;
    private String title;
    private MongoGenre genre;
    private List<MongoAuthor> authors;

    @Override
    public String getID() {
        return id;
    }

    @Override
    public void setID(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public MongoGenre getGenre() {
        return genre;
    }

    public void setGenre(MongoGenre genre) {
        this.genre = genre;
    }

    public List<MongoAuthor> getAuthors() {
        return authors;
    }

    public void setAuthors(List<MongoAuthor> authors) {
        this.authors = authors;
    }
}
