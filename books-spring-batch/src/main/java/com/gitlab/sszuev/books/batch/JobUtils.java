package com.gitlab.sszuev.books.batch;

import org.springframework.batch.core.JobParameters;

import java.util.stream.Collectors;

/**
 * Created by @ssz on 14.03.2021.
 */
public class JobUtils {

    public static String toString(JobParameters args) {
        return args.getParameters().entrySet().stream()
                .map(x -> x.getKey() + "=" + x.getValue()).collect(Collectors.joining(","));
    }
}
