package com.gitlab.sszuev.books.helpers;

import com.zaxxer.hikari.HikariDataSource;
import org.h2.Driver;
import org.hibernate.dialect.H2Dialect;
import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.sql.DriverManager;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;

/**
 * A helper to simplify creation {@link DataSource} and {@link EntityManagerFactory} in runtime from the given parameters.
 * <p>
 * (...and if you have ideas how to do these stuff in a better way, please do not hesitate to say so...)
 * <p>
 * Created by @ssz on 19.03.2021.
 */
@Component
public class RDBConnectionHelper {

    private static final Map<String, String> HIBERNATE_DIALECTS = Map.of(
            Driver.class.getName(), H2Dialect.class.getName(),
            "org.postgresql.Driver", "org.hibernate.dialect.PostgreSQLDialect"
    );

    public DataSource buildReaderDataSource(DataSourceProperties baseProperties,
                                            String url, String user, String pwd,
                                            String driver) {
        return baseProperties
                .initializeDataSourceBuilder()
                .url(url)
                .username(user)
                .password(pwd)
                .driverClassName(driver)
                .type(HikariDataSource.class)
                .build();
    }

    public EntityManagerFactory buildEntityManagerFactory(DataSource dataSource) {
        String driver = getDriverName(dataSource);
        String dialect = getHibernateDialectName(driver);
        Properties props = new Properties();
        props.put("hibernate.dialect", dialect);
        props.put("hibernate.show_sql", "false");
        props.put("hibernate.format_sql", "false");
        props.put("hibernate.connection.driver_class", driver);
        // use 'none' (undocumented option), not 'validation',
        // since we have two different schemas (primary keys are different: serial in postgres and bigint in h2)
        props.put("hibernate.hbm2ddl.auto", "none");

        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource);
        em.setPackagesToScan(com.gitlab.sszuev.books.rdb.RDBBook.class.getPackageName());
        em.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        em.setJpaProperties(props);
        em.setPersistenceUnitName(getClass().getSimpleName());
        em.setPersistenceProviderClass(HibernatePersistenceProvider.class);
        em.afterPropertiesSet();

        EntityManagerFactory emf = em.getObject();
        Assert.notNull(emf, "Null emf.");
        return emf;
    }

    private static String getDriverName(DataSource ds) {
        try {
            return DriverManager.getDriver(ds.getConnection().getMetaData().getURL()).getClass().getName();
        } catch (Exception ex) {
            throw new IllegalStateException("Can't determine driver name for ds=" + ds, ex);
        }
    }

    private static String getHibernateDialectName(String driver) {
        return Optional.ofNullable(HIBERNATE_DIALECTS.get(driver))
                .orElseThrow(() -> new IllegalStateException("Can't determine dialect name for driver " + driver));
    }
}
