package com.gitlab.sszuev.books.batch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.util.Assert;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by @ssz on 14.03.2021.
 */
@ParametersAreNonnullByDefault
public class RemoveAllMongoTasklet implements Tasklet, InitializingBean {
    private static final Logger LOGGER = LoggerFactory.getLogger(RemoveAllMongoTasklet.class);

    private MongoOperations template;
    private Set<String> collections;

    protected RemoveAllMongoTasklet() {
        // protected to preserve possibility of overriding
    }

    /**
     * Sets the {@link MongoOperations} to be used to remove all items.
     *
     * @param template the template implementation to be used.
     */
    public void setTemplate(MongoOperations template) {
        this.template = template;
    }

    /**
     * Sets the Mongo collections to be clean.
     *
     * @param collections a {@code Set} of collections
     */
    public void setCollection(Set<String> collections) {
        this.collections = collections;
    }

    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) {
        collections.forEach(collection -> {
            long count = template.count(new Query(), collection);
            LOGGER.info("Remove all {} entities from collection='{}'.", count, collection);
            template.dropCollection(collection);
        });
        return RepeatStatus.FINISHED;
    }

    @Override
    public void afterPropertiesSet() {
        Assert.notNull(template, "A MongoOperations implementation is required.");
        Assert.notNull(collections, "A collection name is not specified.");
    }

    public static class Builder {
        private MongoOperations template;
        private Set<String> collections;

        public Builder template(MongoOperations template) {
            this.template = template;
            return this;
        }

        public Builder collection(String collection) {
            if (collections == null) {
                collections = new HashSet<>();
            }
            this.collections.add(collection);
            return this;
        }

        public RemoveAllMongoTasklet build() {
            Assert.notNull(this.template, "template is required.");
            Assert.notNull(this.collections, "collection is required.");
            RemoveAllMongoTasklet res = new RemoveAllMongoTasklet();
            res.setTemplate(this.template);
            res.setCollection(this.collections);
            return res;
        }
    }
}
