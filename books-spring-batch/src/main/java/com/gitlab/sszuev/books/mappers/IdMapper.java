package com.gitlab.sszuev.books.mappers;

/**
 * Created by @ssz on 20.03.2021.
 */
public interface IdMapper {

    /**
     * Maps {@code long} RDB entity id to {@code String} MongoDB entity id.
     *
     * @param collectionName {@code String}, not {@code null}, a MongoDB collection name
     * @param id             {@code long}
     * @return {@code String}
     */
    String toObjectId(String collectionName, long id);
}
