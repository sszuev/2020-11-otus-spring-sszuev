package com.gitlab.sszuev.books.mongo;

/**
 * Created by @ssz on 20.03.2021.
 */
public interface HasID {
    String getID();

    void setID(String id);
}
