package com.gitlab.sszuev.books.shell;

import com.gitlab.sszuev.books.batch.JobUtils;
import com.gitlab.sszuev.books.batch.Settings;
import com.gitlab.sszuev.books.helpers.MongoConnectionHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecutionException;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobOperator;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;

import java.util.Objects;

/**
 * Created by @ssz on 11.03.2021.
 */
@ShellComponent
public class Commands {
    private static final Logger LOGGER = LoggerFactory.getLogger(Commands.class);

    private final JobOperator operator;
    private final JobRepository repository;

    public Commands(JobOperator operator, JobRepository repository) {
        this.operator = Objects.requireNonNull(operator);
        this.repository = Objects.requireNonNull(repository);
    }

    /**
     * Copies data from RDF to Mongo.
     * Example: {@code copy jdbc:h2:mem:testdb --dst mongodb://localhost:27017 --dst-db BOOKS-ETL}
     *
     * @param src    source (RDB) connection url, e.g. {@code jdbc:h2:mem:testdb}
     * @param user   source (RDB) connection credential login (optional)
     * @param pwd    source (RDB) connection credential password (optional)
     * @param driver source (RDB) JDBC driver (optional)
     * @param dst    destination (MongoDB) connection url, e.g. {@code mongodb://localhost:27017}, optional: default in-memory flapdoodle mongo db
     * @param dstDB  destination (MongoDB) database name
     * @throws JobExecutionException any batch error
     */
    @ShellMethod(value = "Copy data from RDB to MongoDB", key = {"run", "copy"})
    public void execCopyTask(@ShellOption(help = "jdbc connection url") String src,
                             @ShellOption(help = "jdbc connection login", defaultValue = "sa") String user,
                             @ShellOption(help = "jdbc connection password", defaultValue = "") String pwd,
                             @ShellOption(help = "jdbc driver", defaultValue = "org.h2.Driver") String driver,
                             @ShellOption(help = "destination mongo connection string", defaultValue = MongoConnectionHelper.EMBEDDED_MONGO_CONNECTION_URL) String dst,
                             @ShellOption(help = "destination mongo db name") String dstDB) throws JobExecutionException {
        executeJob(Settings.JOB_COPY, new JobParametersBuilder()
                .addString(Settings.RDB_URL, src)
                .addString(Settings.RDB_LOGIN, user)
                .addString(Settings.RDB_PWD, pwd)
                .addString(Settings.RDB_DRIVER, driver)
                .addString(Settings.MONGO_URL, dst)
                .addString(Settings.MONGO_DB, dstDB)
                .toJobParameters());
    }

    /**
     * Clears all data from the given Mongo DB.
     * Example: {@code clean mongodb://localhost:27017 BOOKS-ETL}
     *
     * @param url connection url, e.g. {@code mongodb://localhost:27017}, optional: default in-memory flapdoodle mongo db
     * @param db  MongoDB database name
     * @throws JobExecutionException any batch error
     */
    @ShellMethod(value = "Clean data from MongoDB", key = {"clean"})
    public void execCleanMongoTask(@ShellOption(help = "Mongo connection string", defaultValue = MongoConnectionHelper.EMBEDDED_MONGO_CONNECTION_URL) String url,
                                   @ShellOption(help = "Mongo db name") String db) throws JobExecutionException {
        executeJob(Settings.JOB_CLEAN, new JobParametersBuilder()
                .addString(Settings.MONGO_URL, url)
                .addString(Settings.MONGO_DB, db)
                .toJobParameters());
    }

    private void executeJob(String name, JobParameters args) throws JobExecutionException {
        long id;
        if (repository.isJobInstanceExists(name, args)) {
            id = operator.startNextInstance(name);
        } else {
            id = operator.start(name, JobUtils.toString(args));
        }
        LOGGER.info("Job '{}' details: {}", name, operator.getSummary(id));
    }

}
