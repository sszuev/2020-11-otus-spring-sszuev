package com.gitlab.sszuev.books.batch;

import com.gitlab.sszuev.books.helpers.MongoConnectionHelper;
import com.gitlab.sszuev.books.helpers.RDBConnectionHelper;
import com.gitlab.sszuev.books.mappers.RDBToMDBMapper;
import com.gitlab.sszuev.books.mongo.HasID;
import com.gitlab.sszuev.books.mongo.MongoAuthor;
import com.gitlab.sszuev.books.mongo.MongoBook;
import com.gitlab.sszuev.books.mongo.MongoGenre;
import com.gitlab.sszuev.books.rdb.RDBBook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.*;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.data.MongoItemWriter;
import org.springframework.batch.item.database.JpaPagingItemReader;
import org.springframework.batch.item.database.builder.JpaPagingItemReaderBuilder;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by @ssz on 11.03.2021.
 */
@ParametersAreNonnullByDefault
@Configuration
public class JobConfig {

    private static final Logger LOGGER = LoggerFactory.getLogger(JobConfig.class);

    @StepScope
    @Bean
    @ConfigurationProperties("app.src.datasource")
    public DataSourceProperties srcDataSourceProperties() {
        return new DataSourceProperties();
    }

    @Primary
    @StepScope
    @Bean
    public JpaPagingItemReader<RDBBook> jpaPagingItemReader(RDBConnectionHelper helper,
                                                            @Value("${app.src.page-size:20}") int pageSize,
                                                            @Value("#{jobParameters['" + Settings.RDB_URL + "']}") String url,
                                                            @Value("#{jobParameters['" + Settings.RDB_LOGIN + "']}") String user,
                                                            @Value("#{jobParameters['" + Settings.RDB_PWD + "']}") String pwd,
                                                            @Value("#{jobParameters['" + Settings.RDB_DRIVER + "']}") String driver) {
        DataSource ds = helper.buildReaderDataSource(srcDataSourceProperties(), url, user, pwd, driver);
        EntityManagerFactory emf = helper.buildEntityManagerFactory(ds);
        return new JpaPagingItemReaderBuilder<RDBBook>()
                .pageSize(pageSize)
                .name(Settings.DB_READER_NAME)
                .entityManagerFactory(emf)
                .queryString("SELECT b FROM RDBBook b JOIN FETCH b.genre")
                .build();
    }

    @StepScope
    @Bean
    public ItemProcessor<RDBBook, MongoBook> fromRDBToMongoDBProcessor(RDBToMDBMapper mapper) {
        return mapper::toMongoEntity;
    }

    @StepScope
    @Bean
    public ItemWriter<MongoBook> mongoItemWriter(MongoConnectionHelper helper,
                                                 @Value("#{jobParameters['" + Settings.MONGO_URL + "']}") String url,
                                                 @Value("#{jobParameters['" + Settings.MONGO_DB + "']}") String db) {
        MongoTemplate template = helper.buildMongoTemplate(url, db);

        MongoItemWriter<MongoBook> res = new MongoItemWriter<>() {
            final Set<String> savedGenres = new HashSet<>();
            final ItemWriter<MongoGenre> genreWriter = new ItemWriter<>();
            final ItemWriter<MongoAuthor> authorWriter = new ItemWriter<>();

            @Override
            public void setTemplate(MongoOperations template) {
                super.setTemplate(template);
                genreWriter.setTemplate(template);
                authorWriter.setTemplate(template);
            }

            @Override
            protected void doWrite(List<? extends MongoBook> items) {
                Collection<MongoGenre> genres = toDistinctCollection(items.stream()
                        .map(MongoBook::getGenre).filter(g -> savedGenres.add(g.getID())));
                Collection<MongoAuthor> authors = toDistinctCollection(items.stream()
                        .flatMap(x -> x.getAuthors().stream()));
                super.doWrite(items);
                genreWriter.doWrite(genres);
                authorWriter.doWrite(authors);
            }

            private <X extends HasID> Collection<X> toDistinctCollection(Stream<X> stream) {
                return stream.collect(Collectors.toMap(HasID::getID, Function.identity(), (a, b) -> a)).values();
            }

            class ItemWriter<E> extends MongoItemWriter<E> {
                public void doWrite(Collection<? extends E> items) {
                    super.doWrite(new ArrayList<>(items));
                }
            }
        };
        res.setCollection(MongoBook.COLLECTION);
        res.setTemplate(template);

        return res;
    }

    @StepScope
    @Bean
    public RemoveAllMongoTasklet cleanMongoDBTasklet(MongoConnectionHelper helper,
                                                     @Value("#{jobParameters['" + Settings.MONGO_URL + "']}") String url,
                                                     @Value("#{jobParameters['" + Settings.MONGO_DB + "']}") String db) {
        MongoTemplate template = helper.buildMongoTemplate(url, db);
        return new RemoveAllMongoTasklet.Builder()
                .template(template)
                .collection(MongoBook.COLLECTION)
                .collection(MongoAuthor.COLLECTION)
                .collection(MongoGenre.COLLECTION)
                .build();
    }

    @Bean
    public Job fromRDBToMongoJob(JobBuilderFactory factory, @Qualifier(Settings.STEP_COPY) Step step) {
        return factory.get(Settings.JOB_COPY)
                .incrementer(new RunIdIncrementer())
                .flow(step)
                .end()
                .listener(new JobExecutionListener() {
                    @Override
                    public void beforeJob(JobExecution je) {
                        LOGGER.info("Start copy JOB {}.", je.getId());
                    }

                    @Override
                    public void afterJob(JobExecution je) {
                        LOGGER.info("Finish copy JOB {}.", je.getId());
                    }
                })
                .build();
    }

    @Bean
    public Job cleanMongoJob(JobBuilderFactory factory, @Qualifier(Settings.STEP_CLEAN) Step step) {
        return factory.get(Settings.JOB_CLEAN)
                .incrementer(new RunIdIncrementer())
                .start(step)
                .listener(new JobExecutionListener() {
                    @Override
                    public void beforeJob(JobExecution je) {
                        LOGGER.info("Start clean JOB {}.", je.getId());
                    }

                    @Override
                    public void afterJob(JobExecution je) {
                        LOGGER.info("Finish clean JOB {}.", je.getId());
                    }
                })
                .build();
    }

    @Bean(Settings.STEP_COPY)
    public Step fromRDBToMongoStep(StepBuilderFactory factory,
                                   @Value("${app.dst.chunk-size:5}") int chunkSize,
                                   ItemReader<RDBBook> reader,
                                   ItemProcessor<RDBBook, MongoBook> processor,
                                   ItemWriter<MongoBook> writer) {
        return factory.get(Settings.STEP_COPY)
                .<RDBBook, MongoBook>chunk(chunkSize)
                .reader(reader)
                .processor(processor)
                .writer(writer)
                .listener(new ItemWriteListener<>() {
                    @Override
                    public void beforeWrite(List<? extends MongoBook> items) {
                        LOGGER.info("To write: {}", items.stream()
                                .map(MongoBook::getTitle)
                                .collect(Collectors.joining("', '", "['", "']")));
                    }

                    @Override
                    public void afterWrite(List<? extends MongoBook> items) {
                        LOGGER.info("{} items have been saved.", items.size());
                    }

                    @Override
                    public void onWriteError(Exception ex, List<? extends MongoBook> items) {
                        LOGGER.error("Can't write {} items", items.size(), ex);
                    }
                })
                .build();
    }

    @Bean(Settings.STEP_CLEAN)
    public Step cleanMongoDBJobStep(StepBuilderFactory factory, RemoveAllMongoTasklet tasklet) {
        return factory.get(Settings.STEP_CLEAN)
                .tasklet(tasklet)
                .build();
    }
}
