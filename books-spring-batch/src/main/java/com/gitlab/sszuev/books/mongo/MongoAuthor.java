package com.gitlab.sszuev.books.mongo;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by @ssz on 20.03.2021.
 */
@Document(value = MongoAuthor.COLLECTION)
public class MongoAuthor implements HasID {
    public static final String COLLECTION = "authors";
    @Id
    private String id;
    private String name;

    @Override
    public String getID() {
        return id;
    }

    @Override
    public void setID(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
