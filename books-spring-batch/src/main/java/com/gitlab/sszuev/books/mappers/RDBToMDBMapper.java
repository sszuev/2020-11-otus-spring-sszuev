package com.gitlab.sszuev.books.mappers;

import com.gitlab.sszuev.books.mongo.MongoAuthor;
import com.gitlab.sszuev.books.mongo.MongoBook;
import com.gitlab.sszuev.books.mongo.MongoGenre;
import com.gitlab.sszuev.books.rdb.RDBAuthor;
import com.gitlab.sszuev.books.rdb.RDBBook;
import com.gitlab.sszuev.books.rdb.RDBGenre;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.stream.Collectors;

/**
 * To map relational db entity to mongo db entity.
 * <p>
 * Created by @ssz on 13.03.2021.
 */
@Component
public class RDBToMDBMapper {
    private final IdMapper mapper;

    public RDBToMDBMapper(IdMapper mapper) {
        this.mapper = Objects.requireNonNull(mapper);
    }

    public MongoBook toMongoEntity(RDBBook book) {
        MongoBook res = new MongoBook();
        res.setID(mapper.toObjectId(MongoBook.COLLECTION, book.getID()));
        res.setTitle(book.getTitle());
        res.setGenre(toMongoEntity(book.getGenre()));
        res.setAuthors(book.getAuthors().stream().map(this::toMongoEntity).collect(Collectors.toList()));
        return res;
    }

    public MongoGenre toMongoEntity(RDBGenre genre) {
        MongoGenre res = new MongoGenre();
        res.setID(mapper.toObjectId(MongoGenre.COLLECTION, genre.getID()));
        res.setName(genre.getName());
        return res;
    }

    public MongoAuthor toMongoEntity(RDBAuthor author) {
        MongoAuthor res = new MongoAuthor();
        res.setID(mapper.toObjectId(MongoAuthor.COLLECTION, author.getID()));
        res.setName(author.getName());
        return res;
    }
}
