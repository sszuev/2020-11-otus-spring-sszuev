package com.gitlab.sszuev.books.rdb;

import javax.persistence.*;
import java.util.List;

/**
 * Created by @ssz on 11.03.2021.
 */
@Entity
@Table(name = "books")
public class RDBBook implements HasID {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "title", nullable = false)
    private String title;

    @ManyToOne(targetEntity = RDBGenre.class, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "genre_id")
    private RDBGenre genre;

    @org.hibernate.annotations.Fetch(org.hibernate.annotations.FetchMode.SUBSELECT)
    @ManyToMany(targetEntity = RDBAuthor.class
            , cascade = {CascadeType.PERSIST, CascadeType.MERGE}
            , fetch = FetchType.EAGER)
    @JoinTable(name = "book_authors", joinColumns = @JoinColumn(name = "book_id"),
            inverseJoinColumns = @JoinColumn(name = "author_id"))
    private List<RDBAuthor> authors;

    @Override
    public Long getID() {
        return id;
    }

    @Override
    public void setID(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public RDBGenre getGenre() {
        return genre;
    }

    public void setGenre(RDBGenre genre) {
        this.genre = genre;
    }

    public List<RDBAuthor> getAuthors() {
        return authors;
    }

    public void setAuthors(List<RDBAuthor> authors) {
        this.authors = authors;
    }
}
