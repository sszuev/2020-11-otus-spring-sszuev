package com.gitlab.sszuev.books.batch;

import org.junit.jupiter.api.BeforeEach;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.batch.test.JobRepositoryTestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.sql.DataSource;

/**
 * Created by @ssz on 14.03.2021.
 */
@SpringBootTest
abstract class JobTestBase {
    @Autowired
    private JobLauncher launcher;
    @Autowired
    private JobRepository repository;
    @Autowired
    private DataSource dataSource;

    @BeforeEach
    public void resetData() {
        jobRepositoryTestUtils().removeJobExecutions();
    }

    protected JobLauncherTestUtils jobLauncherTestUtils() {
        JobLauncherTestUtils res = new JobLauncherTestUtils();
        res.setJobLauncher(launcher);
        res.setJobRepository(repository);
        return res;
    }

    protected JobRepositoryTestUtils jobRepositoryTestUtils() {
        JobRepositoryTestUtils res = new JobRepositoryTestUtils();
        res.setJobRepository(repository);
        res.setDataSource(dataSource);
        return res;
    }
}
