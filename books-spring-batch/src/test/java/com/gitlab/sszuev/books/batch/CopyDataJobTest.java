package com.gitlab.sszuev.books.batch;

import com.gitlab.sszuev.books.helpers.MongoConnectionHelper;
import com.gitlab.sszuev.books.mongo.MongoAuthor;
import com.gitlab.sszuev.books.mongo.MongoBook;
import com.gitlab.sszuev.books.mongo.MongoGenre;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoTemplate;

/**
 * Created by @ssz on 14.03.2021.
 */
public class CopyDataJobTest extends JobTestBase {
    @Autowired
    @Qualifier(value = "fromRDBToMongoJob")
    private Job theJob;
    @Autowired
    private MongoTemplate template;

    private JobLauncherTestUtils testLauncher;

    @BeforeEach
    public void initAndSetUpUtils() {
        testLauncher = jobLauncherTestUtils();
        testLauncher.setJob(theJob);
    }

    @Test
    public void testJob() throws Exception {
        Assumptions.assumeTrue(template.findAll(MongoBook.class).isEmpty());
        Assumptions.assumeTrue(template.findAll(MongoGenre.class).isEmpty());
        Assumptions.assumeTrue(template.findAll(MongoAuthor.class).isEmpty());

        Job job = testLauncher.getJob();
        Assertions.assertNotNull(job);
        Assertions.assertEquals(Settings.JOB_COPY, job.getName());

        // copy jdbc:h2:mem:testdb --dst-db BOOKS-ETL
        JobParameters args = new JobParametersBuilder()
                .addString(Settings.RDB_URL, "jdbc:h2:mem:testdb")
                .addString(Settings.RDB_LOGIN, "sa")
                .addString(Settings.RDB_PWD, "")
                .addString(Settings.MONGO_URL, MongoConnectionHelper.EMBEDDED_MONGO_CONNECTION_URL)
                .addString(Settings.MONGO_DB, "books_test_db")
                .toJobParameters();

        JobExecution execution = testLauncher.launchJob(args);
        Assertions.assertNotNull(execution);
        Assertions.assertEquals("COMPLETED", execution.getExitStatus().getExitCode());

        Assertions.assertEquals(9, template.findAll(MongoBook.class).size());
        Assertions.assertEquals(4, template.findAll(MongoGenre.class).size());
        Assertions.assertEquals(3, template.findAll(MongoAuthor.class).size());
    }
}
