package com.gitlab.sszuev.books.batch;

import com.gitlab.sszuev.books.TestEntityFactory;
import com.gitlab.sszuev.books.helpers.MongoConnectionHelper;
import com.gitlab.sszuev.books.mongo.MongoAuthor;
import com.gitlab.sszuev.books.mongo.MongoBook;
import com.gitlab.sszuev.books.mongo.MongoGenre;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.List;

/**
 * Created by @ssz on 14.03.2021.
 */
public class CleanMongoJobTest extends JobTestBase {
    @Autowired
    @Qualifier(value = "cleanMongoJob")
    private Job theJob;
    @Autowired
    private MongoTemplate template;

    private JobLauncherTestUtils testLauncher;

    @BeforeEach
    public void initAndSetUpUtils() {
        testLauncher = jobLauncherTestUtils();
        testLauncher.setJob(theJob);
    }

    @BeforeEach
    public void createData() {
        MongoGenre g = template.save(TestEntityFactory.newMongoGenre(null, "g"));
        MongoAuthor a1 = template.save(TestEntityFactory.newMongoAuthor(null, "a2"));
        MongoAuthor a2 = template.save(TestEntityFactory.newMongoAuthor(null, "a2"));
        template.save(TestEntityFactory.newMongoBook(null, "B1", g, List.of(a1)));
        template.save(TestEntityFactory.newMongoBook(null, "B2", g, List.of(a1, a2)));
    }

    @Test
    public void testJob() throws Exception {
        Assumptions.assumeFalse(template.findAll(MongoBook.class).isEmpty());
        Assumptions.assumeFalse(template.findAll(MongoGenre.class).isEmpty());
        Assumptions.assumeFalse(template.findAll(MongoAuthor.class).isEmpty());

        Job job = testLauncher.getJob();
        Assertions.assertNotNull(job);
        Assertions.assertEquals(Settings.JOB_CLEAN, job.getName());

        JobParameters args = new JobParametersBuilder()
                .addString(Settings.MONGO_URL, MongoConnectionHelper.EMBEDDED_MONGO_CONNECTION_URL)
                .addString(Settings.MONGO_DB, "books_test_db")
                .toJobParameters();

        JobExecution execution = testLauncher.launchJob(args);
        Assertions.assertNotNull(execution);
        Assertions.assertEquals("COMPLETED", execution.getExitStatus().getExitCode());

        Assertions.assertTrue(template.findAll(MongoBook.class).isEmpty());
        Assertions.assertTrue(template.findAll(MongoGenre.class).isEmpty());
        Assertions.assertTrue(template.findAll(MongoAuthor.class).isEmpty());
    }
}
