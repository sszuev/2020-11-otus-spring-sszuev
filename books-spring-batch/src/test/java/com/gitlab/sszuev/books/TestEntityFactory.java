package com.gitlab.sszuev.books;

import com.gitlab.sszuev.books.mongo.MongoAuthor;
import com.gitlab.sszuev.books.mongo.MongoBook;
import com.gitlab.sszuev.books.mongo.MongoGenre;

import java.util.List;

/**
 * Created by @ssz on 14.03.2021.
 */
public class TestEntityFactory {

    public static MongoBook newMongoBook(String id, String title, MongoGenre genre, List<MongoAuthor> authors) {
        MongoBook res = new MongoBook();
        res.setID(id);
        res.setTitle(title);
        res.setGenre(genre);
        res.setAuthors(authors);
        return res;
    }

    public static MongoGenre newMongoGenre(String id, String name) {
        MongoGenre res = new MongoGenre();
        res.setID(id);
        res.setName(name);
        return res;
    }

    public static MongoAuthor newMongoAuthor(String id, String name) {
        MongoAuthor res = new MongoAuthor();
        res.setID(id);
        res.setName(name);
        return res;
    }
}
