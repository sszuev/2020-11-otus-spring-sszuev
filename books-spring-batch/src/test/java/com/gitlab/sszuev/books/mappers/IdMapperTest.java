package com.gitlab.sszuev.books.mappers;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

/**
 * Created by @ssz on 20.03.2021.
 */
@SpringBootTest
@ContextConfiguration(classes = SimpleIdMapperImpl.class)
public class IdMapperTest {
    @Autowired
    private IdMapper mapper;

    @Test
    public void testSimpleIdMapper() {
        String idA1 = mapper.toObjectId("a", 233232);
        String idA2 = mapper.toObjectId("a", 233232);
        Assertions.assertNotNull(idA1);
        Assertions.assertEquals(idA1, idA2);

        String idB1 = mapper.toObjectId("b", 233232);
        String idB2 = mapper.toObjectId("b", 233232);
        Assertions.assertNotNull(idB1);
        Assertions.assertEquals(idB1, idB2);
        Assertions.assertNotEquals(idA1, idB2);

        String idA3 = mapper.toObjectId("a", 3);
        Assertions.assertNotNull(idA3);
        Assertions.assertNotEquals(idA3, idA1);
        Assertions.assertNotEquals(idA3, idB2);
    }
}
