package com.gitlab.sszuev.books.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by @ssz on 21.01.2021.
 */
@Document(value = "author")
public class Author implements HasID {
    @Id
    private String id;
    private String name;

    @Override
    public String getID() {
        return id;
    }

    @Override
    public void setID(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
