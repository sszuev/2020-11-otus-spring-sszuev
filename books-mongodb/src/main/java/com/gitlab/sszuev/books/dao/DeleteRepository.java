package com.gitlab.sszuev.books.dao;

/**
 * Created by @ssz on 21.01.2021.
 */
interface DeleteRepository {
    /**
     * Deletes an entity by the given id.
     * <p>
     * Returns {@code true} if the entity has been deleted successfully.
     * Otherwise returns {@code false},
     * which may happen, for example, if nothing was found
     * or the corresponding entity is bound by some external constraint.
     *
     * @param id {@code String} - an entity id, not {@code null}
     * @return {@code boolean}
     */
    boolean deleteIfPossible(String id);
}


