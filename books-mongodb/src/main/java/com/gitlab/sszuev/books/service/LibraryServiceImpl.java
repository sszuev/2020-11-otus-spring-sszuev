package com.gitlab.sszuev.books.service;

import com.gitlab.sszuev.books.dao.AuthorRepository;
import com.gitlab.sszuev.books.dao.BookRepository;
import com.gitlab.sszuev.books.dao.CommentRepository;
import com.gitlab.sszuev.books.dao.GenreRepository;
import com.gitlab.sszuev.books.domain.Author;
import com.gitlab.sszuev.books.domain.Book;
import com.gitlab.sszuev.books.domain.Comment;
import com.gitlab.sszuev.books.domain.Genre;
import com.gitlab.sszuev.books.dto.AuthorRecord;
import com.gitlab.sszuev.books.dto.BookRecord;
import com.gitlab.sszuev.books.dto.CommentRecord;
import com.gitlab.sszuev.books.dto.GenreRecord;
import com.gitlab.sszuev.books.mapper.EntityMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by @ssz on 29.12.2020.
 */
@Service
public class LibraryServiceImpl implements LibraryService {

    private final BookRepository bookRepository;
    private final GenreRepository genreRepository;
    private final AuthorRepository authorRepository;
    private final CommentRepository commentRepository;
    private final EntityMapper mapper;

    public LibraryServiceImpl(BookRepository repository,
                              GenreRepository genreRepository,
                              AuthorRepository authorRepository,
                              CommentRepository commentRepository,
                              EntityMapper mapper) {
        this.bookRepository = Objects.requireNonNull(repository);
        this.genreRepository = Objects.requireNonNull(genreRepository);
        this.authorRepository = Objects.requireNonNull(authorRepository);
        this.commentRepository = Objects.requireNonNull(commentRepository);
        this.mapper = Objects.requireNonNull(mapper);
    }

    @Transactional(readOnly = true)
    @Override
    public Optional<BookRecord> find(long id) {
        return mapper.bookID(id).flatMap(bookRepository::findById).map(mapper::toRecord);
    }

    @Transactional(readOnly = true)
    @Override
    public Stream<BookRecord> books() {
        return collectToStream(bookRepository.streamAll());
    }

    @Transactional(readOnly = true)
    @Override
    public Stream<BookRecord> booksByAuthor(String author) {
        return collectToStream(bookRepository.streamByAuthorNameIgnoreCase(author.trim()));
    }

    @Transactional(readOnly = true)
    @Override
    public Stream<BookRecord> booksByGenre(String genre) {
        return collectToStream(bookRepository.streamByGenreNameIgnoreCase(genre.trim()));
    }

    @Transactional(readOnly = true)
    @Override
    public Stream<BookRecord> books(long limit) {
        Stream<Book> res = bookRepository.streamAll();
        if (limit >= 0) {
            res = res.limit(limit);
        }
        return collectToStream(res);
    }

    @Transactional(readOnly = true)
    @Override
    public Stream<GenreRecord> genres() {
        return genreRepository.streamAll().map(mapper::toRecord).collect(Collectors.toList()).stream();
    }

    @Transactional(readOnly = true)
    @Override
    public Stream<AuthorRecord> authors() {
        return authorRepository.streamAll().map(mapper::toRecord).collect(Collectors.toList()).stream();
    }

    @Transactional(readOnly = true)
    @Override
    public Stream<AuthorRecord> authors(long limit) {
        Stream<Author> res = authorRepository.streamAll();
        if (limit >= 0) {
            res = res.limit(limit);
        }
        return res.map(mapper::toRecord).collect(Collectors.toList()).stream();
    }

    @Transactional(readOnly = true)
    @Override
    public Stream<CommentRecord> comments(Long id) {
        Stream<Comment> res;
        if (id == null) {
            res = commentRepository.streamAllOrderByTimestampDesc();
        } else {
            String cid = mapper.bookID(id).orElse(null);
            if (cid == null) {
                return Stream.empty();
            }
            res = commentRepository.streamByBookIdOrderByTimestampDesc(cid);
        }
        return res.map(mapper::toRecord).collect(Collectors.toList()).stream();
    }

    @Transactional
    @Override
    public BookRecord create(String title, String genre, List<String> authors) {
        Book res = bookRepository.save(newBook(title, fetchGenre(genre), fetchAuthors(authors)));
        return mapper.toRecord(res);
    }

    @Transactional
    @Override
    public boolean delete(long id) {
        Book book = mapper.bookID(id).flatMap(bookRepository::findById).orElse(null);
        if (book == null) return false;
        String genre = book.getGenre().getID();
        Set<String> authors = book.getAuthors().stream().map(Author::getID).collect(Collectors.toSet());

        bookRepository.delete(book);
        genreRepository.deleteIfPossible(genre);
        authors.forEach(authorRepository::deleteIfPossible);
        return true;
    }

    @Transactional
    @Override
    public Optional<CommentRecord> addComment(long bookId, String comment) {
        Optional<Book> book = mapper.bookID(bookId).flatMap(bookRepository::findById);
        if (book.isEmpty()) return Optional.empty();
        CommentRecord res = mapper.toRecord(commentRepository.save(newComment(book.get(), comment, LocalDateTime.now())));
        return Optional.of(res);
    }

    @Transactional
    @Override
    public Optional<CommentRecord> deleteComment(long bookId) {
        Comment res = mapper.bookID(bookId).flatMap(commentRepository::findLast).orElse(null);
        if (res == null) return Optional.empty();
        commentRepository.deleteById(res.getID());
        return Optional.of(mapper.toRecord(res));
    }

    private Stream<BookRecord> collectToStream(Stream<Book> res) {
        return res.map(mapper::toRecord).collect(Collectors.toList()).stream();
    }

    private List<Author> fetchAuthors(List<String> authors) {
        // Note: right now there is no unique constraint on column AUTHORS.NAME !
        Map<String, List<Author>> found = authorRepository.streamByNameIn(authors)
                .collect(Collectors.groupingBy(Author::getName, Collectors.toList()));
        return authors.stream()
                .map(a -> found.computeIfAbsent(a, x -> List.of(newAuthor(x))))
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    private Genre fetchGenre(String genre) {
        return genreRepository.streamByName(genre).findFirst().orElseGet(() -> newGenre(genre));
    }

    private Author newAuthor(String name) {
        Author res = new Author();
        res.setName(name);
        return res;
    }

    private Genre newGenre(String name) {
        Genre res = new Genre();
        res.setName(name);
        return res;
    }

    private Book newBook(String title, Genre genre, List<Author> authors) {
        Book res = new Book();
        res.setTitle(title);
        res.setGenre(genre);
        res.setAuthors(authors);
        return res;
    }

    private Comment newComment(Book book, String comment, LocalDateTime time) {
        Comment res = new Comment();
        res.setBook(book);
        res.setTimestamp(time);
        res.setComment(comment);
        return res;
    }
}
