package com.gitlab.sszuev.books.dao.impl;

import com.gitlab.sszuev.books.dao.BookRepository;
import com.gitlab.sszuev.books.dao.GenreCustomRepository;
import com.gitlab.sszuev.books.domain.Genre;
import com.mongodb.client.result.UpdateResult;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * Created by @ssz on 21.01.2021.
 */
@Component
public class GenreCustomRepositoryImpl implements GenreCustomRepository {
    private final MongoTemplate template;
    private final BookRepository bookRepository;

    public GenreCustomRepositoryImpl(MongoTemplate template, BookRepository bookRepository) {
        this.template = Objects.requireNonNull(template);
        this.bookRepository = Objects.requireNonNull(bookRepository);
    }

    @Override
    public boolean deleteIfPossible(String id) {
        Genre g = template.findById(id, Genre.class);
        if (g == null) {
            return false;
        }
        if (bookRepository.existsByGenreId(id)) {
            return false;
        }
        template.remove(g);
        return true;
    }

    @Override
    public Genre upsert(Genre genre) {
        Query find = new Query(Criteria.where(Genre.NAME).is(genre.getName()));
        Update update = new Update();
        update.set(Genre.NAME, genre.getName());
        UpdateResult res = template.upsert(find, update, Genre.class);
        genre.setID(fetchId(res, find));
        return genre;
    }

    private String fetchId(UpdateResult res, Query query) {
        if (res.getMatchedCount() > 0) {
            return Objects.requireNonNull(template.findOne(query, Genre.class)).getID();
        }
        return Objects.requireNonNull(res.getUpsertedId()).asObjectId().getValue().toHexString();
    }
}
