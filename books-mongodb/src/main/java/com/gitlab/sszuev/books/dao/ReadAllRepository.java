package com.gitlab.sszuev.books.dao;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.Query;

import java.util.stream.Stream;

/**
 * Created by @ssz on 23.01.2021.
 *
 * @param <E> - an entity
 */
interface ReadAllRepository<E> {
    /**
     * Lists all entities from the underlying storage.
     * <p>
     * Impl note: it does not work without explicit {@code @Query} (checked spring-data ver. 2.4.3).
     *
     * @return a {@code Stream} of {@link E}s
     */
    @Query("{}")
    Stream<E> streamAll();

    /**
     * Lists all entities with sorting option.
     *
     * @param sort {@link Sort}, not {@code null}
     * @return a sorted {@code Stream} of {@link E}s
     * @see #streamAll()
     */
    @Query("{}")
    Stream<E> streamAll(Sort sort);
}
