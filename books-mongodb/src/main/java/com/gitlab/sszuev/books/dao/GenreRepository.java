package com.gitlab.sszuev.books.dao;

import com.gitlab.sszuev.books.domain.Genre;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.stream.Stream;

/**
 * Created by @ssz on 20.01.2021.
 */
public interface GenreRepository extends MongoRepository<Genre, String>, ReadAllRepository<Genre>, GenreCustomRepository {
    /**
     * Lists all genres from the underlying storage by the specified name.
     *
     * @param name {@code String}, not {@code null}
     * @return a {@code Stream} of {@link Genre}s
     */
    Stream<Genre> streamByName(String name);
}
