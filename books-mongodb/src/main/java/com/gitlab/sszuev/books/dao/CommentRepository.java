package com.gitlab.sszuev.books.dao;

import com.gitlab.sszuev.books.domain.Comment;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;
import java.util.stream.Stream;

/**
 * Created by @ssz on 23.01.2021.
 */
public interface CommentRepository extends MongoRepository<Comment, String>, ReadAllRepository<Comment> {

    /**
     * Lists all comments from the underlying storage
     * by the given {@link com.gitlab.sszuev.books.domain.Book#getID() book-id}.
     * The output is a sorted by {@link Comment#getTimestamp() comment-date} (in reverse order) stream.
     *
     * @param bookId {@code long}, the book-record identifier
     * @return a sorted {@code Stream} of {@link Comment}s
     * @see #streamByBookId(String)
     */
    Stream<Comment> streamByBookIdOrderByTimestampDesc(String bookId);

    /**
     * Lists all comments from the underlying storage
     * by the given {@link com.gitlab.sszuev.books.domain.Book#getID() book-id}.
     *
     * @param bookId {@code long}, the book-record identifier
     * @return a {@code Stream} of {@link Comment}s
     * @see #streamByBookIdOrderByTimestampDesc(String)
     */
    Stream<Comment> streamByBookId(String bookId);

    /**
     * Lists all comments from the underlying storage.
     * The output is a sorted by {@link Comment#getTimestamp() comment-date} (in reverse order) stream.
     *
     * @return a sorted {@code Stream} of {@link Comment}s
     * @see #streamByBookIdOrderByTimestampDesc(String)
     */
    default Stream<Comment> streamAllOrderByTimestampDesc() {
        return streamAll(Sort.by(Sort.Direction.DESC, "timestamp"));
    }

    /**
     * Finds the last (by {@link Comment#getTimestamp() date}) comment on the specified book.
     *
     * @param bookId {@code long}, the book-record identifier
     * @return a {@code Optional} around of {@link Comment}
     */
    default Optional<Comment> findLast(String bookId) {
        try (Stream<Comment> comments = streamByBookIdOrderByTimestampDesc(bookId)) {
            return comments.findFirst();
        }
    }
}
