package com.gitlab.sszuev.books.mapper;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

import java.util.Objects;

/**
 * Impl notes:
 * <ul>
 * <li><b>not</b> thread-safe</li>
 * <li><b>stateful</b>: need to use in session scope (or something like that)</li>
 * </ul>
 * Created by @ssz on 24.01.2021.
 */
public class SimpleIDMapper implements IDMapper<String, Long> {
    final BiMap<String, Long> map = HashBiMap.create();

    protected long next() {
        return map.size() + 1;
    }

    @Override
    public Long toRecordID(String id) {
        return map.computeIfAbsent(Objects.requireNonNull(id), k -> next());
    }

    @Override
    public String toEntityID(Long id) {
        return map.inverse().get(id);
    }
}
