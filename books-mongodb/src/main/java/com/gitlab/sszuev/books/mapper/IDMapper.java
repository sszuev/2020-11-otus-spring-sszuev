package com.gitlab.sszuev.books.mapper;

/**
 * A helper that maps internal {@link EID entity-id} to external {@link RID record-id} and vice verse.
 * <p>
 * A {@code record-id} is used as reference in UI, so it is important to have it in a human-readable form,
 * while an {@code entity-id} comes from database and may not be easy to print.
 * <p>
 * Created by @ssz on 24.01.2021.
 *
 * @param <EID> an entity identifier (domain level)
 * @param <RID> a record identifier (service level)
 */
public interface IDMapper<EID, RID> {
    /**
     * @param id {@link EID}, not {@code null}
     * @return {@link RID}, never {@code null}
     */
    RID toRecordID(EID id);

    /**
     * @param id {@link RID}, not {@code null}
     * @return {@link EID} or {@code null}
     */
    EID toEntityID(RID id);
}
