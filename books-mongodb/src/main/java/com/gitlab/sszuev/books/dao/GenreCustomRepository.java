package com.gitlab.sszuev.books.dao;

import com.gitlab.sszuev.books.domain.Genre;

/**
 * Created by @ssz on 21.01.2021.
 */
public interface GenreCustomRepository extends DeleteRepository {
    /**
     * Saves a given genre-entity (inserts new or updates if it is already exists).
     * Use the returned instance for further operations
     * as the save operation might have changed the entity instance completely.
     *
     * @param genre {@link Genre}, not {@link null}
     * @return {@link Genre}, never {@link null}
     */
    Genre upsert(Genre genre);
}
