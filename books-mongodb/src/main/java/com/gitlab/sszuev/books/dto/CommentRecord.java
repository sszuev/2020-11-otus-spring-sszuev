package com.gitlab.sszuev.books.dto;

import java.time.LocalDateTime;
import java.util.Objects;

/**
 * Created by @ssz on 24.01.2021.
 */
public class CommentRecord extends BaseRecord {
    private final long bookId;
    private final String message;
    private final LocalDateTime timestamp;

    public CommentRecord(long id, long bookId, String message, LocalDateTime timestamp) {
        super(id);
        this.bookId = bookId;
        this.message = Objects.requireNonNull(message);
        this.timestamp = Objects.requireNonNull(timestamp);
    }

    public long getBookId() {
        return bookId;
    }

    public String getMessage() {
        return message;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }
}
