package com.gitlab.sszuev.books.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * Created by @ssz on 20.01.2021.
 */
@Document(value = Genre.COLLECTION)
public class Genre implements HasID {
    public static final String COLLECTION = "genre";
    public static final String NAME = "name";
    @Id
    private String id;
    @Indexed(unique = true)
    @Field(value = Genre.NAME)
    private String name;

    @Override
    public String getID() {
        return id;
    }

    @Override
    public void setID(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
