package com.gitlab.sszuev.books.dao;

import com.gitlab.sszuev.books.domain.Author;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.stream.Stream;

/**
 * Created by @ssz on 21.01.2021.
 */
public interface AuthorRepository extends MongoRepository<Author, String>, ReadAllRepository<Author>, AuthorCustomRepository {
    /**
     * Lists all author entities which names are in the given list.
     *
     * @param names a {@code List} of {@code String}-names to check, not {@code null}
     * @return a {@code Stream} of {@link Author}s
     */
    Stream<Author> streamByNameIn(List<String> names);

}
