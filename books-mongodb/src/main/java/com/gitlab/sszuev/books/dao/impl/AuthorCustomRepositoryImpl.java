package com.gitlab.sszuev.books.dao.impl;

import com.gitlab.sszuev.books.dao.AuthorCustomRepository;
import com.gitlab.sszuev.books.dao.BookRepository;
import com.gitlab.sszuev.books.domain.Author;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * Created by @ssz on 22.01.2021.
 */
@Component
public class AuthorCustomRepositoryImpl implements AuthorCustomRepository {
    private final MongoTemplate template;
    private final BookRepository bookRepository;

    public AuthorCustomRepositoryImpl(MongoTemplate template, BookRepository bookRepository) {
        this.template = Objects.requireNonNull(template);
        this.bookRepository = Objects.requireNonNull(bookRepository);
    }

    @Override
    public boolean deleteIfPossible(String id) {
        Author a = template.findById(id, Author.class);
        if (a == null) {
            return false;
        }
        if (bookRepository.existsByAuthorsId(id)) {
            return false;
        }
        template.remove(a);
        return true;
    }
}
