package com.gitlab.sszuev.books.events;

import com.gitlab.sszuev.books.dao.AuthorRepository;
import com.gitlab.sszuev.books.dao.CommentRepository;
import com.gitlab.sszuev.books.dao.GenreRepository;
import com.gitlab.sszuev.books.domain.Author;
import com.gitlab.sszuev.books.domain.Book;
import com.gitlab.sszuev.books.domain.Genre;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.AfterDeleteEvent;
import org.springframework.data.mongodb.core.mapping.event.BeforeConvertEvent;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;

/**
 * Created by @ssz on 20.01.2021.
 */
@Component
public class BookCascadeMongoEventListener extends AbstractMongoEventListener<Book> {
    private final GenreRepository genreRepository;
    private final AuthorRepository authorRepository;
    private final CommentRepository commentRepository;

    public BookCascadeMongoEventListener(GenreRepository genreRepository,
                                         AuthorRepository authorRepository,
                                         CommentRepository commentRepository) {
        this.genreRepository = Objects.requireNonNull(genreRepository);
        this.authorRepository = Objects.requireNonNull(authorRepository);
        this.commentRepository = Objects.requireNonNull(commentRepository);
    }

    @Override
    public void onBeforeConvert(@NonNull BeforeConvertEvent<Book> event) {
        super.onBeforeConvert(event);
        Book book = event.getSource();
        String title = book.getTitle();
        if (title == null || title.isEmpty()) {
            throw new WrongDataException("No title for '" + toMessage(book) + "'");
        }
        Genre genre = book.getGenre();
        if (genre == null || genre.getName() == null) {
            throw new WrongDataException("No genre for '" + toMessage(book) + "'");
        }
        List<Author> authors = book.getAuthors();
        if (authors == null || authors.isEmpty()) {
            throw new WrongDataException("No authors for '" + toMessage(book) + "'");
        }
        if (genre.getID() == null) {
            genreRepository.upsert(genre);
        }
        authors.stream().filter(a -> a.getID() == null).forEach(authorRepository::save);
    }

    @Override
    public void onAfterDelete(@NonNull AfterDeleteEvent<Book> event) {
        super.onAfterDelete(event);
        String id = event.getSource().get("_id").toString();
        commentRepository.streamByBookId(id).forEach(commentRepository::delete);
    }

    private String toMessage(Book book) {
        return String.format("Book[%s]'%s'", book.getID(), book.getTitle());
    }
}
