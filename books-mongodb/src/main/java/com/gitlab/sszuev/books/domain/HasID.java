package com.gitlab.sszuev.books.domain;

/**
 * Created by @ssz on 20.01.2021.
 */
public interface HasID {

    String getID();

    void setID(String id);
}
