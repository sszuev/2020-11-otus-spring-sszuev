package com.gitlab.sszuev.books.dao;

import com.gitlab.sszuev.books.domain.Book;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.stream.Stream;

/**
 * Created by @ssz on 20.01.2021.
 */
public interface BookRepository extends MongoRepository<Book, String>, ReadAllRepository<Book> {
    /**
     * Answers {@code true} if a book
     * with the specified {@link com.gitlab.sszuev.books.domain.Genre#getID() genre-id}
     * can be found in the underlying storage.
     *
     * @param genreId {@code String}, not {@code null}
     * @return {@code boolean}
     */
    boolean existsByGenreId(String genreId);

    /**
     * Lists all books from the underlying storage
     * by the given {@link com.gitlab.sszuev.books.domain.Genre#getName() genre-name}.
     *
     * @param genreName {@code String} - genre name to search, not {@code null}
     * @return a {@code Stream} of {@link Book}s
     */
    @Query("{ 'genre.name' : { $regex: '^\\\\s*?0\\\\s*$', $options: 'i' } }")
    Stream<Book> streamByGenreNameIgnoreCase(String genreName);

    /**
     * Answers {@code true} if a book
     * with the specified {@link com.gitlab.sszuev.books.domain.Author#getID() author-id}
     * can be found in the underlying storage.
     *
     * @param authorId {@code String}, not {@code null}
     * @return {@code boolean}
     */
    boolean existsByAuthorsId(String authorId);

    /**
     * Lists all books from the underlying storage
     * by the given {@link com.gitlab.sszuev.books.domain.Author#getName() author-name}.
     *
     * @param authorName {@code String} - author name to search, not {@code null}
     * @return a {@code Stream} of {@link Book}s
     */
    @Query("{ 'authors' : { $elemMatch : { 'name' : { $regex: '^\\\\s*?0\\\\s*$', $options: 'i' } } } }")
    Stream<Book> streamByAuthorNameIgnoreCase(String authorName);
}
