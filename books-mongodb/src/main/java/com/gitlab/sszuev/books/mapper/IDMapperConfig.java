package com.gitlab.sszuev.books.mapper;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by @ssz on 24.01.2021.
 */
@Configuration
public class IDMapperConfig {

    @Bean("bookIDMapper")
    public IDMapper<String, Long> bookIDMapper() {
        return new SimpleIDMapper();
    }

    @Bean("genreIDMapper")
    public IDMapper<String, Long> genreIDMapper() {
        return new SimpleIDMapper();
    }

    @Bean("authorIDMapper")
    public IDMapper<String, Long> authorIDMapper() {
        return new SimpleIDMapper();
    }

    @Bean("commentIDMapper")
    public IDMapper<String, Long> commentIDMapper() {
        return new SimpleIDMapper();
    }
}
