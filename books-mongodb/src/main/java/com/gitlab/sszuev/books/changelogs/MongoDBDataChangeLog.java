package com.gitlab.sszuev.books.changelogs;

import com.github.cloudyrock.mongock.ChangeLog;
import com.github.cloudyrock.mongock.ChangeSet;
import com.github.cloudyrock.mongock.driver.mongodb.springdata.v3.decorator.impl.MongockTemplate;
import com.gitlab.sszuev.books.domain.Genre;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.index.Index;

/**
 * Created by @ssz on 27.01.2021.
 */
@ChangeLog(order = "001")
public class MongoDBDataChangeLog {

    @ChangeSet(order = "001", id = "unique_index_genre_name", author = "ssz")
    public void createUniqueIndexForGenreName(MongockTemplate template) {
        template.indexOps(Genre.COLLECTION).ensureIndex(new Index(Genre.NAME, Sort.Direction.ASC).unique());
    }
}
