package com.gitlab.sszuev.books.dto;

/**
 * Created by @ssz on 04.01.2021.
 */
abstract class BaseRecord {
    final long id;

    protected BaseRecord(long id) {
        this.id = id;
    }

    public final long getId() {
        return id;
    }
}
