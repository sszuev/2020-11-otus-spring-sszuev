package com.gitlab.sszuev.books.mapper;

import com.gitlab.sszuev.books.domain.Author;
import com.gitlab.sszuev.books.domain.Book;
import com.gitlab.sszuev.books.domain.Comment;
import com.gitlab.sszuev.books.domain.Genre;
import com.gitlab.sszuev.books.dto.AuthorRecord;
import com.gitlab.sszuev.books.dto.BookRecord;
import com.gitlab.sszuev.books.dto.CommentRecord;
import com.gitlab.sszuev.books.dto.GenreRecord;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * A helper to map JPA Entities to DTO Objects.
 * <p>
 * Created by @ssz on 30.12.2020.
 */
@Component
public class EntityMapper {
    private final IDMapper<String, Long> bookIDs;
    private final IDMapper<String, Long> genreIDs;
    private final IDMapper<String, Long> authorIDs;
    private final IDMapper<String, Long> commentIDs;

    public EntityMapper(@Qualifier("bookIDMapper") IDMapper<String, Long> bookIDs,
                        @Qualifier("genreIDMapper") IDMapper<String, Long> genreIDs,
                        @Qualifier("authorIDMapper") IDMapper<String, Long> authorIDs,
                        @Qualifier("commentIDMapper") IDMapper<String, Long> commentIDs) {
        this.bookIDs = Objects.requireNonNull(bookIDs);
        this.genreIDs = Objects.requireNonNull(genreIDs);
        this.authorIDs = Objects.requireNonNull(authorIDs);
        this.commentIDs = Objects.requireNonNull(commentIDs);
    }

    public BookRecord toRecord(Book book) {
        return new BookRecord(bookIDs.toRecordID(book.getID()), book.getTitle(), toRecord(book.getGenre()),
                book.getAuthors().stream().map(this::toRecord).collect(Collectors.toList()));
    }

    public GenreRecord toRecord(Genre genre) {
        return new GenreRecord(genreIDs.toRecordID(genre.getID()), genre.getName());
    }

    public AuthorRecord toRecord(Author author) {
        return new AuthorRecord(authorIDs.toRecordID(author.getID()), author.getName());
    }

    public CommentRecord toRecord(Comment comment) {
        return new CommentRecord(commentIDs.toRecordID(comment.getID()),
                bookIDs.toRecordID(comment.getBook().getID()),
                comment.getComment(), comment.getTimestamp());
    }

    public Optional<String> bookID(long recordID) {
        return Optional.ofNullable(bookIDs.toEntityID(recordID));
    }
}
