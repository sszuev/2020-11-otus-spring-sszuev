package com.gitlab.sszuev.books.events;

/**
 * Created by @ssz on 20.01.2021.
 */
public class WrongDataException extends IllegalArgumentException {
    public WrongDataException(String s) {
        super(s);
    }
}
