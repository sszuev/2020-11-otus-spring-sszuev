package com.gitlab.sszuev.books.shell;

import com.gitlab.sszuev.books.BootTestBase;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.Shell;

/**
 * Created by @ssz on 30.12.2020.
 */
public class ShellControllerTest extends BootTestBase {
    @Autowired
    private Shell shell;

    @ParameterizedTest(name = "[{index}] ::: {0}")
    @CsvSource({"list"
            , "books", "list-books"
            , "authors", "list-authors"
            , "genres", "list-genres"
            , "comments", "list-comments"})
    public void testList(String command) {
        Assertions.assertNull(shell.evaluate(() -> command));
    }

    @Test
    public void testCreate() {
        Assertions.assertNull(shell.evaluate(() -> "add b1 g1 a1"));
        Assertions.assertNull(shell.evaluate(() -> "create b2 g1 a1 a2 a3 a4 a5 a6 a7"));
        Assertions.assertNull(shell.evaluate(() -> "add b3 g3 a1 a2 a3 a4 a5 a6 a7 a8 a9 a10"));
        Assertions.assertTrue(shell.evaluate(() -> "add b3 g3 a1 a2 a3 a4 a5 a6 a7 a8 a9 a10 a11") instanceof IllegalArgumentException);
    }

    @Test
    public void testDelete() {
        Assertions.assertNull(shell.evaluate(() -> "del 42"));
        Assertions.assertNull(shell.evaluate(() -> "delete 4"));

        Assertions.assertNull(shell.evaluate(() -> "delete 42"));
    }

    @ParameterizedTest(name = "[{index}] ::: {0} {1}")
    @CsvSource({"comments,42", "list-comments,4"})
    public void testListComments(String command, int id) {
        Assertions.assertNull(shell.evaluate(() -> command + " " + id));
    }

    @Test
    public void testAddComment() {
        Assertions.assertNull(shell.evaluate(() -> "comment 42 'Nice!'"));
        Assertions.assertNull(shell.evaluate(() -> "add-comment 4 'BAD'"));
    }

    @Test
    public void testDelComment() {
        Assertions.assertNull(shell.evaluate(() -> "del-comment 42"));
        Assertions.assertNull(shell.evaluate(() -> "delete-comment 4"));
    }

    @ParameterizedTest(name = "[{index}] ::: {0} {1}")
    @CsvSource({"list-by-author, Z", "books-by-author, Y", "author, X"})
    public void testListBooksByAuthor(String command, String parameter) {
        String cmd = command + " " + parameter;
        Assertions.assertNull(shell.evaluate(() -> cmd));
    }

    @ParameterizedTest(name = "[{index}] ::: {0} {1}")
    @CsvSource({"list-by-genre, сказка", "books-by-genre, история", "genre, xxx"})
    public void testListBooksByGenre(String command, String parameter) {
        String cmd = command + " " + parameter;
        Assertions.assertNull(shell.evaluate(() -> cmd));
    }
}
