package com.gitlab.sszuev.books.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.annotation.DirtiesContext;

/**
 * Created by @ssz on 20.01.2021.
 */
@DataMongoTest
@ComponentScan(basePackages = RepositoryTestBase.EVENTS_PACKAGE)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
abstract class RepositoryTestBase {
    static final String EVENTS_PACKAGE = "com.gitlab.sszuev.books.events";

    @Autowired
    MongoTemplate template;

}
