package com.gitlab.sszuev.books.service;

import com.gitlab.sszuev.books.BootTestBase;
import com.gitlab.sszuev.books.TestFactory;
import com.gitlab.sszuev.books.TestUtils;
import com.gitlab.sszuev.books.dao.AuthorRepository;
import com.gitlab.sszuev.books.dao.BookRepository;
import com.gitlab.sszuev.books.dao.CommentRepository;
import com.gitlab.sszuev.books.dao.GenreRepository;
import com.gitlab.sszuev.books.domain.Author;
import com.gitlab.sszuev.books.domain.Book;
import com.gitlab.sszuev.books.domain.Comment;
import com.gitlab.sszuev.books.domain.Genre;
import com.gitlab.sszuev.books.dto.BookRecord;
import com.gitlab.sszuev.books.dto.CommentRecord;
import com.gitlab.sszuev.books.mapper.EntityMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by @ssz on 29.12.2020.
 */
public class LibraryServiceMockTest extends BootTestBase {
    @Autowired
    private LibraryService service;
    @MockBean
    private BookRepository bookRepository;
    @MockBean
    private GenreRepository genreRepository;
    @MockBean
    private AuthorRepository authorRepository;
    @MockBean
    private CommentRepository commentRepository;
    @SpyBean
    private EntityMapper entityMapper;

    @Test
    public void testListBooks() {
        Author a1 = TestFactory.newAuthor("a1", "author1");
        Author a2 = TestFactory.newAuthor("a2", "author2");
        Genre g1 = TestFactory.newGenre("g1", "genre");
        Book b1 = TestFactory.newBook("b1", "test-title-1", g1, List.of(a1));
        Book b2 = TestFactory.newBook("b2", "test-title-2", g1, List.of(a1, a2));

        Mockito.when(bookRepository.streamAll()).thenAnswer(x -> Stream.of(b1, b2));

        Assertions.assertEquals(2, service.books().count());
        List<BookRecord> actual = service.books().collect(Collectors.toList());
        Assertions.assertEquals(Stream.of(b1, b2).map(Book::getTitle).collect(Collectors.toSet()),
                actual.stream().map(BookRecord::getTitle).collect(Collectors.toSet()));
    }

    @Test
    public void testListGenres() {
        Genre g1 = TestFactory.newGenre("g1", "g1");
        Genre g2 = TestFactory.newGenre("g2", "g2");
        Mockito.when(genreRepository.streamAll()).thenAnswer(x -> Stream.of(g1, g2));
        Assertions.assertEquals(2, service.genres().count());
        Assertions.assertEquals(Set.of(g1.getName(), g2.getName()),
                service.genres().map(x -> x.getName()).collect(Collectors.toSet()));
    }

    @Test
    public void testListAuthors() {
        Author a1 = TestFactory.newAuthor("a1", "a1");
        Author a2 = TestFactory.newAuthor("a2", "a2");
        Mockito.when(authorRepository.streamAll()).thenAnswer(x -> Stream.of(a1, a2));
        Assertions.assertEquals(2, service.authors().count());
        Assertions.assertEquals(Set.of(a1.getName(), a2.getName()),
                service.authors().map(x -> x.getName()).collect(Collectors.toSet()));
    }

    @Test
    public void testCreateNewBookTuple() {
        Author author1 = TestFactory.newAuthor("a1", "author1");
        Author author2 = TestFactory.newAuthor("a2", "author2");
        Genre genre = TestFactory.newGenre("g1", "test-genre");
        Book book = TestFactory.newBook("b1", "test-title", genre, List.of(author1, author2));

        Mockito.when(bookRepository.save(Mockito.any())).thenReturn(book);

        BookRecord actual = service.create(book.getTitle(), genre.getName(),
                List.of(author1.getName(), author2.getName()));
        Assertions.assertNotNull(actual);
        Assertions.assertEquals(book.getTitle(), actual.getTitle());
        Assertions.assertEquals(genre.getName(), actual.getGenre().getName());
    }

    @Test
    public void testCreateBookWithExistingAuthorsAndGenre() {
        Exception ex1 = new TestEx();
        Exception ex2 = new TestEx();

        Mockito.when(authorRepository.streamByNameIn(Mockito.eq(List.of("A")))).thenThrow(ex1);
        Mockito.when(genreRepository.streamByName(Mockito.eq("G"))).thenThrow(ex2);
        Assertions.assertSame(ex1, Assertions.assertThrows(TestEx.class,
                () -> service.create("title", "genre", List.of("A"))));
        Assertions.assertSame(ex2, Assertions.assertThrows(TestEx.class,
                () -> service.create("title", "G", List.of("Y", "Z"))));
    }

    @Test
    public void testDelete() {
        Genre genre = createGenre();
        List<Author> authors = createAuthors();
        Book b1 = TestFactory.newBook("b1", "test-title-1", genre, authors);
        Book b2 = TestFactory.newBook("b2", "test-title-2", genre, authors);
        long id1 = -42;
        long id2 = 42;

        Mockito.when(entityMapper.bookID(id1)).thenReturn(Optional.of(b1.getID()));
        Mockito.when(entityMapper.bookID(id2)).thenReturn(Optional.of(b2.getID()));
        Mockito.doNothing().when(bookRepository).delete(Mockito.any());
        Mockito.when(bookRepository.findById(b1.getID())).thenReturn(Optional.of(b1));
        Mockito.when(bookRepository.findById(b2.getID())).thenReturn(Optional.empty());

        Assertions.assertFalse(service.delete(id2));
        Assertions.assertTrue(service.delete(id1));
    }

    @Test
    public void testListComments() {
        LocalDateTime time = LocalDateTime.now();
        long id1 = -42;
        long id2 = 42;
        Book book1 = createBook("b1");
        Book book2 = createBook("b2");
        Comment comment1 = TestFactory.newComment("c1", book1, "comm1", time.minusMinutes(42));
        Comment comment2 = TestFactory.newComment("c2", book1, "comm2", time.minusYears(42));
        Comment comment3 = TestFactory.newComment("c3", book2, "comm3", time.plusHours(42));

        Mockito.when(entityMapper.bookID(id1)).thenReturn(Optional.of(book1.getID()));
        Mockito.when(entityMapper.bookID(id2)).thenReturn(Optional.of(book2.getID()));
        Mockito.when(commentRepository.streamByBookIdOrderByTimestampDesc(book1.getID()))
                .thenReturn(Stream.of(comment1, comment2));
        Mockito.when(commentRepository.streamByBookIdOrderByTimestampDesc(book2.getID()))
                .thenReturn(Stream.of(comment3));
        Mockito.when(commentRepository.streamAllOrderByTimestampDesc()).thenReturn(Stream.of(comment1, comment2, comment3));

        Assertions.assertEquals(TestUtils.toSet(Comment::getComment, comment1, comment2, comment3),
                service.comments(null).map(CommentRecord::getMessage).collect(Collectors.toSet()));
        Assertions.assertEquals(TestUtils.toSet(Comment::getComment, comment1, comment2),
                service.comments(id1).map(CommentRecord::getMessage).collect(Collectors.toSet()));
        Assertions.assertEquals(TestUtils.toSet(Comment::getComment, comment3),
                service.comments(id2).map(CommentRecord::getMessage).collect(Collectors.toSet()));
    }

    @Test
    public void testAddComment() {
        String message = "XXXX";
        LocalDateTime now = LocalDateTime.now();
        long bookId = -42;
        Book book = createBook("b1");

        Mockito.when(commentRepository.save(Mockito.any())).thenAnswer(i -> {
            Comment res = i.getArgument(0);
            res.setID("x");
            return res;
        });
        Mockito.when(entityMapper.bookID(bookId)).thenReturn(Optional.of(book.getID()));
        Mockito.when(bookRepository.findById(Mockito.eq(book.getID()))).thenReturn(Optional.of(book));

        CommentRecord res = service.addComment(bookId, message).orElseThrow(AssertionError::new);

        Assertions.assertEquals(message, res.getMessage());
        Assertions.assertFalse(res.getTimestamp().isBefore(now),
                String.format("Time %s is not after %s", res.getTimestamp(), now));

        Assertions.assertTrue(service.addComment(111, message).isEmpty());
    }

    @Test
    public void testDeleteComment() {
        LocalDateTime time = LocalDateTime.now().minusYears(42);
        long bookId = -42;
        Book book = createBook("b1");
        Comment comment = TestFactory.newComment("c1", book, "comm1", time);

        Mockito.when(entityMapper.bookID(bookId)).thenReturn(Optional.of(book.getID()));
        Mockito.when(commentRepository.findLast(Mockito.eq(book.getID()))).thenReturn(Optional.of(comment));
        Mockito.doNothing().when(commentRepository).deleteById(Mockito.eq(comment.getID()));

        CommentRecord res = service.deleteComment(bookId).orElseThrow(AssertionError::new);
        Assertions.assertEquals(comment.getComment(), res.getMessage());

        Assertions.assertTrue(service.deleteComment(111).isEmpty());
    }

    @Test
    public void testListBooksByAuthor() {
        Author a1 = TestFactory.newAuthor("a1", "author1");
        Author a2 = TestFactory.newAuthor("a2", "author2");
        Genre g1 = createGenre();
        Book b1 = TestFactory.newBook("b1", "test-title-1", g1, List.of(a1));
        Book b2 = TestFactory.newBook("b2", "test-title-2", g1, List.of(a1, a2));

        Mockito.when(bookRepository.streamByAuthorNameIgnoreCase(Mockito.eq(a1.getName()))).thenReturn(Stream.of(b1, b2));
        Mockito.when(bookRepository.streamByAuthorNameIgnoreCase(Mockito.eq(a2.getName()))).thenReturn(Stream.of(b2));

        Assertions.assertEquals(2, service.booksByAuthor(a1.getName()).count());
        Assertions.assertEquals(1, service.booksByAuthor(a2.getName()).count());
        Assertions.assertEquals(0, service.booksByAuthor("X").count());
    }

    @Test
    public void testListBooksByGenre() {
        Author a = TestFactory.newAuthor("a1", "author1");
        Genre g1 = TestFactory.newGenre("g1", "genre1");
        Genre g2 = TestFactory.newGenre("g2", "genre2");
        Book b1 = TestFactory.newBook("b1", "test-title-1", g1, List.of(a));
        Book b2 = TestFactory.newBook("b2", "test-title-2", g2, List.of(a));

        Mockito.when(bookRepository.streamByGenreNameIgnoreCase(Mockito.eq(g1.getName()))).thenReturn(Stream.of(b1));
        Mockito.when(bookRepository.streamByGenreNameIgnoreCase(Mockito.eq(g2.getName()))).thenReturn(Stream.of(b2));

        Assertions.assertEquals(1, service.booksByGenre(g1.getName()).count());
        Assertions.assertEquals(1, service.booksByGenre(g2.getName()).count());
        Assertions.assertEquals(0, service.booksByGenre("X").count());
    }

    private static Genre createGenre() {
        return TestFactory.newGenre("g-id", "genre");
    }

    private static List<Author> createAuthors() {
        return List.of(TestFactory.newAuthor("a-id", "author"));
    }

    private static Book createBook(String id) {
        return TestFactory.newBook(id, "test-title", createGenre(), createAuthors());
    }

    private static class TestEx extends RuntimeException {
    }

}
