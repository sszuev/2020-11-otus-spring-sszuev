package com.gitlab.sszuev.books;

import org.junit.jupiter.api.Assertions;

import java.util.Arrays;
import java.util.Collection;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by @ssz on 20.01.2021.
 */
public class TestUtils {
    public static <X> X assertOne(Collection<X> list) {
        Assertions.assertEquals(1, list.size());
        X res = list.iterator().next();
        Assertions.assertNotNull(res);
        return res;
    }

    public static <X> void assertEquals(X expected, X actual, Function<X, ?> get) {
        Assertions.assertEquals(get.apply(expected), get.apply(actual));
    }

    public static <X> X getFirst(Collection<X> list, Predicate<X> test) {
        return getFirst(list.stream(), test);
    }

    public static <X> X getFirst(Stream<X> list, Predicate<X> test) {
        return list.filter(test).findFirst().orElseThrow(AssertionError::new);
    }

    @SafeVarargs
    public static <E, R> Set<R> toSet(Function<E, R> map, E... values) {
        return Arrays.stream(values).map(map).collect(Collectors.toSet());
    }
}
