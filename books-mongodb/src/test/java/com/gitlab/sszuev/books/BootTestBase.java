package com.gitlab.sszuev.books;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

/**
 * Created by @ssz on 24.01.2021.
 */
@SpringBootTest
@TestPropertySource(properties = "spring.shell.interactive.enabled=false")
public class BootTestBase {
}
