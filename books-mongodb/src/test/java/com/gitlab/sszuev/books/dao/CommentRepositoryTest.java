package com.gitlab.sszuev.books.dao;

import com.gitlab.sszuev.books.TestFactory;
import com.gitlab.sszuev.books.domain.Book;
import com.gitlab.sszuev.books.domain.Comment;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by @ssz on 23.01.2021.
 */
public class CommentRepositoryTest extends RepositoryTestBase {
    @Autowired
    private CommentRepository commentRepository;
    private final List<Comment> comments = new ArrayList<>();

    @BeforeEach
    public void before() {
        LocalDateTime time = LocalDateTime.now();
        Book b1 = template.insert(TestFactory.newBook(null, "B1", TestFactory.newGenre("G"),
                List.of(TestFactory.newAuthor("A1"))));
        Book b2 = template.insert(TestFactory.newBook(null, "B2", TestFactory.newGenre("G"),
                List.of(TestFactory.newAuthor("A1"))));

        comments.add(template.insert(TestFactory.newComment(null, b1, "c1", time.minusHours(42))));
        comments.add(template.insert(TestFactory.newComment(null, b1, "c2", time.minusSeconds(42))));
        comments.add(template.insert(TestFactory.newComment(null, b2, "c3", time.minusDays(42))));
    }

    @AfterEach
    public void after() {
        template.dropCollection(Comment.class);
        template.dropCollection(Book.class);
        comments.clear();
    }

    @Test
    public void testStreamAll() {
        Set<Comment> actual = commentRepository.streamAll().collect(Collectors.toSet());
        Assertions.assertEquals(3, actual.size());
    }

    @Test
    public void testStreamAllOrderedByDate() {
        List<String> expected = comments.stream()
                .sorted(Comparator.comparing(Comment::getTimestamp).reversed())
                .map(Comment::getComment).collect(Collectors.toList());
        List<String> actual = commentRepository.streamAllOrderByTimestampDesc()
                .map(Comment::getComment).collect(Collectors.toList());
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void testStreamByBook() {
        Book b1 = comments.get(0).getBook();
        Book b2 = comments.get(2).getBook();
        Assertions.assertEquals(2, commentRepository.streamByBookIdOrderByTimestampDesc(b1.getID()).count());
        Assertions.assertEquals(1, commentRepository.streamByBookIdOrderByTimestampDesc(b2.getID()).count());
    }

    @Test
    public void testFindLast() {
        Assertions.assertTrue(commentRepository.findLast("x").isEmpty());

        Comment c2 = comments.get(1);
        Comment c3 = comments.get(2);
        Stream.of(c2, c3).forEach(c -> {
            String b = c.getBook().getID();
            Assertions.assertEquals(c.getID(), commentRepository.findLast(b).orElseThrow(AssertionError::new).getID());
        });
    }
}
