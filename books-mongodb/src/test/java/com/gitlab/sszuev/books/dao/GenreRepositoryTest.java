package com.gitlab.sszuev.books.dao;

import com.gitlab.sszuev.books.TestFactory;
import com.gitlab.sszuev.books.TestUtils;
import com.gitlab.sszuev.books.domain.Genre;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

/**
 * Created by @ssz on 21.01.2021.
 */
@ComponentScan(basePackages = "com.gitlab.sszuev.books.changelogs")
public class GenreRepositoryTest extends RepositoryTestBase {
    @Autowired
    private GenreRepository repository;

    @Test
    public void testDeleteIfPossible() {
        Assumptions.assumeFalse(repository.deleteIfPossible("X"));

        Genre g1 = template.insert(TestFactory.newGenre("X"));
        Assumptions.assumeTrue(repository.deleteIfPossible(g1.getID()));

        Genre g2 = template.insert(TestFactory.newBook(null, "B1", TestFactory.newGenre("Y"),
                List.of(TestFactory.newAuthor("A")))).getGenre();
        Assumptions.assumeFalse(repository.deleteIfPossible(g2.getID()));
    }

    @Test
    public void testUpsert() {
        String name = "TheGenreN" + System.currentTimeMillis();
        String g1 = repository.upsert(TestFactory.newGenre(name)).getID();
        Assertions.assertNotNull(g1);
        String g2 = repository.upsert(TestFactory.newGenre(name)).getID();
        Assertions.assertNotNull(g2);
        Assertions.assertEquals(g1, g2);
        String g3 = TestUtils.assertOne(template.find(new Query(Criteria.where("name").is(name)), Genre.class)).getID();
        Assertions.assertEquals(g1, g3);
    }

    @Test
    public void testSave() {
        String name = "TheGenreN" + System.currentTimeMillis();
        Assertions.assertNotNull(repository.save(TestFactory.newGenre(name)).getID());
        Assertions.assertThrows(DuplicateKeyException.class, () -> repository.save(TestFactory.newGenre(name)));
    }

}
