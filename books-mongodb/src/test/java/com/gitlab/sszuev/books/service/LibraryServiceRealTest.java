package com.gitlab.sszuev.books.service;

import com.gitlab.sszuev.books.BootTestBase;
import com.gitlab.sszuev.books.dto.BookRecord;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by @ssz on 09.01.2021.
 */
public class LibraryServiceRealTest extends BootTestBase {
    @Autowired
    private LibraryService service;

    @Test
    public void testAddAndDelete() {
        BookRecord r = service.create("t1", "g1", List.of("a1"));
        Assertions.assertNotNull(r);
        Assertions.assertTrue(service.delete(r.getId()));
    }
}
