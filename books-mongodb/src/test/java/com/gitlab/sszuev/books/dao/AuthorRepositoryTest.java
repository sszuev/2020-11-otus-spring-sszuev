package com.gitlab.sszuev.books.dao;

import com.gitlab.sszuev.books.TestFactory;
import com.gitlab.sszuev.books.TestUtils;
import com.gitlab.sszuev.books.domain.Author;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by @ssz on 22.01.2021.
 */
public class AuthorRepositoryTest extends RepositoryTestBase {
    @Autowired
    private AuthorRepository repository;

    @Test
    public void testDeleteIfPossible() {
        Assertions.assertFalse(repository.deleteIfPossible("X"));

        Author a1 = template.insert(TestFactory.newAuthor("X"));
        Assertions.assertTrue(repository.deleteIfPossible(a1.getID()));

        Author a2 = TestUtils.assertOne(template.insert(TestFactory.newBook(null, "B1", TestFactory.newGenre("Y"),
                List.of(TestFactory.newAuthor("A")))).getAuthors());
        Assertions.assertFalse(repository.deleteIfPossible(a2.getID()));
    }
}
