package com.gitlab.sszuev.books;

import com.gitlab.sszuev.books.domain.Author;
import com.gitlab.sszuev.books.domain.Book;
import com.gitlab.sszuev.books.domain.Comment;
import com.gitlab.sszuev.books.domain.Genre;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by @ssz on 21.01.2021.
 */
public class TestFactory {

    public static Book newBook(String id, String title, Genre genre, List<Author> authors) {
        Book res = new Book();
        res.setID(id);
        res.setTitle(title);
        res.setGenre(genre);
        res.setAuthors(authors);
        return res;
    }

    public static Author newAuthor(String id, String author) {
        Author res = newAuthor(author);
        res.setID(id);
        return res;
    }

    public static Author newAuthor(String author) {
        Author res = new Author();
        res.setName(author);
        return res;
    }

    public static Genre newGenre(String id, String genre) {
        Genre res = newGenre(genre);
        res.setID(id);
        return res;
    }

    public static Genre newGenre(String genre) {
        Genre res = new Genre();
        res.setName(genre);
        return res;
    }

    public static Comment newComment(String id, Book book, String comment, LocalDateTime dt) {
        Comment res = new Comment();
        res.setBook(book);
        res.setID(id);
        res.setComment(comment);
        res.setTimestamp(dt);
        return res;
    }
}
