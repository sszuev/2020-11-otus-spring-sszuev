package com.gitlab.sszuev.books.dao;

import com.gitlab.sszuev.books.TestFactory;
import com.gitlab.sszuev.books.TestUtils;
import com.gitlab.sszuev.books.domain.Author;
import com.gitlab.sszuev.books.domain.Book;
import com.gitlab.sszuev.books.domain.Comment;
import com.gitlab.sszuev.books.domain.Genre;
import com.gitlab.sszuev.books.events.WrongDataException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by @ssz on 20.01.2021.
 */
public class BookRepositoryTest extends RepositoryTestBase {

    @Autowired
    private BookRepository bookRepository;

    @Test
    public void testSaveFailWrongData() {
        // no authors
        Book book1 = new Book();
        book1.setGenre(TestFactory.newGenre("G1"));
        book1.setTitle("B1");
        Assertions.assertThrows(WrongDataException.class, () -> bookRepository.save(book1));

        // no genres
        Book book2 = new Book();
        book2.setTitle("B2");
        book2.setAuthors(List.of(TestFactory.newAuthor("A2")));
        Assertions.assertThrows(WrongDataException.class, () -> bookRepository.save(book2));

        // no title
        Book book3 = new Book();
        book3.setGenre(TestFactory.newGenre("G3"));
        book3.setAuthors(List.of(TestFactory.newAuthor("A3")));
        Assertions.assertThrows(WrongDataException.class, () -> bookRepository.save(book3));
    }

    @DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD) // just in case
    @Test
    public void testSaveSuccess() {
        String genre1 = "G1";
        String genre2 = "G2";
        String author1 = "A1";
        String author2 = "A2";
        Book book1 = saveAndTest(TestFactory.newBook(null, "B1", TestFactory.newGenre(genre1), List.of(TestFactory.newAuthor(author1))));
        Assertions.assertEquals(genre1, TestUtils.assertOne(template.findAll(Genre.class)).getName());

        Book book2 = saveAndTest(TestFactory.newBook(null, "B2", TestFactory.newGenre(genre1), List.of(TestFactory.newAuthor(author2))));
        List<Genre> genres2 = template.findAll(Genre.class);
        Assertions.assertEquals(1, genres2.size());
        Assertions.assertEquals(Set.of(genre1), genres2.stream().map(Genre::getName).collect(Collectors.toSet()));

        saveAndTest(TestFactory.newBook(null, "B3", TestFactory.newGenre(genre2),
                List.of(TestUtils.assertOne(book1.getAuthors()), TestUtils.assertOne(book2.getAuthors()))));

        List<Genre> genres = template.findAll(Genre.class);
        Assertions.assertEquals(2, genres.size());
        Assertions.assertEquals(Set.of(genre1, genre2),
                genres.stream().map(Genre::getName).collect(Collectors.toSet()));

        List<Author> authors = template.findAll(Author.class);
        Assertions.assertEquals(2, authors.size());
        Assertions.assertEquals(Set.of(author1, author2),
                authors.stream().map(Author::getName).collect(Collectors.toSet()));
    }

    @Test
    public void testExistsByGenreId() {
        Book book = template.insert(TestFactory.newBook(null, "B1", TestFactory.newGenre("G"),
                List.of(TestFactory.newAuthor("A"))));
        Assertions.assertNotNull(book);

        Assertions.assertTrue(bookRepository.existsByGenreId(book.getGenre().getID()));
        Assertions.assertFalse(bookRepository.existsByGenreId("UnknownGenreId"));
    }

    @Test
    public void testExistsByAuthorId() {
        Book b1 = template.insert(TestFactory.newBook(null, "B1", TestFactory.newGenre("G"),
                List.of(TestFactory.newAuthor("A1"))));
        Assertions.assertNotNull(b1);
        Book b2 = template.insert(TestFactory.newBook(null, "B2", b1.getGenre(),
                List.of(TestUtils.assertOne(b1.getAuthors()), TestFactory.newAuthor("A2"))));
        Assertions.assertNotNull(b2);

        Author a1 = TestUtils.getFirst(b2.getAuthors(), x -> "A1".equals(x.getName()));
        Author a2 = TestUtils.getFirst(b2.getAuthors(), x -> "A2".equals(x.getName()));

        Assertions.assertTrue(bookRepository.existsByAuthorsId(a1.getID()));
        Assertions.assertTrue(bookRepository.existsByAuthorsId(a2.getID()));
        Assertions.assertFalse(bookRepository.existsByAuthorsId("UnknownAuthorID"));
    }

    @Test
    public void testDelete() {
        Book b1 = template.insert(TestFactory.newBook(null, "B1", TestFactory.newGenre("G1"),
                List.of(TestFactory.newAuthor("A1"))));
        Book b2 = template.insert(TestFactory.newBook(null, "B2", TestFactory.newGenre("G2"),
                List.of(TestFactory.newAuthor("A2"))));
        int count = template.findAll(Book.class).size();

        template.insert(TestFactory.newComment(null, b1, "c1", LocalDateTime.now().minusHours(42)));
        template.insert(TestFactory.newComment(null, b1, "c2", LocalDateTime.now().minusSeconds(42)));
        template.insert(TestFactory.newComment(null, b2, "c3", LocalDateTime.now().plusNanos(42)));

        bookRepository.delete(b2);
        Assertions.assertEquals(--count, template.findAll(Book.class).size());
        Assertions.assertEquals(2, template.findAll(Comment.class).size());

        bookRepository.delete(b1);
        Assertions.assertEquals(--count, template.findAll(Book.class).size());
        Assertions.assertEquals(0, template.findAll(Comment.class).size());
    }

    @Test
    public void testListBooksByAuthorName() {
        template.insert(TestFactory.newBook(null, "B1", TestFactory.newGenre("G1"),
                List.of(TestFactory.newAuthor(" john doe "))));
        template.insert(TestFactory.newBook(null, "B2", TestFactory.newGenre("G2"),
                List.of(TestFactory.newAuthor("John Doe"), TestFactory.newAuthor("Doe John"))));

        Assertions.assertEquals(2, bookRepository.streamByAuthorNameIgnoreCase("John Doe").count());
        Assertions.assertEquals(2, bookRepository.streamByAuthorNameIgnoreCase("john DOE").count());
        Assertions.assertEquals(0, bookRepository.streamByAuthorNameIgnoreCase("john").count());
        Assertions.assertEquals(1, bookRepository.streamByAuthorNameIgnoreCase("doe john").count());
    }

    @Test
    public void testListBooksByGenreName() {
        template.insert(TestFactory.newBook(null, "B1", TestFactory.newGenre("Tale"),
                List.of(TestFactory.newAuthor("a1"))));
        template.insert(TestFactory.newBook(null, "B2", TestFactory.newGenre(" historical novel"),
                List.of(TestFactory.newAuthor("a2"))));

        Assertions.assertEquals(1, bookRepository.streamByGenreNameIgnoreCase("tale").count());
        Assertions.assertEquals(1, bookRepository.streamByGenreNameIgnoreCase("Historical Novel").count());
        Assertions.assertEquals(0, bookRepository.streamByGenreNameIgnoreCase("novel").count());
    }

    private Book saveAndTest(Book givenBook) {
        Genre givenGenre = givenBook.getGenre();
        List<Author> givenAuthors = givenBook.getAuthors();

        Book savedBook = bookRepository.save(givenBook);
        Assertions.assertNotNull(savedBook.getID());
        Genre savedGenre = savedBook.getGenre();
        Assertions.assertNotNull(savedGenre.getID());
        List<Author> savedAuthors = savedBook.getAuthors();
        savedAuthors.forEach(a -> Assertions.assertNotNull(a.getID()));
        Assertions.assertEquals(givenAuthors.size(), savedAuthors.size());

        Book foundBook = template.findById(savedBook.getID(), Book.class);
        Assertions.assertNotNull(foundBook);
        Genre foundGenre = foundBook.getGenre();
        TestUtils.assertEquals(foundGenre, savedGenre, Genre::getID);
        List<Author> foundAuthors = foundBook.getAuthors();
        Assertions.assertEquals(givenAuthors.size(), foundAuthors.size());

        TestUtils.assertEquals(givenGenre, foundGenre, Genre::getName);
        TestUtils.assertEquals(givenBook, foundBook, Book::getTitle);
        TestUtils.assertEquals(givenAuthors, foundAuthors,
                authors -> authors.stream().map(Author::getName).collect(Collectors.toSet()));
        return foundBook;
    }
}
