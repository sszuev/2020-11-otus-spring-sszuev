package com.gitlab.sszuev.books.service;

import com.gitlab.sszuev.books.domain.Book;

import java.util.Optional;
import java.util.stream.Stream;

/**
 * Created by @ssz on 21.12.2020.
 */
public interface BooksService {

    /**
     * Finds the book-record by the given id.
     *
     * @param id {@code long}
     * @return an {@code Optional} around {@link Book}
     */
    Optional<Book> find(long id);

    /**
     * Lists all book-records.
     *
     * @return a {@code Stream} of {@link Book}s
     */
    Stream<Book> list();

    /**
     * Creates a new book-record with the specified parameters.
     *
     * @param title  {@code String}, not {@code null}
     * @param author {@code String}, not {@code null}
     * @param genre  {@code String}, not {@code null}
     * @return {@link Book}
     */
    Book create(String title, String author, String genre);

    /**
     * Deletes the given book-record by its id.
     *
     * @param id {@code long}
     * @return {@code true} if the record was deleted, otherwise {@code false}
     */
    boolean delete(long id);
}
