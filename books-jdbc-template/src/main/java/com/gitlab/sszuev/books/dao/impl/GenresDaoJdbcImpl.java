package com.gitlab.sszuev.books.dao.impl;

import com.gitlab.sszuev.books.dao.GenresDao;
import com.gitlab.sszuev.books.domain.Genre;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.stream.Stream;

/**
 * Created by @ssz on 20.12.2020.
 */
@Repository
public class GenresDaoJdbcImpl implements GenresDao {
    static final BiFunction<Long, String, Genre> FACTORY = Genre::new;
    private static final RowMapper<Genre> GENRE_ROW_MAPPER =
            (rs, n) -> FACTORY.apply(rs.getLong("id"), rs.getString("name"));

    private final NamedParameterJdbcOperations jdbc;

    public GenresDaoJdbcImpl(NamedParameterJdbcOperations jdbc) {
        this.jdbc = Objects.requireNonNull(jdbc);
    }

    @Override
    public Optional<Genre> findById(long id) {
        return DAOUtils.select(jdbc, "SELECT id, name FROM genres WHERE ID = :id", Map.of("id", id), GENRE_ROW_MAPPER);
    }

    @Override
    public boolean deleteById(long id) {
        return jdbc.update("DELETE FROM genres WHERE id = :id", Map.of("id", id)) == 1;
    }

    @Transactional
    @Override
    public Genre save(Genre e) {
        return DAOUtils.saveSimpleEntity(jdbc, "genres", FACTORY, e);
    }

    @Override
    public Stream<Genre> listAll() {
        return jdbc.query("SELECT id, name FROM genres", GENRE_ROW_MAPPER).stream();
    }

    @Override
    public Stream<Genre> listByName(String name) {
        return jdbc.query("SELECT id, name FROM genres WHERE name = :name",
                Map.of("name", Objects.requireNonNull(name)), GENRE_ROW_MAPPER).stream();
    }
}
