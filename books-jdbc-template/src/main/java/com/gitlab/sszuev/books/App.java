package com.gitlab.sszuev.books;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by @ssz on 19.12.2020.
 */
@SpringBootApplication
public class App {

    static {
        forciblyDisableJDKWarnings();
    }

    public static void main(String... args) {
        SpringApplication.run(App.class);
    }

    /**
     * Workaround to suppress warnings.
     * Checked on jdk11 (11.0.9), win10x64
     *
     * @see <a href='https://stackoverflow.com/questions/46454995/'>SO</a>
     */
    private static void forciblyDisableJDKWarnings() {
        try {
            java.lang.reflect.Field field = sun.misc.Unsafe.class.getDeclaredField("theUnsafe");
            field.setAccessible(true);
            sun.misc.Unsafe unsafe = (sun.misc.Unsafe) field.get(null);
            Class<?> clazz = Class.forName("jdk.internal.module.IllegalAccessLogger");
            java.lang.reflect.Field logger = clazz.getDeclaredField("logger");
            unsafe.putObjectVolatile(clazz, unsafe.staticFieldOffset(logger), null);
        } catch (Exception ignore) {
            // ignore
        }
    }
}
