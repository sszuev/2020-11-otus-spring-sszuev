package com.gitlab.sszuev.books.dao.impl;

import com.gitlab.sszuev.books.domain.HasID;
import com.gitlab.sszuev.books.domain.HasName;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.BiFunction;

/**
 * In the task description there is restriction: do <b>NOT</b> create AbstractDao!
 * To avoid that monkey style with copy-pasting,
 * all operations that appeared more than one time have been moved here, to this separated utility class.
 * <p>
 * Created by @ssz on 20.12.2020.
 */
final class DAOUtils {

    /**
     * Executes insert.
     *
     * @param jdbc  {@link NamedParameterJdbcOperations}, not {@code null}
     * @param query {@code String}, an {@code INSERT} query
     * @param data  a {@code Map} with query parameters
     * @return {@code long} - an id of new entity
     */
    public static long executeInsert(NamedParameterJdbcOperations jdbc, String query, Map<String, Object> data) {
        KeyHolder res = new GeneratedKeyHolder();
        if (jdbc.update(query, new MapSqlParameterSource(data), res) != 1) {
            throw new IllegalStateException("No rows have been affected by executing '" + query + "'");
        }
        return Objects.requireNonNull(res.getKeyAs(Long.class));
    }

    /**
     * Performs {@code SELECT} for single object.
     *
     * @param jdbc   {@link NamedParameterJdbcOperations}, not {@code null}
     * @param query  {@code String}, a {@code SELECT} query
     * @param data   a {@code Map} with query parameters
     * @param mapper {@link RowMapper} accepting {@link X}, not {@code null}
     * @param <X>    {@link HasID}, entity, not {@code null}
     * @return an {@code Optional} (empty if nothing found)
     */
    public static <X extends HasID> Optional<X> select(NamedParameterJdbcOperations jdbc,
                                                       String query,
                                                       Map<String, Object> data,
                                                       RowMapper<X> mapper) {
        try {
            return Optional.of(Objects.requireNonNull(jdbc.queryForObject(query, data, mapper)));
        } catch (EmptyResultDataAccessException ignore) {
            return Optional.empty();
        }
    }

    static <X extends HasID & HasName> X saveSimpleEntity(NamedParameterJdbcOperations jdbc,
                                                          String table,
                                                          BiFunction<Long, String, X> factory,
                                                          X entity) {
        Long id = entity.getID();
        if (id == null) {
            // new instance
            id = insertSimpleEntity(jdbc, table, entity);
        } else {
            // same instance
            updateSimpleEntity(jdbc, table, entity);
        }
        return factory.apply(id, entity.getName());
    }

    private static <X extends HasID & HasName> long insertSimpleEntity(NamedParameterJdbcOperations jdbc, String table, X entity) {
        String query = String.format("INSERT INTO %s (name) VALUES (:name)", table);
        return executeInsert(jdbc, query, Map.of("name", entity.getName()));
    }

    private static <X extends HasID & HasName> void updateSimpleEntity(NamedParameterJdbcOperations jdbc, String table, X entity) {
        long id = entity.getID();
        String name = entity.getName();
        Map<String, Object> data = Map.of("id", id, "name", name);
        String query = String.format("UPDATE %s SET name = :name WHERE id = :id", table);
        if (jdbc.update(query, data) == 1) {
            return;
        }
        query = String.format("INSERT INTO %s (id, name) VALUES (:id, :name)", table);
        if (jdbc.update(query, data) == 1) {
            return;
        }
        throw new IllegalStateException("Unable to update record (id = " + id + ") for " + table);
    }
}
