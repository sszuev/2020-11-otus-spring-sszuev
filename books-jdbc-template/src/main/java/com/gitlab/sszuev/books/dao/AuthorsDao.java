package com.gitlab.sszuev.books.dao;

import com.gitlab.sszuev.books.domain.Author;

/**
 * A DAO that provides CRUD operations for {@link Author}s instances.
 * <p>
 * Created by @ssz on 20.12.2020.
 */
public interface AuthorsDao extends CRUDBaseDao<Author>, ByNameBaseDao<Author> {
}
