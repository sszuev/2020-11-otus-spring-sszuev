package com.gitlab.sszuev.books.shell;

import com.gitlab.sszuev.books.domain.Book;
import com.gitlab.sszuev.books.domain.HasName;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * Created by @ssz on 21.12.2020.
 */
@Component
public class PrettyPrinter {
    private static final int FIRST_COL = 5;
    private static final int SECOND_COL = 40;
    private static final int THIRD_COL = 30;
    private static final int FOURTH_COL = 20;

    public String print(Book book) {
        return printRow(String.valueOf(book.getID()), book.getTitle(), print(book.getAuthor()), print(book.getGenre()));
    }

    public String printHeader() {
        return printRow("ID", "TITLE", "AUTHOR", "GENRE");
    }

    public String printSeparator() {
        return "-".repeat(FIRST_COL + SECOND_COL + THIRD_COL + FOURTH_COL);
    }

    protected String printRow(String first, String second, String third, String fourth) {
        return String.format("%s | %s | %s | %s",
                StringUtils.rightPad(first, FIRST_COL),
                StringUtils.rightPad(second, SECOND_COL),
                StringUtils.rightPad(third, THIRD_COL),
                StringUtils.rightPad(fourth, FOURTH_COL));
    }

    protected String print(HasName entity) {
        return Optional.ofNullable(entity).map(HasName::getName).filter(x -> !x.isEmpty()).orElse("NULL");
    }
}
