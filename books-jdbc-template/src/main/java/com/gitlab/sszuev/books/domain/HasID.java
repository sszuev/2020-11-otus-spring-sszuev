package com.gitlab.sszuev.books.domain;

/**
 * The <b>technical</b> interface for combining entities that have id.
 * <p>
 * Note: the discussion about the practical applicability of technical interfaces
 * in bounds of spring-based application is
 * <a href='https://gitlab.com/sszuev/2020-11-otus-spring-sszuev/-/merge_requests/1#note_457420110'>here</a>.
 * And <a href='https://gitlab.com/sszuev/2020-11-otus-spring-sszuev/-/merge_requests/2#note_461361854'>here</a>
 * is a recent example when such approach was helpful.
 * <p>
 * Created by @ssz on 19.12.2020.
 */
public interface HasID {
    /**
     * Returns the unique identifier of entity.
     *
     * @return {@code Long}
     */
    Long getID();
}
