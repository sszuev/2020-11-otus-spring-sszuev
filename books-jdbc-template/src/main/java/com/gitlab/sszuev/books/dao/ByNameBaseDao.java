package com.gitlab.sszuev.books.dao;

import com.gitlab.sszuev.books.domain.HasName;

import java.util.stream.Stream;

/**
 * A package-local "abstract DAO", just to simplify the code.
 * Right now it describes only single operation to retrieve entities by name.
 * Created by @ssz on 23.12.2020.
 *
 * @param <E> - entity with {@code String} name
 * @see CRUDBaseDao
 */
interface ByNameBaseDao<E extends HasName> {
    /**
     * Lists all entities from the underlying storage by the specified name.
     *
     * @param name {@code String}, not {@code null}
     * @return a {@code Stream} of {@link E}s
     */
    Stream<E> listByName(String name);
}
