package com.gitlab.sszuev.books.dao.impl;

import com.gitlab.sszuev.books.dao.AuthorsDao;
import com.gitlab.sszuev.books.domain.Author;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.stream.Stream;

/**
 * Created by @ssz on 20.12.2020.
 */
@Repository
public class AuthorsDaoJdbcImpl implements AuthorsDao {
    static final BiFunction<Long, String, Author> FACTORY = Author::new;
    private static final RowMapper<Author> AUTHOR_ROW_MAPPER =
            (rs, n) -> FACTORY.apply(rs.getLong("id"), rs.getString("name"));

    private final NamedParameterJdbcOperations jdbc;

    public AuthorsDaoJdbcImpl(NamedParameterJdbcOperations jdbc) {
        this.jdbc = Objects.requireNonNull(jdbc);
    }

    @Override
    public Optional<Author> findById(long id) {
        return DAOUtils.select(jdbc, "SELECT id, name FROM authors WHERE id = :id", Map.of("id", id), AUTHOR_ROW_MAPPER);
    }

    @Transactional
    @Override
    public Author save(Author e) {
        return DAOUtils.saveSimpleEntity(jdbc, "authors", FACTORY, e);
    }

    @Override
    public boolean deleteById(long id) {
        return jdbc.update("DELETE FROM authors WHERE id = :id", Map.of("id", id)) == 1;
    }

    @Override
    public Stream<Author> listAll() {
        return jdbc.query("SELECT id, name FROM authors", AUTHOR_ROW_MAPPER).stream();
    }

    @Override
    public Stream<Author> listByName(String name) {
        return jdbc.query("SELECT id, name FROM authors WHERE name = :name",
                Map.of("name", Objects.requireNonNull(name)), AUTHOR_ROW_MAPPER).stream();
    }
}
