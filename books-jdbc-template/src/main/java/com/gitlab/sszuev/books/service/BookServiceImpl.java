package com.gitlab.sszuev.books.service;

import com.gitlab.sszuev.books.dao.AuthorsDao;
import com.gitlab.sszuev.books.dao.BooksDao;
import com.gitlab.sszuev.books.dao.GenresDao;
import com.gitlab.sszuev.books.domain.Author;
import com.gitlab.sszuev.books.domain.Book;
import com.gitlab.sszuev.books.domain.Genre;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * Created by @ssz on 21.12.2020.
 */
@Service
public class BookServiceImpl implements BooksService {
    private final BooksDao booksRepo;
    private final AuthorsDao authorsRepo;
    private final GenresDao genresRepo;

    public BookServiceImpl(BooksDao booksRepo, AuthorsDao authorsRepo, GenresDao genresRepo) {
        this.booksRepo = Objects.requireNonNull(booksRepo);
        this.authorsRepo = Objects.requireNonNull(authorsRepo);
        this.genresRepo = Objects.requireNonNull(genresRepo);
    }

    @Override
    public Optional<Book> find(long id) {
        return booksRepo.findById(id);
    }

    @Override
    public Stream<Book> list() {
        return booksRepo.listAll();
    }

    @Override
    public Book create(String title, String author, String genre) {
        Author a = authorsRepo.listByName(author).findFirst().orElseGet(() -> new Author(null, author));
        Genre g = genresRepo.listByName(genre).findFirst().orElseGet(() -> new Genre(null, genre));
        return booksRepo.save(new Book(null, title, a, g));
    }

    @Override
    public boolean delete(long id) {
        Optional<Book> res = find(id);
        if (res.isEmpty()) return false;
        return booksRepo.delete(res.get());
    }
}
