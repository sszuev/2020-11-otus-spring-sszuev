package com.gitlab.sszuev.books.domain;

/**
 * A domain entity that describes an author item.
 * <p>
 * Created by @ssz on 19.12.2020.
 */
public final class Author extends SimpleEntity implements HasID, HasName {
    public Author(Long id, String name) {
        super(id, name);
    }
}
