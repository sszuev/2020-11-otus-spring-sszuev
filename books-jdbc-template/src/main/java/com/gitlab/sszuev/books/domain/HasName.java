package com.gitlab.sszuev.books.domain;

/**
 * The <b>technical</b> interface for combining entities that have name.
 * Note: see description in {@link HasID}.
 * <p>
 * Created by @ssz on 19.12.2020.
 */
public interface HasName {
    /**
     * Returns the name of entity (as string).
     *
     * @return {@code String}
     */
    String getName();
}
