package com.gitlab.sszuev.books.domain;

import java.util.Objects;

/**
 * A package-local base entity, just to simplify code.
 * <p>
 * Created by @ssz on 23.12.2020.
 */
abstract class SimpleEntity {
    protected final Long id;
    protected final String name;

    protected SimpleEntity(Long id, String name) {
        this.id = id;
        this.name = Objects.requireNonNull(name);
    }

    public Long getID() {
        return id;
    }

    public String getName() {
        return name;
    }
}
