package com.gitlab.sszuev.books.dao;

import com.gitlab.sszuev.books.domain.Book;

/**
 * A DAO that provides CRUD operations for {@link Book}s instances.
 * <p>
 * Created by @ssz on 20.12.2020.
 */
public interface BooksDao extends CRUDBaseDao<Book> {

    /**
     * Deletes the given book including all its related parts (author, genre).
     *
     * @param entity {@link Book}, not {@code null}
     * @return {@code boolean}, {@code true} if the record is deleted
     */
    boolean delete(Book entity);
}
