package com.gitlab.sszuev.books.shell;

import com.gitlab.sszuev.books.domain.Book;
import com.gitlab.sszuev.books.service.BooksService;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;

import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * Created by @ssz on 21.12.2020.
 */
@ShellComponent
public class BooksCommands {

    private final BooksService service;
    private final PrettyPrinter printer;

    public BooksCommands(BooksService service, PrettyPrinter printer) {
        this.service = Objects.requireNonNull(service);
        this.printer = Objects.requireNonNull(printer);
    }

    @ShellMethod(value = "Print all records.", key = {"show-all", "list-all", "print-all", "list"})
    public void printAll(@ShellOption(help = "Limit", defaultValue = "-1") long limit) {
        System.out.println(printer.printHeader());
        System.out.println(printer.printSeparator());
        Stream<Book> list = service.list();
        if (limit != -1) {
            list = list.limit(limit);
        }
        list.map(printer::print).forEach(System.out::println);
    }

    @ShellMethod(value = "Print one record.", key = {"show", "print"})
    public void print(@ShellOption(help = "The book ID") long id) {
        Optional<Book> book = service.find(id);
        if (book.isPresent()) {
            System.out.println(printer.printHeader());
            System.out.println(printer.printSeparator());
            System.out.println(printer.print(book.get()));
        } else {
            System.out.println("Record with id=" + id + " not found.");
        }
    }

    @ShellMethod(value = "Add new book record.", key = {"add", "create"})
    public void add(@ShellOption(help = "The book's title") String title,
                    @ShellOption(help = "The book's author") String author,
                    @ShellOption(help = "The book's genre") String genre) {
        long id = service.create(title, author, genre).getID();
        System.out.println("New record has been added, id = " + id + ".");
    }

    @ShellMethod(value = "Delete book record.", key = {"del", "delete"})
    public void delete(@ShellOption(help = "The book ID") long id) {
        if (service.delete(id)) {
            System.out.println("Record with id=" + id + " has been deleted.");
        } else {
            System.out.println("Record with id=" + id + " has not been deleted!");
        }
    }
}
