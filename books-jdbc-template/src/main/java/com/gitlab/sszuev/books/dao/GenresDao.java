package com.gitlab.sszuev.books.dao;

import com.gitlab.sszuev.books.domain.Genre;

/**
 * A DAO that provides CRUD operations for {@link Genre}s instances.
 * <p>
 * Created by @ssz on 20.12.2020.
 */
public interface GenresDao extends CRUDBaseDao<Genre>, ByNameBaseDao<Genre> {
}
