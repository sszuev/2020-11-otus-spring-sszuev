package com.gitlab.sszuev.books.dao.impl;

import com.gitlab.sszuev.books.dao.AuthorsDao;
import com.gitlab.sszuev.books.dao.BooksDao;
import com.gitlab.sszuev.books.dao.GenresDao;
import com.gitlab.sszuev.books.domain.Author;
import com.gitlab.sszuev.books.domain.Book;
import com.gitlab.sszuev.books.domain.Genre;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * Created by @ssz on 20.12.2020.
 */
@Repository
public class BooksDaoJdbcImpl implements BooksDao {
    private static final RowMapper<Book> BOOK_ROW_MAPPER = new BookMapper();

    private final AuthorsDao authorsDao;
    private final GenresDao genresDao;
    private final NamedParameterJdbcOperations jdbc;

    public BooksDaoJdbcImpl(AuthorsDao authorsDao, GenresDao genresDao, NamedParameterJdbcOperations jdbc) {
        this.authorsDao = Objects.requireNonNull(authorsDao);
        this.genresDao = Objects.requireNonNull(genresDao);
        this.jdbc = Objects.requireNonNull(jdbc);
    }

    @Override
    public Optional<Book> findById(long id) {
        // although it doesn't matter here, use INNER JOIN to minimize selects invocation count
        return DAOUtils.select(jdbc, "SELECT b.id, b.title, b.author_id, a.name author_name, b.genre_id, g.name genre_name " +
                        "FROM books b JOIN authors a ON b.author_id = a.id JOIN genres g ON b.genre_id = g.id " +
                        "WHERE b.id = :id", Map.of("id", id), BOOK_ROW_MAPPER);
    }

    @Override
    public Stream<Book> listAll() {
        // use INNER JOIN to avoid "n + 1 problem" (schema allows to use joins)
        return jdbc.query("SELECT b.id, b.title, b.author_id, a.name author_name, b.genre_id, g.name genre_name " +
                "FROM books b JOIN authors a ON b.author_id = a.id JOIN genres g ON b.genre_id = g.id",
                BOOK_ROW_MAPPER).stream();
    }

    @Transactional
    @Override
    public Book save(Book e) {
        String title = e.getTitle();
        Author author = authorsDao.save(e.getAuthor());
        Genre genre = genresDao.save(e.getGenre());
        Long id = e.getID();
        if (id == null) {
            id = DAOUtils.executeInsert(jdbc, "INSERT INTO books (title, author_id, genre_id) VALUES (:title, :author, :genre)",
                    Map.of("title", title, "author", author.getID(), "genre", genre.getID()));
            return new Book(id, title, author, genre);
        }
        Map<String, Object> data = Map.of("id", id, "title", title, "author", author.getID(), "genre", genre.getID());
        String query = "UPDATE books SET title = :title, author_id = :author, genre_id = :genre WHERE id = :id";
        if (jdbc.update(query, data) != 1) {
            query = "INSERT INTO books (id, title, author_id, genre_id) VALUES (:id, :title, :author, :genre)";
            if (jdbc.update(query, data) != 1) {
                throw new IllegalStateException("Unable to update books-record with id = " + id);
            }
        }
        return new Book(id, title, author, genre);
    }

    @Override
    public boolean deleteById(long id) {
        return jdbc.update("DELETE FROM books WHERE id = :id", Map.of("id", id)) == 1;
    }

    @Transactional
    @Override
    public boolean delete(Book entity) {
        long id = entity.getID();
        long authorId = Objects.requireNonNull(entity.getAuthor().getID());
        long genreId = Objects.requireNonNull(entity.getGenre().getID());
        try {
            genresDao.deleteById(genreId);
        } catch (DataIntegrityViolationException ignore) {
        }
        try {
            authorsDao.deleteById(authorId);
        } catch (DataIntegrityViolationException ignore) {
        }
        return deleteById(id);
    }

    private static class BookMapper implements RowMapper<Book> {

        @Override
        public Book mapRow(ResultSet rs, int i) throws SQLException {
            long id = rs.getLong("id");
            String title = rs.getString("title");
            long authorId = rs.getLong("author_id");
            long genreId = rs.getLong("genre_id");
            String genreName = rs.getString("genre_name");
            String authorName = rs.getString("author_name");
            return new Book(id, title,
                    AuthorsDaoJdbcImpl.FACTORY.apply(authorId, authorName),
                    GenresDaoJdbcImpl.FACTORY.apply(genreId, genreName));
        }
    }
}
