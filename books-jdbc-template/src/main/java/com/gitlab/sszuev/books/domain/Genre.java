package com.gitlab.sszuev.books.domain;

/**
 * A domain entity that describes a genre item.
 * <p>
 * Created by @ssz on 19.12.2020.
 */
public final class Genre extends SimpleEntity implements HasID, HasName {
    public Genre(Long id, String name) {
        super(id, name);
    }
}
