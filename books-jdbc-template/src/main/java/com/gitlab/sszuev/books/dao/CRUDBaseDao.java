package com.gitlab.sszuev.books.dao;

import com.gitlab.sszuev.books.domain.HasID;

import java.util.Optional;
import java.util.stream.Stream;

/**
 * A package-local "abstract DAO", just to simplify the code.
 * Contains CRUD methods to operate entities.
 * <p>
 * Notice:
 * Although there is a recommendation in the task description "Do <b>NOT</b> create an AbstractDAO",
 * I decided not to follow that advice because I really don't see a big problem here.
 * Anyway, this interface can be removed at any time If there are good arguments appeared.
 * <p>
 * Created by @ssz on 23.12.2020.
 *
 * @param <E> - entity with {@code long} id
 * @see ByNameBaseDao
 * @see HasID
 */
interface CRUDBaseDao<E extends HasID> {

    /**
     * Retrieves an entity by its id.
     *
     * @param id {@code long} - an entity id
     * @return a {@code Optional} around of {@link E}, possibly empty
     */
    Optional<E> findById(long id);

    /**
     * Lists all entities from the underlying storage.
     *
     * @return a {@code Stream} of {@link E}s
     */
    Stream<E> listAll();

    /**
     * Saves a given entity into the underlying storage.
     * Use the returned instance for further operations
     * as the save operation might have changed the entity instance completely.
     * <p>
     * Impl notes:
     * Operation {@code insert} is performed for a new entity, for an existing entity there is operation  {@code update}.
     * An entity is always considered as a new if its {@link HasID#getID() id} is {@code null},
     * but a new entity may still have nonnull id.
     *
     * @param entity {@link E}, never {@code null}
     * @return the saved {@link E}, will never be {@literal null}.
     */
    E save(E entity);

    /**
     * Deletes the entity with the given id.
     * Returns {@code true} if the entity has been deleted.
     *
     * @param id {@code long} - an entity id
     * @return {@code boolean}
     */
    boolean deleteById(long id);

    /**
     * Returns entity by its id.
     *
     * @param id {@code long} - an entity id
     * @return {@link E}, never {@code null}
     * @throws IllegalStateException in case no entity was found
     */
    default E getById(long id) {
        return findById(id).orElseThrow(() -> new IllegalStateException("Can't find entity with Id " + id));
    }
}
