package com.gitlab.sszuev.books.domain;

import java.util.Objects;

/**
 * A domain entity that describes a book item.
 * Right now a book has <b>one</b> {@link Author author} and <b>one</b> {@link Genre genre}.
 * <p>
 * Created by @ssz on 19.12.2020.
 */
public final class Book implements HasID {
    private final Long id;
    private final String title;
    private final Author author;
    private final Genre genre;

    public Book(Long id, String title, Author author, Genre genre) {
        this.id = id;
        this.title = Objects.requireNonNull(title);
        this.author = Objects.requireNonNull(author);
        this.genre = Objects.requireNonNull(genre);
    }

    @Override
    public Long getID() {
        return id;
    }

    /**
     * Returns the book's title.
     *
     * @return {@code String}
     */
    public String getTitle() {
        return title;
    }

    /**
     * Returns the book's author.
     *
     * @return {@link Author}
     */
    public Author getAuthor() {
        return author;
    }

    /**
     * Returns the genre of the book.
     *
     * @return {@link Genre}
     */
    public Genre getGenre() {
        return genre;
    }
}
