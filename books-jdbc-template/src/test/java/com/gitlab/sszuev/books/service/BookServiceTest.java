package com.gitlab.sszuev.books.service;

import com.gitlab.sszuev.books.dao.AuthorsDao;
import com.gitlab.sszuev.books.dao.BooksDao;
import com.gitlab.sszuev.books.dao.GenresDao;
import com.gitlab.sszuev.books.domain.Author;
import com.gitlab.sszuev.books.domain.Book;
import com.gitlab.sszuev.books.domain.Genre;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;
import java.util.stream.Stream;

/**
 * Created by @ssz on 21.12.2020.
 */
@SpringBootTest
public class BookServiceTest {
    @Autowired
    private BooksService service;
    @MockBean
    private BooksDao booksRepo;
    @MockBean
    private AuthorsDao authorsRepo;
    @MockBean
    private GenresDao genresDao;

    @Test
    public void testFind() {
        long id = 42;
        String title = "test-title-1";
        String author = "test-author-1";
        String genre = "test-genre-1";
        Mockito.when(booksRepo.findById(Mockito.eq(id))).thenReturn(Optional.of(newBook(id, title, author, genre)));

        Assertions.assertTrue(service.find(2).isEmpty());
        Assertions.assertEquals(title, service.find(id).orElseThrow(AssertionError::new).getTitle());
    }

    @Test
    public void testList() {
        long id = 42;
        String title = "test-title-2";
        String author = "test-author-2";
        String genre = "test-genre-2";
        Mockito.when(booksRepo.listAll()).thenAnswer(x -> Stream.of(newBook(id, title, author, genre)));

        Assertions.assertEquals(1, service.list().count());
        Book actual = service.list().findFirst().orElseThrow(AssertionError::new);
        Assertions.assertEquals(title, actual.getTitle());
    }

    @Test
    public void testCreate() {
        String title = "test-title-3";
        String author = "test-author-3";
        String genre = "test-genre-3";
        Mockito.when(booksRepo.save(Mockito.any())).thenReturn(newBook(null, title, author, genre));

        Book actual = service.create(title, author, genre);
        Assertions.assertNotNull(actual);
        Assertions.assertEquals(title, actual.getTitle());

        Exception ex1 = new TestEx();
        Exception ex2 = new TestEx();

        Mockito.when(authorsRepo.listByName(Mockito.eq("X"))).thenThrow(ex1);
        Mockito.when(genresDao.listByName(Mockito.eq("Y"))).thenThrow(ex2);
        Assertions.assertSame(ex1, Assertions.assertThrows(TestEx.class, () -> service.create(title, "X", genre)));
        Assertions.assertSame(ex2, Assertions.assertThrows(TestEx.class, () -> service.create(title, author, "Y")));
    }

    @Test
    public void testDelete() {
        Book b1 = newBook(42L, "test-title-4-1", "test-author-4-1", "test-genre-4-1");
        Book b2 = newBook(2L, "test-title-4-2", "test-author-4-2", "test-genre-4-2");
        Mockito.when(booksRepo.delete(Mockito.eq(b1))).thenReturn(true);
        Mockito.when(booksRepo.delete(Mockito.eq(b2))).thenReturn(false);
        Mockito.when(booksRepo.findById(Mockito.eq(b1.getID()))).thenReturn(Optional.of(b1));
        Mockito.when(booksRepo.findById(Mockito.eq(b2.getID()))).thenReturn(Optional.of(b2));

        Assertions.assertFalse(service.delete(2));
        Assertions.assertTrue(service.delete(b1.getID()));
    }

    private static Book newBook(Long id, String title, String author, String genre) {
        return newBook(id, title, newAuthor(author), newGenre(genre));
    }

    private static Book newBook(Long id, String title, Author author, Genre genre) {
        return new Book(id, title, author, genre);
    }

    private static Author newAuthor(String author) {
        return new Author(null, author);
    }

    private static Genre newGenre(String genre) {
        return new Genre(null, genre);
    }

    private static class TestEx extends RuntimeException {
    }
}
