package com.gitlab.sszuev.books.dao;

import com.gitlab.sszuev.books.dao.impl.AuthorsDaoJdbcImpl;
import com.gitlab.sszuev.books.domain.Author;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.context.annotation.Import;

import java.util.stream.Collectors;

/**
 * To test {@link AuthorsDao}.
 * Created by @ssz on 20.12.2020.
 */
@JdbcTest
@Import(AuthorsDaoJdbcImpl.class)
public class AuthorsDaoTest {

    @Autowired
    private AuthorsDao authorsDAO;

    @Test
    public void testGetByIdSuccess() {
        long id = 2;
        Author x = authorsDAO.findById(id).orElseThrow(AssertionError::new);
        Assertions.assertEquals(id, x.getID());
        Assertions.assertEquals("A1", x.getName());
    }

    @Test
    public void testGetByIdFail() {
        Assertions.assertTrue(authorsDAO.findById(42).isEmpty());
    }

    @Test
    public void testDeleteSuccess() {
        Assertions.assertTrue(authorsDAO.deleteById(3));
    }

    @Test
    public void testDeleteFail() {
        Assertions.assertFalse(authorsDAO.deleteById(42));
    }

    @Test
    public void testListAll() {
        Assertions.assertEquals(2, authorsDAO.listAll().count());
        authorsDAO.listAll().collect(Collectors.toList()).forEach(author -> {
            Assertions.assertNotNull(author);
            Assertions.assertNotNull(author.getID());
        });
    }

    @Test
    public void testListByName() {
        Assertions.assertEquals(1, authorsDAO.listByName("A1").count());
        Assertions.assertEquals(1, authorsDAO.listByName("A2").count());
        Assertions.assertEquals(0, authorsDAO.listByName("A3").count());
        Author author = authorsDAO.listByName("A1").findFirst().orElseThrow(AssertionError::new);
        Assertions.assertEquals(2, author.getID());
    }

    @Test
    public void testSaveExisting() {
        long id = 2;
        String name = "XXXX";
        Author given = new Author(id, name);
        Author actual = authorsDAO.save(given);
        Assertions.assertNotNull(actual);
        Assertions.assertEquals(id, actual.getID());
        Assertions.assertEquals(name, actual.getName());
    }

    @Test
    public void testSaveNewWithNoId() {
        String name = "YYYY";
        Author given = new Author(null, name);
        Author actual = authorsDAO.save(given);
        Assertions.assertNotNull(actual);
        Assertions.assertNotNull(actual.getID());
        Assertions.assertEquals(name, actual.getName());
    }

    @Test
    public void testSaveNewWithFixedId() {
        String name = "ZZZZ";
        Author given = new Author(42L, name);
        Author actual = authorsDAO.save(given);
        Assertions.assertNotNull(actual);
        Assertions.assertNotNull(actual.getID());
        Assertions.assertEquals(name, actual.getName());
    }
}
