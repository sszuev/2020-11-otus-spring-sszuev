package com.gitlab.sszuev.books.dao;

import com.gitlab.sszuev.books.dao.impl.GenresDaoJdbcImpl;
import com.gitlab.sszuev.books.domain.Genre;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.context.annotation.Import;
import org.springframework.dao.DataIntegrityViolationException;

import java.util.stream.Collectors;

/**
 * To test {@link GenresDao}.
 * Created by @ssz on 20.12.2020.
 */
@JdbcTest
@Import(GenresDaoJdbcImpl.class)
public class GenresDaoTest {
    @Autowired
    private GenresDao genresDAO;

    @Test
    public void testGetById() {
        int id = 3;
        Genre x = genresDAO.getById(id);
        Assertions.assertNotNull(x);
        Assertions.assertEquals(id, x.getID());
        Assertions.assertEquals("G1", x.getName());

        Assertions.assertTrue(genresDAO.findById(42).isEmpty());
    }

    @Test
    public void testDelete() {
        Assertions.assertThrows(DataIntegrityViolationException.class, () -> genresDAO.deleteById(3));
        Assertions.assertTrue(genresDAO.deleteById(4));
        Assertions.assertFalse(genresDAO.deleteById(42));
    }

    @Test
    public void testSave() {
        Genre g = genresDAO.save(new Genre(null, "XX"));
        Assertions.assertNotNull(g.getID());
        Assertions.assertEquals("XX", g.getName());
    }

    @Test
    public void testListAll() {
        Assertions.assertEquals(2, genresDAO.listAll().count());
        genresDAO.listAll().collect(Collectors.toList()).forEach(genre -> {
            Assertions.assertNotNull(genre);
            Assertions.assertNotNull(genre.getID());
        });
    }

    @Test
    public void testListByName() {
        Assertions.assertEquals(1, genresDAO.listByName("G1").count());
        Assertions.assertEquals(1, genresDAO.listByName("G2").count());
        Assertions.assertEquals(0, genresDAO.listByName("A3").count());
    }
}
