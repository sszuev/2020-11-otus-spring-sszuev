package com.gitlab.sszuev.books.dao;

import com.gitlab.sszuev.books.dao.impl.AuthorsDaoJdbcImpl;
import com.gitlab.sszuev.books.dao.impl.BooksDaoJdbcImpl;
import com.gitlab.sszuev.books.dao.impl.GenresDaoJdbcImpl;
import com.gitlab.sszuev.books.domain.Author;
import com.gitlab.sszuev.books.domain.Book;
import com.gitlab.sszuev.books.domain.Genre;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.context.annotation.Import;

/**
 * To test {@link BooksDao}.
 * Created by @ssz on 20.12.2020.
 */
@JdbcTest
@Import({BooksDaoJdbcImpl.class, AuthorsDaoJdbcImpl.class, GenresDaoJdbcImpl.class})
public class BooksDaoTest {
    @Autowired
    private BooksDao booksDao;
    @Autowired
    private AuthorsDao authorsDao;
    @Autowired
    private GenresDao genresDao;

    @Test
    public void testGetById() {
        int id = 4;
        Book x = booksDao.getById(id);
        Assertions.assertNotNull(x);
        Assertions.assertEquals(id, x.getID());
        Assertions.assertEquals("T1", x.getTitle());
        Assertions.assertEquals(2, x.getAuthor().getID());
        Assertions.assertEquals(3, x.getGenre().getID());

        Assertions.assertTrue(booksDao.findById(42).isEmpty());
    }

    @Test
    public void testUpdateAndCreateGenre() {
        long bookId = 4;
        long authorId = 2;
        long genreID = 3;
        String title = "Changed title 1";
        String author = "Changed author 1";
        String genre = "New style 1";

        Book e = booksDao.save(new Book(bookId, title, new Author(authorId, author), new Genre(null, genre)));

        Assertions.assertEquals(bookId, e.getID());
        Assertions.assertEquals(title, e.getTitle());
        Assertions.assertEquals(authorId, e.getAuthor().getID());
        Assertions.assertNotNull(e.getGenre().getID());
        Assertions.assertNotEquals(genreID, e.getGenre().getID());
        Assertions.assertEquals(genre, e.getGenre().getName());
    }

    @Test
    public void testUpdateAndCreateAuthor() {
        long bookId = 4;
        long authorId = 2;
        long genreID = 3;
        String title = "Changed title 2";
        String author = "New author 2";
        String genre = "New style 2";

        Book e = booksDao.save(new Book(bookId, title, new Author(null, author), new Genre(genreID, genre)));

        Assertions.assertEquals(bookId, e.getID());
        Assertions.assertEquals(title, e.getTitle());
        Assertions.assertNotNull(e.getAuthor().getID());
        Assertions.assertNotEquals(authorId, e.getAuthor().getID());
        Assertions.assertEquals(genreID, e.getGenre().getID());
        Assertions.assertEquals(genre, e.getGenre().getName());
        Assertions.assertEquals(author, e.getAuthor().getName());
    }

    @Test
    public void testCreateFreshBook() {
        long bookId = 4;
        long authorId = 2;
        long genreID = 3;
        String title = "New title 3";
        String author = "Changed author 3";
        String genre = "Changed style 3";

        Book e = booksDao.save(new Book(null, title, new Author(authorId, author), new Genre(genreID, genre)));

        Assertions.assertNotNull(e.getID());
        Assertions.assertNotEquals(bookId, e.getID());
        Assertions.assertEquals(title, e.getTitle());
        Assertions.assertEquals(authorId, e.getAuthor().getID());
        Assertions.assertEquals(genreID, e.getGenre().getID());
        Assertions.assertEquals(genre, e.getGenre().getName());
    }

    @Test
    public void testCreateBookWithNewId() {
        long bookId = 42;
        long authorId = 2;
        long genreID = 3;
        String title = "New title 4";
        String author = "Changed author 4";
        String genre = "Changed style 4";

        Book e = booksDao.save(new Book(bookId, title, new Author(authorId, author), new Genre(null, genre)));

        Assertions.assertNotNull(e.getID());
        Assertions.assertEquals(bookId, e.getID());
        Assertions.assertEquals(title, e.getTitle());
        Assertions.assertEquals(authorId, e.getAuthor().getID());
        Assertions.assertNotNull(e.getGenre().getID());
        Assertions.assertNotEquals(genreID, e.getGenre().getID());
        Assertions.assertEquals(genre, e.getGenre().getName());
    }


    @Test
    public void testDeleteOnlyBook() {
        Assertions.assertTrue(authorsDao.findById(2).isPresent());
        Assertions.assertTrue(genresDao.findById(3).isPresent());
        Assertions.assertTrue(booksDao.deleteById(4));
        Assertions.assertTrue(authorsDao.findById(2).isPresent());
        Assertions.assertTrue(genresDao.findById(3).isPresent());
    }

    @Test
    public void testClear() {
        Author a = authorsDao.findById(2).orElseThrow(AssertionError::new);
        Genre g = genresDao.findById(3).orElseThrow(AssertionError::new);
        Assertions.assertTrue(booksDao.delete(new Book(4L, "title", a, g)));
        Assertions.assertTrue(authorsDao.findById(2).isPresent());
        Assertions.assertTrue(genresDao.findById(3).isPresent());
    }

    @Test
    public void testListAll() {
        Assertions.assertEquals(1, booksDao.listAll().count());
        Book book = booksDao.listAll().findFirst().orElseThrow(AssertionError::new);
        Assertions.assertEquals(4, book.getID());
        Assertions.assertEquals("T1", book.getTitle());
        Assertions.assertEquals(2, book.getAuthor().getID());
        Assertions.assertEquals(3, book.getGenre().getID());
    }

}
