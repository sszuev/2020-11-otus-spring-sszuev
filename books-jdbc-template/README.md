#### This is a home-work 'DAO, Spring-JDBC & Spring-Shell'

A simple console application to access database that stores information about books.

###### If you are not from [otus](https://otus.ru)-team please ignore it: there is nothing interesting here.

##### Requirements:

- Git
- Java **11**
- Maven **3+**

##### Build and run:

```
$ git clone git@gitlab.com:sszuev/2020-11-otus-spring-sszuev.git
$ cd 2020-11-otus-spring-sszuev/books
$ mvn clean package
$ java -jar target/books-library.jar
```
