package com.gitlab.sszuev.questions.impl.dao;

import com.gitlab.sszuev.questions.dao.QuestionsDao;
import com.gitlab.sszuev.questions.domain.Answer;
import com.gitlab.sszuev.questions.domain.Question;
import com.gitlab.sszuev.questions.impl.domain.QuestionImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

/**
 * Simple {@link QuestionsDao} implementation that holds all data in memory.
 * <p>
 * Created by @ssz on 26.11.2020.
 */
@Repository
public class CSVFileResourceQuestionsDaoImpl implements QuestionsDao {
    private final Map<Long, QuestionImpl> data;

    @Autowired
    public CSVFileResourceQuestionsDaoImpl(@Value("${db.file.path}") String resource) {
        this(CSVResourceUtils.load(Objects.requireNonNull(resource)));
    }

    protected CSVFileResourceQuestionsDaoImpl(Map<Long, QuestionImpl> data) {
        this.data = Objects.requireNonNull(data);
    }

    @Override
    public Optional<Question> findQuestionById(Long id) {
        return Optional.ofNullable(id).map(data::get);
    }

    @Override
    public Stream<Question> findAllQuestions() {
        return data.values().stream().map(Function.identity());
    }

    @Override
    public Stream<Answer> findAllAnswersByQuestionId(Long id) {
        return Optional.ofNullable(id).map(data::get)
                .map(QuestionImpl::answers)
                .orElse(Stream.empty());
    }
}
