package com.gitlab.sszuev.questions.domain;

/**
 * A domain entity that describes a student testing answer item.
 * <p>
 * Created by @ssz on 26.11.2020.
 */
public interface Answer extends HasMessage, HasID {
}
