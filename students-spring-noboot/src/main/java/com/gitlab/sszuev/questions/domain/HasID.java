package com.gitlab.sszuev.questions.domain;

/**
 * Created by @ssz on 26.11.2020.
 */
interface HasID {

    /**
     * Returns an identifier of entity.
     *
     * @return {@code Long}
     */
    Long getId();
}
