package com.gitlab.sszuev.questions.domain;

/**
 * Created by @ssz on 26.11.2020.
 */
interface HasMessage {

    /**
     * Returns a text message which this object encapsulates.
     *
     * @return {@code String}
     */
    String getMessage();
}
