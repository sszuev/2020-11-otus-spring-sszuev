package com.gitlab.sszuev.questions;

import com.gitlab.sszuev.questions.service.QuizException;
import com.gitlab.sszuev.questions.service.QuizService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * Created by @ssz on 28.11.2020.
 */
@Configuration
@ComponentScan
@PropertySource("classpath:app.properties")
public class App {

    public static void main(String... args) {
        try (AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(App.class)) {
            context.getBean(QuizService.class).process();
        } catch (QuizException e) {
            System.err.println(e.getMessage());
        }
    }

}
