package com.gitlab.sszuev.questions.impl.service;

import com.gitlab.sszuev.questions.domain.Question;
import com.gitlab.sszuev.questions.service.QuestionService;
import com.gitlab.sszuev.questions.service.QuizException;
import com.gitlab.sszuev.questions.service.QuizService;
import com.gitlab.sszuev.questions.service.UserController;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Iterator;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Created by @ssz on 03.12.2020.
 */
@Service
public class QuizServiceImpl implements QuizService {
    private final QuestionService questionService;
    private final UserController userController;
    private final long timeoutInSeconds;

    public QuizServiceImpl(QuestionService questionService,
                           UserController userController,
                           @Value("${message.timeout.sec}") long timeoutInSeconds) {
        this.questionService = Objects.requireNonNull(questionService);
        this.userController = Objects.requireNonNull(userController);
        this.timeoutInSeconds = timeoutInSeconds;
    }

    @Override
    public long process() throws QuizException {
        try {
            return processQuestions();
        } catch (IOException | TimeoutException e) {
            throw new QuizException(e.getMessage(), e);
        }
    }

    public long processQuestions() throws IOException, TimeoutException {
        userController.say("Let's go.");
        userController.say("What is your name?");
        String name = userController.receive(timeoutInSeconds, TimeUnit.SECONDS);
        long score = 0;
        long count = 0;
        Iterator<Question> questions = questionService.questions().iterator();
        while (questions.hasNext()) {
            Question q = questions.next();
            userController.say(String.format("Question N%d: %s", ++count, q.getMessage()));
            String res = userController.receive(timeoutInSeconds, TimeUnit.SECONDS);
            score += questionService.evaluate(q, res);
        }
        userController.say(String.format("Dear %s! Your score is %d (out of %d possible).", name, score, count));
        userController.say("fin.");
        return score;
    }
}
