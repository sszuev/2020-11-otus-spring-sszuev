package com.gitlab.sszuev.questions.impl.domain;

import com.gitlab.sszuev.questions.domain.Answer;
import com.gitlab.sszuev.questions.domain.Question;

import java.util.Collection;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Stream;

/**
 * Created by @ssz on 26.11.2020.
 */
public class QuestionImpl implements Question {
    private final long id;
    private final String txt;
    private final Collection<AnswerImpl> answers;

    public QuestionImpl(long id, String txt, Collection<AnswerImpl> answers) {
        this.id = id;
        this.txt = Objects.requireNonNull(txt);
        this.answers = Objects.requireNonNull(answers);
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public String getMessage() {
        return txt;
    }

    public Stream<AnswerImpl> listAnswers() {
        return answers.stream();
    }

    public Stream<Answer> answers() {
        return listAnswers().map(Function.identity());
    }

    @Override
    public String toString() {
        return String.format("Question{id=%d, txt='%s'}", id, txt);
    }
}
