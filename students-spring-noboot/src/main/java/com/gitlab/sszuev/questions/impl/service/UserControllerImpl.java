package com.gitlab.sszuev.questions.impl.service;

import com.gitlab.sszuev.questions.service.UserController;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Implementation that is based on standard {@link System#in input} and {@link System#out output}.
 * <p>
 * Created by @ssz on 03.12.2020.
 */
@Service("messageConsoleService")
public class UserControllerImpl implements UserController {
    @Override
    public void say(String message) {
        System.out.println(Objects.requireNonNull(message));
    }

    @Override
    public String receive(long timeout, TimeUnit unit) throws IOException, TimeoutException {
        return read(System.in, timeout, unit);
    }

    @Override
    public String receive() throws IOException {
        try {
            return receive(Long.MAX_VALUE, TimeUnit.DAYS);
        } catch (TimeoutException e) {
            throw new IllegalStateException("Unexpected failure", e);
        }
    }

    public String read(InputStream in, long timeout, TimeUnit unit) throws IOException, TimeoutException {
        if (timeout <= 0) {
            throw new IllegalArgumentException("Non-positive timeout " + timeout);
        }
        long begin = System.currentTimeMillis();
        long limit = unit.toMillis(timeout);
        BufferedReader br = new BufferedReader(new InputStreamReader(in, StandardCharsets.UTF_8));
        while (System.currentTimeMillis() - begin < limit) {
            if (br.ready()) {
                return br.readLine();
            }
        }
        throw new TimeoutException("Waiting limit (" + limit + " ms) is exceeded.");
    }
}
