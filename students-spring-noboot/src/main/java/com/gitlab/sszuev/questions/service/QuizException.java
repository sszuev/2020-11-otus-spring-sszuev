package com.gitlab.sszuev.questions.service;

/**
 * Going to be business exception.
 * <p>
 * Created by @ssz on 03.12.2020.
 */
public class QuizException extends RuntimeException {
    public QuizException(String message, Throwable cause) {
        super(message, cause);
    }
}
