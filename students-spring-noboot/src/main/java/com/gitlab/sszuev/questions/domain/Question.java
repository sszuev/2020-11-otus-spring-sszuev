package com.gitlab.sszuev.questions.domain;

/**
 * A domain entity that describes a student testing question item.
 * Each question has text message, identifier and attached correct answers (a student must choose one of them).
 * <p>
 * Created by @ssz on 26.11.2020.
 */
public interface Question extends HasMessage, HasID {
}
