package com.gitlab.sszuev.questions.impl.service;

import com.gitlab.sszuev.questions.dao.QuestionsDao;
import com.gitlab.sszuev.questions.domain.Answer;
import com.gitlab.sszuev.questions.domain.Question;
import com.gitlab.sszuev.questions.service.QuestionService;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.stream.Stream;

/**
 * Created by @ssz on 28.11.2020.
 */
@Service
public class QuestionServiceImpl implements QuestionService {

    private final QuestionsDao repo;

    public QuestionServiceImpl(QuestionsDao repo) {
        this.repo = Objects.requireNonNull(repo);
    }

    @Override
    public Stream<Question> questions() {
        return repo.findAllQuestions();
    }

    @Override
    public Stream<Answer> answers(Question q) {
        return repo.findAllAnswersByQuestionId(q.getId());
    }

    @Override
    public Question getQuestion(long id) {
        return repo.findQuestionById(id).orElse(null);
    }
}
