package com.gitlab.sszuev.questions.service;

/**
 * System core.
 * Performs student testing.
 * <p>
 * Created by @ssz on 03.12.2020.
 */
public interface QuizService {

    /**
     * Performs the student testing.
     *
     * @return {@code long}, the total score
     * @throws QuizException in case of any error
     */
    long process() throws QuizException;
}
