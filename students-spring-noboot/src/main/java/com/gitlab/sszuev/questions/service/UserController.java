package com.gitlab.sszuev.questions.service;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * A service that responsible for sending and receiving text messages from some UI.
 * <p>
 * Created by @ssz on 03.12.2020.
 */
public interface UserController {

    /**
     * Sends the message.
     *
     * @param message {@code String}, not {@code null}
     * @throws IOException if an I/O error occurs
     */
    void say(String message) throws IOException;

    /**
     * Receives the message.
     *
     * @return {@code String}, the received message
     * @throws IOException if an I/O error occurs
     */
    String receive() throws IOException;

    /**
     * Waits if necessary for at most the given time for the receiving message.
     *
     * @param timeout the maximum time to wait, positive {@code long}
     * @param unit    {@link TimeUnit}, the time unit of the timeout argument, not {@code null}
     * @return {@code String}, the received message
     * @throws IOException      if an I/O error occurs
     * @throws TimeoutException if the wait timed out
     */
    String receive(long timeout, TimeUnit unit) throws IOException, TimeoutException;

}
