package com.gitlab.sszuev.questions.impl.domain;

import com.gitlab.sszuev.questions.domain.Answer;

import java.util.Objects;

/**
 * Created by @ssz on 28.11.2020.
 */
public class AnswerImpl implements Answer {
    private final long id;
    private final String txt;

    public AnswerImpl(long id, String txt) {
        this.id = id;
        this.txt = Objects.requireNonNull(txt);
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public String getMessage() {
        return txt;
    }

    @Override
    public String toString() {
        return String.format("Answer{id=%d, txt='%s'}", id, txt);
    }
}
