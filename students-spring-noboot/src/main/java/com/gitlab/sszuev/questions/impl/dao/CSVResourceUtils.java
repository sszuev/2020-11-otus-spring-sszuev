package com.gitlab.sszuev.questions.impl.dao;

import com.gitlab.sszuev.questions.domain.Question;
import com.gitlab.sszuev.questions.impl.domain.AnswerImpl;
import com.gitlab.sszuev.questions.impl.domain.QuestionImpl;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * A helper to load data from CSV resource.
 * Its access is package local since it seems right now this functionality has no big future.
 * <p>
 * Created by @ssz on 26.11.2020.
 */
class CSVResourceUtils {

    /**
     * Loads data from a csv-file using default settings ("{@code ,}" as a separator, "{@code "}" as a escape quote).
     *
     * @param file - a path to resource file
     * @return a {@code Map}
     * @throws RuntimeException in case of any error
     */
    public static Map<Long, QuestionImpl> load(String file) {
        try (Reader r = newBufferedReader(file);
             CSVParser p = CSVParser.parse(r, CSVFormat.DEFAULT)) {
            return StreamSupport.stream(p.spliterator(), false)
                    .map(CSVResourceUtils::parseRecord)
                    .collect(Collectors.toMap(Question::getId, Function.identity()));
        } catch (Exception e) {
            throw new IllegalStateException("Can't parse file " + file, e);
        }
    }

    /**
     * Opens a buffered {@code Reader} for the given class path resource.
     *
     * @param file - a path to resource file
     * @return {@link Reader}
     * @throws RuntimeException in case of wrong input
     * @throws IOException      any io exception
     */
    public static Reader newBufferedReader(String file) throws IOException {
        if (!Objects.requireNonNull(file, "Null file").startsWith("/")) {
            throw new IllegalArgumentException("Wrong resource file is given: " + file);
        }
        InputStream in = new org.springframework.core.io.ClassPathResource(file, CSVResourceUtils.class).getInputStream();
        Reader r = new InputStreamReader(in, StandardCharsets.UTF_8.newDecoder());
        return new BufferedReader(r);
    }

    /**
     * A factory method to parse csv-record into an instance of {@link QuestionImpl}.
     *
     * @param record {@link CSVRecord}, not {@code null}
     * @return a fresh {@link QuestionImpl}
     */
    static QuestionImpl parseRecord(CSVRecord record) {
        try {
            String[] _answers = record.get(2).split(",\\s*");
            List<AnswerImpl> answers = new ArrayList<>(_answers.length);
            for (int i = 0; i < _answers.length; i++) {
                answers.add(new AnswerImpl(i + 1, _answers[i]));
            }
            return new QuestionImpl(Long.parseLong(record.get(0)), record.get(1), answers);
        } catch (RuntimeException e) {
            throw new IllegalStateException("Can't parse record " + record, e);
        }
    }
}
