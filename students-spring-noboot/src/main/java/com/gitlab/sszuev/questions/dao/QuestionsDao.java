package com.gitlab.sszuev.questions.dao;

import com.gitlab.sszuev.questions.domain.Answer;
import com.gitlab.sszuev.questions.domain.Question;

import java.util.Optional;
import java.util.stream.Stream;

/**
 * A DAO that provides {@link Question}s.
 * <p>
 * Created by @ssz on 26.11.2020.
 */
public interface QuestionsDao {

    /**
     * Finds a {@code Question} by the given id.
     *
     * @param id {@code Long} a question id
     * @return an {@code Optional} of {@link Question}
     */
    Optional<Question> findQuestionById(Long id);

    /**
     * Lists all question entities.
     *
     * @return a {@code Stream} over all {@link Question}s
     */
    Stream<Question> findAllQuestions();

    /**
     * Lists all answer entities for the given question id,
     *
     * @param id {@code Long} a question id
     * @return a {@code Stream} over all {@link Answer}s
     */
    Stream<Answer> findAllAnswersByQuestionId(Long id);
}
