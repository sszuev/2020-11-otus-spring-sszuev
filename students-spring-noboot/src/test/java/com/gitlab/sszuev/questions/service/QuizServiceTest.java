package com.gitlab.sszuev.questions.service;

import com.gitlab.sszuev.questions.App;
import com.gitlab.sszuev.questions.domain.Question;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.IOException;
import java.util.stream.Stream;

/**
 * Created by @ssz on 03.12.2020.
 */
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {App.class, QuizServiceTest.TestConfig.class})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class QuizServiceTest {
    @Autowired
    private QuizService service;
    @Autowired
    private QuestionService questionService;
    @Autowired
    private UserController userController;

    @Test
    public void testProcessSendMessageException() throws Exception {
        String txt = "XXX";
        Mockito.doThrow(new IOException(txt)).when(userController).say(Mockito.anyString());
        Assertions.assertEquals(txt, Assertions.assertThrows(QuizException.class, service::process).getMessage());
    }

    @Test
    public void testProcessSuccessScoring() throws Exception {
        int scorePerEvaluate = 42;
        Mockito.when(userController.receive(Mockito.anyLong(), Mockito.any())).thenReturn("name", "answer1", "answer2");

        Mockito.when(questionService.evaluate(Mockito.any(), Mockito.anyString())).thenReturn(scorePerEvaluate);

        Question q1 = Mockito.mock(Question.class);
        Mockito.when(questionService.questions()).thenReturn(Stream.of(q1));
        Assertions.assertEquals(scorePerEvaluate, service.process());
        Mockito.verify(questionService, Mockito.times(1)).evaluate(Mockito.any(), Mockito.anyString());

        Question q2 = Mockito.mock(Question.class);
        Mockito.when(questionService.questions()).thenReturn(Stream.of(q1, q2));
        Assertions.assertEquals(scorePerEvaluate * 2, service.process());
        Mockito.verify(questionService, Mockito.times(3)).evaluate(Mockito.any(), Mockito.anyString());
    }

    @Profile("test")
    @Configuration
    static class TestConfig {

        @Bean
        public QuestionService questionService() {
            return Mockito.mock(QuestionService.class);
        }

        @Bean
        public UserController userController() {
            return Mockito.mock(UserController.class);
        }
    }
}
