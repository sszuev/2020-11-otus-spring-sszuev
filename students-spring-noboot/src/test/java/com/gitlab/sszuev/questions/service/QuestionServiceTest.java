package com.gitlab.sszuev.questions.service;

import com.gitlab.sszuev.questions.App;
import com.gitlab.sszuev.questions.domain.Question;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by @ssz on 28.11.2020.
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = App.class)
@TestPropertySource("classpath:test.properties")
public class QuestionServiceTest {

    @Autowired
    private QuestionService service;

    @Test
    public void testListQuestions() {
        Assertions.assertEquals(2, service.questions().count());
    }

    @Test
    public void testListAnswers() {
        Assertions.assertEquals(6, service.questions().flatMap(x -> service.answers(x)).count());
    }

    @Test
    public void testContent() {
        Map<Long, String> actual = service.questions()
                .collect(Collectors.toMap(Question::getId, q -> q.getMessage().trim().toLowerCase()));

        Map<Long, String> expected = new HashMap<>();
        expected.put(3L, "how a u?");
        expected.put(4L, "how old a u?");
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void testGetQuestion() {
        Assertions.assertNull(service.getQuestion(1));
        Assertions.assertNull(service.getQuestion(2));
        Assertions.assertNotNull(service.getQuestion(3));
        Assertions.assertNotNull(service.getQuestion(4));
    }

    @Test
    public void testEvaluateGoodAnswer() {
        Question q = service.getQuestion(3);
        Assertions.assertNotNull(q);
        Assertions.assertEquals(1, service.evaluate(q, "y"));
        Assertions.assertEquals(1, service.evaluate(q, "yes"));
    }

    @Test
    public void testEvaluateWrongAnswer() {
        Question q = service.getQuestion(3);
        Assertions.assertNotNull(q);
        Assertions.assertEquals(0, service.evaluate(q, "not"));
        Assertions.assertEquals(0, service.evaluate(q, "yes?"));
    }
}
