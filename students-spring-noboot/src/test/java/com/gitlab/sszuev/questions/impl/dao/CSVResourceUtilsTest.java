package com.gitlab.sszuev.questions.impl.dao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;

/**
 * Created by @ssz on 26.11.2020.
 */
public class CSVResourceUtilsTest {

    @Test
    public void testLoadApplicationResource() {
        Map<Long, ?> actual = CSVResourceUtils.load("/data.csv");
        Assertions.assertEquals(2, actual.size());
        Assertions.assertEquals(new HashSet<>(Arrays.asList(3L, 4L)), actual.keySet());
    }
}
