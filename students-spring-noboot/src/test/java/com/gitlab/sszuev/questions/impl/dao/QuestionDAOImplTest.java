package com.gitlab.sszuev.questions.impl.dao;

import com.gitlab.sszuev.questions.dao.QuestionsDao;
import com.gitlab.sszuev.questions.impl.domain.AnswerImpl;
import com.gitlab.sszuev.questions.impl.domain.QuestionImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

/**
 * To test {@link CSVFileResourceQuestionsDaoImpl}.
 * Created by @ssz on 26.11.2020.
 */
public class QuestionDAOImplTest {

    @Test
    public void testLoadCSVResource() {
        Assertions.assertEquals(2, new CSVFileResourceQuestionsDaoImpl("/data.csv").findAllQuestions().count());
    }

    @Test
    public void testEmptyDAO() {
        QuestionsDao dao = new CSVFileResourceQuestionsDaoImpl(Collections.emptyMap());
        Assertions.assertEquals(0, dao.findAllQuestions().count());
        Assertions.assertTrue(dao.findQuestionById(42L).isEmpty());
        Assertions.assertTrue(dao.findQuestionById(null).isEmpty());

        Assertions.assertEquals(0, dao.findAllAnswersByQuestionId(null).count());
    }

    @Test
    public void testSomeDAOWithMockQuestions() {
        Map<Long, QuestionImpl> data = new HashMap<>();
        data.put(1L, Mockito.mock(QuestionImpl.class));
        data.put(2L, Mockito.mock(QuestionImpl.class));
        Mockito.when(data.get(2L).answers()).thenReturn(Stream.of(Mockito.mock(AnswerImpl.class)));

        QuestionsDao dao = new CSVFileResourceQuestionsDaoImpl(data);
        Assertions.assertEquals(2, dao.findAllQuestions().count());
        Assertions.assertFalse(dao.findQuestionById(1L).isEmpty());
        Assertions.assertFalse(dao.findQuestionById(2L).isEmpty());
        Assertions.assertTrue(dao.findQuestionById(3L).isEmpty());

        Assertions.assertEquals(0, dao.findAllAnswersByQuestionId(1L).count());
        Assertions.assertEquals(1, dao.findAllAnswersByQuestionId(2L).count());
        Assertions.assertEquals(0, dao.findAllAnswersByQuestionId(3L).count());
    }
}
