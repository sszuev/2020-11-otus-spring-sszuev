package com.gitlab.sszuev.questions.impl.service;

import com.gitlab.sszuev.questions.App;
import com.gitlab.sszuev.questions.service.UserController;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * To test {@link UserControllerImpl}.
 * Placed in impl tests since we know that it is all about console message service only.
 * <p>
 * Created by @ssz on 03.12.2020.
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = App.class)
public class UserControllerImplTest {
    private static final PrintStream SYS_OUT = System.out;
    private static final InputStream SYS_IN = System.in;

    @Autowired
    @Qualifier("messageConsoleService")
    private UserController service;

    @AfterEach
    void restoreSysOut() {
        System.setOut(SYS_OUT);
    }

    @AfterEach
    void restoreSysIn() {
        System.setIn(SYS_IN);
    }

    @Test
    public void testSend() throws Exception {
        String data = "The test message";

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(out, true, StandardCharsets.UTF_8));

        service.say(data);
        String actual = out.toString(StandardCharsets.UTF_8);
        String expected = data + System.lineSeparator();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void testReceiveTimeoutExceeded() {
        Assertions.assertThrows(TimeoutException.class, () -> service.receive(10, TimeUnit.MILLISECONDS));
    }

    @Test
    public void testReceive() throws Exception {
        String data = "The test message";

        InputStream in = new ByteArrayInputStream(data.getBytes());
        System.setIn(in);

        String actual = service.receive();
        Assertions.assertEquals(data, actual);
    }

    @Test
    public void testReceiveWithTimeout() throws Exception {
        String data = "The test message";

        InputStream in = new ByteArrayInputStream(data.getBytes());
        System.setIn(in);

        String actual = service.receive(1, TimeUnit.SECONDS);
        Assertions.assertEquals(data, actual);
    }

}
