package com.gitlab.sszuev.books.controller;

import com.gitlab.sszuev.books.dto.BookRecord;
import com.gitlab.sszuev.books.dto.PagedResult;
import com.gitlab.sszuev.books.service.LibraryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by @ssz on 31.01.2021.
 */
@Controller
public class LibraryController {
    private static final Logger LOGGER = LoggerFactory.getLogger(LibraryController.class);

    private final LibraryService service;

    public LibraryController(LibraryService service) {
        this.service = Objects.requireNonNull(service);
    }

    @GetMapping(value = {"/", "/books"})
    public ModelAndView listBooks(@RequestParam(value = "page", required = false) Integer page) {
        int current = toCurrentPageNumber(page);
        PagedResult<BookRecord> res = service.getPage(current - 1);
        Map<String, Object> model = Map.of(
                "books", res.listContent().map(RowBean::toRow).collect(Collectors.toList()),
                "pages", IntStream.rangeClosed(1, res.getPageCount()).boxed().collect(Collectors.toList()),
                "page", current);
        return new ModelAndView("list", model);
    }

    @PostMapping(value = "/delete")
    public ModelAndView deleteBook(@RequestParam Long id,
                                    @RequestParam(value = "page", required = false) Integer page) {
        LOGGER.info("Delete book #{}.", id);
        service.delete(id);
        return new ModelAndView("redirect:/books", Map.of("page", toCurrentPageNumber(page)));
    }

    @GetMapping("/create")
    public ModelAndView createBook() {
        return new ModelAndView("edit", Map.of("book", new RowBean()));
    }

    @GetMapping("/edit/{id}")
    public ModelAndView editBook(@PathVariable(value = "id") long id,
                                 @RequestParam(value = "page", required = false) Integer page) {
        BookRecord book = service.find(id).orElseThrow();
        return new ModelAndView("edit", Map.of("page", toCurrentPageNumber(page), "book", RowBean.toRow(book)));
    }

    @PostMapping(value = {"/save"})
    public ModelAndView saveBook(@RequestParam(value = "page", required = false) Integer page,
                                 @ModelAttribute RowBean book) {
        if (book.getId() == null) {
            LOGGER.info("Create book '{}'.", book.getTitle());
        } else {
            LOGGER.info("Edit book #{}: '{}'.", book.getId(), book.getTitle());
        }
        service.save(book.getId(), book.getTitle(), book.getGenre(), book.listAuthors().collect(Collectors.toList()));
        return new ModelAndView("redirect:/books", Map.of("page", toCurrentPageNumber(page)));
    }

    private int toCurrentPageNumber(Integer page) {
        return page == null ? 1 : page < 1 ? 1 : page;
    }
}
