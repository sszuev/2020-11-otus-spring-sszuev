package com.gitlab.sszuev.books;

import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.access.AccessDeniedHandler;

/**
 * Created by @ssz on 02.03.2021.
 */
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .exceptionHandling().accessDeniedHandler(createAccessDeniedHandler("/403"))
                .and().authorizeRequests().antMatchers("/create", "/delete/**").hasRole("ADMIN")
                .and().authorizeRequests().antMatchers("/edit/**").hasAnyRole("ADMIN", "MANAGER")
                .and().authorizeRequests().antMatchers("/**").authenticated()
                .and().logout().logoutUrl("/logout").logoutSuccessUrl("/login").permitAll()
                .and().formLogin().defaultSuccessUrl("/").permitAll();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @SuppressWarnings("SameParameterValue")
    private static AccessDeniedHandler createAccessDeniedHandler(String page) {
        return (req, res, ex) -> {
            req.setAttribute(WebAttributes.ACCESS_DENIED_403, ex);
            res.setStatus(HttpStatus.FORBIDDEN.value());
            if (HttpMethod.POST.matches(req.getMethod())) {
                res.sendRedirect(req.getRequestURI());
            } else {
                req.getRequestDispatcher(page).forward(req, res);
            }
        };
    }
}
