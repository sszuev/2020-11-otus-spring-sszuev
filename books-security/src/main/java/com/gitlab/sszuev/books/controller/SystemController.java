package com.gitlab.sszuev.books.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;
import java.util.Map;

/**
 * Created by @ssz on 06.03.2021.
 */
@Controller
public class SystemController {

    @GetMapping("/403")
    public ModelAndView accessDenied(Principal user) {
        return new ModelAndView("403", Map.of("details",
                String.format("User '%s' does not have permissions to view this page.", user.getName())));
    }
}
