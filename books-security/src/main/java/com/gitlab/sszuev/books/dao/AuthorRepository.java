package com.gitlab.sszuev.books.dao;

import com.gitlab.sszuev.books.domain.Author;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;
import java.util.stream.Stream;

/**
 * Created by @ssz on 01.02.2021.
 */
public interface AuthorRepository extends JpaRepository<Author, Long> {

    /**
     * Lists all author entities which names are in the given list.
     *
     * @param names a {@code Collection} of {@code String}-names to check, not {@code null}
     * @return a {@code Stream} of {@link Author}s
     */
    Stream<Author> streamByNameIn(Collection<String> names);
}
