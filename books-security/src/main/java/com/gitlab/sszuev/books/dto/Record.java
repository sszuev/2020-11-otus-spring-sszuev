package com.gitlab.sszuev.books.dto;

/**
 * Created by @ssz on 04.01.2021.
 */
public abstract class Record {
    final long id;

    protected Record(long id) {
        this.id = id;
    }

    public final long getId() {
        return id;
    }
}
