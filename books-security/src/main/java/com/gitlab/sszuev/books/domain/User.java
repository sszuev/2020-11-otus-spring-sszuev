package com.gitlab.sszuev.books.domain;

import javax.persistence.*;

/**
 * A domain entity that describes an user item.
 * <p>
 * Created by @ssz on 02.03.2021.
 */
@Entity
@Table(name = "users")
public class User implements HasID {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "login", nullable = false, unique = true)
    private String login;
    @Column(name = "pwd", nullable = false)
    private String password;
    @Column(name = "role")
    private Integer role;

    @Override
    public Long getID() {
        return id;
    }

    @Override
    public void setID(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getRole() {
        return role;
    }

    public void setRole(Integer role) {
        this.role = role;
    }
}
