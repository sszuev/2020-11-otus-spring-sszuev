package com.gitlab.sszuev.books.controller;

import com.gitlab.sszuev.books.dto.AuthorRecord;
import com.gitlab.sszuev.books.dto.BookRecord;

import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Mutable analogue of {@link com.gitlab.sszuev.books.dto.BookRecord}.
 * <p>
 * Created by @ssz on 03.03.2021.
 */
public class RowBean {
    private Long id;
    private String title;
    private String genre;
    private String authors;

    public static RowBean toRow(BookRecord record) {
        RowBean res = new RowBean();
        res.setId(record.getId());
        res.setTitle(record.getTitle());
        res.setGenre(record.getGenre().getName());
        res.setAuthors(record.listAuthors().map(AuthorRecord::getName).collect(Collectors.joining(", ")));
        return res;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getAuthors() {
        return authors;
    }

    public void setAuthors(String authors) {
        this.authors = authors;
    }

    public Stream<String> listAuthors() {
        return Arrays.stream(this.authors.split(",\\s*"));
    }
}
