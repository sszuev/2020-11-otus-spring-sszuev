package com.gitlab.sszuev.books.dto;

import java.util.Collection;
import java.util.Objects;
import java.util.stream.Stream;

/**
 * To hold {@link Record record}s.
 * Created by @ssz on 31.01.2021.
 */
public final class PagedResult<E extends Record> {
    private final int pages;
    private final Collection<E> content;

    protected PagedResult(int pages, Collection<E> content) {
        this.pages = pages;
        this.content = Objects.requireNonNull(content);
    }

    public Stream<E> listContent() {
        return content.stream();
    }

    public int getPageCount() {
        return pages;
    }
}
