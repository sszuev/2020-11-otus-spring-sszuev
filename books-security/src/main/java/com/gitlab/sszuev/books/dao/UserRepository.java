package com.gitlab.sszuev.books.dao;

import com.gitlab.sszuev.books.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * Created by @ssz on 02.03.2021.
 */
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByLogin(String name);
}
