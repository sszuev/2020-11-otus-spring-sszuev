package com.gitlab.sszuev.books;

import com.gitlab.sszuev.books.domain.Author;
import com.gitlab.sszuev.books.domain.Book;
import com.gitlab.sszuev.books.domain.Genre;
import com.gitlab.sszuev.books.domain.User;
import com.gitlab.sszuev.books.dto.AuthorRecord;
import com.gitlab.sszuev.books.dto.BookRecord;
import com.gitlab.sszuev.books.dto.GenreRecord;
import com.gitlab.sszuev.books.dto.PagedResult;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by @ssz on 31.01.2021.
 */
public class TestFactory {
    public static Book newBook(Long id, String title, Genre genre, List<Author> authors) {
        Book res = new Book();
        res.setID(id);
        res.setTitle(title);
        res.setGenre(genre);
        res.setAuthors(authors);
        return res;
    }

    public static Author newAuthor(long id, String author) {
        Author res = newAuthor(author);
        res.setID(id);
        return res;
    }

    public static Author newAuthor(String author) {
        Author res = new Author();
        res.setName(author);
        return res;
    }

    public static Genre newGenre(long id, String genre) {
        Genre res = newGenre(genre);
        res.setID(id);
        return res;
    }

    public static Genre newGenre(String genre) {
        Genre res = new Genre();
        res.setName(genre);
        return res;
    }

    public static User newUser(String login, String pwd) {
        User res = new User();
        res.setLogin(login);
        res.setPassword(pwd);
        return res;
    }

    @SuppressWarnings("unchecked")
    public static PagedResult<BookRecord> createMockPageResult(BookRecord... books) {
        PagedResult<BookRecord> res = Mockito.mock(PagedResult.class);
        Mockito.when(res.getPageCount()).thenReturn(1);
        Mockito.when(res.listContent()).thenReturn(Arrays.stream(books));
        return res;
    }

    public static BookRecord createMockBook(Long id, String title, String genre, List<String> authors) {
        GenreRecord g = createMockGenre(genre);
        List<AuthorRecord> list = authors.stream().map(TestFactory::createMockAuthor).collect(Collectors.toList());
        BookRecord res = Mockito.mock(BookRecord.class);
        if (id != null) {
            Mockito.when(res.getId()).thenReturn(id);
        }
        Mockito.when(res.getTitle()).thenReturn(title);
        Mockito.when(res.getGenre()).thenReturn(g);
        Mockito.when(res.listAuthors()).thenAnswer(i -> list.stream());
        return res;
    }

    public static GenreRecord createMockGenre(String genre) {
        GenreRecord res = Mockito.mock(GenreRecord.class);
        Mockito.when(res.getName()).thenReturn(genre);
        return res;
    }

    public static AuthorRecord createMockAuthor(String author) {
        AuthorRecord res = Mockito.mock(AuthorRecord.class);
        Mockito.when(res.getName()).thenReturn(author);
        return res;
    }
}
