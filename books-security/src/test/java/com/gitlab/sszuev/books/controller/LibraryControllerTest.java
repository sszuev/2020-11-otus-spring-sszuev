package com.gitlab.sszuev.books.controller;

import com.gitlab.sszuev.books.TestFactory;
import com.gitlab.sszuev.books.dto.BookRecord;
import com.gitlab.sszuev.books.dto.PagedResult;
import com.gitlab.sszuev.books.service.LibraryService;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by @ssz on 31.01.2021.
 */
@WithMockUser(username = "admin", authorities = {"ROLE_ADMIN"})
@WebMvcTest(controllers = LibraryController.class)
public class LibraryControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private LibraryService libraryService;

    @Test
    public void testListBooks() throws Exception {
        BookRecord b1 = TestFactory.createMockBook(null, "book1", "genre1", List.of("a1", "a2"));
        BookRecord b2 = TestFactory.createMockBook(null, "book2", "genre2", List.of("a3", "a2"));
        PagedResult<BookRecord> books = TestFactory.createMockPageResult(b1, b2);
        BDDMockito.given(libraryService.getPage(4)).willReturn(books);
        ResultActions res = mockMvc.perform(MockMvcRequestBuilders.get("/books").param("page", "5"))
                .andExpect(MockMvcResultMatchers.status().isOk());

        testContainsInContent(res, b1);
        testContainsInContent(res, b2);
    }

    @Test
    public void testDelete() throws Exception {
        long id = 42;
        mockMvc.perform(MockMvcRequestBuilders.post("/delete")
                .param("page", "5").param("id", String.valueOf(id)))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection());
        Mockito.verify(libraryService, Mockito.times(1)).delete(id);
    }

    @Test
    public void testEdit() throws Exception {
        long id = 42;
        BookRecord b = TestFactory.createMockBook(id, "book1", "genre1", List.of("a1", "a2"));
        BDDMockito.given(libraryService.find(id)).willReturn(Optional.of(b));
        ResultActions res = mockMvc.perform(MockMvcRequestBuilders.get("/edit/{id}", id).param("page", "5"))
                .andExpect(MockMvcResultMatchers.status().isOk());
        testContainsInContent(res, b);

        // save:
        mockMvc.perform(MockMvcRequestBuilders.post("/save/")
                .param("id", String.valueOf(b.getId()))
                .param("title", b.getTitle())
                .param("genre", b.getGenre().getName())
                .param("authors", getAuthorsAsString(b))
                .param("page", "5"))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection());
        Mockito.verify(libraryService, Mockito.times(1)).save(id, b.getTitle(), b.getGenre().getName(),
                b.listAuthors().map(x -> x.getName()).collect(Collectors.toList()));
    }

    @Test
    public void testCreate() throws Exception {
        String title = "book1";
        String genre = "genre1";
        List<String> authors = List.of("a2", "a4");
        String authorsString = collect(authors.stream());
        ResultActions res = mockMvc.perform(MockMvcRequestBuilders.get("/create"))
                .andExpect(MockMvcResultMatchers.status().isOk());
        testContentString(res, s -> Matchers.not(Matchers.containsString(s)),
                title, genre, authorsString);

        // save:
        mockMvc.perform(MockMvcRequestBuilders.post("/save/")
                .param("title", title)
                .param("genre", genre)
                .param("authors", authorsString)
                .param("page", "5"))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection());
        Mockito.verify(libraryService, Mockito.times(1)).save(null, title, genre, authors);
    }

    private void testContainsInContent(ResultActions res, BookRecord b) throws Exception {
        testContentString(res, Matchers::containsString,
                b.getTitle(), b.getGenre().getName(), getAuthorsAsString(b));
    }

    private void testContentString(ResultActions res,
                                   Function<String, Matcher<String>> matcher,
                                   String title,
                                   String genre,
                                   String authors) throws Exception {
        res.andExpect(contentString(matcher.apply(title)));
        res.andExpect(contentString(matcher.apply(genre)));
        res.andExpect(contentString(matcher.apply(authors)));
    }

    private ResultMatcher contentString(Matcher<String> matcher) {
        return MockMvcResultMatchers.content().string(matcher);
    }

    public static String getAuthorsAsString(BookRecord record) {
        return collect(record.listAuthors().map(x -> x.getName()));
    }

    private static String collect(Stream<String> stream) {
        return stream.collect(Collectors.joining(", "));
    }

}
