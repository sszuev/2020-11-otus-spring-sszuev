package com.gitlab.sszuev.books.service;

import com.gitlab.sszuev.books.TestFactory;
import com.gitlab.sszuev.books.dao.UserRepository;
import com.gitlab.sszuev.books.domain.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Optional;

/**
 * Created by @ssz on 02.03.2021.
 */
@SpringBootTest
public class UserDetailsServiceTest {
    @Autowired
    private UserDetailsService service;
    @MockBean
    private UserRepository repository;

    @Test
    public void testUserNotFound() {
        Assertions.assertThrows(UsernameNotFoundException.class,
                () -> service.loadUserByUsername("User" + System.currentTimeMillis()));
    }

    @Test
    public void testLoadDetails() {
        User u = TestFactory.newUser("x", "y");
        Mockito.when(repository.findByLogin(u.getLogin())).thenReturn(Optional.of(u));
        UserDetails ud = service.loadUserByUsername(u.getLogin());
        Assertions.assertNotNull(ud);
        Assertions.assertEquals(u.getLogin(), ud.getUsername());
        Assertions.assertEquals(u.getPassword(), ud.getPassword());
    }
}
