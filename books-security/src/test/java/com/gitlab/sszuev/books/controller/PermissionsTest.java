package com.gitlab.sszuev.books.controller;

import com.gitlab.sszuev.books.TestFactory;
import com.gitlab.sszuev.books.dto.BookRecord;
import com.gitlab.sszuev.books.dto.PagedResult;
import com.gitlab.sszuev.books.service.LibraryService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;
import java.util.Optional;

/**
 * Created by @ssz on 07.03.2021.
 */
@WebMvcTest(controllers = LibraryController.class)
public class PermissionsTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private LibraryService libraryService;

    @WithMockUser(username = "user", authorities = {"ROLE_USER"})
    @ParameterizedTest
    @ValueSource(strings = {"/edit/2", "/edit", "/create", "/delete"})
    public void testRoleUserAccessDeniedOnGet(String url) throws Exception {
        testAccessDeniedOnGet(url);
    }

    @WithMockUser(username = "user", authorities = {"ROLE_USER"})
    @Test
    public void testRoleUserOkOnList() throws Exception {
        testOkOnList();
    }

    @WithMockUser(username = "manager", authorities = {"ROLE_MANAGER"})
    @ParameterizedTest
    @ValueSource(strings = {"/create", "/delete"})
    public void testRoleManagerAccessDeniedOnGet(String url) throws Exception {
        testAccessDeniedOnGet(url);
    }

    @WithMockUser(username = "manager", authorities = {"ROLE_MANAGER"})
    @Test
    public void testRoleManagerOkOnList() throws Exception {
        testOkOnList();
    }

    @WithMockUser(username = "manager", authorities = {"ROLE_MANAGER"})
    @Test
    public void testRoleManagerOkOnEdit() throws Exception {
        BookRecord b = TestFactory.createMockBook(42L, "book4", "genre4", List.of("a3"));
        Mockito.when(libraryService.find(b.getId())).thenReturn(Optional.of(b));
        mockMvc.perform(MockMvcRequestBuilders.get("/edit/{id}", b.getId()).param("page", "5"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    private void testAccessDeniedOnGet(String url) throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
        Mockito.verifyNoInteractions(libraryService);
    }

    private void testOkOnList() throws Exception {
        BookRecord b = TestFactory.createMockBook(null, "book3", "genre3", List.of("a3", "a2"));
        PagedResult<BookRecord> books = TestFactory.createMockPageResult(b);
        BDDMockito.given(libraryService.getPage(0)).willReturn(books);

        mockMvc.perform(MockMvcRequestBuilders.get("/books"))
                .andExpect(MockMvcResultMatchers.status().isOk());
        Mockito.verify(libraryService, Mockito.times(1)).getPage(0);
    }
}
