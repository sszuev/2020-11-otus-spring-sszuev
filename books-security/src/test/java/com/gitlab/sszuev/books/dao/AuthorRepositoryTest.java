package com.gitlab.sszuev.books.dao;

import com.gitlab.sszuev.books.domain.Author;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by @ssz on 01.02.2021.
 */
public class AuthorRepositoryTest extends RepositoryTestBase {
    @Autowired
    private AuthorRepository repository;

    @Test
    public void testListWithNameIn() {
        Assertions.assertEquals(1, repository.streamByNameIn(List.of("Пушкин АС", "Грег Иган")).count());
        Assertions.assertEquals(2, repository.streamByNameIn(List.of("Т Цзян", "А.С. Пушкин", "Пушкин АС")).count());
        Assertions.assertEquals(0, repository.streamByNameIn(List.of("J Doe")).count());
        Author author = repository.streamByNameIn(List.of("Пушкин АС")).findFirst().orElseThrow(AssertionError::new);
        Assertions.assertEquals(10, author.getID());
    }
}
