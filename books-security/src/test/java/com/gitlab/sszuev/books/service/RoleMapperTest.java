package com.gitlab.sszuev.books.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by @ssz on 07.03.2021.
 */
@SpringBootTest
@ContextConfiguration(classes = RoleMapper.class)
public class RoleMapperTest {
    @Autowired
    private RoleMapper roleMapper;

    @Test
    public void testMap() {
        test(null, "USER");
        test(2, "USER");
        test(16, "MANAGER");
        test(256, "ADMIN");

        test(0b100010000, "ADMIN", "MANAGER"); // 272
        test(0b100010010, "ADMIN", "MANAGER", "USER"); // 274
        test(0b10010, "MANAGER", "USER"); // 18
    }

    private void test(Integer given, String expected) {
        Assertions.assertEquals(List.of(expected), roleMapper.apply(given).collect(Collectors.toList()));
    }

    private void test(Integer given, String ... expected) {
        Assertions.assertEquals(Set.of(expected), roleMapper.apply(given).collect(Collectors.toSet()));
    }
}
