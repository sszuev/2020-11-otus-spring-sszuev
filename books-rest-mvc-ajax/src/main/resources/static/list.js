// the page cache for genres, used in datalists;
// notice that if someone else works with the service the cache may not contain up-to-date information,
// but this is seems ok - it is just to improve usability
var genresCache = null;
$(function() {
    renderPage(new URLSearchParams(window.location.search).get('page'));
});
function renderPage(current) {
    if (current == null || !isInteger(current) || current < 0) {
        current = 1;
    }
    $.get('/api/books/pages/' + current).done(function(response) {
        drawContent(response, current);
    });
}
function drawContent(data, current) {
    if (data.content.length == 0 && current > 1) {
        // if nothing found on the given page then switch to the last one
        renderPage(data.pageCount);
        return;
    }
    var tbody = $('tbody');
    var pages = $('.pages');
    tbody.html('');
    pages.html('');
    tbody.append(drawNewRow());
    data.content.forEach(function(b) {
        tbody.append(drawEditRow(current, b.id, b.title, b.genre.name,
            Array.from(b.authors, x => x.name).join(', ')));
    });
    for (var i = 0; i < (data.pageSize - data.content.length); i++) {
        tbody.append(drawStubRow());
    }
    $('input.btn-save').hide();
    $('input.btn-cancel').hide();
    window.history.replaceState(null, null, '?page=' + current);
    if (data.pageCount < 2) {
        // only one page found => no need in paging
        return;
    }
    for (var p = 1; p <= data.pageCount; p++) {
        var clazz = current == p ? 'active' : '';
        pages.append(`
            <div class='pagination'>
                <a class='${clazz}' onclick='javascript:renderPage(${p})'>${p}</a>
            </div>
        `)
    }
}
function drawNewRow() {
    var id = '\"new\"';
    return `<tr id=${id}>
                <td>&nbsp;</td>
                <td></td>
                <td></td>
                <td></td>
                <td><input type='button' class='btn btn-save' value='(save)'
                        onclick='javascript:saveRecord(${id})'/>
                    <input type='button' class='btn btn-edit' value='(new)'
                        onclick='javascript:toEditRow(${id})'/>
                </td>
                <td><input type='button' class='btn btn-cancel' value='(cancel)'
                        onclick='javascript:toViewRow(${id})'/>
                </td>
            </tr>`;
}
function drawEditRow(current, id, title, genre, authors) {
    return `<tr id='${id}'>
                <td>${id}</td>
                <td>${title}</td>
                <td>${genre}</td>
                <td>${authors}</td>
                <td><input type='button' class='btn btn-save' value='(save)'
                        onclick='javascript:updateRecord(${id}, ${current})'/>
                    <input type='button' class='btn btn-edit' value='(edit)'
                        onclick='javascript:toEditRow(${id})'/>
                </td>
                <td><input type='button' class='btn btn-cancel' value='(cancel)'
                        onclick='javascript:toViewRow(${id})'/>
                    <input type='button' class='btn btn-delete' value='(delete)'
                        onclick='javascript:deleteRecord(${id}, ${current})'/>
                </td>
            </tr>`;
}
function drawStubRow() {
    return `<tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>`;
}
function deleteRecord(id, current) {
    $.ajax({
        type: 'DELETE',
        url: '/api/books/' + id,
    }).done(function(response) {
        renderPage(current);
    })
}
function updateRecord(id, current) {
    var request = getSaveRequest(id);
    if (request == null) {
        return;
    }
    $.ajax({
        type: 'PUT',
        url: '/api/books/' + id,
        contentType: "application/json",
        data: request
    }).done(function(response) {
        invalidateGenreCacheIfNeeded(request);
        renderPage(current);
    })
}
function saveRecord(id) {
    var request = getSaveRequest(id);
    if (request == null) {
        return;
    }
    $.ajax({
        type: 'POST',
        url: '/api/books',
        contentType: "application/json",
        data: request
    }).done(function(response) {
        invalidateGenreCacheIfNeeded(request);
        renderPage(1);
    })
}
function getSaveRequest(id) {
    var row = $('#' + id);
    var title = $('input', $(row.children()[1])).val();
    if (title == null) {
        return null;
    }
    var genre = $('input', $(row.children()[2])).val();
    if (genre == null) {
        return null;
    }
    var authors = $('input', $(row.children()[3])).val();
    if (authors == null) {
        return null;
    }
    return JSON.stringify({ 'title' : title, 'genre' : genre, 'authors' : $.map(authors.split(","), $.trim) });
}
function invalidateGenreCacheIfNeeded(request) {
    if (genresCache == null) {
        return;
    }
    if ($.inArray(JSON.parse(request).genre, genresCache) > 0) {
        return;
    }
    genresCache = null;
}
function toEditRow(id) {
    var row = $('#' + id);
    var i1 = toInput($(row.children()[1]));
    var i2 = toInput($(row.children()[2]));
    var i3 = toInput($(row.children()[3]));
    $('.btn-delete', row).hide();
    $('.btn-edit', row).hide();
    $('.btn-cancel', row).show();
    var bs = $('.btn-save', row);
    bs.show();
    var disableSave = function(e) {
        bs.prop('disabled', i1.val().length == 0 || i2.val().length == 0 || i3.val().length == 0);
    };
    disableSave();
    $.each([ i1, i2, i3 ], function(k, v) {
        v.on('input', disableSave);
    });
    // handle datalists:
    if (genresCache != null) {
        addDatalist(i2, 'list_' + id, genresCache);
    } else {
        // async!
        $.get('/api/genres').done(function(response) {
            genresCache = $.map(response, function(v, k) { return v.name; });
            addDatalist(i2, 'list_' + id, genresCache);
        });
    }
    // disable other rows:
    $('tr:not(#' + id + ') .btn').prop('disabled', true);
}
function toViewRow(id) {
    var row = $('#' + id);
    fromInput($(row.children()[1]));
    fromInput($(row.children()[2]));
    fromInput($(row.children()[3]));
    $('.btn-cancel', row).hide();
    $('.btn-save', row).hide();
    $('.btn-delete', row).show();
    $('.btn-edit', row).show();
    // enable other rows:
    $('tr:not(#' + id + ') .btn').prop('disabled', false);
}
function toInput(cell) {
    var input = $('input', cell);
    if (input.length != 0) {
        return input;
    }
    var id = cell.parent().attr('id');
    var txt = cell.text();
    cell.html(`<input type='text' value='${txt}'></input>`);
    input = $('input', cell);
    input.data('prev', txt);
    return input;
}
function addDatalist(input, listId, list) {
    var cell = input.parent();
    var select = $('#' + listId, cell);
    if (select.length != 0) {
        return select;
    }
    input.attr('list', listId);
    cell.append(`<datalist id='${listId}'/>`);
    select = $('#' + listId, cell);
    list.forEach(function(opt) {
        select.append($('<option></option>').attr('value', opt));
    });
}
function fromInput(cell) {
    var i = $('input', cell);
    if (i.length == 0) {
        return;
    }
    var txt = i.data('prev');
    cell.html(txt);
}
function isInteger(val) {
    return $.isNumeric(val) && Math.floor(val) == val;
}