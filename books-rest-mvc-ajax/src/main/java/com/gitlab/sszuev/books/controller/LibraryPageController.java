package com.gitlab.sszuev.books.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;
import java.util.Objects;

/**
 * Created by @ssz on 31.01.2021.
 */
@Controller
public class LibraryPageController {
    private final JQuerySupport jQuerySupport;

    public LibraryPageController(JQuerySupport jQuerySupport) {
        this.jQuerySupport = Objects.requireNonNull(jQuerySupport);
    }

    @GetMapping(value = {"/", "/books"})
    public ModelAndView listPage() {
        return new ModelAndView("list", Map.of("jquery", jQuerySupport.getJSMinScriptLocation()));
    }
}
