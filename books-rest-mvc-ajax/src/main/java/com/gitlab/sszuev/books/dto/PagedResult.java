package com.gitlab.sszuev.books.dto;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * To hold {@link Record record}s.
 * Created by @ssz on 31.01.2021.
 */
public final class PagedResult<E extends Record> {
    private final int pages;
    private final int size;
    private final Collection<E> content;

    protected PagedResult(int pages, int size, Collection<E> content) {
        this.pages = pages;
        this.size = size;
        this.content = Objects.requireNonNull(content);
    }

    public Stream<E> listContent() {
        return content.stream();
    }

    public List<E> getContent() {
        return listContent().collect(Collectors.toUnmodifiableList());
    }

    public int getPageCount() {
        return pages;
    }

    public int getPageSize() {
        return size;
    }
}
