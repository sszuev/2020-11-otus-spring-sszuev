package com.gitlab.sszuev.books.controller;

import com.gitlab.sszuev.books.dto.BookRecord;
import com.gitlab.sszuev.books.dto.GenreRecord;
import com.gitlab.sszuev.books.dto.PagedResult;
import com.gitlab.sszuev.books.service.LibraryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Created by @ssz on 04.02.2021.
 */
@RestController
public class LibraryRestController {
    private static final Logger LOGGER = LoggerFactory.getLogger(LibraryRestController.class);
    private final LibraryService service;

    public LibraryRestController(LibraryService service) {
        this.service = Objects.requireNonNull(service);
    }

    @GetMapping("/api/books/pages/{page}")
    public PagedResult<BookRecord> getBooksPage(@PathVariable Integer page) {
        return service.getBookPage(page - 1);
    }

    @GetMapping("/api/genres")
    public List<GenreRecord> getGenres() {
        return service.getAllGenres();
    }

    @DeleteMapping("/api/books/{id}")
    public void deleteBook(@PathVariable long id) {
        LOGGER.info("Delete book #{}.", id);
        service.deleteBook(id);
    }

    @PutMapping(value = "/api/books/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void editBook(@PathVariable long id, @RequestBody SaveRequest request) {
        LOGGER.info("Edit book #{}: {}.", id, request);
        service.saveBook(id, request.title, request.genre, request.authors);
    }

    @PostMapping(value = "/api/books", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void createBook(@RequestBody SaveRequest request) {
        LOGGER.info("Create book: {}.", request);
        service.saveBook(null, request.title, request.genre, request.authors);
    }

    public static final class SaveRequest implements Serializable {
        private final String title;
        private final String genre;
        private final List<String> authors;

        public SaveRequest(String title, String genre, String... authors) {
            this.title = Objects.requireNonNull(title);
            this.genre = Objects.requireNonNull(genre);
            if (authors.length == 0) {
                throw new IllegalArgumentException();
            }
            this.authors = List.of(authors);
        }

        @Override
        public String toString() {
            return String.format("{title='%s', genre='%s', authors=%s}",
                    title, genre, authors.stream().map(x -> "'" + x + "'").collect(Collectors.joining(",")));
        }
    }
}
