package com.gitlab.sszuev.books.dao;

import com.gitlab.sszuev.books.domain.Genre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;
import java.util.stream.Stream;

/**
 * Created by @ssz on 01.02.2021.
 */
public interface GenreRepository extends JpaRepository<Genre, Long> {

    /**
     * Finds a {@code Genre} by its name, which must be unique into database.
     *
     * @param name {@code String}, not {@code null}
     * @return an {@code Optional} around {@link Genre}
     */
    Optional<Genre> findByName(String name);

    /**
     * Lists all entities from the underlying storage.
     *
     * @return a {@code Stream} of {@link Genre}s
     */
    @Query("SELECT g FROM Genre g")
    Stream<Genre> streamAll();
}
