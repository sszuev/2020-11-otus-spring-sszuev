package com.gitlab.sszuev.books.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import java.util.Properties;

/**
 * A class-helper that provides access to JQuery meta-info, to be injected into page.
 * Thanks to this helper when upgrading dependencies we can just increase a pom-version of {@code org.webjars:jquery}.
 * <p>
 * Created by @ssz on 07.02.2021.
 */
@Component
public class JQuerySupport {
    private final static String WEBJARS_JQUERY_POM_PROPERTIES = "classpath:/META-INF/maven/org.webjars/jquery/pom.properties";
    private final String jqueryMinLocation;

    public JQuerySupport(@Value(WEBJARS_JQUERY_POM_PROPERTIES) Resource resource) throws IOException {
        Properties p = new Properties();
        try (InputStream in = resource.getInputStream()) {
            p.load(in);
        }
        String version = Objects.requireNonNull(p.getProperty("version"));
        this.jqueryMinLocation = "/webjars/jquery/" + version + "/jquery.min.js";
    }

    public String getJSMinScriptLocation() {
        return jqueryMinLocation;
    }
}
