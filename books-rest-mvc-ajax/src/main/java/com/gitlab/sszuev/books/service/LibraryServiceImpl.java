package com.gitlab.sszuev.books.service;

import com.gitlab.sszuev.books.dao.AuthorRepository;
import com.gitlab.sszuev.books.dao.BookRepository;
import com.gitlab.sszuev.books.dao.GenreRepository;
import com.gitlab.sszuev.books.domain.Author;
import com.gitlab.sszuev.books.domain.Book;
import com.gitlab.sszuev.books.domain.Genre;
import com.gitlab.sszuev.books.dto.BookRecord;
import com.gitlab.sszuev.books.dto.EntityMapper;
import com.gitlab.sszuev.books.dto.GenreRecord;
import com.gitlab.sszuev.books.dto.PagedResult;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Created by @ssz on 31.01.2021.
 */
@Service
public class LibraryServiceImpl implements LibraryService {
    private final BookRepository bookRepository;
    private final AuthorRepository authorRepository;
    private final GenreRepository genreRepository;
    private final EntityMapper mapper;
    private final int pageSize;

    public LibraryServiceImpl(BookRepository bookRepository,
                              AuthorRepository authorRepository,
                              GenreRepository genreRepository,
                              EntityMapper entityMapper,
                              @Value("${app.web.page-size:5}") int pageSize) {
        this.bookRepository = Objects.requireNonNull(bookRepository);
        this.authorRepository = Objects.requireNonNull(authorRepository);
        this.genreRepository = Objects.requireNonNull(genreRepository);
        this.mapper = Objects.requireNonNull(entityMapper);
        this.pageSize = pageSize;
    }

    @Transactional(readOnly = true)
    @Override
    public PagedResult<BookRecord> getBookPage(int page) {
        if (page < 0) throw new IllegalArgumentException("Negative page number : " + page);
        PageRequest request = PageRequest.of(page, pageSize, Sort.by(Sort.Direction.DESC, "id"));
        return mapper.toRecords(bookRepository.fetchAll(request));
    }

    @Transactional(readOnly = true)
    @Override
    public List<GenreRecord> getAllGenres() {
        return genreRepository.streamAll().map(mapper::toRecord).collect(Collectors.toUnmodifiableList());
    }

    @Transactional
    @Override
    public BookRecord saveBook(Long id, String title, String genre, List<String> authors) {
        return mapper.toRecord(bookRepository.save(newBook(id, title, fetchGenre(genre), fetchAuthors(authors))));
    }

    @Transactional
    @Override
    public void deleteBook(long id) {
        bookRepository.deleteById(id);
    }

    private List<Author> fetchAuthors(List<String> authors) {
        // Note: right now there is no unique constraint on column AUTHORS.NAME !
        Map<String, List<Author>> found = authorRepository.streamByNameIn(authors)
                .collect(Collectors.groupingBy(Author::getName, Collectors.toList()));
        return authors.stream().distinct()
                .map(a -> found.computeIfAbsent(a, x -> List.of(newAuthor(x))))
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    private Genre fetchGenre(String genre) {
        return genreRepository.findByName(genre).orElseGet(() -> newGenre(genre));
    }

    private Author newAuthor(String name) {
        Author res = new Author();
        res.setName(name);
        return res;
    }

    private Genre newGenre(String name) {
        Genre res = new Genre();
        res.setName(name);
        return res;
    }

    private Book newBook(Long id, String title, Genre genre, List<Author> authors) {
        Book res = new Book();
        res.setID(id);
        res.setTitle(title);
        res.setGenre(genre);
        res.setAuthors(authors);
        return res;
    }
}
