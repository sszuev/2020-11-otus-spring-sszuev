package com.gitlab.sszuev.books.dao;

import com.gitlab.sszuev.books.domain.Book;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * Created by @ssz on 31.01.2021.
 */
public interface BookRepository extends JpaRepository<Book, Long> {
    /**
     * Returns a {@code Page} of entities.
     * To solve "n+1 problem" there is {@code JOIN FETCH} in {@link Query}.
     *
     * @param pageable {@link Pageable}, not {@code null}
     * @return {@link Page} a page of entities
     * @see #findAll(Pageable)
     */
    @Query(value = "SELECT b FROM Book b JOIN FETCH b.genre", countQuery = "SELECT COUNT(b) FROM Book b")
    Page<Book> fetchAll(Pageable pageable);
}
