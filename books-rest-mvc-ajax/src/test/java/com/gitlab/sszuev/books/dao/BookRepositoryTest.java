package com.gitlab.sszuev.books.dao;

import com.gitlab.sszuev.books.TestUtils;
import com.gitlab.sszuev.books.domain.Book;
import org.hibernate.SessionFactory;
import org.hibernate.stat.Statistics;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

/**
 * Created by @ssz on 31.01.2021.
 */
public class BookRepositoryTest extends RepositoryTestBase {
    private static final Logger LOGGER = LoggerFactory.getLogger(BookRepositoryTest.class);
    // see test-data.sql
    private static final long TOTAL_TEST_BOOKS_NUMBER = 8;
    @Autowired
    private BookRepository repository;

    @Test
    public void testFetchAllWithPaging() {
        Statistics statistics = tem.getEntityManager().getEntityManagerFactory().unwrap(SessionFactory.class).getStatistics();
        statistics.setStatisticsEnabled(true);
        statistics.clear();

        printLine();
        Page<Book> page1 = repository.fetchAll(PageRequest.of(0, 5));
        page1.forEach(b -> LOGGER.debug(TestUtils.print(b)));
        assertTotalPages(5, page1);

        printLine();
        Page<Book> page2 = repository.fetchAll(PageRequest.of(2, 3));
        page2.forEach(b -> LOGGER.debug(TestUtils.print(b)));
        assertTotalPages(3, page2);

        printLine();
        Page<Book> page3 = repository.fetchAll(PageRequest.of(1, 2));
        page3.forEach(b -> LOGGER.debug(TestUtils.print(b)));
        assertTotalPages(2, page3);

        Assertions.assertEquals(6, statistics.getPrepareStatementCount());
    }

    private void assertTotalPages(int pageSize, Page<?> actual) {
        Assertions.assertEquals(TOTAL_TEST_BOOKS_NUMBER, actual.getTotalElements());
        Assertions.assertEquals(Math.round(TOTAL_TEST_BOOKS_NUMBER / (float) pageSize), actual.getTotalPages());
    }

    private static void printLine() {
        LOGGER.debug("^".repeat(42));
    }

}
