package com.gitlab.sszuev.books;

import com.gitlab.sszuev.books.domain.Author;
import com.gitlab.sszuev.books.domain.Book;
import com.gitlab.sszuev.books.domain.Genre;

import java.util.List;

/**
 * Created by @ssz on 31.01.2021.
 */
public class TestEntityFactory {
    public static Book newBook(Long id, String title, Genre genre, List<Author> authors) {
        Book res = new Book();
        res.setID(id);
        res.setTitle(title);
        res.setGenre(genre);
        res.setAuthors(authors);
        return res;
    }

    public static Author newAuthor(long id, String author) {
        Author res = newAuthor(author);
        res.setID(id);
        return res;
    }

    public static Author newAuthor(String author) {
        Author res = new Author();
        res.setName(author);
        return res;
    }

    public static Genre newGenre(long id, String genre) {
        Genre res = newGenre(genre);
        res.setID(id);
        return res;
    }

    public static Genre newGenre(String genre) {
        Genre res = new Genre();
        res.setName(genre);
        return res;
    }

}
