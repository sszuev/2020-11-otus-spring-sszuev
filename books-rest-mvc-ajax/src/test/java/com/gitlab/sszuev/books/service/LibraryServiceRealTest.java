package com.gitlab.sszuev.books.service;

import com.gitlab.sszuev.books.dto.BookRecord;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * Created by @ssz on 08.02.2021.
 */
@SpringBootTest
public class LibraryServiceRealTest {

    @Autowired
    private LibraryService service;

    @Test
    public void testAddNewBookWithDuplicateAuthors() {
        BookRecord r = service.saveBook(null, "t1", "сказка", List.of("Пушкин АС", "Пушкин АС", "А.С. Пушкин"));
        Assertions.assertNotNull(r);
    }
}
