package com.gitlab.sszuev.books.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.sszuev.books.dto.BookRecord;
import com.gitlab.sszuev.books.dto.GenreRecord;
import com.gitlab.sszuev.books.dto.PagedResult;
import com.gitlab.sszuev.books.service.LibraryService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by @ssz on 05.02.2021.
 */
@WebMvcTest(controllers = LibraryRestController.class)
public class LibraryRestControllerTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(LibraryRestControllerTest.class);

    @Autowired
    private MockMvc mvc;
    @MockBean
    private LibraryService service;

    @Test
    public void testListBooks() throws Exception {
        BookRecord b1 = TestRecordFactory.createMockBook(null, "book1", "genre1", List.of("a1", "a2"));
        BookRecord b2 = TestRecordFactory.createMockBook(null, "book2", "genre2", List.of("a3", "a2"));
        PagedResult<BookRecord> givenBooks = TestRecordFactory.createMockPageResult(b1, b2);
        Mockito.when(service.getBookPage(0)).thenReturn(givenBooks);

        MvcResult res = mvc.perform(MockMvcRequestBuilders.get("/api/books/pages/{page}", 1)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        Assertions.assertEquals(200, res.getResponse().getStatus());
        String content = res.getResponse().getContentAsString();
        Assertions.assertNotNull(content);

        Map<String, Object> map = mapFromJson(content);
        Assertions.assertEquals(1, map.get("pageCount"));
        List<Map<String, Object>> actualBooks = cast(map.get("content"));
        LOGGER.info("Books: {}", actualBooks);
        Assertions.assertEquals(2, actualBooks.size());
        actualBooks.forEach(b -> {
            Assertions.assertNotNull(b.get("id"));
            Assertions.assertNotNull(b.get("title"));
            Assertions.assertNotNull(b.get("genre"));
            Map<String, Object> genre = cast(b.get("genre"));
            Assertions.assertNotNull(genre.get("name"));
            Assertions.assertNotNull(b.get("authors"));
            List<Map<String, Object>> authors = cast(b.get("authors"));
            Assertions.assertEquals(2, authors.size());
            authors.forEach(a -> Assertions.assertNotNull(a.get("name")));
        });
    }

    @Test
    public void testDelete() throws Exception {
        long id = 42;
        mvc.perform(MockMvcRequestBuilders.delete("/api/books/{id}", id))
                .andExpect(MockMvcResultMatchers.status().isOk());
        Mockito.verify(service, Mockito.times(1)).deleteBook(id);
    }

    @Test
    public void testEdit() throws Exception {
        long id = 42;
        String title = "b1";
        String genre = "g1";
        List<String> authors = List.of("a1", "a2");
        String request = toSaveRequest(title, genre, authors);
        LOGGER.info("Request: {}", request);
        mvc.perform(MockMvcRequestBuilders.put("/api/books/{id}", id)
                .contentType(MediaType.APPLICATION_JSON)
                .content(request))
                .andExpect(MockMvcResultMatchers.status().isOk());
        Mockito.verify(service, Mockito.times(1)).saveBook(id, title, genre, authors);
    }

    @Test
    public void testSave() throws Exception {
        String title = "b2";
        String genre = "g2";
        List<String> authors = List.of("a3", "a2");
        String request = toSaveRequest(title, genre, authors);
        LOGGER.info("Request: {}", request);
        mvc.perform(MockMvcRequestBuilders.post("/api/books")
                .contentType(MediaType.APPLICATION_JSON)
                .content(request))
                .andExpect(MockMvcResultMatchers.status().isOk());
        Mockito.verify(service, Mockito.times(1)).saveBook(null, title, genre, authors);
    }

    @Test
    public void testListGenres() throws Exception {
        GenreRecord g1 = TestRecordFactory.createMockGenre("g1");
        GenreRecord g2 = TestRecordFactory.createMockGenre("g2");
        Mockito.when(service.getAllGenres()).thenReturn(List.of(g1, g2));

        MvcResult res = mvc.perform(MockMvcRequestBuilders.get("/api/genres")
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        Assertions.assertEquals(200, res.getResponse().getStatus());
        String content = res.getResponse().getContentAsString();
        Assertions.assertNotNull(content);

        List<Map<String, Object>> actualGenres = listFromJson(content);

        LOGGER.info("Genres: {}", actualGenres);
        Assertions.assertEquals(2, actualGenres.size());
        actualGenres.forEach(g -> {
            Assertions.assertNotNull(g.get("id"));
            Assertions.assertNotNull(g.get("name"));
        });
    }

    @SuppressWarnings("unchecked")
    private static Map<String, Object> mapFromJson(String json) throws JsonProcessingException {
        return new ObjectMapper().readValue(json, Map.class);
    }

    @SuppressWarnings("unchecked")
    private static <X> List<X> listFromJson(String json) throws JsonProcessingException {
        return new ObjectMapper().readValue(json, List.class);
    }

    private static String toSaveRequest(String title, String genre, List<String> authors) {
        return String.format("{\"title\":\"%s\",\"genre\":\"%s\",\"authors\":%s}", title, genre,
                authors.stream().map(x -> "\"" + x + "\"").collect(Collectors.joining(", ", "[", "]")));
    }

    @SuppressWarnings("unchecked")
    private static <X> X cast(Object obj) {
        return (X) obj;
    }
}
