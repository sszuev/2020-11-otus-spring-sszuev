package com.gitlab.sszuev.books.controller;

import com.gitlab.sszuev.books.dto.AuthorRecord;
import com.gitlab.sszuev.books.dto.BookRecord;
import com.gitlab.sszuev.books.dto.GenreRecord;
import com.gitlab.sszuev.books.dto.PagedResult;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by @ssz on 05.02.2021.
 */
class TestRecordFactory {
    @SuppressWarnings("unchecked")
    public static PagedResult<BookRecord> createMockPageResult(BookRecord... books) {
        PagedResult<BookRecord> res = Mockito.mock(PagedResult.class);
        Mockito.when(res.getPageCount()).thenReturn(1);
        Mockito.when(res.getPageSize()).thenReturn(10);
        Mockito.when(res.listContent()).thenAnswer(i -> Arrays.stream(books));
        Mockito.when(res.getContent()).thenReturn(Arrays.asList(books));
        return res;
    }

    public static BookRecord createMockBook(Long id, String title, String genre, List<String> authors) {
        GenreRecord g = createMockGenre(genre);
        List<AuthorRecord> list = authors.stream().map(TestRecordFactory::createMockAuthor).collect(Collectors.toList());
        BookRecord res = Mockito.mock(BookRecord.class);
        if (id != null) {
            Mockito.when(res.getId()).thenReturn(id);
        }
        Mockito.when(res.getTitle()).thenReturn(title);
        Mockito.when(res.getGenre()).thenReturn(g);
        Mockito.when(res.listAuthors()).thenAnswer(i -> list.stream());
        Mockito.when(res.getAuthors()).thenReturn(list);
        return res;
    }

    public static GenreRecord createMockGenre(String genre) {
        GenreRecord res = Mockito.mock(GenreRecord.class);
        Mockito.when(res.getName()).thenReturn(genre);
        return res;
    }

    public static AuthorRecord createMockAuthor(String author) {
        AuthorRecord res = Mockito.mock(AuthorRecord.class);
        Mockito.when(res.getName()).thenReturn(author);
        return res;
    }
}
