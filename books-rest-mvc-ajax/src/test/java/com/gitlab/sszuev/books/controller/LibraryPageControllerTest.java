package com.gitlab.sszuev.books.controller;

import org.hamcrest.Matchers;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 * Created by @ssz on 31.01.2021.
 */
@WebMvcTest(controllers = LibraryPageController.class)
@Import(JQuerySupport.class)
public class LibraryPageControllerTest {
    @Autowired
    private MockMvc mvc;

    @ParameterizedTest(name = "[{index}] ::: {0}")
    @CsvSource({"/", "/books"})
    public void testMainPage(String path) throws Exception {
        mvc.perform(MockMvcRequestBuilders.get(path))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string(Matchers.containsString("/webjars/jquery/3.5.1/jquery.min.js")));
    }
}
