#### This is a home-work 'Modern Spring MVC (SPA: REST-API + AJAX)'

A simple web application to manage information about books. Once launched, the application must be available at
the http://localhost:8080 address.

###### If you are not from [otus](https://otus.ru)-team please ignore it: there is nothing interesting here.

##### Requirements:

- Git
- Java **11**
- Maven **3+**

##### Build and run:

```
$ git clone git@gitlab.com:sszuev/2020-11-otus-spring-sszuev.git
$ cd 2020-11-otus-spring-sszuev/books-rest-mvc-ajax
$ mvn clean package
$ java -jar target/books-library.jar
```