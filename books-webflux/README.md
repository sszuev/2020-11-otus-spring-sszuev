#### This is a home-work 'Spring WebFlux'

A simple web application to manage information about books.

###### If you are not from [otus](https://otus.ru)-team please ignore it: there is nothing interesting here.
Once launched, the application must be available at the address http://localhost:8080.

##### Requirements:

- Git
- Java **11**
- Maven **3+**

##### Build and run:

```
$ git clone git@gitlab.com:sszuev/2020-11-otus-spring-sszuev.git
$ cd 2020-11-otus-spring-sszuev/books-webflux
$ mvn clean package
$ java -jar target/books-library.jar
```