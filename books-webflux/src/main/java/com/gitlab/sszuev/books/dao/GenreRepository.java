package com.gitlab.sszuev.books.dao;

import com.gitlab.sszuev.books.domain.Genre;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Mono;

/**
 * Created by @ssz on 13.02.2021.
 */
public interface GenreRepository extends ReactiveMongoRepository<Genre, String> {

    /**
     * Finds genre by the specified name.
     *
     * @param name {@code String}, not {@code null}
     * @return a {@link Mono} of {@link Genre}s
     */
    Mono<Genre> findByName(String name);
}
