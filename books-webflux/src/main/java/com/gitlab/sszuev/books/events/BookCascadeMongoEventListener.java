package com.gitlab.sszuev.books.events;

import com.gitlab.sszuev.books.domain.Author;
import com.gitlab.sszuev.books.domain.Book;
import com.gitlab.sszuev.books.domain.Genre;
import com.gitlab.sszuev.books.domain.HasID;
import com.mongodb.client.result.UpdateResult;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.BeforeConvertEvent;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.util.ReflectionUtils;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Objects;

/**
 * To control {@link Book}'s save operation.
 * <p>
 * Created by @ssz on 20.01.2021.
 */
@Component
public class BookCascadeMongoEventListener extends AbstractMongoEventListener<Book> {
    private final MongoOperations template;

    public BookCascadeMongoEventListener(MongoOperations template) {
        this.template = Objects.requireNonNull(template);
    }

    @Override
    public void onBeforeConvert(@NonNull BeforeConvertEvent<Book> event) {
        super.onBeforeConvert(event);
        Book book = event.getSource();
        String title = book.getTitle();
        if (title == null || title.isEmpty()) {
            throw new WrongDataException("No title for '" + toMessage(book) + "'");
        }
        Genre genre = book.getGenre();
        if (genre == null || genre.getName() == null) {
            throw new WrongDataException("No genre for '" + toMessage(book) + "'");
        }
        List<Author> authors = book.getAuthors();
        if (authors == null || authors.isEmpty()) {
            throw new WrongDataException("No authors for '" + toMessage(book) + "'");
        }
        if (authors.stream().map(Author::getName).anyMatch(Objects::isNull)) {
            throw new WrongDataException("Null author specified for '" + toMessage(book) + "'");
        }
        if (genre.getID() != null) {
            template.save(genre);
        } else {
            setId(genre, upsertGenre(genre.getName()));
        }
        authors.forEach(author -> {
            String id = author.getID();
            Author a = template.save(author);
            if (id == null) {
                setId(author, a.getID());
            }
        });
    }

    private String upsertGenre(String name) {
        Query find = new Query(Criteria.where(Genre.NAME).is(name));
        Update update = new Update();
        update.set(Genre.NAME, name);
        UpdateResult res = template.upsert(find, update, Genre.class);
        if (res.getMatchedCount() > 0) {
            return Objects.requireNonNull(template.findOne(find, Genre.class)).getID();
        }
        return Objects.requireNonNull(res.getUpsertedId()).asObjectId().getValue().toHexString();
    }

    private void setId(HasID entity, String id) {
        // only reflection: all entities here are immutable
        Field field = ReflectionUtils.findField(entity.getClass(), new ReflectionUtils.AnnotationFieldFilter(Id.class));
        ReflectionUtils.setField(Objects.requireNonNull(field), entity, id);
    }

    private String toMessage(Book book) {
        return String.format("Book[%s]'%s'", book.getID(), book.getTitle());
    }
}
