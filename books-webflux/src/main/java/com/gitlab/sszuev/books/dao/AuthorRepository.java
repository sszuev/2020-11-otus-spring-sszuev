package com.gitlab.sszuev.books.dao;

import com.gitlab.sszuev.books.domain.Author;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;

import java.util.Collection;

/**
 * Created by @ssz on 21.02.2021.
 */
public interface AuthorRepository extends ReactiveMongoRepository<Author, String> {

    /**
     * Lists all author entities which names are in the given list.
     *
     * @param names a {@code List} of {@code String}-names to check, not {@code null}
     * @return a {@link Flux} of {@link Author}s
     */
    Flux<Author> findByNameIn(Collection<String> names);
}
