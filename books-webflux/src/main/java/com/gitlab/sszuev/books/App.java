package com.gitlab.sszuev.books;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by @ssz on 13.02.2021.
 */
@com.github.cloudyrock.spring.v5.EnableMongock
@SpringBootApplication
public class App {

    public static void main(String... args) {
        SpringApplication.run(App.class, args);
    }
}
