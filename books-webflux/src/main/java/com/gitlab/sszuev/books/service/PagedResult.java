package com.gitlab.sszuev.books.service;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by @ssz on 31.01.2021.
 *
 * @param <E> anything
 */
public final class PagedResult<E> {
    private final int pages;
    private final int size;
    private final Collection<E> content;

    private PagedResult(int pages, int size, Collection<E> content) {
        this.pages = pages;
        this.size = size;
        this.content = Objects.requireNonNull(content);
    }

    public static <X> PagedResult<X> of(int pages, int size, Collection<X> content) {
        return new PagedResult<>(pages, size, content);
    }

    public Stream<E> listContent() {
        return content.stream();
    }

    public List<E> getContent() {
        return listContent().collect(Collectors.toUnmodifiableList());
    }

    public int getPageCount() {
        return pages;
    }

    public int getPageSize() {
        return size;
    }
}