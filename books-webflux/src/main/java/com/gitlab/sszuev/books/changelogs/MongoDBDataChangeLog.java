package com.gitlab.sszuev.books.changelogs;

import com.github.cloudyrock.mongock.ChangeLog;
import com.github.cloudyrock.mongock.ChangeSet;
import com.github.cloudyrock.mongock.driver.mongodb.springdata.v3.decorator.impl.MongockTemplate;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by @ssz on 23.02.2021.
 */
@ChangeLog(order = "100")
public class MongoDBDataChangeLog {
    private static final Logger LOGGER = LoggerFactory.getLogger(MongoDBDataChangeLog.class);

    @ChangeSet(order = "001", id = "load-data-from-resources", author = "ssz")
    public void populateData(MongockTemplate template, Environment env) throws IOException {
        String file = env.getProperty("app.mongodb.data");
        if (file == null) {
            // no data is given
            return;
        }
        if (file.startsWith("classpath:")) {
            file = file.substring(10);
        }
        Resource resource = new ClassPathResource(file);
        if (!resource.exists()) {
            throw new IllegalStateException("Can't find class-path resource '" + file + "'");
        }
        LOGGER.info("Insert data from the file {}.", file);
        JsonReadHelper helper = new JsonReadHelper((name, json) -> {
            LOGGER.debug("Insert into {} data {}", name, json);
            template.getCollection(name).insertOne(Document.parse(json));
        });
        try (InputStream in = resource.getInputStream()) {
            helper.read(in);
        }
    }
}
