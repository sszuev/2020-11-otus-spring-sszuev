package com.gitlab.sszuev.books.controller;

import com.gitlab.sszuev.books.domain.Author;
import com.gitlab.sszuev.books.domain.Book;
import com.gitlab.sszuev.books.domain.Genre;
import com.gitlab.sszuev.books.service.LibraryService;
import com.gitlab.sszuev.books.service.PagedResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Created by @ssz on 13.02.2021.
 */
@RestController
public class LibraryRestController {
    private static final Logger LOGGER = LoggerFactory.getLogger(LibraryRestController.class);

    private final LibraryService service;

    public LibraryRestController(LibraryService service) {
        this.service = Objects.requireNonNull(service);
    }

    @GetMapping("/api/genres")
    public Flux<Genre> findGenres() {
        return service.findAllGenres();
    }

    @GetMapping("/api/authors")
    public Flux<Author> findAuthors() {
        return service.findAllAuthors();
    }

    @GetMapping("/api/books/pages/{page}")
    public Mono<PagedResult<Book>> findBooksPage(@PathVariable Integer page) {
        return service.findBookPage(page - 1);
    }

    @GetMapping("/api/books")
    public Flux<Book> findBooks(@RequestParam(value = "page", required = false) Integer page) {
        return page == null ? service.findAllBooks() : service.findBooksOfPage(page - 1);
    }

    @PostMapping(value = "/api/books", consumes = MediaType.APPLICATION_JSON_VALUE)
    public Mono<Book> createBook(@RequestBody SaveRequest request) {
        LOGGER.info("Create book: {}.", request);
        return service.saveBook(null, request.title, request.genre, request.authors)
                .doOnSuccess(x -> LOGGER.info("The book '{}' has been created.", x.getID()));
    }

    @PutMapping(value = "/api/books/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public Mono<Book> editBook(@PathVariable String id, @RequestBody SaveRequest request) {
        LOGGER.info("Edit book '{}': {}.", id, request);
        return service.saveBook(id, request.title, request.genre, request.authors)
                .doOnSuccess(x -> LOGGER.info("The book '{}' has been modified.", x.getID()));
    }

    @DeleteMapping("/api/books/{id}")
    public Mono<Void> deleteBook(@PathVariable String id) {
        LOGGER.info("Delete book '{}'.", id);
        return service.deleteBook(id)
                .doOnSuccess(x -> LOGGER.info("The book '{}' has been deleted.", id));
    }

    public static final class SaveRequest implements Serializable {
        private final String title;
        private final String genre;
        private final List<String> authors;

        public SaveRequest(String title, String genre, String... authors) {
            this.title = Objects.requireNonNull(title);
            this.genre = Objects.requireNonNull(genre);
            if (authors.length == 0) {
                throw new IllegalArgumentException();
            }
            this.authors = List.of(authors);
        }

        @Override
        public String toString() {
            return String.format("{title='%s', genre='%s', authors=%s}",
                    title, genre, authors.stream().map(x -> "'" + x + "'").collect(Collectors.joining(",")));
        }
    }

}
