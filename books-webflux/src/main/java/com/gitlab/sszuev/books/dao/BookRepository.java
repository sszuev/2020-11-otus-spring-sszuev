package com.gitlab.sszuev.books.dao;

import com.gitlab.sszuev.books.domain.Book;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;

/**
 * Created by @ssz on 21.02.2021.
 */
public interface BookRepository extends ReactiveMongoRepository<Book, String> {

    /**
     * Returns all instance answering the specified {@link Pageable} parameters.
     *
     * @param pageable {@link Pageable}, not {@code null}
     * @return a {@link Flux} of {@link Book}s
     */
    @Query("{}")
    // does not work without explicit query
    Flux<Book> findAll(Pageable pageable);
}
