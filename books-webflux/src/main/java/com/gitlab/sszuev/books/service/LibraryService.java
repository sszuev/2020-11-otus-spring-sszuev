package com.gitlab.sszuev.books.service;

import com.gitlab.sszuev.books.dao.AuthorRepository;
import com.gitlab.sszuev.books.dao.BookRepository;
import com.gitlab.sszuev.books.dao.GenreRepository;
import com.gitlab.sszuev.books.domain.Author;
import com.gitlab.sszuev.books.domain.Book;
import com.gitlab.sszuev.books.domain.Genre;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.GroupedFlux;
import reactor.core.publisher.Mono;

import java.util.*;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * To work with different kinds of {@link org.reactivestreams.Publisher Publisher}s.
 * <p>
 * Created by @ssz on 21.02.2021.
 *
 * @see <a href='https://gitlab.com/sszuev/2020-11-otus-spring-sszuev/-/raw/master/books-rest-mvc-ajax/src/main/java/com/gitlab/sszuev/books/service/LibraryService.java'>com.gitlab.sszuev.books.service.LibraryService</a>
 */
@Component
public class LibraryService {
    private final BookRepository bookRepository;
    private final GenreRepository genreRepository;
    private final AuthorRepository authorRepository;
    private final int pageSize;

    public LibraryService(BookRepository books,
                          GenreRepository genres,
                          AuthorRepository authors,
                          @Value("${app.web.page-size:5}") int pageSize) {
        this.authorRepository = authors;
        this.genreRepository = Objects.requireNonNull(genres);
        this.bookRepository = Objects.requireNonNull(books);
        this.pageSize = requireNonNegative(pageSize);
    }

    /**
     * Gets all genres.
     *
     * @return a {@link Flux} of {@link Genre genre}s
     */
    @Transactional(readOnly = true)
    public Flux<Genre> findAllGenres() {
        return genreRepository.findAll();
    }

    /**
     * Gets all authors.
     *
     * @return a {@link Flux} of {@link Author author}s
     */
    @Transactional(readOnly = true)
    public Flux<Author> findAllAuthors() {
        return authorRepository.findAll();
    }

    /**
     * Lists all books.
     *
     * @return a {@link Flux} of {@link Book book}s
     */
    @Transactional(readOnly = true)
    public Flux<Book> findAllBooks() {
        return bookRepository.findAll();
    }

    /**
     * Gets a page of books.
     *
     * @param index non-negative {@code int}
     * @return a {@link Mono} of {@link PagedResult} with {@link Book book}s
     */
    @Transactional(readOnly = true)
    public Mono<PagedResult<Book>> findBookPage(int index) {
        PageRequest request = PageRequest.of(requireNonNegative(index), pageSize, Sort.by(Sort.Direction.DESC, "_id"));
        return bookRepository.count()
                .map(count -> (int) Math.ceil((double) count / pageSize))
                .flatMap(total -> bookRepository.findAll(request)
                        .collectList()
                        .map(all -> PagedResult.of(total, pageSize, all)));
    }

    /**
     * Lists books of the specified page.
     *
     * @param index non-negative {@code int}
     * @return a {@link Flux} of {@link Book book}s
     */
    @Transactional(readOnly = true)
    public Flux<Book> findBooksOfPage(int index) {
        PageRequest request = PageRequest.of(requireNonNegative(index), pageSize, Sort.by(Sort.Direction.DESC, "_id"));
        return bookRepository.findAll(request);
    }

    /**
     * Creates (if {@code id} is {@code null}) or updates a book-record with the specified parameters.
     *
     * @param id      {@code String}
     * @param title   {@code String}, not {@code null}
     * @param genre   {@code String}, not {@code null}
     * @param authors {@code List} of {@code String}s, not {@code null}, not empty
     * @return a {@link Mono} wrapping {@link Book}
     */
    @Transactional
    public Mono<Book> saveBook(String id, String title, String genre, List<String> authors) {
        return fetchAuthors(new HashSet<>(authors))
                .flatMap(list -> bookRepository.save(new Book(id, title, new Genre(null, genre), list)));
    }

    /**
     * Deletes a book-record by the specified {@code id}.
     *
     * @param id of book-record, {@code String}, not {@code null}
     */
    @Transactional
    public Mono<Void> deleteBook(String id) {
        return bookRepository.deleteById(Objects.requireNonNull(id));
    }

    private Mono<List<Author>> fetchAuthors(Set<String> authors) {
        // Note: right now there is no unique constraint on column AUTHORS.NAME !
        Flux<GroupedFlux<String, Author>> found = authorRepository.findByNameIn(authors)
                .groupBy(Author::getName, Function.identity());

        Mono<Map<String, Mono<List<Author>>>> res = found.collectMap(GroupedFlux::key, Flux::collectList)
                .doOnNext(m -> completeWithKeys(m, authors, n -> List.of(new Author(null, n))));

        return res.flatMap(m -> values(m, LibraryService::join));
    }

    private static <K, V> void completeWithKeys(Map<K, Mono<V>> map,
                                                Collection<K> keys,
                                                Function<K, ? extends V> create) {
        keys.forEach(k -> map.computeIfAbsent(k, x -> Mono.fromCallable(() -> create.apply(x))));
    }

    private static <X> List<X> join(List<X> left, List<X> right) {
        return Stream.concat(left.stream(), right.stream()).collect(Collectors.toList());
    }

    private static <X> Mono<X> join(Mono<X> left, Mono<X> right, BinaryOperator<X> combine) {
        return left.flatMap(v1 -> right.map(v2 -> combine.apply(v1, v2)));
    }

    private static <X> Mono<X> values(Map<?, Mono<X>> map, BinaryOperator<X> combine) {
        Mono<X> res = null;
        for (Mono<X> v : map.values()) {
            res = res == null ? v : join(res, v, combine);
        }
        return res;
    }

    private static int requireNonNegative(int page) {
        if (page < 0) throw new IllegalArgumentException("Negative page number : " + page);
        return page;
    }

}
