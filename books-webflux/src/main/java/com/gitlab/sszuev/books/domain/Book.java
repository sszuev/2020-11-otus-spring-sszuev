package com.gitlab.sszuev.books.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * Created by @ssz on 20.01.2021.
 */
@Document(value = "book")
public final class Book implements HasID {
    @Id
    private final String id;
    private final String title;
    private final Genre genre;
    private final List<Author> authors;

    public Book(String id, String title, Genre genre, List<Author> authors) {
        this.id = id;
        this.title = title;
        this.genre = genre;
        this.authors = authors;
    }

    @Override
    public String getID() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public Genre getGenre() {
        return genre;
    }

    public List<Author> getAuthors() {
        return authors;
    }

}
