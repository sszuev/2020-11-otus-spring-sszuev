package com.gitlab.sszuev.books.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * Created by @ssz on 20.01.2021.
 */
@Document(value = Genre.COLLECTION)
public final class Genre implements HasID {
    public static final String COLLECTION = "genre";
    public static final String NAME = "name";
    @Id
    private final String id;
    @Indexed(unique = true)
    @Field(value = Genre.NAME)
    private final String name;

    public Genre(String id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String getID() {
        return id;
    }

    public String getName() {
        return name;
    }

}
