package com.gitlab.sszuev.books.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by @ssz on 21.01.2021.
 */
@Document(value = "author")
public final class Author implements HasID {
    @Id
    private final String id;
    private final String name;

    public Author(String id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String getID() {
        return id;
    }

    public String getName() {
        return name;
    }
}
