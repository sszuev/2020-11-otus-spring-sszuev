package com.gitlab.sszuev.books.changelogs;

import com.github.cloudyrock.mongock.ChangeLog;
import com.github.cloudyrock.mongock.ChangeSet;
import com.github.cloudyrock.mongock.driver.mongodb.springdata.v3.decorator.impl.MongockTemplate;
import com.gitlab.sszuev.books.domain.Genre;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.index.Index;

/**
 * Created by @ssz on 27.01.2021.
 */
@ChangeLog(order = "001")
public class MongoDBSchemaChangeLog {
    private static final Logger LOGGER = LoggerFactory.getLogger(MongoDBSchemaChangeLog.class);

    @ChangeSet(order = "001", id = "unique_index_genre_name", author = "ssz")
    public void createUniqueIndexForGenreName(MongockTemplate template) {
        LOGGER.info("Insert unique constraint for {}:{}", Genre.COLLECTION, Genre.NAME);
        template.indexOps(Genre.COLLECTION).ensureIndex(new Index(Genre.NAME, Sort.Direction.ASC).unique());
    }
}