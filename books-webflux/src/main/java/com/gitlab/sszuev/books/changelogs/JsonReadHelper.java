package com.gitlab.sszuev.books.changelogs;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.NullNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import java.util.function.BiConsumer;

/**
 * A helper to read json string data.
 * <p>
 * Created by @ssz on 23.02.2021.
 */
public class JsonReadHelper {

    private static final Logger LOGGER = LoggerFactory.getLogger(JsonReadHelper.class);
    private final BiConsumer<String, String> processor;

    /**
     * Constructs the helper.
     *
     * @param processor {@link BiConsumer} that consumes pair of collection name and element json
     */
    public JsonReadHelper(BiConsumer<String, String> processor) {
        this.processor = Objects.requireNonNull(processor);
    }

    /**
     * Processes the data from the given {@code InputStream}.
     * Supports only {@code UTF-8}, {@code UTF-16} and {@code UTF-32}.
     *
     * @param in {@link InputStream}
     * @throws IOException something goes wrong
     */
    public void read(InputStream in) throws IOException {
        JsonParser parser = new JsonFactory().createParser(Objects.requireNonNull(in));
        ObjectMapper mapper = new ObjectMapper();
        JsonToken t = parser.nextToken();
        while (t != JsonToken.END_OBJECT) {
            if (t != JsonToken.FIELD_NAME) {
                t = parser.nextToken();
                continue;
            }
            String collectionName = parser.getValueAsString();
            LOGGER.debug("Collection: {}", collectionName);
            t = parser.nextToken();
            if (t != JsonToken.START_ARRAY) {
                throw new IllegalStateException();
            }
            while (t != JsonToken.END_ARRAY) {
                t = parser.nextToken();
                TreeNode n = mapper.readTree(parser);
                if (n instanceof NullNode) break;
                processor.accept(collectionName, n.toString());
            }
            t = parser.nextToken();
        }
    }
}
