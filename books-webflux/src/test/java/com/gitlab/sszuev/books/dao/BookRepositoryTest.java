package com.gitlab.sszuev.books.dao;

import com.gitlab.sszuev.books.TestUtils;
import com.gitlab.sszuev.books.domain.Author;
import com.gitlab.sszuev.books.domain.Book;
import com.gitlab.sszuev.books.domain.Genre;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by @ssz on 22.02.2021.
 */
public class BookRepositoryTest extends RepositoryTestBase {
    @Autowired
    private BookRepository books;

    @Test
    public void testSave() {
        String title1 = "TheBook1";
        String title2 = "TheBook2";
        String id1 = "B1";
        Author a1 = new Author("A1", "a1");
        Author a2 = new Author(null, "a2");
        Genre g1 = new Genre(null, "g1");
        Genre g2 = new Genre("G2", "g2");

        // save new with fixed id
        Book res1 = books.save(new Book(id1, title1, g1, List.of(a1, a2))).block();

        Assertions.assertNotNull(res1);
        TestUtils.assertEqualsSet(List.of(a1, a2), TestUtils.assertHasID(template.findAll(Author.class)), Author::getName);
        TestUtils.assertEqualsSet(List.of(g1), TestUtils.assertHasID(template.findAll(Genre.class)), Genre::getName);
        Assertions.assertEquals(id1, TestUtils.assertOne(TestUtils.assertHasID(template.findAll(Book.class))).getID());
        Assertions.assertEquals(title1, res1.getTitle());
        Assertions.assertEquals(id1, res1.getID());
        TestUtils.assertEqualsSet(List.of(a1, a2), TestUtils.assertHasID(res1.getAuthors()), Author::getName);
        Assertions.assertEquals(g1.getName(), TestUtils.assertHasID(res1.getGenre()).getName());

        // save new without id
        Book res2 = books.save(new Book(null, title2, g1, List.of(a1))).block();

        Assertions.assertNotNull(res2);
        Assertions.assertEquals(2, template.findAll(Author.class).size());
        Assertions.assertEquals(1, template.findAll(Genre.class).size());
        Assertions.assertEquals(2, template.findAll(Book.class).size());
        Assertions.assertEquals(title2, TestUtils.assertHasID(res2).getTitle());
        TestUtils.assertEqualsSet(List.of(a1), TestUtils.assertHasID(res2.getAuthors()), Author::getName);
        Assertions.assertEquals(g1.getName(), TestUtils.assertHasID(res2.getGenre()).getName());

        // modify existing
        Book res3 = books.save(new Book(id1, title2, g2, List.of(a2))).block();
        Assertions.assertNotNull(res3);
        Assertions.assertEquals(2, TestUtils.assertHasID(template.findAll(Author.class)).size());
        Assertions.assertEquals(2, TestUtils.assertHasID(template.findAll(Genre.class)).size());
        Assertions.assertEquals(2, TestUtils.assertHasID(template.findAll(Book.class)).size());
        Assertions.assertEquals(id1, res3.getID());
        Assertions.assertEquals(title2, res3.getTitle());
        TestUtils.assertEqualsSet(List.of(a2), TestUtils.assertHasID(res3.getAuthors()), Author::getName);
        Assertions.assertEquals(g2.getID(), res3.getGenre().getID());
        Assertions.assertEquals(g2.getName(), res3.getGenre().getName());
    }

}
