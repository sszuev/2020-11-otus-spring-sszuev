package com.gitlab.sszuev.books.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.annotation.DirtiesContext;

/**
 * Created by @ssz on 14.02.2021.
 */
@DataMongoTest
@ComponentScan(basePackages = {RepositoryTestBase.CHANGELOG_PACKAGE, RepositoryTestBase.EVENT_PACKAGE})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
abstract class RepositoryTestBase {
    static final String CHANGELOG_PACKAGE = "com.gitlab.sszuev.books.changelogs";
    static final String EVENT_PACKAGE = "com.gitlab.sszuev.books.events";
    @Autowired
    MongoTemplate template;
}
