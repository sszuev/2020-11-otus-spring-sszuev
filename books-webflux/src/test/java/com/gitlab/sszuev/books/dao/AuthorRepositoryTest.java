package com.gitlab.sszuev.books.dao;

import com.gitlab.sszuev.books.domain.Author;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.Collections;

/**
 * Created by @ssz on 22.02.2021.
 */
public class AuthorRepositoryTest extends RepositoryTestBase {
    @Autowired
    private AuthorRepository repository;

    @Test
    public void testFindIn() {
        template.insert(new Author(null, "a1"));
        template.insert(new Author(null, "a2"));
        template.insert(new Author(null, "a3"));

        Assertions.assertEquals(2, repository.findByNameIn(Arrays.asList("a1", "a2")).toStream().count());
        Assertions.assertEquals(2, repository.findByNameIn(Arrays.asList("a3", "a2", "a4")).toStream().count());
        Assertions.assertEquals(1, repository.findByNameIn(Arrays.asList("a3", "a4")).toStream().count());
        Assertions.assertEquals(1, repository.findByNameIn(Collections.singleton("a1")).toStream().count());
        Assertions.assertEquals(0, repository.findByNameIn(Collections.emptyList()).toStream().count());
        Assertions.assertEquals(0, repository.findByNameIn(Collections.singleton("x")).toStream().count());
    }
}
