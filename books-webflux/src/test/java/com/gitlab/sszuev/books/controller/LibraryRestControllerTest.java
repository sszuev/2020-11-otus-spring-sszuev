package com.gitlab.sszuev.books.controller;

import com.gitlab.sszuev.books.domain.Author;
import com.gitlab.sszuev.books.domain.Book;
import com.gitlab.sszuev.books.domain.Genre;
import com.gitlab.sszuev.books.service.LibraryService;
import com.gitlab.sszuev.books.service.PagedResult;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by @ssz on 13.02.2021.
 */
@WebFluxTest(controllers = LibraryRestController.class)
@TestPropertySource(properties = "mongock.enabled=false")
public class LibraryRestControllerTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(LibraryRestControllerTest.class);

    @Autowired
    private WebTestClient client;
    @MockBean
    private LibraryService service;

    @Test
    public void testGenres() {
        Genre g1 = new Genre("1", "G1");
        Genre g2 = new Genre("2", "G2");
        Mockito.when(service.findAllGenres()).thenReturn(Flux.just(g1, g2));

        List<Genre> res = client.get()
                .uri("/api/genres")
                .exchange()
                .expectStatus()
                .isOk()
                .expectBodyList(Genre.class)
                .returnResult()
                .getResponseBody();

        Assertions.assertNotNull(res);
        Assertions.assertEquals(2, res.size());
        Assertions.assertEquals(Set.of(g1.getID(), g2.getID()),
                res.stream().map(Genre::getID).collect(Collectors.toSet()));
        Assertions.assertEquals(Set.of(g1.getName(), g2.getName()),
                res.stream().map(Genre::getName).collect(Collectors.toSet()));
    }

    @Test
    public void testPages() {
        Genre g1 = new Genre(null, "g1");
        Author a1 = new Author(null, "a1");
        Book b1 = new Book("b1", "B1", g1, List.of(a1));
        Book b2 = new Book("b2", "B2", g1, List.of(a1));
        PagedResult<Book> p1 = PagedResult.of(1, 2, List.of(b1, b2));

        Mockito.when(service.findBookPage(0)).thenReturn(Mono.just(p1));

        @SuppressWarnings("unchecked")
        Map<String, Object> res = client.get()
                .uri("/api/books/pages/{page}", "1")
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(Map.class)
                .returnResult()
                .getResponseBody();

        Assertions.assertNotNull(res);
        Assertions.assertEquals(2, res.get("pageSize"));
        Assertions.assertEquals(1, res.get("pageCount"));
        @SuppressWarnings("unchecked") List<Object> list = (List<Object>) res.get("content");
        Assertions.assertEquals(2, list.size());
    }

    @Test
    public void testDelete() {
        String id = "bookIdToDelete";
        Mockito.when(service.deleteBook(id)).thenReturn(Mono.empty());
        client.delete()
                .uri("/api/books/{id}", id)
                .exchange()
                .expectStatus().isOk();
        Mockito.verify(service, Mockito.times(1)).deleteBook(id);
    }

    @Test
    public void testCreate() {
        testSave(null);
    }

    @Test
    public void testEdit() {
        testSave("someBookID");
    }

    private void testSave(String id) {
        String title = "b2";
        String genre = "g2";
        List<String> authors = List.of("a3", "a2");
        String request = toSaveRequest(title, genre, authors);
        Mockito.when(service.saveBook(id, title, genre, authors))
                .thenReturn(Mono.just(new Book(id == null ? "X" : id,
                        title, new Genre("Y", genre),
                        authors.stream().map(x -> new Author(x, x)).collect(Collectors.toList()))));

        LOGGER.info("Request: {}", request);
        WebTestClient.RequestBodySpec spec;
        if (id == null) {
            spec = client.post().uri("/api/books");
        } else {
            spec = client.put().uri("/api/books/{id}", id);
        }
        Book res = spec
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(request)
                .exchange()
                .expectStatus().isOk()
                .expectBody(Book.class)
                .returnResult()
                .getResponseBody();

        Mockito.verify(service, Mockito.times(1)).saveBook(id, title, genre, authors);
        Assertions.assertNotNull(res);
        Assertions.assertEquals(title, res.getTitle());
    }

    private static String toSaveRequest(String title, String genre, List<String> authors) {
        return String.format("{\"title\":\"%s\",\"genre\":\"%s\",\"authors\":%s}", title, genre,
                authors.stream().map(x -> "\"" + x + "\"").collect(Collectors.joining(", ", "[", "]")));
    }
}
