package com.gitlab.sszuev.books.dao;

import com.gitlab.sszuev.books.domain.Genre;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import reactor.test.StepVerifier;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.Predicate;

/**
 * Created by @ssz on 13.02.2021.
 */
public class GenreRepositoryTest extends RepositoryTestBase {
    @Autowired
    private GenreRepository repository;

    @Test
    public void testSave() {
        String name = "TheGenreN" + System.currentTimeMillis();
        Assertions.assertNotNull(repository.save(new Genre(null, name)).block());
        Assertions.assertThrows(DuplicateKeyException.class, () -> repository.save(new Genre(null, name)).block());
    }

    @Test
    public void testFindAll() {
        Genre g1 = template.insert(new Genre(null, "X"));
        Genre g2 = template.insert(new Genre(null, "Y"));
        Map<String, String> data = new HashMap<>(Map.of(g1.getID(), g1.getName(), g2.getID(), g2.getName()));
        Predicate<Genre> test = g -> Objects.equals(g.getName(), data.remove(g.getID()));

        StepVerifier.create(repository.findAll())
                .expectNextMatches(test)
                .expectNextMatches(test)
                .expectComplete()
                .verify();

        Assertions.assertTrue(data.isEmpty());
    }

}
