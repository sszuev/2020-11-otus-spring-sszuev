package com.gitlab.sszuev.books;

import com.gitlab.sszuev.books.domain.HasID;
import org.junit.jupiter.api.Assertions;

import java.util.Collection;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by @ssz on 14.02.2021.
 */
public class TestUtils {

    public static <X> X assertOne(Collection<X> list) {
        Assertions.assertEquals(1, list.size());
        X res = list.iterator().next();
        Assertions.assertNotNull(res);
        return res;
    }

    public static <X> void assertEqualsSet(Collection<X> expected, Collection<X> actual, Function<X, ?> get) {
        assertEqualsSet(expected, get::apply, actual, get::apply);
    }

    public static <X, Y, Z> void assertEqualsSet(Collection<X> expected, Function<X, Z> get1,
                                                 Collection<Y> actual, Function<Y, Z> get2) {
        Assertions.assertEquals(expected.size(), actual.size());
        Assertions.assertEquals(expected.stream().map(get1).collect(Collectors.toSet()),
                actual.stream().map(get2).collect(Collectors.toSet()));
    }

    public static <X extends HasID> Collection<X> assertHasID(Collection<X> entities) {
        Assertions.assertNotNull(entities);
        entities.forEach(TestUtils::assertHasID);
        return entities;
    }

    public static <X extends HasID> X assertHasID(X entity) {
        Assertions.assertNotNull(entity);
        Assertions.assertNotNull(entity.getID());
        return entity;
    }
}
