package com.gitlab.sszuev.books.service;

import com.gitlab.sszuev.books.TestUtils;
import com.gitlab.sszuev.books.dao.AuthorRepository;
import com.gitlab.sszuev.books.dao.BookRepository;
import com.gitlab.sszuev.books.domain.Author;
import com.gitlab.sszuev.books.domain.Book;
import com.gitlab.sszuev.books.domain.Genre;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.TestPropertySource;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by @ssz on 21.02.2021.
 */
@SpringBootTest
@TestPropertySource(properties = {"mongock.enabled=false", "app.web.page-size=" + LibraryServiceTest.TEST_PAGE_SIZE})
public class LibraryServiceTest {
    static final int TEST_PAGE_SIZE = 2;

    @Autowired
    private LibraryService service;
    @MockBean
    private BookRepository books;
    @MockBean
    private AuthorRepository authors;

    @Test
    public void testFindAll() {
        Genre g1 = new Genre(null, "g1");
        Genre g2 = new Genre(null, "g2");
        Author a1 = new Author(null, "a1");
        Author a2 = new Author(null, "a2");
        Book b1 = new Book("b1", "B1", g1, List.of(a1));
        Book b2 = new Book("b2", "B2", g2, List.of(a1, a2));
        Book b3 = new Book("b3", "B3", g2, List.of(a2));
        Book b4 = new Book("b4", "B4", g1, List.of(a2));
        Book b5 = new Book("b5", "B5", g2, List.of(a1));
        Mockito.when(books.count()).thenReturn(Mono.just(5L));
        Mockito.when(books.findAll(Mockito.any(Pageable.class))).thenAnswer(i -> {
            Pageable p = i.getArgument(0);
            if (p.getPageNumber() == 1) {
                return Flux.just(b1, b2);
            }
            if (p.getPageNumber() == 2) {
                return Flux.just(b3, b4);
            }
            if (p.getPageNumber() == 3) {
                return Flux.just(b5);
            }
            return Flux.empty();
        });

        testPage(service.findBookPage(1).block(), 3, TEST_PAGE_SIZE, b1, b2);
        testPage(service.findBookPage(2).block(), 3, TEST_PAGE_SIZE, b3, b4);
        testPage(service.findBookPage(3).block(), 3, TEST_PAGE_SIZE, b5);
        testPage(service.findBookPage(4).block(), 3, TEST_PAGE_SIZE);
    }

    @SuppressWarnings("SameParameterValue")
    private void testPage(PagedResult<Book> page, int numberOfChunks, int chunkSize, Book... expected) {
        Assertions.assertNotNull(page);
        Assertions.assertEquals(numberOfChunks, page.getPageCount());
        Assertions.assertEquals(chunkSize, page.getPageSize());
        Assertions.assertEquals(Set.of(expected), page.listContent().collect(Collectors.toSet()));
    }

    @Test
    public void testSave() {
        String a1 = "a1";
        String a2 = "a2";
        String g1 = "g1";
        String title = "theBook";
        Mockito.when(books.save(Mockito.any())).thenAnswer(i -> Mono.just(i.getArgument(0)));
        Mockito.when(authors.findByNameIn(Mockito.any())).thenReturn(Flux.just(new Author("a1", a1)));

        Book res = service.saveBook(null, title, g1, List.of(a1, a2)).block();
        Assertions.assertNotNull(res);
        Assertions.assertEquals(title, res.getTitle());
        Assertions.assertEquals(g1, res.getGenre().getName());
        TestUtils.assertEqualsSet(List.of(a1, a2), Function.identity(), res.getAuthors(), Author::getName);

        Mockito.verify(books, Mockito.times(1)).save(Mockito.any());
        Mockito.verify(authors, Mockito.times(1)).findByNameIn(Mockito.any());
    }

    @Test
    public void testDelete() {
        Mockito.when(books.deleteById(Mockito.anyString())).thenReturn(Mono.empty());

        service.deleteBook("X").block();

        Mockito.verify(books, Mockito.times(1)).deleteById(Mockito.anyString());
    }
}
