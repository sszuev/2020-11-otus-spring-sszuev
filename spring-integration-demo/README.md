#### This is a home-work ' Spring Integration: Endpoints & Flow Components '

A simplest console demonstration based on butterfly-lifecycle model.  
No tests, no practical applications, nothing, but demo.

###### For [OtusTeam](https://otus.ru).

##### Requirements:

- Git
- Java **11**
- Maven **3+**

##### Build and run:

```
$ git clone git@gitlab.com:sszuev/2020-11-otus-spring-sszuev.git
$ cd 2020-11-otus-spring-sszuev/spring-integration-demo
$ mvn clean package
$ java -jar target/spring-integration-demo.jar
```