package com.gitlab.sszuev.integration.services;

import com.gitlab.sszuev.integration.domain.Insect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * A service to kill the insect.
 * <p>
 * Created by @ssz on 04.04.2021.
 */
@Service
public class TerminationService implements Transformer<Insect> {
    private static final Logger LOGGER = LoggerFactory.getLogger(TerminationService.class);

    @Override
    public Insect transform(Insect insect) {
        if (insect.getStage() == null) {
            return insect;
        }
        LOGGER.info("KILL {}.", insect);
        return new Insect(insect.getId(), null);
    }
}
