package com.gitlab.sszuev.integration.core;

import com.gitlab.sszuev.integration.domain.Insect;
import com.gitlab.sszuev.integration.domain.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Created by @ssz on 04.04.2021.
 */
@Component
public class DemoProcessor {
    private static final Logger LOGGER = LoggerFactory.getLogger(DemoProcessor.class);

    private final DemoGateway gateway;
    private final InsectFactory factory;
    private final int generationNum;
    private final int numOfEggsInClutch;

    public DemoProcessor(DemoGateway gateway,
                         InsectFactory factory,
                         @Value("${app.generationNum:5}") int generationNum,
                         @Value("${app.numOfEggsInClutch:5}") int numOfEggsInClutch) {
        this.gateway = Objects.requireNonNull(gateway);
        this.factory = Objects.requireNonNull(factory);
        this.generationNum = generationNum;
        this.numOfEggsInClutch = numOfEggsInClutch;
    }

    public void process() {
        List<Insect> before = factory.newEggsClutch(numOfEggsInClutch);
        for (int i = 1; i < generationNum + 1; i++) {
            LOGGER.info("GENERATION #{} START.", i);
            LOGGER.info("#{} :: POPULATION-BEFORE = {}", i, before.size());
            List<Insect> after = gateway.process(before).stream()
                    .filter(x -> Stage.ADULT == x.getStage()).collect(Collectors.toList());
            LOGGER.info("#{} :: POPULATION-AFTER = {}", i, after.size());
            LOGGER.info("GENERATION #{} FIN.", i);
            if (after.size() == 0) {
                return;
            }
            // they survived and start spawning children for the next iteration
            before = after.stream().flatMap(x -> factory.newRandomEggsClutch(numOfEggsInClutch).stream())
                    .collect(Collectors.toList());
        }
    }

}
