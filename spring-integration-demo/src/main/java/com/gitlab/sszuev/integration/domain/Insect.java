package com.gitlab.sszuev.integration.domain;

import java.util.Objects;

/**
 * The insect.
 * <p>
 * Created by @ssz on 04.04.2021.
 */
public class Insect {

    protected final String id;
    protected final Stage stage;

    public Insect(String id, Stage stage) {
        this.id = Objects.requireNonNull(id);
        this.stage = stage;
    }

    public String getId() {
        return id;
    }

    public Stage getStage() {
        return stage;
    }

    public Stage nextStage() {
        return stage != null ? stage == Stage.ADULT ? null : Stage.values()[stage.ordinal() + 1] : null;
    }

    @Override
    public String toString() {
        return String.format("Insect{id='%s', stage=%s}", id, stage);
    }
}
