package com.gitlab.sszuev.integration.services;

import com.gitlab.sszuev.integration.domain.Insect;
import com.gitlab.sszuev.integration.domain.Stage;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by @ssz on 04.04.2021.
 */
@Service
public class KillWithProbabilityService extends TerminationService {
    private final Random random;
    private final Map<Stage, Double> probabilitiesMap;

    /**
     * @param random        {@link Random}
     * @param probabilities a {@code double[]} of probabilities
     */
    public KillWithProbabilityService(Random random,
                                      @Value("${app.stage.probabilities:0.9,0.9,0.8,0.5}") double[] probabilities) {
        this.random = Objects.requireNonNull(random);
        if (probabilities.length != Stage.values().length) {
            throw new IllegalArgumentException("Wrong probabilities array: " + Arrays.toString(probabilities));
        }
        this.probabilitiesMap = new EnumMap<>(Stage.class);
        for (Stage s : Stage.values()) {
            probabilitiesMap.put(s, probabilities[s.ordinal()]);
        }
    }

    @Override
    public Insect transform(Insect insect) {
        Stage st = insect.getStage();
        if (st == null) return insect;
        double p = probabilitiesMap.get(st);
        return random.nextDouble() > p ? super.transform(insect) : insect;
    }
}
