package com.gitlab.sszuev.integration.core;

import com.gitlab.sszuev.integration.domain.Insect;
import com.gitlab.sszuev.integration.domain.Stage;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * A factory to generate insects.
 * <p>
 * Created by @ssz on 03.04.2021.
 */
@Component
public class InsectFactory {
    private static final AtomicLong COUNTER = new AtomicLong();
    private final Random random;

    public InsectFactory(Random random) {
        this.random = Objects.requireNonNull(random);
    }

    public Insect newEgg() {
        return new Insect("BF" + COUNTER.incrementAndGet(), Stage.EGG);
    }

    public List<Insect> newRandomEggsClutch(int num) {
        return newEggsClutch(1 + random.nextInt(num));
    }

    public List<Insect> newEggsClutch(int num) {
        return IntStream.range(0, num).mapToObj(i -> newEgg()).collect(Collectors.toList());
    }
}
