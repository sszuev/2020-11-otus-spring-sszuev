package com.gitlab.sszuev.integration.core;


import com.gitlab.sszuev.integration.domain.Insect;
import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;

import java.util.Collection;

@MessagingGateway
public interface DemoGateway {

    /**
     * Performs a single lifecycle process from born to next iteration (childfree adulthood).
     *
     * @param insects a {@code Collection} of insects before lifecycle
     * @return a {@code Collection} of insects after lifecycle
     */
    @Gateway(requestChannel = "inputChannel", replyChannel = "outputChannel")
    Collection<Insect> process(Collection<Insect> insects);

}
