package com.gitlab.sszuev.integration.services;

import com.gitlab.sszuev.integration.domain.Insect;
import com.gitlab.sszuev.integration.domain.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * A service to perform insects metamorphose.
 * Created by @ssz on 04.04.2021.
 *
 * @see Stage
 */
@Service
public class MetamorphoseService implements Transformer<Insect> {
    private static final Logger LOGGER = LoggerFactory.getLogger(MetamorphoseService.class);

    @Override
    public Insect transform(Insect insect) {
        Stage next = insect.nextStage();
        if (next == null) {
            return insect;
        }
        LOGGER.info("TRANSFORM {} => {}.", insect, next);
        return new Insect(insect.getId(), next);
    }
}
