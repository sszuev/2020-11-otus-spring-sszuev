package com.gitlab.sszuev.integration.services;

/**
 * Just some abstraction.
 * Created by @ssz on 04.04.2021.
 */
interface Transformer<X> {

    X transform(X entity);
}
