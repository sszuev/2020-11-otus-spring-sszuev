package com.gitlab.sszuev.integration.domain;

/**
 * A metamorphose stage.
 * <p>
 * Created by @ssz on 04.04.2021.
 */
public enum Stage {
    EGG, LARVA, PUPA, ADULT,
    ;
}
