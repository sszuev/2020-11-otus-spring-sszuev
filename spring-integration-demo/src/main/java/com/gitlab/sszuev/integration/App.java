package com.gitlab.sszuev.integration;

import com.gitlab.sszuev.integration.core.DemoProcessor;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.MessageChannels;
import org.springframework.integration.dsl.Pollers;
import org.springframework.integration.scheduling.PollerMetadata;
import org.springframework.messaging.MessageChannel;

import java.util.Random;

@IntegrationComponentScan
@ComponentScan
@Configuration
@EnableIntegration
public class App {
    
    static {
        forciblyDisableJDKWarnings();
    }

    private static final Random RANDOM = new Random();

    public static void main(String... args) {
        try (ConfigurableApplicationContext context = SpringApplication.run(App.class, args)) {
            context.getBean(DemoProcessor.class).process();
        }
    }

    @Bean
    public IntegrationFlow lifecycleFlow() {
        return IntegrationFlows.from("inputChannel")
                .split()
                // egg -> larva
                .handle("killWithProbabilityService", "transform")
                .handle("metamorphoseService", "transform")
                // larva -> pupa
                .handle("killWithProbabilityService", "transform")
                .handle("metamorphoseService", "transform")
                // pupa -> adult
                .handle("killWithProbabilityService", "transform")
                .handle("metamorphoseService", "transform")
                // adult -> null
                .handle("killWithProbabilityService", "transform")
                .aggregate()
                .channel("outputChannel")
                .get();
    }

    @Bean
    public MessageChannel inputChannel() {
        return MessageChannels.queue(10).get();
    }

    @Bean
    public MessageChannel outputChannel() {
        return MessageChannels.publishSubscribe().get();
    }

    @Bean(name = PollerMetadata.DEFAULT_POLLER)
    public PollerMetadata poller() {
        return Pollers.fixedRate(100).maxMessagesPerPoll(2).get();
    }

    @Bean
    public Random random() {
        return RANDOM;
    }

    /**
     * Workaround to suppress warnings.
     * Checked on jdk11 (11.0.9), win10x64
     *
     * @see <a href='https://stackoverflow.com/questions/46454995/'>SO</a>
     */
    private static void forciblyDisableJDKWarnings() {
        try {
            java.lang.reflect.Field field = sun.misc.Unsafe.class.getDeclaredField("theUnsafe");
            field.setAccessible(true);
            sun.misc.Unsafe unsafe = (sun.misc.Unsafe) field.get(null);
            Class<?> clazz = Class.forName("jdk.internal.module.IllegalAccessLogger");
            java.lang.reflect.Field logger = clazz.getDeclaredField("logger");
            unsafe.putObjectVolatile(clazz, unsafe.staticFieldOffset(logger), null);
        } catch (Exception ignore) {
            // ignore
        }
    }
}
