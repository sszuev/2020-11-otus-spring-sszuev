#### This is a home-work 'Application for testing students'

There is nothing interesting here: if you are not from [otus](https://otus.ru) team please ignore it.

##### Requirements:

- Git
- Java **11**
- Maven **3+**

##### Build and run:

```
$ git clone git@gitlab.com:sszuev/2020-11-otus-spring-sszuev.git
$ cd 2020-11-otus-spring-sszuev/students-testing-spring-boot
$ mvn clean package
$ java -jar target/students-testing.jar