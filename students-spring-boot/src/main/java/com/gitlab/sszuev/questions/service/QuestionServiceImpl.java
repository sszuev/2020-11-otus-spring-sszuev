package com.gitlab.sszuev.questions.service;

import com.gitlab.sszuev.questions.dao.QuestionsDAO;
import com.gitlab.sszuev.questions.domain.Answer;
import com.gitlab.sszuev.questions.domain.Question;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.stream.Stream;

/**
 * Created by @ssz on 09.12.2020.
 */
@Service
public class QuestionServiceImpl implements QuestionService {

    private final QuestionsDAO repo;

    public QuestionServiceImpl(QuestionsDAO repo) {
        this.repo = Objects.requireNonNull(repo);
    }

    @Override
    public Stream<Question> questions() {
        return repo.findAllQuestions();
    }

    @Override
    public Stream<Answer> answers(Question q) {
        return repo.findAllAnswersByQuestionId(q);
    }

}
