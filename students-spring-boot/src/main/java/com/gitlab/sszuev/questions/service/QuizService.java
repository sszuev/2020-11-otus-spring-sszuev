package com.gitlab.sszuev.questions.service;

/**
 * Created by @ssz on 09.12.2020.
 */
public interface QuizService {

    /**
     * Performs the student testing.
     *
     * @return {@code long}, the total score
     * @throws BusinessException in case of any error
     */
    long process() throws BusinessException;
}
