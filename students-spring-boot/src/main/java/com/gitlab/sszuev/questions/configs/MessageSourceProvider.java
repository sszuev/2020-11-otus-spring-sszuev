package com.gitlab.sszuev.questions.configs;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.AbstractResourceBasedMessageSource;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

import java.nio.charset.StandardCharsets;
import java.util.Objects;

/**
 * To provide access to <b>{@code file:/app.properties}</b> bundle.
 * <p>
 * Created by @ssz on 06.12.2020.
 */
@Configuration
public class MessageSourceProvider {

    @Bean
    public MessageSource messageSource(AppConfig config) {
        String file = Objects.requireNonNull(config.getSettings().getProperties());
        AbstractResourceBasedMessageSource res = new ReloadableResourceBundleMessageSource();
        res.setBasename(file);
        res.setDefaultEncoding(StandardCharsets.UTF_8.name());
        return res;
    }
}
