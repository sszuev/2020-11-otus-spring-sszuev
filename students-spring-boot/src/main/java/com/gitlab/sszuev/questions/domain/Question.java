package com.gitlab.sszuev.questions.domain;

import java.util.Objects;

/**
 * A domain entity that describes a student testing question item.
 * Created by @ssz on 06.12.2020.
 */
public final class Question implements HasID, HasMessage { // final since it is a part of model
    private final long id;
    private final String txt;

    public Question(long id, String txt) {
        this.id = id;
        this.txt = Objects.requireNonNull(txt);
    }

    @Override
    public Long getID() {
        return id;
    }

    @Override
    public String getMessage() {
        return txt;
    }

    @Override
    public String toString() {
        return String.format("Question{id=%d, txt='%s'}", id, txt);
    }
}
