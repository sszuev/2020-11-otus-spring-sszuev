package com.gitlab.sszuev.questions.configs;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Locale;

/**
 * To handle <b>{@code file:/application.yml}</b>.
 * <p>
 * Created by @ssz on 06.12.2020.
 */
@SuppressWarnings("unused")
@ConfigurationProperties(prefix = "app")
public class AppConfig {
    private Locale locale;
    private Settings settings;

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    public Settings getSettings() {
        return settings;
    }

    public void setSettings(Settings settings) {
        this.settings = settings;
    }

    public static class Settings {
        private String properties;
        private Integer timeoutInSec;

        public String getProperties() {
            return properties;
        }

        public void setProperties(String properties) {
            this.properties = properties;
        }

        public Integer getTimeoutInSec() {
            return timeoutInSec;
        }

        public void setTimeoutInSec(int timeout) {
            this.timeoutInSec = timeout;
        }
    }
}
