package com.gitlab.sszuev.questions.service;

import java.util.concurrent.TimeUnit;

/**
 * A service that responsible for sending and receiving text messages from some UI.
 * <p>
 * Created by @ssz on 09.12.2020.
 */
public interface UserController {

    /**
     * Sends the message.
     *
     * @param message {@code String}, not {@code null}
     * @throws BusinessException if an I/O error occurs
     */
    void say(String message) throws BusinessException;

    /**
     * Waits if necessary for at most the given time for the receiving message.
     *
     * @param timeout the maximum time to wait, positive {@code long}
     * @param unit    {@link TimeUnit}, the time unit of the timeout argument, not {@code null}
     * @return {@code String}, the received message
     * @throws BusinessException if an I/O error occurs or the wait timed out
     */
    String receive(long timeout, TimeUnit unit) throws BusinessException;
}
