package com.gitlab.sszuev.questions.service;

import com.gitlab.sszuev.questions.configs.AppConfig;
import com.gitlab.sszuev.questions.domain.Question;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

/**
 * Created by @ssz on 09.12.2020.
 */
@Service
public class QuizServiceImpl implements QuizService {

    private final QuestionService questionService;
    private final UserController userController;
    private final AppConfig config;
    private final MessageSource messages;

    public QuizServiceImpl(QuestionService questionService,
                           UserController userController,
                           AppConfig config,
                           MessageSource messages) {
        this.questionService = Objects.requireNonNull(questionService);
        this.userController = Objects.requireNonNull(userController);
        this.config = Objects.requireNonNull(config);
        this.messages = Objects.requireNonNull(messages);
    }

    private long getTimeoutInSec() {
        return Optional.ofNullable(config.getSettings())
                .map(AppConfig.Settings::getTimeoutInSec)
                .filter(x -> x > 0)
                .map(Integer::longValue)
                .orElse(Long.MAX_VALUE);
    }

    private String getMessage(String key, Object... args) {
        return Objects.requireNonNull(messages.getMessage(key, args, config.getLocale()), "Can't find key for " + key);
    }

    @Override
    public long process() throws BusinessException {
        try {
            return processQuestions(getTimeoutInSec());
        } catch (BusinessException e) {
            throw e;
        } catch (Exception e) {
            throw new BusinessException(BusinessException.Code.UNKNOWN, e);
        }
    }

    public long processQuestions(long timeoutInSeconds) {
        userController.say(getMessage("quiz.say.greeting"));
        userController.say(getMessage("quiz.ask.name"));
        String name = userController.receive(timeoutInSeconds, TimeUnit.SECONDS);
        long score = 0;
        long count = 0;
        Iterator<Question> questions = questionService.questions().iterator();
        while (questions.hasNext()) {
            Question q = questions.next();
            userController.say(getMessage("quiz.say.question", ++count, q.getMessage()));
            String res = userController.receive(timeoutInSeconds, TimeUnit.SECONDS);
            score += questionService.evaluate(q, res);
        }
        userController.say(getMessage("quiz.say.score", name, score, count));
        userController.say(getMessage("quiz.say.goodbye"));
        return score;
    }
}
