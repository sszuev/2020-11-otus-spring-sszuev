package com.gitlab.sszuev.questions.dao;

import com.gitlab.sszuev.questions.configs.AppConfig;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * A helper to parse CSV-file.
 * <p>
 * Created by @ssz on 06.12.2020.
 */
@Component
public class CSVResource {

    private final Resource resource;

    @Autowired
    public CSVResource(ResourceLoader loader, AppConfig config, MessageSource messages) {
        this(loader, messages.getMessage("data.location", null, config.getLocale()));
    }

    public CSVResource(ResourceLoader loader, String location) {
        this(loader.getResource(location));
    }

    public CSVResource(Resource resource) {
        this.resource = Objects.requireNonNull(resource);
    }

    /**
     * Loads records from the encapsulated resource.
     * A caller is responsible for {@code Stream} closing.
     *
     * @return a {@link Stream} of {@link Record}s (<b>please don't forget to call {@link Stream#close()}</b>)
     */
    public Stream<Record> load() {
        CSVParser p = createParser();
        return StreamSupport.stream(p.spliterator(), false)
                .map(this::parseRecord)
                .onClose(() -> {
                    try {
                        p.close();
                    } catch (IOException e) {
                        throw new UncheckedIOException(e);
                    }
                });
    }

    private CSVParser createParser() {
        try {
            return CSVParser.parse(resource.getInputStream(), StandardCharsets.UTF_8, CSVFormat.DEFAULT);
        } catch (IOException e) {
            throw new IllegalStateException("Can't parse " + resource, e);
        }
    }

    private Record parseRecord(CSVRecord record) {
        try {
            return new Record(Long.parseLong(record.get(0)), record.get(1), record.get(2).split(",\\s*"));
        } catch (RuntimeException e) {
            throw new IllegalStateException("Can't parse record " + record, e);
        }
    }

    /**
     * Represents a csv application record.
     */
    public static class Record {
        private final Long id;
        private final String question;
        private final String[] answers;

        private Record(Long id, String question, String[] answers) {
            this.id = id;
            this.question = question;
            this.answers = answers;
        }

        public Long getId() {
            return id;
        }

        public String getQuestion() {
            return question;
        }

        public Stream<String> answers() {
            return Arrays.stream(answers);
        }

        @Override
        public String toString() {
            return String.format("Record{id=%d, question='%s', answers=%s}", id, question, Arrays.toString(answers));
        }
    }

}
