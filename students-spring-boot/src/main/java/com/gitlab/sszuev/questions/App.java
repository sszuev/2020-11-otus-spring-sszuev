package com.gitlab.sszuev.questions;

import com.gitlab.sszuev.questions.configs.AppConfig;
import com.gitlab.sszuev.questions.service.QuizService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

/**
 * Created by @ssz on 06.12.2020.
 */
@SpringBootApplication
@EnableConfigurationProperties(AppConfig.class)
public class App {
    @Autowired
    private QuizService service;

    public static void main(String... args) {
        SpringApplication.run(App.class, args);
    }

    @Bean
    @ConditionalOnProperty(prefix = "app.console", name = "enabled", havingValue = "true", matchIfMissing = true)
    public CommandLineRunner processor() {
        return args -> service.process();
    }
}
