package com.gitlab.sszuev.questions.service;

import java.util.Objects;

/**
 * Created by @ssz on 09.12.2020.
 */
public class BusinessException extends RuntimeException {
    private final Code code;

    public BusinessException(Code code, Throwable cause) {
        this(code, code.name(), cause);
    }

    public BusinessException(Code code, String message, Throwable cause) {
        super(message, cause);
        this.code = Objects.requireNonNull(code);
    }

    public Code getCode() {
        return code;
    }

    public enum Code {
        WRONG_INPUT,
        IO_ERROR,
        TIMEOUT_ERROR,
        UNKNOWN,
    }
}
