package com.gitlab.sszuev.questions.service;

import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.Scanner;
import java.util.concurrent.*;

/**
 * Implementation that is based on standard {@link System#in input} and {@link System#out output}.
 * <p>
 * Created by @ssz on 09.12.2020.
 *
 * @see <a href='https://gitlab.com/sszuev/2020-11-otus-spring-sszuev/-/blob/master/students-testing/src/main/java/com/gitlab/sszuev/questions/impl/service/UserControllerImpl.java'>students-testing -- UserControllerImpl</a>
 */
@Service
public class UserControllerImpl implements UserController {

    @Override
    public void say(String message) {
        System.out.println(Objects.requireNonNull(message));
    }

    @Override
    public String receive(long timeout, TimeUnit unit) throws BusinessException {
        try {
            return read(System.in, StandardCharsets.UTF_8, timeout, unit);
        } catch (IOException e) {
            throw new BusinessException(BusinessException.Code.IO_ERROR, e);
        } catch (TimeoutException e) {
            throw new BusinessException(BusinessException.Code.TIMEOUT_ERROR, e);
        } catch (IllegalArgumentException e) {
            throw new BusinessException(BusinessException.Code.WRONG_INPUT, e);
        }
    }

    public String read(InputStream in, Charset ch, long timeout, TimeUnit unit) throws IOException, TimeoutException {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        Future<String> res = executor.submit(() -> readLine(in, ch, timeout, unit));
        try {
            return res.get(timeout, unit);
        } catch (TimeoutException e) {
            res.cancel(true);
            throw e;
        } catch (ExecutionException e) {
            Throwable t = e.getCause();
            if (t instanceof IOException) {
                throw (IOException) t;
            }
            if (t instanceof TimeoutException) {
                throw (TimeoutException) t;
            }
            throw new IllegalStateException(t == null ? e : t);
        } catch (Throwable e) {
            throw new IllegalStateException(e);
        } finally {
            executor.shutdownNow();
        }
    }

    public String readLine(InputStream in, Charset ch, long timeout, TimeUnit unit) throws TimeoutException {
        if (timeout <= 0) {
            throw new IllegalArgumentException("Non-positive timeout " + timeout);
        }
        long begin = System.currentTimeMillis();
        long limit = unit.toMillis(timeout);
        Scanner sc = new Scanner(in, ch);
        while (System.currentTimeMillis() - begin < limit) {
            if (sc.hasNext()) {
                return sc.nextLine();
            }
        }
        throw new TimeoutException("Waiting limit (" + limit + " ms) is exceeded.");
    }

}
