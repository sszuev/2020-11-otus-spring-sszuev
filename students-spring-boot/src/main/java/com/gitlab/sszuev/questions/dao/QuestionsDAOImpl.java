package com.gitlab.sszuev.questions.dao;

import com.gitlab.sszuev.questions.domain.Answer;
import com.gitlab.sszuev.questions.domain.Question;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Simple {@link QuestionsDAO} implementation that holds all data in memory.
 * Created by @ssz on 06.12.2020.
 */
@Component
public class QuestionsDAOImpl implements QuestionsDAO {
    private final Map<Question, List<Answer>> data;

    public QuestionsDAOImpl(CSVResource resource) {
        this.data = toMap(resource);
    }

    private static Map<Question, List<Answer>> toMap(CSVResource resource) {
        try (Stream<CSVResource.Record> records = resource.load()) {
            return records.collect(Collectors.toMap(QuestionsDAOImpl::toQuestion, QuestionsDAOImpl::toAnswers));
        }
    }

    private static Question toQuestion(CSVResource.Record r) {
        return new Question(r.getId(), r.getQuestion());
    }

    private static List<Answer> toAnswers(CSVResource.Record r) {
        List<String> answers = r.answers().collect(Collectors.toList());
        List<Answer> res = new ArrayList<>();
        for (int i = 0; i < answers.size(); i++) {
            res.add(new Answer(i + 1, answers.get(i)));
        }
        return res;
    }

    @Override
    public Stream<Question> findAllQuestions() {
        return data.keySet().stream();
    }

    @Override
    public Stream<Answer> findAllAnswersByQuestionId(Question question) {
        return Optional.ofNullable(question).map(data::get).stream().flatMap(Collection::stream);
    }
}
