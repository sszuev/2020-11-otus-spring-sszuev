package com.gitlab.sszuev.questions.domain;

/**
 * <b>Technical</b> interface for combining entities tha have message.
 * <p>
 * Created by @ssz on 06.12.2020.
 */
public interface HasMessage {
    /**
     * Returns a text message which this object encapsulates.
     *
     * @return {@code String}
     */
    String getMessage();
}
