package com.gitlab.sszuev.questions.domain;

/**
 * <b>Technical</b> interface for combining entities tha have id.
 * <p>
 * Created by @ssz on 06.12.2020.
 */
public interface HasID {
    /**
     * Returns an identifier of entity.
     *
     * @return {@code Long}
     */
    Long getID();

}
