package com.gitlab.sszuev.questions.service;

import com.gitlab.sszuev.questions.domain.Answer;
import com.gitlab.sszuev.questions.domain.Question;

import java.util.Objects;
import java.util.stream.Stream;

/**
 * The main service that provides access to the question data.
 * <p>
 * Created by @ssz on 09.12.2020.
 */
public interface QuestionService {

    /**
     * Lists all questions from the system.
     *
     * @return a {@code Stream} of {@link Question}s
     */
    Stream<Question> questions();

    /**
     * Lists all <b>correct</b> answers for the given question.
     * A student must choose one of this list.
     *
     * @param question {@link Question}, not {@code null}
     * @return a {@code Stream} of {@link Answer}s
     */
    Stream<Answer> answers(Question question);

    /**
     * Gets the question by its id.
     *
     * @param id {@code long}
     * @return {@link Question} or {@code null} if not found
     */
    default Question getQuestion(long id) {
        return questions().filter(x -> Objects.equals(x.getID(), id)).findFirst().orElse(null);
    }

    /**
     * Evaluates the given {@code answer} for the specified {@code question}.
     * Returns the test score as a positive integer number.
     * The score {@code 0} means the answer is wrong.
     * <p>
     * The default implementation just compares in case insensitive manner the given answer and the answer from db.
     *
     * @param question {@link Question}, not {@code null}
     * @param answer   {@code String}, the answer, not {@code null}
     * @return {@code int}, test score
     */
    default int evaluate(Question question, String answer) {
        String txt = Objects.requireNonNull(answer).toLowerCase().trim();
        return answers(Objects.requireNonNull(question)).map(Answer::getMessage)
                .map(x -> x.toLowerCase().trim())
                .anyMatch(txt::equals) ? 1 : 0;
    }
}
