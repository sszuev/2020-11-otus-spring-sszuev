package com.gitlab.sszuev.questions.service;

import com.gitlab.sszuev.questions.App;
import com.gitlab.sszuev.questions.domain.Question;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.stream.Stream;

/**
 * Created by @ssz on 09.12.2020.
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {App.class, QuizServiceTest.TestConfig.class})
public class QuizServiceTest {

    @Autowired
    private QuizService service;
    @Autowired
    private QuestionService questionService;
    @Autowired
    private UserController userController;

    @Test
    public void testProcessSendMessageException() {
        Mockito.doThrow(new UncheckedIOException(new IOException("XXX"))).when(userController).say(Mockito.anyString());
        Assertions.assertEquals(BusinessException.Code.UNKNOWN, Assertions.assertThrows(BusinessException.class, service::process).getCode());
    }

    @Test
    public void testProcessSuccessScoring() {
        int scorePerEvaluate = 42;
        Mockito.when(userController.receive(Mockito.anyLong(), Mockito.any())).thenReturn("name", "answer1", "answer2");

        Mockito.when(questionService.evaluate(Mockito.any(), Mockito.anyString())).thenReturn(scorePerEvaluate);

        Question q1 = newQuestion(1, "A");
        Mockito.when(questionService.questions()).thenReturn(Stream.of(q1));
        Assertions.assertEquals(scorePerEvaluate, service.process());
        Mockito.verify(questionService, Mockito.times(1)).evaluate(Mockito.any(), Mockito.anyString());

        Question q2 = newQuestion(2, "B");
        Mockito.when(questionService.questions()).thenReturn(Stream.of(q1, q2));
        Assertions.assertEquals(scorePerEvaluate * 2, service.process());
        Mockito.verify(questionService, Mockito.times(3)).evaluate(Mockito.any(), Mockito.anyString());
    }

    private static Question newQuestion(long id, String q) {
        return new Question(id, q);
    }

    @TestConfiguration
    static class TestConfig {

        @Bean
        public QuestionService questionService() {
            return Mockito.mock(QuestionService.class);
        }

        @Bean
        public UserController userController() {
            return Mockito.mock(UserController.class);
        }
    }

}
