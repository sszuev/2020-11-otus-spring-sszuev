package com.gitlab.sszuev.questions.service;

import com.gitlab.sszuev.questions.App;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeUnit;

/**
 * To test {@link UserController}.
 * <p>
 * Created by @ssz on 09.12.2020.
 *
 * @see <a href='https://gitlab.com/sszuev/2020-11-otus-spring-sszuev/-/blob/master/students-testing/src/test/java/com/gitlab/sszuev/questions/impl/service/UserControllerImplTest.java'>students-testing -- UserControllerImplTest</a>
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = App.class)
@TestPropertySource("classpath:test.properties")
public class UserControllerTest {
    private static final PrintStream SYS_OUT = System.out;
    private static final InputStream SYS_IN = System.in;

    @Autowired
    private UserController service;

    @AfterEach
    void restoreSysOut() {
        System.setOut(SYS_OUT);
    }

    @AfterEach
    void restoreSysIn() {
        System.setIn(SYS_IN);
    }

    @Test
    public void testSend() {
        String data = "The test message";

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(out, true, StandardCharsets.UTF_8));

        service.say(data);
        String actual = out.toString(StandardCharsets.UTF_8);
        String expected = data + System.lineSeparator();
        Assertions.assertEquals(expected, actual);
    }

    @Timeout(5)
    @Test
    public void testReceiveTimeoutExceeded() {
        BusinessException.Code actual = Assertions.assertThrows(BusinessException.class,
                () -> service.receive(1, TimeUnit.SECONDS)).getCode();
        Assertions.assertEquals(BusinessException.Code.TIMEOUT_ERROR, actual);
    }

    @Timeout(5)
    @Test
    public void testReceiveWithTimeout() {
        String data = "The test message";

        InputStream in = new ByteArrayInputStream(data.getBytes());
        System.setIn(in);

        String actual = service.receive(1, TimeUnit.SECONDS);
        Assertions.assertEquals(data, actual);
    }

}
