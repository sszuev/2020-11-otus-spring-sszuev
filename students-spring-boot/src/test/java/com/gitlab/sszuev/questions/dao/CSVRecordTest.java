package com.gitlab.sszuev.questions.dao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * See <b>{@code ./data.csv}</b>
 * <p>
 * Created by @ssz on 06.12.2020.
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest
@TestPropertySource("classpath:test.properties")
public class CSVRecordTest {

    @Autowired
    private CSVResource resource;

    @Test
    public void testLoad() {
        Assertions.assertEquals(2, resource.load().peek(this::testRecord).count());

        List<CSVResource.Record> actual = resource.load().collect(Collectors.toList());
        Assertions.assertEquals(2, actual.size());
        Assertions.assertEquals(Arrays.asList(3L, 4L), actual.stream()
                .map(CSVResource.Record::getId).collect(Collectors.toList()));
        Assertions.assertEquals(5, actual.get(0).answers().count());
        Assertions.assertEquals(1, actual.get(1).answers().count());
    }

    private void testRecord(CSVResource.Record record) {
        Assertions.assertNotNull(record.getId());
        Assertions.assertNotNull(record.getQuestion());
        Assertions.assertTrue(record.answers().count() > 0);
    }
}
