package com.gitlab.sszuev.books.dao.impl;

import com.gitlab.sszuev.books.dao.GenreRepository;
import com.gitlab.sszuev.books.domain.Genre;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.stream.Stream;

/**
 * Created by @ssz on 01.01.2021.
 */
@Repository
public class GenreRepositoryImpl implements GenreRepository {
    @PersistenceContext
    private EntityManager em;

    @Override
    public Stream<Genre> listByName(String name) {
        return getByNameQuery(name).getResultStream();
    }

    @Override
    public List<Genre> getByName(String name) {
        return getByNameQuery(name).getResultList();
    }

    private TypedQuery<Genre> getByNameQuery(String name) {
        TypedQuery<Genre> query = em.createQuery("SELECT g FROM Genre g WHERE g.name = :name", Genre.class);
        query.setParameter("name", name);
        return query;
    }

    @Override
    public Stream<Genre> listAll() {
        return getAllQuery().getResultStream();
    }

    @Override
    public List<Genre> getAll() {
        return getAllQuery().getResultList();
    }

    private TypedQuery<Genre> getAllQuery() {
        return em.createQuery("SELECT g FROM Genre g", Genre.class);
    }

    @Override
    public boolean deleteById(long id) {
        Genre g = em.find(Genre.class, id);
        if (g == null) {
            // nothing found
            return false;
        }
        // Note: entity 'Genre' has no reference on entity 'Book' (but 'Book' has)
        Query q = em.createQuery("DELETE FROM Genre g WHERE g.id = :id " +
                "AND (SELECT count(b) FROM Book b WHERE b.genre.id = :id) = 0");
        q.setParameter("id", id);
        if (q.executeUpdate() > 0) {
            // remove also from the cache (to be sure there is no "zombie" entities)
            em.detach(g);
            return true;
        }
        // entity is found but cannot be deleted since it is bound on FK constraint
        return false;
    }
}
