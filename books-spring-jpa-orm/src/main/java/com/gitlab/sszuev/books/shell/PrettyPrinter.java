package com.gitlab.sszuev.books.shell;

import com.gitlab.sszuev.books.dto.AuthorRecord;
import com.gitlab.sszuev.books.dto.BookRecord;
import com.gitlab.sszuev.books.dto.CommentRecord;
import com.gitlab.sszuev.books.dto.GenreRecord;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.time.format.DateTimeFormatter;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Utils to print formatted output.
 * <p>
 * Created by @ssz on 30.12.2020.
 */
@Component
public class PrettyPrinter {

    private static final int FIRST_COL = 5;
    private static final int SECOND_COL = 40;
    private static final int THIRD_COL = 30;
    private static final int FOURTH_COL = 30;
    private static final DateTimeFormatter DT_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");

    private static String quote(String str) {
        return str.startsWith("\"") ? str : String.format("\"%s\"", str);
    }

    public String printBookHeader() {
        return printRow("ID", "TITLE", "GENRE", "AUTHORS");
    }

    public String printGenreHeader() {
        return printRow("ID", "GENRE");
    }

    public String printAuthorHeader() {
        return printRow("ID", "AUTHOR");
    }

    public String printCommentHeader() {
        return printRow("ID", "COMMENT", "TIMESTAMP", "BOOK");
    }

    public String printSeparator() {
        return "-".repeat(FIRST_COL + SECOND_COL + THIRD_COL + FOURTH_COL);
    }

    public String printBookRow(BookRecord book) {
        return printRow(String.valueOf(book.getId()), quote(book.getTitle()),
                book.getGenre().getName(), printAuthors(book.listAuthors().map(AuthorRecord::getName)));
    }

    public String printGenre(GenreRecord genre) {
        return printRow(String.valueOf(genre.getId()), genre.getName());
    }

    public String printAuthor(AuthorRecord author) {
        return printRow(String.valueOf(author.getId()), author.getName());
    }

    public String printComment(CommentRecord comment) {
        return printRow(String.valueOf(comment.getId()), quote(comment.getMessage()),
                DT_FORMATTER.format(comment.getTimestamp()), String.valueOf(comment.getBookId()));
    }

    protected String printRow(String first, String second) {
        return String.format("%s | %s", StringUtils.rightPad(first, FIRST_COL), StringUtils.rightPad(second, SECOND_COL));
    }

    protected String printRow(String first, String second, String third, String fourth) {
        return String.format("%s | %s | %s | %s",
                StringUtils.rightPad(first, FIRST_COL),
                StringUtils.rightPad(second, SECOND_COL),
                StringUtils.rightPad(third, THIRD_COL),
                StringUtils.rightPad(fourth, FOURTH_COL));
    }

    private String printAuthors(Stream<String> authors) {
        return authors.collect(Collectors.joining("; "));
    }
}
