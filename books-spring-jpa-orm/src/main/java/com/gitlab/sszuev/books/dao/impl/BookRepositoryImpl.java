package com.gitlab.sszuev.books.dao.impl;

import com.gitlab.sszuev.books.dao.BookRepository;
import com.gitlab.sszuev.books.domain.Book;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * Created by @ssz on 29.12.2020.
 */
@Repository
public class BookRepositoryImpl implements BookRepository {
    @PersistenceContext
    private EntityManager em;

    @Override
    public Optional<Book> findById(long id) {
        return Optional.ofNullable(em.find(Book.class, id));
    }

    @Override
    public <S extends Book> S save(S book) {
        if (book.getID() == null) {
            em.persist(book);
            return book;
        }
        return em.merge(book);
    }

    @Override
    public boolean deleteById(long id) {
        Book b = em.find(Book.class, id);
        if (b == null) {
            return false;
        }
        em.remove(b);
        return true;
    }

    @Override
    public Stream<Book> listAll() {
        return getAllQuery().getResultStream();
    }

    @Override
    public List<Book> getAll() {
        return getAllQuery().getResultList();
    }

    private TypedQuery<Book> getAllQuery() {
        // genre is unique
        return em.createQuery("SELECT b FROM Book b JOIN FETCH b.genre", Book.class);
    }

}
