package com.gitlab.sszuev.books.dao;

import com.gitlab.sszuev.books.domain.Book;

import java.util.Optional;

/**
 * A DAO that provides CRUD operations for {@link Book}s instances.
 * Created by @ssz on 29.12.2020.
 */
public interface BookRepository extends ReadAllRepository<Book>, SaveRepository<Book>, DeleteRepository {

    /**
     * Retrieves a book entity by its id.
     *
     * @param id {@code long} - an entity id
     * @return a {@code Optional} around of {@link Book}, possibly empty
     */
    Optional<Book> findById(long id);

    /**
     * Returns an entity by its id.
     *
     * @param id {@code long} - an entity id
     * @return {@link Book}, never {@code null}
     * @throws IllegalStateException in case no entity was found
     */
    default Book getById(long id) {
        return findById(id).orElseThrow(() -> new IllegalStateException("Can't find entity with Id " + id));
    }
}
