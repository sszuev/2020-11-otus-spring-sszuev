package com.gitlab.sszuev.books.dao;

import com.gitlab.sszuev.books.domain.Genre;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by @ssz on 01.01.2021.
 */
public interface GenreRepository extends ReadAllRepository<Genre>, DeleteRepository {
    /**
     * Lists all genres from the underlying storage by the specified name.
     *
     * @param name {@code String}, not {@code null}
     * @return a {@code Stream} of {@link Genre}s
     */
    Stream<Genre> listByName(String name);

    /**
     * Gets all genres from the underlying storage by the specified name.
     *
     * @param name {@code String}, not {@code null}
     * @return a {@code List} of {@link Genre}s
     */
    default List<Genre> getByName(String name) {
        return listByName(name).collect(Collectors.toList());
    }
}
