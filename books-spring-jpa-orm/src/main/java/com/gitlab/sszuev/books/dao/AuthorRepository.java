package com.gitlab.sszuev.books.dao;

import com.gitlab.sszuev.books.domain.Author;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by @ssz on 01.01.2021.
 */
public interface AuthorRepository extends ReadAllRepository<Author>, DeleteRepository {

    /**
     * Lists all author entities which names are in the given list.
     *
     * @param names a {@code List} of {@code String}-names to check, not {@code null}
     * @return a {@code Stream} of {@link Author}s
     */
    Stream<Author> listWithNameIn(List<String> names);

    /**
     * Gets all author entities which names are in the given list.
     *
     * @param names a {@code List} of {@code String}-names to check, not {@code null}
     * @return a {@code List} of {@link Author}s
     */
    default List<Author> getWithNameIn(List<String> names) {
        return listWithNameIn(names).collect(Collectors.toList());
    }

    /**
     * Lists all author entities which names are in the given list.
     *
     * @param names an {@code Array} of {@code String}-names to check, not {@code null}
     * @return a {@code Stream} of {@link Author}s
     */
    default Stream<Author> listWithNameIn(String... names) {
        return listWithNameIn(List.of(names));
    }
}
