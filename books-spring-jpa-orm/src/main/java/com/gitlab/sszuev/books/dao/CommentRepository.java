package com.gitlab.sszuev.books.dao;

import com.gitlab.sszuev.books.domain.Comment;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by @ssz on 04.01.2021.
 */
public interface CommentRepository extends ReadAllRepository<Comment>, SaveRepository<Comment>, DeleteRepository {

    /**
     * Finds the last (by {@link Comment#getDate() date}) comment on the specified book.
     *
     * @param bookId {@code long}, the book-record identifier
     * @return a {@code Optional} around of {@link Comment}
     */
    Optional<Comment> findLast(long bookId);

    /**
     * Lists all comments from the underlying storage.
     *
     * @param bookId {@code long}, the book-record identifier
     * @return a {@code Stream} of {@link Comment}s
     */
    Stream<Comment> listByBook(long bookId);

    /**
     * Gets all comments as {@code List}.
     *
     * @param bookId {@code long}, the book-record identifier
     * @return a {@code List} of {@link Comment}s
     */
    default List<Comment> getByBook(long bookId) {
        return listByBook(bookId).collect(Collectors.toList());
    }
}
