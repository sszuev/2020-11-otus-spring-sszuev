package com.gitlab.sszuev.books.domain;

import javax.persistence.*;

/**
 * A domain entity that describes an author item.
 * <p>
 * Created by @ssz on 29.12.2020.
 */
@Entity
@Table(name = "authors")
public final class Author implements HasID {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "name", nullable = false)
    private String name;

    @Override
    public Long getID() {
        return id;
    }

    @Override
    public void setID(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
