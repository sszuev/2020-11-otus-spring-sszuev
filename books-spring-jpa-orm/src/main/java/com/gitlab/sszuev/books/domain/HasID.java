package com.gitlab.sszuev.books.domain;

/**
 * The technical interface for combining entities that have {@code long} id.
 * <p>
 * Created by @ssz on 29.12.2020.
 */
public interface HasID {
    /**
     * Returns the unique identifier of the entity.
     *
     * @return {@code Long}
     */
    Long getID();

    /**
     * Sets the specified unique identifier to the entity.
     *
     * @param id {@code Long}
     */
    void setID(Long id);
}
