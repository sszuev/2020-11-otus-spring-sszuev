package com.gitlab.sszuev.books.dao.impl;

import com.gitlab.sszuev.books.dao.AuthorRepository;
import com.gitlab.sszuev.books.domain.Author;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.stream.Stream;

/**
 * Created by @ssz on 01.01.2021.
 */
@Repository
public class AuthorRepositoryImpl implements AuthorRepository {
    @PersistenceContext
    private EntityManager em;

    @Override
    public Stream<Author> listWithNameIn(List<String> names) {
        return getWithNameInQuery(names).getResultStream();
    }

    @Override
    public List<Author> getWithNameIn(List<String> names) {
        return getWithNameInQuery(names).getResultList();
    }

    private TypedQuery<Author> getWithNameInQuery(List<String> names) {
        TypedQuery<Author> query = em.createQuery("SELECT a FROM Author a WHERE a.name in :names", Author.class);
        query.setParameter("names", names);
        return query;
    }

    @Override
    public Stream<Author> listAll() {
        return getAllQuery().getResultStream();
    }

    @Override
    public List<Author> getAll() {
        return getAllQuery().getResultList();
    }

    private TypedQuery<Author> getAllQuery() {
        return em.createQuery("SELECT a FROM Author a", Author.class);
    }

    @Override
    public boolean deleteById(long id) {
        Author a = em.find(Author.class, id);
        if (a == null) {
            // no entity found
            return false;
        }
        // Note: entity 'Author' has no reference on entity 'Book'
        Query q = em.createQuery("DELETE FROM Author a WHERE a.id = :id " +
                "AND (SELECT count(a) FROM Book b JOIN b.authors a WHERE a.id = :id) = 0");
        q.setParameter("id", id);
        if (q.executeUpdate() > 0) {
            // remove also from the cache (to be sure there is no "zombie" entities)
            em.detach(a);
            return true;
        }
        // entity is found but cannot be deleted since it is bound on FK constraint
        return false;
    }
}
