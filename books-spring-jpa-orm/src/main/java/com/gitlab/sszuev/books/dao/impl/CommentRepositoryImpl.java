package com.gitlab.sszuev.books.dao.impl;

import com.gitlab.sszuev.books.dao.CommentRepository;
import com.gitlab.sszuev.books.domain.Comment;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * Created by @ssz on 04.01.2021.
 */
@Repository
public class CommentRepositoryImpl implements CommentRepository {
    @PersistenceContext
    private EntityManager em;

    @Override
    public <S extends Comment> S save(S comment) {
        if (comment.getID() == null) {
            em.persist(comment);
            return comment;
        }
        return em.merge(comment);
    }

    @Override
    public boolean deleteById(long id) {
        Comment c = em.find(Comment.class, id);
        if (c == null) return false;
        em.remove(c);
        return true;
    }

    @Override
    public Optional<Comment> findLast(long id) {
        return getByBookQuery(id).getResultStream().findFirst();
    }

    @Override
    public Stream<Comment> listByBook(long id) {
        return getByBookQuery(id).getResultStream();
    }

    @Override
    public List<Comment> getByBook(long id) {
        return getByBookQuery(id).getResultList();
    }

    private TypedQuery<Comment> getByBookQuery(long id) {
        TypedQuery<Comment> query = em.createQuery("SELECT c FROM Comment c WHERE c.book.id = :id " +
                "ORDER BY c.date DESC", Comment.class);
        query.setParameter("id", id);
        return query;
    }

    @Override
    public Stream<Comment> listAll() {
        return getAllQuery().getResultStream();
    }

    @Override
    public List<Comment> getAll() {
        return getAllQuery().getResultList();
    }

    private TypedQuery<Comment> getAllQuery() {
        return em.createQuery("SELECT c FROM Comment c ORDER BY c.date DESC", Comment.class);
    }
}
