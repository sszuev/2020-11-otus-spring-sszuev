package com.gitlab.sszuev.books.dto;

import com.gitlab.sszuev.books.domain.Author;
import com.gitlab.sszuev.books.domain.Book;
import com.gitlab.sszuev.books.domain.Comment;
import com.gitlab.sszuev.books.domain.Genre;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

/**
 * A helper to map JPA Entities to DTO Objects.
 * <p>
 * Created by @ssz on 30.12.2020.
 */
@Component
public class EntityMapper {

    public BookRecord mapToDTO(Book book) {
        return new BookRecord(book.getID(), book.getTitle(), mapToDTO(book.getGenre()),
                book.getAuthors().stream().map(this::mapToDTO).collect(Collectors.toList()));
    }

    public GenreRecord mapToDTO(Genre genre) {
        return new GenreRecord(genre.getID(), genre.getName());
    }

    public AuthorRecord mapToDTO(Author author) {
        return new AuthorRecord(author.getID(), author.getName());
    }

    public CommentRecord mapToDTO(Comment comment) {
        return new CommentRecord(comment.getID(), comment.getBook().getID(), comment.getComment(), comment.getDate());
    }
}
