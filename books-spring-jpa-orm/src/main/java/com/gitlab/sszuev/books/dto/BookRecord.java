package com.gitlab.sszuev.books.dto;

import java.util.Collection;
import java.util.Objects;
import java.util.stream.Stream;

/**
 * Simple DTO to hold book's data.
 * <p>
 * Created by @ssz on 30.12.2020.
 */
public final class BookRecord extends BaseRecord {
    private final String title;
    private final GenreRecord genre;
    private final Collection<AuthorRecord> authors;

    protected BookRecord(long id, String title, GenreRecord genre, Collection<AuthorRecord> authors) {
        super(id);
        this.title = Objects.requireNonNull(title);
        this.genre = Objects.requireNonNull(genre);
        this.authors = Objects.requireNonNull(authors);
    }

    public String getTitle() {
        return title;
    }

    public GenreRecord getGenre() {
        return genre;
    }

    public Stream<AuthorRecord> listAuthors() {
        return authors.stream();
    }
}
