package com.gitlab.sszuev.books.service;

import com.gitlab.sszuev.books.dao.AuthorRepository;
import com.gitlab.sszuev.books.dao.BookRepository;
import com.gitlab.sszuev.books.dao.CommentRepository;
import com.gitlab.sszuev.books.dao.GenreRepository;
import com.gitlab.sszuev.books.domain.Author;
import com.gitlab.sszuev.books.domain.Book;
import com.gitlab.sszuev.books.domain.Comment;
import com.gitlab.sszuev.books.domain.Genre;
import com.gitlab.sszuev.books.dto.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by @ssz on 29.12.2020.
 */
@Service
public class LibraryServiceImpl implements LibraryService {

    private final BookRepository bookRepository;
    private final GenreRepository genreRepository;
    private final AuthorRepository authorRepository;
    private final CommentRepository commentRepository;
    private final EntityMapper mapper;

    public LibraryServiceImpl(BookRepository repository,
                              GenreRepository genreRepository,
                              AuthorRepository authorRepository,
                              CommentRepository commentRepository,
                              EntityMapper mapper) {
        this.bookRepository = Objects.requireNonNull(repository);
        this.genreRepository = Objects.requireNonNull(genreRepository);
        this.authorRepository = Objects.requireNonNull(authorRepository);
        this.commentRepository = Objects.requireNonNull(commentRepository);
        this.mapper = Objects.requireNonNull(mapper);
    }

    @Transactional(readOnly = true)
    @Override
    public Optional<BookRecord> find(long id) {
        return bookRepository.findById(id).map(mapper::mapToDTO);
    }

    @Transactional(readOnly = true)
    @Override
    public Stream<BookRecord> books() {
        return bookRepository.listAll().map(mapper::mapToDTO).collect(Collectors.toList()).stream();
    }

    @Transactional(readOnly = true)
    @Override
    public Stream<BookRecord> books(long limit) {
        Stream<Book> res = bookRepository.listAll();
        if (limit >= 0) {
            res = res.limit(limit);
        }
        return res.map(mapper::mapToDTO).collect(Collectors.toList()).stream();
    }

    @Transactional(readOnly = true)
    @Override
    public Stream<GenreRecord> genres() {
        return genreRepository.listAll().map(mapper::mapToDTO).collect(Collectors.toList()).stream();
    }

    @Transactional(readOnly = true)
    @Override
    public Stream<AuthorRecord> authors() {
        return authorRepository.listAll().map(mapper::mapToDTO).collect(Collectors.toList()).stream();
    }

    @Transactional(readOnly = true)
    @Override
    public Stream<AuthorRecord> authors(long limit) {
        Stream<Author> res = authorRepository.listAll();
        if (limit >= 0) {
            res = res.limit(limit);
        }
        return res.map(mapper::mapToDTO).collect(Collectors.toList()).stream();
    }

    @Transactional(readOnly = true)
    @Override
    public Stream<CommentRecord> comments(Long id) {
        return (id == null ? commentRepository.listAll() : commentRepository.listByBook(id))
                .map(mapper::mapToDTO)
                .collect(Collectors.toList()).stream();
    }

    @Transactional
    @Override
    public BookRecord create(String title, String genre, List<String> authors) {
        Book res = bookRepository.save(newBook(title, fetchGenre(genre), fetchAuthors(authors)));
        return mapper.mapToDTO(res);
    }

    @Transactional
    @Override
    public boolean delete(long id) {
        Optional<Book> book = bookRepository.findById(id);
        if (book.isEmpty()) return false;
        long genre = book.get().getGenre().getID();
        Set<Long> authors = book.get().getAuthors().stream().map(Author::getID).collect(Collectors.toSet());

        boolean res = bookRepository.deleteById(id);
        genreRepository.deleteById(genre);
        authors.forEach(authorRepository::deleteById);
        return res;
    }

    @Transactional
    @Override
    public Optional<CommentRecord> addComment(long bookId, String comment) {
        Optional<Book> book = bookRepository.findById(bookId);
        if (book.isEmpty()) return Optional.empty();
        CommentRecord res = mapper.mapToDTO(commentRepository.save(newComment(book.get(), comment, LocalDateTime.now())));
        return Optional.of(res);
    }

    @Transactional
    @Override
    public Optional<CommentRecord> deleteComment(long bookId) {
        return commentRepository.findLast(bookId).filter(c -> commentRepository.deleteById(c.getID())).map(mapper::mapToDTO);
    }

    private List<Author> fetchAuthors(List<String> authors) {
        // Note: right now there is no unique constraint on column AUTHORS.NAME !
        Map<String, List<Author>> found = authorRepository.listWithNameIn(authors)
                .collect(Collectors.groupingBy(Author::getName, Collectors.toList()));
        return authors.stream()
                .map(a -> found.computeIfAbsent(a, x -> List.of(newAuthor(x))))
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    private Genre fetchGenre(String genre) {
        return genreRepository.listByName(genre).findFirst().orElseGet(() -> newGenre(genre));
    }

    private Author newAuthor(String name) {
        Author res = new Author();
        res.setName(name);
        return res;
    }

    private Genre newGenre(String name) {
        Genre res = new Genre();
        res.setName(name);
        return res;
    }

    private Book newBook(String title, Genre genre, List<Author> authors) {
        Book res = new Book();
        res.setTitle(title);
        res.setGenre(genre);
        res.setAuthors(authors);
        return res;
    }

    private Comment newComment(Book book, String comment, LocalDateTime time) {
        Comment res = new Comment();
        res.setBook(book);
        res.setDate(time);
        res.setComment(comment);
        return res;
    }
}
