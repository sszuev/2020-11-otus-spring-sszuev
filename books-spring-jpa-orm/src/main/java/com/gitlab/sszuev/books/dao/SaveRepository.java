package com.gitlab.sszuev.books.dao;

/**
 * Created by @ssz on 04.01.2021.
 */
interface SaveRepository<E> {
    /**
     * Saves the given entity.
     *
     * @param entity, must not be {@code null}.
     * @return the saved entity; will never be {@literal null}.
     */
    <S extends E> S save(S entity);
}
