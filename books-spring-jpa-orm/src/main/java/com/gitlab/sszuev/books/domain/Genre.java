package com.gitlab.sszuev.books.domain;

import javax.persistence.*;

/**
 * A domain entity that describes a gene item.
 * <p>
 * Created by @ssz on 29.12.2020.
 */
@Entity
@Table(name = "genres")
public final class Genre implements HasID {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "name", nullable = false, unique = true)
    private String name;

    @Override
    public Long getID() {
        return id;
    }

    @Override
    public void setID(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
