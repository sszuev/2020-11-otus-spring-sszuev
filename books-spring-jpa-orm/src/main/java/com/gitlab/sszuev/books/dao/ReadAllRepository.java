package com.gitlab.sszuev.books.dao;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by @ssz on 02.01.2021.
 */
interface ReadAllRepository<E> {
    /**
     * Lists all entities from the underlying storage.
     *
     * @return a {@code Stream} of {@link E}s
     */
    Stream<E> listAll();

    /**
     * Gets all entities as {@code List}.
     *
     * @return a {@code List} of {@link E}s
     */
    default List<E> getAll() {
        return listAll().collect(Collectors.toList());
    }
}
