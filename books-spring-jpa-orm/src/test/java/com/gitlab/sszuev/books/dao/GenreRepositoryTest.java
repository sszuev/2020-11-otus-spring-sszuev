package com.gitlab.sszuev.books.dao;

import com.gitlab.sszuev.books.TestUtils;
import com.gitlab.sszuev.books.dao.impl.GenreRepositoryImpl;
import com.gitlab.sszuev.books.domain.Genre;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.Import;

import java.util.List;

/**
 * Created by @ssz on 01.01.2021.
 */
@DataJpaTest
@Import(GenreRepositoryImpl.class)
public class GenreRepositoryTest {
    @Autowired
    private GenreRepository repository;
    @Autowired
    private TestEntityManager tem;

    @Test
    public void testListByName() {
        Assertions.assertEquals(1, repository.listByName("сказка").count());
        Assertions.assertEquals(1, repository.listByName("роман (историческая повесть)").count());
        Assertions.assertEquals(0, repository.listByName("новелла").count());
        Genre author = repository.listByName("сказка").findFirst().orElseThrow(AssertionError::new);
        Assertions.assertEquals(4, author.getID());
    }

    @Test
    public void testListAll() {
        Assertions.assertEquals(2, repository.listAll().count());
        List<Genre> res = repository.getAll();
        Assertions.assertEquals(2, res.size());
        Assertions.assertNotNull(TestUtils.getFirst(res, 3).getName());
        Assertions.assertNotNull(TestUtils.getFirst(res, 4).getName());
    }

    @Test
    public void testDeleteSuccess() {
        long id = 4;
        Genre before = tem.find(Genre.class, id);
        Assumptions.assumeFalse(before == null);

        Assertions.assertTrue(repository.deleteById(id));

        Genre after = tem.find(Genre.class, id);
        Assertions.assertNull(after);
    }

    @Test
    public void testDeleteFail() {
        long id = 3;
        Assumptions.assumeFalse(tem.find(Genre.class, id) == null);

        Assertions.assertFalse(repository.deleteById(id));

        Assertions.assertEquals(2, TestUtils.count(tem, "genres"));
    }

}
