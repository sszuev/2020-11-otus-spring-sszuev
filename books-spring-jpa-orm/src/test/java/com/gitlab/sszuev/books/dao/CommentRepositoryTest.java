package com.gitlab.sszuev.books.dao;

import com.gitlab.sszuev.books.TestUtils;
import com.gitlab.sszuev.books.dao.impl.CommentRepositoryImpl;
import com.gitlab.sszuev.books.domain.Book;
import com.gitlab.sszuev.books.domain.Comment;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.Import;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by @ssz on 04.01.2021.
 */
@DataJpaTest
@Import(CommentRepositoryImpl.class)
public class CommentRepositoryTest {

    @Autowired
    private CommentRepository repository;
    @Autowired
    private TestEntityManager tem;

    @Test
    public void testListByBook() {
        Assertions.assertEquals(2, repository.listByBook(42).count());
        List<Comment> res = repository.getByBook(42);
        Assertions.assertEquals(2, res.size());
        Assertions.assertNotNull(TestUtils.getFirst(res, 2).getDate());
        Assertions.assertNotNull(TestUtils.getFirst(res, 5).getComment());

        Assertions.assertEquals(0, repository.listByBook(4).count());
        Assertions.assertTrue(repository.getByBook(44).isEmpty());
    }

    @Test
    public void testListAll() {
        Assertions.assertEquals(2, repository.listAll().count());
        List<Comment> res = repository.getAll();
        Assertions.assertEquals(2, res.size());
        Assertions.assertNotNull(TestUtils.getFirst(res, 2).getDate());
        Assertions.assertNotNull(TestUtils.getFirst(res, 5).getComment());
    }

    @Test
    public void testSave() {
        LocalDateTime time = LocalDateTime.now().plusDays(42);
        Book book = tem.find(Book.class, 4L);

        Comment comment = TestUtils.newComment(null, book, "very nice", time);
        Assertions.assertSame(comment, repository.save(comment));
        Assertions.assertNotNull(comment.getID());
        Assertions.assertSame(book, comment.getBook());
        Assertions.assertEquals(time, comment.getDate());

        Assertions.assertEquals(3, TestUtils.count(tem, "comments"));
    }

    @Test
    public void testEdit() {
        String txt = "cool";
        LocalDateTime time = LocalDateTime.now().plusDays(42);
        Book book = tem.find(Book.class, 42L);
        Comment given = TestUtils.newComment(5L, book, txt, time);
        tem.detach(book);

        Comment actual = repository.save(given);

        Assertions.assertNotSame(given, actual);
        Assertions.assertEquals(5, actual.getID());
        Assertions.assertEquals(txt, actual.getComment());
        Assertions.assertEquals(time, actual.getDate());
        Assertions.assertEquals(42, actual.getBook().getID());

        Assertions.assertEquals(2, TestUtils.count(tem, "comments"));
    }

    @Test
    public void testFindLast() {
        Comment actual = repository.findLast(42).orElseThrow(AssertionError::new);
        Assertions.assertEquals(5, actual.getID());
        Assertions.assertEquals("the comment N2", actual.getComment());
        Assertions.assertEquals(42, actual.getBook().getID());
        Assertions.assertEquals(LocalDate.of(2029, 8, 8), actual.getDate().toLocalDate());

        Assertions.assertTrue(repository.findLast(4).isEmpty());
        Assertions.assertTrue(repository.findLast(-42).isEmpty());
    }

    @Test
    public void testDelete() {
        Comment comment1 = tem.find(Comment.class, 2L);
        Comment comment2 = tem.find(Comment.class, 5L);
        Assumptions.assumeFalse(comment2 == null);
        Assumptions.assumeFalse(comment1 == null);

        Assertions.assertTrue(repository.deleteById(comment2.getID()));
        Assertions.assertTrue(repository.deleteById(comment1.getID()));
        Assertions.assertFalse(repository.deleteById(comment1.getID()));
        Assertions.assertFalse(repository.deleteById(-42));

        Assertions.assertEquals(0, TestUtils.count(tem, "comments"));

        Assertions.assertNull(tem.find(Comment.class, comment1.getID()));
        Assertions.assertNull(tem.find(Comment.class, comment2.getID()));
    }

}
