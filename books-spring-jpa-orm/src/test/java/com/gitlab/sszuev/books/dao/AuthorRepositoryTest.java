package com.gitlab.sszuev.books.dao;

import com.gitlab.sszuev.books.TestUtils;
import com.gitlab.sszuev.books.dao.impl.AuthorRepositoryImpl;
import com.gitlab.sszuev.books.domain.Author;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.Import;

import java.util.List;

/**
 * Created by @ssz on 01.01.2021.
 */
@DataJpaTest
@Import(AuthorRepositoryImpl.class)
public class AuthorRepositoryTest {
    @Autowired
    private AuthorRepository repository;
    @Autowired
    private TestEntityManager tem;

    @Test
    public void testListWithNameIn() {
        Assertions.assertEquals(1, repository.listWithNameIn("Пушкин АС", "Грег Иган").count());
        Assertions.assertEquals(2, repository.listWithNameIn("Т Цзян", "А.С. Пушкин", "Пушкин АС").count());
        Assertions.assertEquals(0, repository.listWithNameIn("J Doe").count());
        Author author = repository.listWithNameIn("Пушкин АС").findFirst().orElseThrow(AssertionError::new);
        Assertions.assertEquals(2, author.getID());
    }

    @Test
    public void testListAll() {
        Assertions.assertEquals(2, repository.listAll().count());
        List<Author> res = repository.getAll();
        Assertions.assertEquals(2, res.size());
        Assertions.assertEquals("Пушкин АС", TestUtils.getFirst(res, 2).getName());
        Assertions.assertEquals("А.С. Пушкин", TestUtils.getFirst(res, 3).getName());
    }

    @Test
    public void testDeleteSuccess() {
        long id = 3;
        Author before = tem.find(Author.class, id);
        Assumptions.assumeFalse(before == null);

        Assertions.assertTrue(repository.deleteById(id));

        Author after = tem.find(Author.class, id);
        Assertions.assertNull(after);
    }

    @Test
    public void testDeleteFail() {
        long id = 2;
        Assumptions.assumeFalse(tem.find(Author.class, id) == null);

        Assertions.assertFalse(repository.deleteById(id));

        Assertions.assertEquals(2, TestUtils.count(tem, "book_authors"));
    }
}
