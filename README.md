##### This is a student's work for the course "Spring Framework".

###### For [OtusTeam](https://otus.ru).

###### Projects:

- [books-spring-batch](books-spring-batch) - **SpringBatch**: the simple ETL console utility to migrate Book's database from RDB to MongoDB
  <details>
    <summary>details</summary>

  - **spring-batch**
  - **spring-shell**
  - **spring-data-mongodb**, **spring-data-jpa**, **hibernate**
  - **de.flapdoodle.embed.mongo**, **postgresql**, **h2**
  - **spring-boot-test**, **spring-batch-test**

  </details>

- [books-library-security](books-security) - **SpringSecurity**: the classic-mvc webapp with form-based authentication + URL authorization
  <details>
    <summary>details</summary>

  - **spring-security-config**, **spring-security-web**
  - **spring-web**, **spring-webmvc**
  - **thymeleaf**, **thymeleaf-extras-springsecurity5**
  - **spring-data-jpa**, **hibernate**, **h2**
  - **spring-boot**, **spring-boot-test**

  </details>

- [books-library-webflux](books-webflux) - **WebFlux**: the reactive SPA webapp (**JQuery**, **MongoDB**)
  <details>
    <summary>details</summary>

  - **io.projectreactor**
  - **spring-webflux**
  - **spring-web**, **spring-webmvc**
  - **thymeleaf**
  - **org.webjars:jquery**
  - **spring-data-mongodb**, **spring-data-mongodb-reactive**, **de.flapdoodle.embed.mongo**
  - **mongock**
  - **spring-boot**, **spring-boot-test**

  </details>

- [books-library-rest-mvc-ajax](books-rest-mvc-ajax) - the SPA webapp (**SpringMVC**, **RestController**, **JQuery**)
  <details>
    <summary>details</summary>

  - **org.webjars:jquery**
  - **spring-boot**
  - **spring-boot-test**
  - **spring-web**, **spring-webmvc**
  - **thymeleaf**
  - **spring-data-jpa**
  - **hibernate**
  - **h2**

  </details>

- [books-library-classic-mvc](books-classic-mvc) - the classic-mvc webapp (**SpringMVC**, **Thymeleaf**, **Actuator**,
  **Docker**, **Hystrix Circuit Breaker**, **Testcontainers**)
  <details>
    <summary>details</summary>

  - **spring-boot**
  - **spring-web**, **spring-webmvc**, **thymeleaf**
  - **spring-data-jpa**, **hibernate**, **postgres**
  - **spring-boot-test**, **h2**, **testcontainers**
  - **spring-actuator**
  - **spring-cloud-netflix-hystrix**
  - **docker**, **docker-compose**

  </details>

- [books-library-mongodb](books-mongodb) - the console application with NoSQL (**SpringData**, **MongoDB**)
  <details>
    <summary>details</summary>

  - **spring-boot**
  - **spring-boot-test**
  - **spring-shell**
  - **spring-data-mongodb**
  - **mongock**
  - **embed.mongo**

  </details>

- [books-library-spring-data](books-spring-data) - the console application with classic DAO (**SpringDataJPA**, **CrudRepository**)
  <details>
    <summary>details</summary>

  - **spring-boot**
  - **spring-boot-test**
  - **spring-shell**
  - **spring-data-jpa**
  - **hibernate**
  - **liquibase**
  - **h2**

  </details>    

- [books-library-spring-jpa-orm](books-spring-jpa-orm) - the console application with custom DAO (**ORM**, **JPQL**, etc)
  <details>
    <summary>details</summary>

  - **spring-boot**
  - **spring-boot-test**
  - **spring-shell**
  - **jpa**, **spring-data-jpa**, **jpql**
  - **hibernate**
  - **liquibase**
  - **h2**

  </details>   

- [books-library](books-jdbc-template) - the console app with Spring **JdbcTemplate**
  <details>
    <summary>details</summary>

  - **spring-boot**
  - **spring-boot-test**
  - **spring-shell**
  - **spring-jdbc**
  - **liquibase**
  - **h2**

  </details>

- [spring-integration-demo](spring-integration-demo) - **SpringIntegration**: the simplest console demonstration based on butterfly-lifecycle model
  <details>
    <summary>details</summary>

  - **spring-boot**
  - **spring-boot-integration**
  - **spring-messaging**

  </details>   

- [students-testing-spring-shell](students-spring-shell) - **SpringShell**: the third console application for testing students
  <details>
    <summary>details</summary>

  - **spring-boot**
  - **spring-boot-test**
  - **spring-shell**

  </details>   

- [students-testing-spring-boot](students-spring-boot) - **SpringBoot**: the second console application for testing students
  <details>
    <summary>details</summary>

  - **spring-boot**
  - **spring-boot-test**
  - `Scanner`

  </details> 

- [students-testing](students-spring-noboot) - the first console application without boot (just **SpringContext**)
  <details>
    <summary>details</summary>

  - **spring-context**
  - **spring-test**
  - **junit-jupiter**
  - **mockito**
  - `BufferedReader`

  </details>