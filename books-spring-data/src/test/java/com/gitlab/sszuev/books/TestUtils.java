package com.gitlab.sszuev.books;

import com.gitlab.sszuev.books.domain.*;
import org.junit.jupiter.api.Assertions;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * Contains utils and factory methods for tests.
 * <p>
 * Created by @ssz on 29.12.2020.
 */
public class TestUtils {

    public static <X> X assertOne(Collection<X> list) {
        Assertions.assertEquals(1, list.size());
        X res = list.iterator().next();
        Assertions.assertNotNull(res);
        return res;
    }

    public static <X extends HasID> X getFirst(Collection<X> list, long id) {
        return getFirst(list.stream(), id);
    }

    public static <X extends HasID> X getFirst(Stream<X> list, long id) {
        return getFirst(list, x -> Objects.equals(x.getID(), id));
    }

    public static <X> X getFirst(Collection<X> list, Predicate<X> test) {
        return getFirst(list.stream(), test);
    }

    public static <X> X getFirst(Stream<X> list, Predicate<X> test) {
        return list.filter(test).findFirst().orElseThrow(AssertionError::new);
    }

    @SuppressWarnings("UnusedReturnValue")
    public static String assertNotEmpty(String s) {
        Assertions.assertNotNull(s);
        Assertions.assertFalse(s.isEmpty());
        return s;
    }

    public static Book newBook(Long id, String title, Genre genre, List<Author> authors) {
        Book res = new Book();
        res.setID(id);
        res.setTitle(title);
        res.setGenre(genre);
        res.setAuthors(authors);
        return res;
    }

    public static Author newAuthor(long id, String author) {
        Author res = newAuthor(author);
        res.setID(id);
        return res;
    }

    public static Author newAuthor(String author) {
        Author res = new Author();
        res.setName(author);
        return res;
    }

    public static Genre newGenre(long id, String genre) {
        Genre res = newGenre(genre);
        res.setID(id);
        return res;
    }

    public static Genre newGenre(String genre) {
        Genre res = new Genre();
        res.setName(genre);
        return res;
    }

    public static Comment newComment(Long id, Book book, String comment, LocalDateTime dt) {
        Comment res = new Comment();
        res.setBook(book);
        res.setID(id);
        res.setComment(comment);
        res.setDate(dt);
        if (book.getComments() == null) {
            book.setComments(new ArrayList<>());
        }
        book.getComments().add(res);
        return res;
    }

    public static long count(TestEntityManager tem, String table) {
        List<?> res = tem.getEntityManager().createNativeQuery("SELECT COUNT(*) FROM " + table).getResultList();
        Assertions.assertNotNull(res);
        Assertions.assertEquals(1, res.size());
        BigInteger count = (BigInteger) res.get(0);
        return count.longValue();
    }
}
