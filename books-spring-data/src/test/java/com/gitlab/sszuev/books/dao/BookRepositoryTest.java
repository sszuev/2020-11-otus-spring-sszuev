package com.gitlab.sszuev.books.dao;

import com.gitlab.sszuev.books.TestUtils;
import com.gitlab.sszuev.books.domain.Author;
import com.gitlab.sszuev.books.domain.Book;
import com.gitlab.sszuev.books.domain.Comment;
import com.gitlab.sszuev.books.domain.Genre;
import org.hibernate.SessionFactory;
import org.hibernate.stat.Statistics;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Stream;

/**
 * Created by @ssz on 29.12.2020.
 */
@DataJpaTest
@AutoConfigureTestDatabase(connection = org.springframework.boot.jdbc.EmbeddedDatabaseConnection.H2)
public class BookRepositoryTest {

    private static final long DATA_BOOK_ID_FIRST = 4;
    private static final long DATA_BOOK_ID_SECOND = 42;
    private static final long DATA_GENRE_ID_FIRST = 3;
    private static final long DATA_AUTHOR_ID_FIRST = 2;
    private static final long DATA_COMMENT_ID_SECOND = 2;
    private static final LocalDate DATA_COMMENT_DATE_SECOND = LocalDate.of(1919, 10, 10);

    @Autowired
    private BookRepository repository;
    @Autowired
    private TestEntityManager tem;

    @Test
    public void testListBooks() {
        long num = 3;
        Assertions.assertEquals(num, repository.streamAll().count());

        List<Book> books = repository.fetchAll();
        Assertions.assertEquals(num, books.size());

        Book b1 = TestUtils.getFirst(books, DATA_BOOK_ID_FIRST);
        books.remove(b1);
        Book b2 = TestUtils.getFirst(books, DATA_BOOK_ID_SECOND);
        TestUtils.assertNotEmpty(b2.getTitle());
        TestUtils.assertNotEmpty(b1.getTitle());

        Genre g1 = b1.getGenre();
        Genre g2 = b2.getGenre();
        Assertions.assertSame(g1, g2);
        Assertions.assertEquals(DATA_GENRE_ID_FIRST, g1.getID());
        TestUtils.assertNotEmpty(g1.getName());

        Author a1 = TestUtils.assertOne(b1.getAuthors());
        Author a2 = TestUtils.assertOne(b2.getAuthors());
        Assertions.assertSame(a1, a2);
        Assertions.assertEquals(DATA_AUTHOR_ID_FIRST, a1.getID());
        TestUtils.assertNotEmpty(a1.getName());

        Assertions.assertTrue(b1.getComments().isEmpty());
        Assertions.assertEquals(2, b2.getComments().size());
        Comment c1 = TestUtils.getFirst(b2.getComments(), 2);
        Comment c2 = TestUtils.getFirst(b2.getComments(), 5);
        Assertions.assertEquals(DATA_COMMENT_ID_SECOND, c1.getID());
        Assertions.assertEquals(DATA_COMMENT_DATE_SECOND, c1.getDate().toLocalDate());
        TestUtils.assertNotEmpty(c1.getComment());

        Assertions.assertNotNull(c2.getDate());
        Assertions.assertSame(b2, c2.getBook());
        TestUtils.assertNotEmpty(c2.getComment());
    }

    @Test
    public void testSaveFreshBookTuple() {
        Author author1 = TestUtils.newAuthor("A1");
        Author author2 = TestUtils.newAuthor("A2");
        Genre genre = TestUtils.newGenre("G0");

        Book book = TestUtils.newBook(null, "TheBook1", genre, List.of(author1, author2));
        Assertions.assertSame(book, repository.save(book));
        Assertions.assertNotNull(book.getID());

        Book actualBook = tem.find(Book.class, book.getID());
        Assertions.assertEquals(book.getTitle(), actualBook.getTitle());

        Genre actualGenre = actualBook.getGenre();
        Assertions.assertNotNull(actualGenre);
        Assertions.assertEquals(genre.getName(), actualGenre.getName());
        Assertions.assertNotNull(actualGenre.getID());

        List<Author> actualAuthors = actualBook.getAuthors();
        Assertions.assertEquals(2, actualAuthors.size());
        Stream.of(author1, author2)
                .forEach(e -> Assertions.assertNotNull(TestUtils.getFirst(actualAuthors,
                        a -> Objects.equals(e.getName(), a.getName())).getID()));
    }

    @Test
    public void testSaveBookWithExistingAuthorsAndGenre() {
        long beforeRefCount = TestUtils.count(tem, "book_authors");
        long beforeBookCount = TestUtils.count(tem, "books");
        Assumptions.assumeTrue(beforeBookCount == 2);
        Assumptions.assumeTrue(beforeRefCount == 2);

        Genre genre = tem.find(Genre.class, 4L);
        Author author = tem.find(Author.class, 3L);

        Book book = TestUtils.newBook(null, "TheBook2", genre, List.of(author));
        Assertions.assertSame(book, repository.save(book));
        Assertions.assertNotNull(book.getID());

        long afterRefCount = TestUtils.count(tem, "book_authors");
        long afterBookCount = TestUtils.count(tem, "books");
        Assertions.assertEquals(beforeBookCount + 1, afterBookCount);
        Assertions.assertEquals(beforeRefCount + 1, afterRefCount);
    }

    @Test
    public void testDeleteBooks() {
        testDeleteBook(42, Map.of("genres", 3, "authors", 3, "comments", 1, "books", 2, "book_authors", 2));
        testDeleteBook(4, Map.of("genres", 3, "authors", 3, "comments", 1, "books", 1, "book_authors", 1));
        testDeleteBook(55, Map.of("genres", 3, "authors", 3, "comments", 0, "books", 0, "book_authors", 0));
    }

    private void testDeleteBook(long id, Map<String, Integer> expected) {
        Book before = tem.find(Book.class, id);
        Assumptions.assumeFalse(before == null);

        repository.deleteById(id);

        Book after = tem.find(Book.class, id);
        Assertions.assertNull(after);

        testContentCounts(expected);
    }

    private void testContentCounts(Map<String, Integer> expected) {
        expected.forEach((t, v) -> Assertions.assertEquals(v.longValue(), TestUtils.count(tem, t),
                "Wrong content for table " + t));
    }

    @Test
    public void testGetById() {
        long id = 4;
        Book x = repository.getOne(id);
        Assertions.assertNotNull(x);
        Assertions.assertEquals(id, x.getID());
        Assertions.assertNotNull(x.getTitle());
        Assertions.assertEquals(2, TestUtils.assertOne(x.getAuthors()).getID());
        Assertions.assertEquals(3, x.getGenre().getID());

        Assertions.assertFalse(repository.findById(42L).isEmpty());
        Assertions.assertTrue(repository.findById(-42L).isEmpty());
    }

    @Test
    public void testListAllStatementCount() {
        Statistics statistics = tem.getEntityManager().getEntityManagerFactory().unwrap(SessionFactory.class).getStatistics();
        statistics.setStatisticsEnabled(true);
        statistics.clear();

        // two query: 1) from book join genre 2) from book_authors where ...
        List<Book> all = repository.fetchAll();
        Assertions.assertNotNull(all);
        Assertions.assertEquals(3, all.size());
        all.forEach(book -> {
            Assertions.assertNotNull(book.getID());
            Assertions.assertNotNull(book.getTitle());
            Assertions.assertNotNull(book.getGenre());
            Assertions.assertTrue(book.getAuthors() != null && book.getAuthors().size() > 0);
        });
        Assertions.assertEquals(2, statistics.getPrepareStatementCount());

        // comments ARE LAZY
        all.forEach(book -> {
            Assertions.assertNotNull(book.getComments());
            int actual = book.getComments().size();
            Assertions.assertTrue(book.getID() == 4 ? actual == 0 : actual > 0);
        });
        Assertions.assertEquals(5, statistics.getPrepareStatementCount());
    }
}
