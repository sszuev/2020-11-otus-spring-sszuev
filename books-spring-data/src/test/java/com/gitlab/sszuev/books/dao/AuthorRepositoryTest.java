package com.gitlab.sszuev.books.dao;

import com.gitlab.sszuev.books.TestUtils;
import com.gitlab.sszuev.books.domain.Author;
import com.gitlab.sszuev.books.domain.Book;
import org.hibernate.SessionFactory;
import org.hibernate.stat.Statistics;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.List;
import java.util.stream.Stream;

/**
 * Created by @ssz on 01.01.2021.
 */
@DataJpaTest
@AutoConfigureTestDatabase(connection = org.springframework.boot.jdbc.EmbeddedDatabaseConnection.H2)
public class AuthorRepositoryTest {
    @Autowired
    private AuthorRepository repository;
    @Autowired
    private TestEntityManager tem;

    @Test
    public void testListWithNameIn() {
        Assertions.assertEquals(1, repository.streamByNameIn("Пушкин АС", "Грег Иган").count());
        Assertions.assertEquals(2, repository.streamByNameIn("Т Цзян", "А.С. Пушкин", "Пушкин АС").count());
        Assertions.assertEquals(0, repository.streamByNameIn("J Doe").count());
        Author author = repository.streamByNameIn("Пушкин АС").findFirst().orElseThrow(AssertionError::new);
        Assertions.assertEquals(2, author.getID());
    }

    @Test
    public void testListAll() {
        Assertions.assertEquals(3, repository.streamAll().count());
        List<Author> res = repository.findAll();
        Assertions.assertEquals(3, res.size());
        Assertions.assertEquals("Пушкин АС", TestUtils.getFirst(res, 2).getName());
        Assertions.assertEquals("А.С. Пушкин", TestUtils.getFirst(res, 3).getName());
    }

    @Test
    public void testDeleteSuccess() {
        long id = 3;
        Author before = tem.find(Author.class, id);
        Assumptions.assumeFalse(before == null);

        Assertions.assertTrue(repository.deleteIfPossible(id));

        Author after = tem.find(Author.class, id);
        Assertions.assertNull(after);
    }

    @Test
    public void testDeleteFail() {
        long id = 2;
        Assumptions.assumeFalse(tem.find(Author.class, id) == null);

        Assertions.assertFalse(repository.deleteIfPossible(id));

        Assertions.assertEquals(3, TestUtils.count(tem, "book_authors"));
    }

    @Test
    public void testStreamByAuthorNameStatementCount() {
        long num = 2;
        String name = "Пушкин АС";

        Statistics statistics = tem.getEntityManager().getEntityManagerFactory().unwrap(SessionFactory.class).getStatistics();
        statistics.setStatisticsEnabled(true);
        statistics.clear();
        Stream<String> books = repository.streamByNameIncludingBooks(name).flatMap(a -> a.getBooks().stream().map(Book::getTitle));
        Assertions.assertEquals(num, books.count());
        Assertions.assertEquals(1, statistics.getPrepareStatementCount());
        tem.clear();
    }

    @Test
    public void testListBooksByAuthor() {
        Assertions.assertEquals(1, repository.streamByNameIncludingBooks("Пушкин АС").count());
        Assertions.assertEquals(1, repository.streamByNameIncludingBooks("пушкин ас ").count());
        Assertions.assertEquals(1, repository.streamByNameIncludingBooks("Джон Р.Р. Толк").count());
        Assertions.assertEquals(0, repository.streamByNameIncludingBooks("Дж Р.Р.Толк").count());
    }
}
