package com.gitlab.sszuev.books.service;

import com.gitlab.sszuev.books.TestUtils;
import com.gitlab.sszuev.books.dao.AuthorRepository;
import com.gitlab.sszuev.books.dao.BookRepository;
import com.gitlab.sszuev.books.dao.CommentRepository;
import com.gitlab.sszuev.books.dao.GenreRepository;
import com.gitlab.sszuev.books.domain.Author;
import com.gitlab.sszuev.books.domain.Book;
import com.gitlab.sszuev.books.domain.Comment;
import com.gitlab.sszuev.books.domain.Genre;
import com.gitlab.sszuev.books.dto.BookRecord;
import com.gitlab.sszuev.books.dto.CommentRecord;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by @ssz on 29.12.2020.
 */
@SpringBootTest
public class LibraryServiceMockTest {
    @Autowired
    private LibraryService service;
    @MockBean
    private BookRepository bookRepository;
    @MockBean
    private GenreRepository genreRepository;
    @MockBean
    private AuthorRepository authorRepository;
    @MockBean
    private CommentRepository commentRepository;

    @Test
    public void testListBooks() {
        Author a1 = TestUtils.newAuthor(-1, "author1");
        Author a2 = TestUtils.newAuthor(-2, "author2");
        Genre g1 = TestUtils.newGenre(-3, "genre");
        Book b1 = TestUtils.newBook(-42L, "test-title-1", g1, List.of(a1));
        Book b2 = TestUtils.newBook(-21L, "test-title-2", g1, List.of(a1, a2));

        Mockito.when(bookRepository.streamAll()).thenAnswer(x -> Stream.of(b1, b2));

        Assertions.assertEquals(2, service.books().count());
        List<BookRecord> books = service.books().collect(Collectors.toList());
        BookRecord actual1 = getFirst(books.stream(), b1);
        BookRecord actual2 = getFirst(books.stream(), b2);

        Assertions.assertEquals(b1.getTitle(), actual1.getTitle());
        Assertions.assertEquals(b2.getTitle(), actual2.getTitle());
    }

    @Test
    public void testListBooksByAuthor() {
        Author a1 = TestUtils.newAuthor(-1, "author1");
        Author a2 = TestUtils.newAuthor(-2, "author2");
        Genre g1 = TestUtils.newGenre(-3, "genre");
        Book b1 = TestUtils.newBook(-42L, "test-title-1", g1, List.of(a1));
        Book b2 = TestUtils.newBook(-21L, "test-title-2", g1, List.of(a1, a2));
        a1.setBooks(List.of(b1, b2));
        a2.setBooks(List.of(b2));

        Mockito.when(authorRepository.streamByNameIncludingBooks(Mockito.eq(a1.getName()))).thenAnswer(x -> Stream.of(a1));
        Mockito.when(authorRepository.streamByNameIncludingBooks(Mockito.eq(a2.getName()))).thenAnswer(x -> Stream.of(a2));

        Assertions.assertEquals(2, service.booksByAuthor(a1.getName()).count());
        List<BookRecord> books = service.booksByAuthor(a1.getName()).collect(Collectors.toList());
        BookRecord actual1 = getFirst(books.stream(), b1);
        BookRecord actual2 = getFirst(books.stream(), b2);

        Assertions.assertEquals(b1.getTitle(), actual1.getTitle());
        Assertions.assertEquals(b2.getTitle(), actual2.getTitle());

        Assertions.assertEquals(1, service.booksByAuthor(a2.getName()).count());
        Assertions.assertEquals(0, service.booksByAuthor("X").count());
    }

    @Test
    public void testListBooksByGenre() {
        Author a = TestUtils.newAuthor(-1, "author1");
        Genre g1 = TestUtils.newGenre(-3, "genre1");
        Genre g2 = TestUtils.newGenre(-4, "genre2");
        Book b1 = TestUtils.newBook(-42L, "test-title-1", g1, List.of(a));
        Book b2 = TestUtils.newBook(-21L, "test-title-2", g2, List.of(a));
        g1.setBooks(List.of(b1));
        g2.setBooks(List.of(b2));

        Mockito.when(genreRepository.streamByNameIncludingBooks(Mockito.eq(g1.getName()))).thenAnswer(x -> Stream.of(g1));
        Mockito.when(genreRepository.streamByNameIncludingBooks(Mockito.eq(g2.getName()))).thenAnswer(x -> Stream.of(g2));

        Map.of(g1, b1, g2, b2).forEach((g, b) -> {
            BookRecord actual = TestUtils.assertOne(service.booksByGenre(g.getName()).collect(Collectors.toList()));
            Assertions.assertEquals(b.getTitle(), actual.getTitle());
        });
        Assertions.assertEquals(0, service.booksByGenre("X").count());
    }

    @Test
    public void testListGenres() {
        Genre g1 = TestUtils.newGenre(1, "g1");
        Genre g2 = TestUtils.newGenre(21, "g2");
        Mockito.when(genreRepository.streamAll()).thenAnswer(x -> Stream.of(g1, g2));
        Assertions.assertEquals(2, service.genres().count());
        Assertions.assertEquals(Set.of(g1.getName(), g2.getName()),
                service.genres().map(x -> x.getName()).collect(Collectors.toSet()));
    }

    @Test
    public void testListAuthors() {
        Author a1 = TestUtils.newAuthor(2, "a1");
        Author a2 = TestUtils.newAuthor(42, "a2");
        Mockito.when(authorRepository.streamAll()).thenAnswer(x -> Stream.of(a1, a2));
        Assertions.assertEquals(2, service.authors().count());
        Assertions.assertEquals(Set.of(a1.getName(), a2.getName()),
                service.authors().map(x -> x.getName()).collect(Collectors.toSet()));
    }

    @Test
    public void testCreateNewBookTuple() {
        Author author1 = TestUtils.newAuthor(-1, "author1");
        Author author2 = TestUtils.newAuthor(-2, "author2");
        Genre genre = TestUtils.newGenre(-3, "test-genre");
        Book book = TestUtils.newBook(-42L, "test-title", genre, List.of(author1, author2));

        Mockito.when(bookRepository.save(Mockito.any())).thenReturn(book);

        BookRecord actual = service.create(book.getTitle(), genre.getName(),
                List.of(author1.getName(), author2.getName()));
        Assertions.assertNotNull(actual);
        Assertions.assertEquals(book.getTitle(), actual.getTitle());
        Assertions.assertEquals(book.getID(), actual.getId());
        Assertions.assertEquals(genre.getName(), actual.getGenre().getName());
    }

    @Test
    public void testCreateBookWithExistingAuthorsAndGenre() {
        Exception ex1 = new TestEx();
        Exception ex2 = new TestEx();

        Mockito.when(authorRepository.streamByNameIn(Mockito.eq(List.of("A")))).thenThrow(ex1);
        Mockito.when(genreRepository.streamByName(Mockito.eq("G"))).thenThrow(ex2);
        Assertions.assertSame(ex1, Assertions.assertThrows(TestEx.class,
                () -> service.create("title", "genre", List.of("A"))));
        Assertions.assertSame(ex2, Assertions.assertThrows(TestEx.class,
                () -> service.create("title", "G", List.of("Y", "Z"))));
    }

    @Test
    public void testDelete() {
        Genre g = TestUtils.newGenre(33, "g");
        Author a = TestUtils.newAuthor(44, "a");
        Book b1 = TestUtils.newBook(42L, "test-title-1", g, List.of(a));
        Book b2 = TestUtils.newBook(2L, "test-title-2", g, List.of(a));

        Mockito.doNothing().when(bookRepository).delete(Mockito.any());
        Mockito.when(bookRepository.findById(Mockito.eq(b1.getID()))).thenReturn(Optional.of(b1));
        Mockito.when(bookRepository.findById(Mockito.eq(b2.getID()))).thenReturn(Optional.empty());

        Assertions.assertFalse(service.delete(b2.getID()));
        Assertions.assertTrue(service.delete(b1.getID()));
    }

    @Test
    public void testListComments() {
        LocalDateTime time = LocalDateTime.now();
        long bookId = -42;
        Book book = createBook(bookId);
        Comment comment1 = TestUtils.newComment(-4L, book, "comm1", time.minusMinutes(42));
        Comment comment2 = TestUtils.newComment(-5L, book, "co mm2", time.minusYears(42));

        Mockito.when(commentRepository.streamAllByBookId(Mockito.eq(bookId))).thenReturn(Stream.of(comment1, comment2));
        Mockito.when(commentRepository.streamAll()).thenReturn(Stream.of(comment1, comment2));

        Stream.of(bookId, null).forEach(id -> {
            List<CommentRecord> comments = service.comments(id).collect(Collectors.toList());

            Assertions.assertEquals(2, comments.size());
            CommentRecord c1 = TestUtils.getFirst(comments, x -> x.getId() == -4);
            CommentRecord c2 = TestUtils.getFirst(comments, x -> x.getId() == -5);
            Assertions.assertEquals(comment1.getComment(), c1.getMessage());
            Assertions.assertEquals(comment2.getDate(), c2.getTimestamp());
        });

    }

    @Test
    public void testAddComment() {
        String comment = "XXXX";
        LocalDateTime now = LocalDateTime.now();
        long commentId = 42;
        long bookId = -42;
        Book book = createBook(bookId);

        Mockito.when(commentRepository.save(Mockito.any())).thenAnswer(i -> {
            Comment res = i.getArgument(0);
            res.setID(commentId);
            return res;
        });
        Mockito.when(bookRepository.findById(Mockito.eq(book.getID()))).thenReturn(Optional.of(book));

        CommentRecord res = service.addComment(bookId, comment).orElseThrow(AssertionError::new);

        Assertions.assertEquals(commentId, res.getId());
        Assertions.assertEquals(bookId, res.getBookId());
        Assertions.assertEquals(comment, res.getMessage());
        Assertions.assertFalse(res.getTimestamp().isBefore(now),
                String.format("Time %s is not after %s", res.getTimestamp(), now));

        Assertions.assertTrue(service.addComment(111, comment).isEmpty());
    }

    @Test
    public void testDeleteComment() {
        LocalDateTime time = LocalDateTime.now().minusYears(42);
        long commentId = 42;
        long bookId = -42;
        Book book = createBook(bookId);
        Comment comment = TestUtils.newComment(commentId, book, "comm1", time);

        Mockito.when(commentRepository.findLast(Mockito.eq(bookId))).thenReturn(Optional.of(comment));
        Mockito.doNothing().when(commentRepository).deleteById(Mockito.eq(commentId));

        CommentRecord res = service.deleteComment(bookId).orElseThrow(AssertionError::new);
        Assertions.assertEquals(commentId, res.getId());
        Assertions.assertEquals(bookId, res.getBookId());

        Assertions.assertTrue(service.deleteComment(111).isEmpty());
    }

    private static BookRecord getFirst(Stream<BookRecord> list, Book b) {
        return TestUtils.getFirst(list, x -> x.getId() == b.getID());
    }

    private static Genre createGenre() {
        return TestUtils.newGenre(-2, "genre");
    }

    private static List<Author> createAuthors() {
        return List.of(TestUtils.newAuthor(-3, "author"));
    }

    private static Book createBook(long id) {
        return TestUtils.newBook(id, "test-title", createGenre(), createAuthors());
    }

    private static class TestEx extends RuntimeException {
    }

}
