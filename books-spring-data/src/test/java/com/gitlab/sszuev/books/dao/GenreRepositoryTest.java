package com.gitlab.sszuev.books.dao;

import com.gitlab.sszuev.books.TestUtils;
import com.gitlab.sszuev.books.domain.Book;
import com.gitlab.sszuev.books.domain.Genre;
import org.hibernate.SessionFactory;
import org.hibernate.stat.Statistics;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.List;
import java.util.stream.Stream;

/**
 * Created by @ssz on 01.01.2021.
 */
@DataJpaTest
@AutoConfigureTestDatabase(connection = org.springframework.boot.jdbc.EmbeddedDatabaseConnection.H2)
public class GenreRepositoryTest {
    @Autowired
    private GenreRepository repository;
    @Autowired
    private TestEntityManager tem;

    @Test
    public void testListByName() {
        Assertions.assertEquals(1, repository.streamByName("сказка").count());
        Assertions.assertEquals(1, repository.streamByName("роман (историческая повесть)").count());
        Assertions.assertEquals(0, repository.streamByName("новелла").count());
        Genre author = repository.streamByName("сказка").findFirst().orElseThrow(AssertionError::new);
        Assertions.assertEquals(4, author.getID());
    }

    @Test
    public void testListAll() {
        Assertions.assertEquals(3, repository.streamAll().count());
        List<Genre> res = repository.findAll();
        Assertions.assertEquals(3, res.size());
        Assertions.assertNotNull(TestUtils.getFirst(res, 3).getName());
        Assertions.assertNotNull(TestUtils.getFirst(res, 4).getName());
    }

    @Test
    public void testDeleteSuccess() {
        long id = 4;
        Genre before = tem.find(Genre.class, id);
        Assumptions.assumeFalse(before == null);

        Assertions.assertTrue(repository.deleteIfPossible(id));

        Genre after = tem.find(Genre.class, id);
        Assertions.assertNull(after);
    }

    @Test
    public void testDeleteFail() {
        long id = 3;
        Assumptions.assumeFalse(tem.find(Genre.class, id) == null);

        Assertions.assertFalse(repository.deleteIfPossible(id));

        Assertions.assertEquals(3, TestUtils.count(tem, "genres"));
    }

    @Test
    public void testStreamByGenreNameStatementCount() {
        long num = 2;
        String name = "роман (историческая повесть)";

        Statistics statistics = tem.getEntityManager().getEntityManagerFactory().unwrap(SessionFactory.class).getStatistics();
        statistics.setStatisticsEnabled(true);
        statistics.clear();
        Stream<String> books = repository.streamByNameIncludingBooks(name).flatMap(a -> a.getBooks().stream().map(Book::getTitle));
        Assertions.assertEquals(num, books.count());
        Assertions.assertEquals(1, statistics.getPrepareStatementCount());
        tem.clear();
    }

    @Test
    public void testStreamByGenreName() {
        Assertions.assertEquals(1, repository.streamByNameIncludingBooks("роман (историческая повесть)").count());
        Assertions.assertEquals(1, repository.streamByNameIncludingBooks("история").count());
        Assertions.assertEquals(1, repository.streamByNameIncludingBooks(" История").count());
        Assertions.assertEquals(0, repository.streamByNameIncludingBooks("Дж Р.Р.Толк").count());
    }

}
