package com.gitlab.sszuev.books.dao.impl;

import com.gitlab.sszuev.books.dao.GenreCustomRepository;
import com.gitlab.sszuev.books.domain.Genre;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 * Created by @ssz on 14.01.2021.
 */
@SuppressWarnings("unused")
public class GenreCustomRepositoryImpl implements GenreCustomRepository {

    @PersistenceContext
    private EntityManager em;

    @Override
    public boolean deleteIfPossible(long id) {
        Genre g = em.find(Genre.class, id);
        if (g == null) {
            // nothing found
            return false;
        }
        // Note: entity 'Genre' has no reference on entity 'Book' (but 'Book' has)
        Query q = em.createQuery("DELETE FROM Genre g WHERE g.id = :id " +
                "AND (SELECT count(b) FROM Book b WHERE b.genre.id = :id) = 0");
        q.setParameter("id", id);
        if (q.executeUpdate() > 0) {
            // remove also from the cache (to be sure there is no "zombie" entities)
            em.detach(g);
            return true;
        }
        // entity is found but cannot be deleted since it is bound on FK constraint
        return false;
    }

}
