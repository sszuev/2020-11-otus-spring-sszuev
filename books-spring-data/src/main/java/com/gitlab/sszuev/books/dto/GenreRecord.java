package com.gitlab.sszuev.books.dto;

/**
 * Created by @ssz on 02.01.2021.
 */
public final class GenreRecord extends SimpleRecord {
    protected GenreRecord(long id, String name) {
        super(id, name);
    }
}
