package com.gitlab.sszuev.books.service;

import com.gitlab.sszuev.books.domain.Comment;
import com.gitlab.sszuev.books.dto.AuthorRecord;
import com.gitlab.sszuev.books.dto.BookRecord;
import com.gitlab.sszuev.books.dto.CommentRecord;
import com.gitlab.sszuev.books.dto.GenreRecord;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * A service to access to library data.
 * <p>
 * Created by @ssz on 29.12.2020.
 *
 * @see <a href='https://gitlab.com/sszuev/2020-11-otus-spring-sszuev/-/blob/15c3f1845f97320908d6066c076cda9d35c18c41/books/src/main/java/com/gitlab/sszuev/books/service/BooksService.java'>previous verion</a>
 */
public interface LibraryService {

    /**
     * Lists all book-records.
     *
     * @return a {@code Stream} of {@link BookRecord book}s
     */
    Stream<BookRecord> books();

    /**
     * Lists books by author name.
     *
     * @param author {@code String} - author name to search, not {@code null}
     * @return a {@code Stream} of {@link BookRecord book}s
     */
    Stream<BookRecord> booksByAuthor(String author);

    /**
     * Lists books by genre name.
     *
     * @param genre {@code String} - genre name to search, not {@code null}
     * @return a {@code Stream} of {@link BookRecord book}s
     */
    Stream<BookRecord> booksByGenre(String genre);


    /**
     * Finds a book-record by the given id.
     *
     * @param id of book-record, {@code long}
     * @return an {@code Optional} of {@link BookRecord book}s
     */
    Optional<BookRecord> find(long id);

    /**
     * Lists all genres.
     *
     * @return a {@code Stream} of {@link GenreRecord genre}s
     */
    Stream<GenreRecord> genres();

    /**
     * Lists all authors.
     *
     * @return a {@code Stream} of {@link AuthorRecord author}s
     */
    Stream<AuthorRecord> authors();

    /**
     * Lists all comments for the given book or
     * for all books in the system if {@code id} is {@code null}.
     *
     * @param id of book-record, {@code Long}; or {@code null} to list all comments
     * @return a {@code Stream} of book {@link CommentRecord comment}s
     */
    Stream<CommentRecord> comments(Long id);

    /**
     * Creates a new book-record with the specified parameters.
     *
     * @param title   {@code String}, not {@code null}
     * @param genre   {@code String}, not {@code null}
     * @param authors {@code List} of {@code String}s, not {@code null}, not empty
     * @return {@link BookRecord}
     */
    BookRecord create(String title, String genre, List<String> authors);

    /**
     * Deletes a book-record by the specified {@code id}.
     * Also, if it is possible, deletes all its related components (genre, authors, comments).
     *
     * @param id of book-record, {@code long}
     * @return {@code true} if the record has been deleted, otherwise {@code false}
     */
    boolean delete(long id);

    /**
     * Adds comment for the given book-record.
     *
     * @param bookId  - {@code long}, the book-record identifier
     * @param comment - {@code String}, comment body, not {@code null}
     * @return an {@code Optional} around {@link CommentRecord comment}
     */
    Optional<CommentRecord> addComment(long bookId, String comment);

    /**
     * Deletes the last (by {@link Comment#getDate() date}) comment for the specified book.
     *
     * @param bookId - {@code long}, the book-record identifier
     * @return an {@code Optional} around {@link CommentRecord comment},
     * that has been deleted (empty if no deletion occurred or the comment/book was not found)
     */
    Optional<CommentRecord> deleteComment(long bookId);

    /**
     * Lists all book-records with (head) limit.
     *
     * @param head {@code long} (negative to list all)
     * @return a {@code Stream} of {@link BookRecord}s
     */
    default Stream<BookRecord> books(long head) {
        return head < 0 ? books() : books().limit(head);
    }

    /**
     * Lists all authors with (head) limit.
     *
     * @param head {@code long} (negative to list all)
     * @return a {@code Stream} of {@code String}-authors
     */
    default Stream<AuthorRecord> authors(long head) {
        return head < 0 ? authors() : authors().limit(head);
    }
}
