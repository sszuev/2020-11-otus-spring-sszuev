package com.gitlab.sszuev.books.dao;

import com.gitlab.sszuev.books.domain.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * Created by @ssz on 04.01.2021.
 */
public interface CommentRepository extends JpaRepository<Comment, Long> {

    /**
     * Lists all comments from the underlying storage by the given id.
     * The output is a sorted (by {@link Comment#getDate() date} in reverse order) stream.
     *
     * @param bookId {@code long}, the book-record identifier
     * @return a sorted {@code Stream} of {@link Comment}s
     * @see #findAllByBookId(long)
     */
    @Query("SELECT c FROM Comment c WHERE c.book.id = :bookId ORDER BY c.date DESC")
    Stream<Comment> streamAllByBookId(long bookId);

    /**
     * Lists all comments from the underlying.
     * The output is a sorted (by {@link Comment#getDate() date} in reverse order) stream.
     *
     * @return a sorted {@code Stream} of {@link Comment}s
     * @see JpaRepository#findAll()
     */
    @Query("SELECT c FROM Comment c ORDER BY c.date DESC")
    Stream<Comment> streamAll();

    /**
     * Gets all comments as {@code List}.
     *
     * @param bookId {@code long}, the book-record identifier
     * @return a {@code List} of {@link Comment}s
     */
    List<Comment> findAllByBookId(long bookId);

    /**
     * Finds the last (by {@link Comment#getDate() date}) comment on the specified book.
     *
     * @param bookId {@code long}, the book-record identifier
     * @return a {@code Optional} around of {@link Comment}
     */
    default Optional<Comment> findLast(long bookId) {
        try (Stream<Comment> comments = streamAllByBookId(bookId)) {
            return comments.findFirst();
        }
    }
}
