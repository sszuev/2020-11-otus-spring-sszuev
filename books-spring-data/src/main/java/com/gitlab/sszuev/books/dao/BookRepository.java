package com.gitlab.sszuev.books.dao;

import com.gitlab.sszuev.books.domain.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.stream.Stream;

/**
 * A DAO that provides CRUD operations for {@link Book}s instances.
 * Created by @ssz on 29.12.2020.
 */
public interface BookRepository extends JpaRepository<Book, Long> {

    /**
     * Returns a {@code List} of entities.
     * To solve "n+1 problem" there is {@code JOIN FETCH} in {@link Query}.
     *
     * @return a {@code List} of all {@link Book Book}s
     * @see JpaRepository#findAll()
     */
    @Query("SELECT b FROM Book b JOIN FETCH b.genre")
    List<Book> fetchAll();

    /**
     * Lists entities.
     *
     * @return a {@code Stream} of all {@link Book Book}s
     */
    default Stream<Book> streamAll() {
        return fetchAll().stream();
    }

}
