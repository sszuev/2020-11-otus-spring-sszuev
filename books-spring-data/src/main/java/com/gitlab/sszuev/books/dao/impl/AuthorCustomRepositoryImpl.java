package com.gitlab.sszuev.books.dao.impl;

import com.gitlab.sszuev.books.dao.AuthorCustomRepository;
import com.gitlab.sszuev.books.domain.Author;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 * Created by @ssz on 01.01.2021.
 */
@SuppressWarnings("unused")
public class AuthorCustomRepositoryImpl implements AuthorCustomRepository {
    @PersistenceContext
    private EntityManager em;

    @Override
    public boolean deleteIfPossible(long id) {
        Author a = em.find(Author.class, id);
        if (a == null) {
            // no entity found
            return false;
        }
        // Note: entity 'Author' has no reference on entity 'Book'
        Query q = em.createQuery("DELETE FROM Author a WHERE a.id = :id " +
                "AND (SELECT count(a) FROM Book b JOIN b.authors a WHERE a.id = :id) = 0");
        q.setParameter("id", id);
        if (q.executeUpdate() > 0) {
            // remove also from the cache (to be sure there is no "zombie" entities)
            em.detach(a);
            return true;
        }
        // entity is found but cannot be deleted since it is bound on FK constraint
        return false;
    }
}
