package com.gitlab.sszuev.books.shell;

import com.gitlab.sszuev.books.dto.BookRecord;
import com.gitlab.sszuev.books.dto.CommentRecord;
import com.gitlab.sszuev.books.service.LibraryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;

import javax.annotation.PostConstruct;
import java.io.PrintStream;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by @ssz on 30.12.2020.
 *
 * @see <a href='https://gitlab.com/sszuev/2020-11-otus-spring-sszuev/-/blob/15c3f1845f97320908d6066c076cda9d35c18c41/books/src/main/java/com/gitlab/sszuev/books/shell/BooksCommands.java'>previous version</a>
 */
@ShellComponent
public class ShellController {

    private final LibraryService service;
    private final PrettyPrinter printer;
    private final PrintStream out;

    @Autowired
    public ShellController(LibraryService service, PrettyPrinter printer) {
        this(service, printer, System.out);
    }

    protected ShellController(LibraryService service,
                              PrettyPrinter printer,
                              PrintStream out) {
        this.service = Objects.requireNonNull(service);
        this.printer = Objects.requireNonNull(printer);
        this.out = Objects.requireNonNull(out);
    }

    @PostConstruct
    public void postInit() {
        out.println("Welcome to Library!");
    }

    @ShellMethod(value = "Print all book records.", key = {"list", "books", "list-books"})
    public void printBooks(@ShellOption(help = "Limit", defaultValue = "-1") long limit) {
        printBooks(service.books(limit));
    }

    @ShellMethod(value = "Print books by author.", key = {"list-by-author", "books-by-author", "author"})
    public void printBooksByAuthor(@ShellOption(help = "author name") String author) {
        printBooks(service.booksByAuthor(author));
    }

    @ShellMethod(value = "Print books by genre.", key = {"list-by-genre", "books-by-genre", "genre"})
    public void printBooksByGenre(@ShellOption(help = "genre name") String genre) {
        printBooks(service.booksByGenre(genre));
    }

    private void printBooks(Stream<BookRecord> books) {
        out.println(printer.printBookHeader());
        out.println(printer.printSeparator());
        books.map(printer::printBookRow).forEach(out::println);
    }

    @ShellMethod(value = "Print all genres.", key = {"genres", "list-genres"})
    public void printGenres() {
        out.println(printer.printGenreHeader());
        out.println(printer.printSeparator());
        service.genres().map(printer::printGenre).forEach(out::println);
    }

    @ShellMethod(value = "Print all authors.", key = {"authors", "list-authors"})
    public void printAuthors(@ShellOption(help = "Limit", defaultValue = "-1") long limit) {
        out.println(printer.printAuthorHeader());
        out.println(printer.printSeparator());
        service.authors(limit).map(printer::printAuthor).forEach(out::println);
    }

    @ShellMethod(value = "Print book comments.", key = {"comments", "list-comments"})
    public void printComments(@ShellOption(help = "The book ID (optional)", defaultValue = ShellOption.NULL) Long id) {
        if (id != null && service.find(id).isEmpty()) {
            out.println("No book with id=" + id + " found!");
            return;
        }
        out.println(printer.printCommentHeader());
        out.println(printer.printSeparator());
        service.comments(id).map(printer::printComment).forEach(out::println);
    }

    @ShellMethod(value = "Add new book record.", key = {"add", "create"})
    public void add(@ShellOption(help = "The book title") String title,
                    @ShellOption(help = "The book genre") String genre,
                    @ShellOption(help = "The book primary author") String author1,
                    @ShellOption(help = "The book author #2", defaultValue = ShellOption.NULL) String author2,
                    @ShellOption(help = "The book author #3", defaultValue = ShellOption.NULL) String author3,
                    @ShellOption(help = "The book author #4", defaultValue = ShellOption.NULL) String author4,
                    @ShellOption(help = "The book author #5", defaultValue = ShellOption.NULL) String author5,
                    @ShellOption(help = "The book author #6", defaultValue = ShellOption.NULL) String author6,
                    @ShellOption(help = "The book author #7", defaultValue = ShellOption.NULL) String author7,
                    @ShellOption(help = "The book author #8", defaultValue = ShellOption.NULL) String author8,
                    @ShellOption(help = "The book author #9", defaultValue = ShellOption.NULL) String author9,
                    @ShellOption(help = "The book author #10", defaultValue = ShellOption.NULL) String author10) {
        List<String> authors = Stream.of(author1, author2, author3, author4, author5, author6, author7, author8, author9, author10)
                .filter(Objects::nonNull).collect(Collectors.toList());
        long id = service.create(title, genre, authors).getId();
        out.println("New record has been added, id = " + id + ".");
    }

    @ShellMethod(value = "Delete book record.", key = {"del", "delete"})
    public void delete(@ShellOption(help = "The book ID") long id) {
        if (service.delete(id)) {
            out.println("Record with id=" + id + " has been deleted.");
        } else {
            out.println("Record with id=" + id + " has not been deleted!");
        }
    }

    @ShellMethod(value = "Add comment on the book record.", key = {"add-comment", "create-comment", "comment"})
    public void addComment(@ShellOption(help = "The book ID") long id,
                           @ShellOption(help = "The comment message") String comment) {
        Optional<CommentRecord> res = service.addComment(id, comment);
        if (res.isPresent()) {
            out.println("New comment for the book-record " + id + " has been added, its id = " + res.get().getId() + ".");
        } else {
            out.println("No book-record with id=" + id + " found!");
        }
    }

    @ShellMethod(value = "Delete last comment for the record.", key = {"del-comment", "delete-comment"})
    public void deleteComment(@ShellOption(help = "The book ID") long id) {
        Optional<CommentRecord> res = service.deleteComment(id);
        if (res.isPresent()) {
            out.println("Comment for the book-record " + id + " has been deleted, its id was " + res.get().getId() + ".");
        } else {
            out.println("No comment has been deleted!");
        }
    }
}
