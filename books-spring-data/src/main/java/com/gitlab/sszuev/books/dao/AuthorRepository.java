package com.gitlab.sszuev.books.dao;

import com.gitlab.sszuev.books.domain.Author;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.stream.Stream;

/**
 * Created by @ssz on 01.01.2021.
 */
public interface AuthorRepository extends JpaRepository<Author, Long>, AuthorCustomRepository {

    /**
     * Lists all authors from the underlying storage.
     *
     * @return a {@code Stream} of {@link Author}s
     */
    @Query("SELECT a FROM Author a")
    Stream<Author> streamAll();

    /**
     * Lists all authors by the specified name including book's data loaded.
     *
     * @param name {@code String} - author name to search, not {@code null}
     * @return a {@code Stream} of {@link Author}s
     */
    @EntityGraph(attributePaths = {"books", "books.genre"}, type = EntityGraph.EntityGraphType.FETCH)
    @Query("SELECT a FROM Author a WHERE LOWER(TRIM(a.name)) = LOWER(TRIM(:name))")
    Stream<Author> streamByNameIncludingBooks(String name);

    /**
     * Lists all author entities which names are in the given list.
     *
     * @param names a {@code List} of {@code String}-names to check, not {@code null}
     * @return a {@code Stream} of {@link Author}s
     */
    Stream<Author> streamByNameIn(List<String> names);

    /**
     * Lists all author entities which names are in the given list.
     *
     * @param names an {@code Array} of {@code String}-names to check, not {@code null}
     * @return a {@code Stream} of {@link Author}s
     */
    default Stream<Author> streamByNameIn(String... names) {
        return streamByNameIn(List.of(names));
    }
}
