package com.gitlab.sszuev.books.dao;

import com.gitlab.sszuev.books.domain.Genre;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.stream.Stream;

/**
 * Created by @ssz on 01.01.2021.
 */
public interface GenreRepository extends JpaRepository<Genre, Long>, GenreCustomRepository {

    /**
     * Lists all entities from the underlying storage.
     *
     * @return a {@code Stream} of {@link Genre}s
     */
    @Query("SELECT g FROM Genre g")
    Stream<Genre> streamAll();

    /**
     * Lists all genres from the underlying storage by the specified name.
     *
     * @param name {@code String}, not {@code null}
     * @return a {@code Stream} of {@link Genre}s
     */
    Stream<Genre> streamByName(String name);

    /**
     * Lists all genres from the underlying storage by the specified name including book's data loaded.
     *
     * @param name {@code String} - genre name to search, not {@code null}
     * @return a {@code Stream} of {@link Genre}s
     */
    @EntityGraph(attributePaths = {"books", "books.genre"}, type = EntityGraph.EntityGraphType.FETCH)
    @Query("SELECT g FROM Genre g WHERE LOWER(TRIM(g.name)) = LOWER(TRIM(:name))")
    Stream<Genre> streamByNameIncludingBooks(String name);
}
