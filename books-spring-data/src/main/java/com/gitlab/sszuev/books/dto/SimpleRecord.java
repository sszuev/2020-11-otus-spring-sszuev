package com.gitlab.sszuev.books.dto;

/**
 * Created by @ssz on 02.01.2021.
 */
abstract class SimpleRecord extends BaseRecord {
    final String name;

    SimpleRecord(long id, String name) {
        super(id);
        this.name = name;
    }

    public final String getName() {
        return name;
    }
}
