SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;
SET default_tablespace = '';
SET default_table_access_method = heap;

CREATE TABLE public.authors
(
    id   integer                NOT NULL,
    name character varying(255) NOT NULL
);

ALTER TABLE public.authors OWNER TO dev;

CREATE SEQUENCE public.authors_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE CACHE 1;

ALTER TABLE public.authors_id_seq OWNER TO dev;

ALTER SEQUENCE public.authors_id_seq OWNED BY public.authors.id;

CREATE TABLE public.book_authors
(
    book_id   bigint NOT NULL,
    author_id bigint NOT NULL
);

ALTER TABLE public.book_authors OWNER TO dev;

CREATE TABLE public.books
(
    id       integer                NOT NULL,
    title    character varying(255) NOT NULL,
    genre_id bigint                 NOT NULL
);

ALTER TABLE public.books OWNER TO dev;

CREATE SEQUENCE public.books_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE CACHE 1;

ALTER TABLE public.books_id_seq OWNER TO dev;

ALTER SEQUENCE public.books_id_seq OWNED BY public.books.id;

CREATE TABLE public.genres
(
    id   integer                NOT NULL,
    name character varying(255) NOT NULL
);

ALTER TABLE public.genres OWNER TO dev;

CREATE SEQUENCE public.genres_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE CACHE 1;

ALTER TABLE public.genres_id_seq OWNER TO dev;

ALTER SEQUENCE public.genres_id_seq OWNED BY public.genres.id;

ALTER TABLE ONLY public.authors ALTER COLUMN id SET DEFAULT nextval('public.authors_id_seq'::regclass);

ALTER TABLE ONLY public.books ALTER COLUMN id SET DEFAULT nextval('public.books_id_seq'::regclass);

ALTER TABLE ONLY public.genres ALTER COLUMN id SET DEFAULT nextval('public.genres_id_seq'::regclass);

ALTER TABLE ONLY public.authors ADD CONSTRAINT authors_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.book_authors ADD CONSTRAINT book_authors_pkey PRIMARY KEY (book_id, author_id);

ALTER TABLE ONLY public.books ADD CONSTRAINT books_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.genres ADD CONSTRAINT genres_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.genres ADD CONSTRAINT genres_unique_name UNIQUE (name);

ALTER TABLE ONLY public.book_authors
    ADD CONSTRAINT book_authors_author_id_fkey FOREIGN KEY (author_id) REFERENCES public.authors(id);

ALTER TABLE ONLY public.book_authors
    ADD CONSTRAINT book_authors_book_id_fkey FOREIGN KEY (book_id) REFERENCES public.books(id);

ALTER TABLE ONLY public.books
    ADD CONSTRAINT books_genre_id_fkey FOREIGN KEY (genre_id) REFERENCES public.genres(id);