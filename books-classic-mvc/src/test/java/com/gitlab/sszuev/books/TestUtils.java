package com.gitlab.sszuev.books;

import com.gitlab.sszuev.books.domain.Author;
import com.gitlab.sszuev.books.domain.Book;
import com.gitlab.sszuev.books.dto.BookRecord;

import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by @ssz on 31.01.2021.
 */
public class TestUtils {

    public static BookRecord getFirst(Stream<BookRecord> list, long id) {
        return getFirst(list, x -> Objects.equals(x.getId(), id));
    }

    public static <X> X getFirst(Stream<X> list, Predicate<X> test) {
        return list.filter(test).findFirst().orElseThrow(AssertionError::new);
    }

    public static String print(Book b) {
        return String.format("%d ::: %s ::: %s",
                b.getID(), b.getTitle(),
                b.getAuthors().stream().map(Author::getName).collect(Collectors.joining(", ")));
    }
}
