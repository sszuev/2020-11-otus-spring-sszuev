package com.gitlab.sszuev.books.integration;

import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.ext.ScriptUtils;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.util.Objects;

/**
 * Created by @ssz on 18.04.2021.
 */
@Testcontainers
@ExtendWith(SpringExtension.class)
@SpringBootTest
@ActiveProfiles("itest")
@ContextConfiguration(initializers = {IntegrationTestBase.Initializer.class})
public abstract class IntegrationTestBase {
    protected static final String POSTGRES_DOCKER_IMAGE_NAME = "postgres:13.2";
    protected static final String DB_NAME = "integration-tests-db";
    protected static final String DB_USER = "dev";
    protected static final String DB_PWD = "test";
    protected static final String DB_SCHEMA = "ps_schema.sql";
    protected static final String DB_DATA = "ps_data.sql";

    @SuppressWarnings("rawtypes")
    @Container
    protected static PostgreSQLContainer<?> container = new PostgreSQLContainer(POSTGRES_DOCKER_IMAGE_NAME) {
        {
            withDatabaseName(DB_NAME);
            withUsername(DB_USER);
            withPassword(DB_PWD);
        }

        @Override
        protected void runInitScriptIfRequired() {
            ScriptUtils.runInitScript(getDatabaseDelegate(), DB_SCHEMA);
            ScriptUtils.runInitScript(getDatabaseDelegate(), DB_DATA);
        }
    };

    static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        @Override
        public void initialize(ConfigurableApplicationContext context) {
            TestPropertyValues.of(
                    "spring.datasource.url=" + container.getJdbcUrl(),
                    "spring.datasource.username=" + container.getUsername(),
                    "spring.datasource.password=" + container.getPassword()
            ).applyTo(context.getEnvironment());
        }
    }

    @Autowired
    protected JdbcTemplate template;

    @BeforeEach
    public void validateSettings() throws Exception {
        DataSource dataSource = template.getDataSource();
        Assumptions.assumeTrue(null != dataSource);
        Connection connection = dataSource.getConnection();
        Assumptions.assumeTrue(null != connection);
        DatabaseMetaData data = connection.getMetaData();
        Assumptions.assumeTrue(null != data);
        Assumptions.assumeTrue(Objects.equals(container.getJdbcUrl(), data.getURL()));
    }
}
