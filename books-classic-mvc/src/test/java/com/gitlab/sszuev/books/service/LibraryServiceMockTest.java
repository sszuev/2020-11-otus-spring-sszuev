package com.gitlab.sszuev.books.service;

import com.gitlab.sszuev.books.TestUtils;
import com.gitlab.sszuev.books.dao.AuthorRepository;
import com.gitlab.sszuev.books.dao.BookRepository;
import com.gitlab.sszuev.books.dao.GenreRepository;
import com.gitlab.sszuev.books.domain.Author;
import com.gitlab.sszuev.books.domain.Book;
import com.gitlab.sszuev.books.domain.EntityFactory;
import com.gitlab.sszuev.books.domain.Genre;
import com.gitlab.sszuev.books.dto.BookRecord;
import com.gitlab.sszuev.books.dto.PagedResult;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Pageable;
import org.springframework.data.support.PageableExecutionUtils;

import java.util.List;
import java.util.stream.Stream;

/**
 * Created by @ssz on 31.01.2021.
 */
@SpringBootTest
public class LibraryServiceMockTest {
    @Autowired
    private LibraryService service;
    @MockBean
    private BookRepository bookRepository;
    @MockBean
    private GenreRepository genreRepository;
    @MockBean
    private AuthorRepository authorRepository;

    @Test
    public void testGetBooks() {
        Author a1 = EntityFactory.newAuthor(-1L, "author1");
        Author a2 = EntityFactory.newAuthor(-2L, "author2");
        Genre g1 = EntityFactory.newGenre(-3L, "genre");
        Book b1 = EntityFactory.newBook(-42L, "test-title-1", g1, List.of(a1));
        Book b2 = EntityFactory.newBook(-21L, "test-title-2", g1, List.of(a1, a2));

        Mockito.when(bookRepository.fetchAll(Mockito.any())).thenAnswer(i -> {
            Pageable p = i.getArgument(0);
            return p.getPageNumber() > 0 ?
                    PageableExecutionUtils.getPage(List.of(), p, () -> 0) :
                    PageableExecutionUtils.getPage(List.of(b1, b2), p, () -> 2);
        });

        PagedResult<BookRecord> books1 = service.getPage(0);
        Assertions.assertNotNull(books1);
        Assertions.assertEquals(1, books1.getPageCount());
        Assertions.assertEquals(2, books1.listContent().count());
        Stream.of(b1, b2).forEach(b -> Assertions.assertEquals(b.getTitle(),
                TestUtils.getFirst(books1.listContent(), b.getID()).getTitle()));

        PagedResult<BookRecord> books2 = service.getPage(42);
        Assertions.assertEquals(0, books2.getPageCount());
        Assertions.assertEquals(0, books2.listContent().count());

        Assertions.assertThrows(IllegalArgumentException.class, () -> service.getPage(-12));
    }

    @Test
    public void testDeleteBook() {
        long id1 = 42;
        long id2 = -2;
        long id3 = -1;
        service.delete(id1);
        service.delete(id1);
        service.delete(id2);
        Mockito.verify(bookRepository, Mockito.times(2)).deleteById(id1);
        Mockito.verify(bookRepository, Mockito.times(1)).deleteById(id2);
        Mockito.verify(bookRepository, Mockito.never()).deleteById(id3);
    }

    @Test
    public void testCreateFreshBookTuple() {
        Author author1 = EntityFactory.newAuthor(-1L, "author1");
        Author author2 = EntityFactory.newAuthor(-2L, "author2");
        Genre genre = EntityFactory.newGenre(-3L, "test-genre");
        Book book = EntityFactory.newBook(-42L, "test-title", genre, List.of(author1, author2));

        Mockito.when(bookRepository.save(Mockito.any())).thenReturn(book);

        BookRecord actual = service.save(null, book.getTitle(), genre.getName(),
                List.of(author1.getName(), author2.getName()));
        Assertions.assertNotNull(actual);
        Assertions.assertEquals(book.getTitle(), actual.getTitle());
        Assertions.assertEquals(book.getID(), actual.getId());
        Assertions.assertEquals(genre.getName(), actual.getGenre().getName());
    }

    @Test
    public void testCreateBookWithExistingAuthorsAndGenre() {
        Exception ex1 = new TestEx();
        Exception ex2 = new TestEx();

        Mockito.when(authorRepository.streamByNameIn(Mockito.eq(List.of("A")))).thenThrow(ex1);
        Mockito.when(genreRepository.findByName(Mockito.eq("G"))).thenThrow(ex2);
        Assertions.assertSame(ex1, Assertions.assertThrows(TestEx.class,
                () -> service.save(null, "title", "genre", List.of("A"))));
        Assertions.assertSame(ex2, Assertions.assertThrows(TestEx.class,
                () -> service.save(null, "title", "G", List.of("Y", "Z"))));
    }

    private static class TestEx extends RuntimeException {
    }

}
