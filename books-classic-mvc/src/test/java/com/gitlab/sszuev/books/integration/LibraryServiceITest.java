package com.gitlab.sszuev.books.integration;

import com.gitlab.sszuev.books.dto.BookRecord;
import com.gitlab.sszuev.books.dto.PagedResult;
import com.gitlab.sszuev.books.service.LibraryService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by @ssz on 18.04.2021.
 */
public class LibraryServiceITest extends IntegrationTestBase {
    private static final Logger LOGGER = LoggerFactory.getLogger(LibraryServiceITest.class);

    private static final int EXPECTED_PAGE_COUNT = 3;
    private static final int EXPECTED_PAGE_SIZE = 3;

    private static final long LAST_RECORD_ID = 81;
    private static final String LAST_RECORD_TITLE = "Хобби";

    @Autowired
    private LibraryService service;

    @Test
    public void testAddDeleteRecord() {
        String suffix = "-" + System.currentTimeMillis();
        String title = "TheBook" + suffix;
        String genre = "TheTale" + suffix;
        List<String> authors = List.of("TheAuthor1" + suffix, "TheAuthor2" + suffix);

        BookRecord record = service.save(null, title, genre, authors);
        assertBookRecord(record, title, genre, authors);
        Assertions.assertTrue(record.getId() > LAST_RECORD_ID);

        Assertions.assertTrue(service.find(record.getId()).map(r -> assertBookRecord(r, title, genre, authors)).isPresent());

        PagedResult<BookRecord> page1 = service.getPage(0);
        Assertions.assertNotNull(page1);
        Assertions.assertEquals(EXPECTED_PAGE_COUNT, page1.getPageCount());
        List<BookRecord> records1 = page1.listContent().peek(LibraryServiceITest::debug).collect(Collectors.toList());
        Assertions.assertEquals(EXPECTED_PAGE_SIZE, records1.size());
        BookRecord lastRecord1 = records1.get(0);
        Assertions.assertEquals(record.getId(), lastRecord1.getId());
        assertBookRecord(lastRecord1, title, genre, authors);

        service.delete(record.getId());

        PagedResult<BookRecord> page2 = service.getPage(0);
        Assertions.assertNotNull(page2);
        Assertions.assertEquals(EXPECTED_PAGE_COUNT, page2.getPageCount());
        List<BookRecord> records2 = page2.listContent().peek(LibraryServiceITest::debug).collect(Collectors.toList());
        Assertions.assertEquals(EXPECTED_PAGE_SIZE, records2.size());
        BookRecord lastRecord2 = records2.get(0);
        Assertions.assertEquals(LAST_RECORD_ID, lastRecord2.getId());
        Assertions.assertEquals(LAST_RECORD_TITLE, lastRecord2.getTitle());
    }

    private static BookRecord assertBookRecord(BookRecord record, String title, String genre, List<String> authors) {
        Assertions.assertNotNull(record);
        Assertions.assertEquals(title, record.getTitle());
        Assertions.assertEquals(genre, record.getGenre().getName());
        Assertions.assertEquals(authors,
                record.listAuthors().map(x -> x.getName()).sorted().collect(Collectors.toList()));
        return record;
    }

    private static void debug(BookRecord record) {
        LOGGER.debug("BOOK::{} || {}", record.getId(), record.getTitle());
    }

}
