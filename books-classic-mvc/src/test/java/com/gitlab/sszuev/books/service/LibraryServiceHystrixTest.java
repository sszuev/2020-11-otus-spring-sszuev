package com.gitlab.sszuev.books.service;

import com.gitlab.sszuev.books.dto.BookRecord;
import com.gitlab.sszuev.books.dto.PagedResult;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by @ssz on 19.04.2021.
 */
@SpringBootTest
public class LibraryServiceHystrixTest {
    @Autowired
    private LibraryService service;
    @MockBean
    private LibraryServiceImpl delegate;

    @Test
    public void testHystrixStub() {
        Mockito.when(delegate.getPage(Mockito.anyInt())).thenThrow(new IllegalStateException("FromMockEx"));
        PagedResult<BookRecord> res = service.getPage(2);
        Assertions.assertNotNull(res);
        List<BookRecord> content = res.listContent().collect(Collectors.toList());
        Assertions.assertEquals(1, content.size());
        Assertions.assertEquals(-1, content.get(0).getId());
    }
}
