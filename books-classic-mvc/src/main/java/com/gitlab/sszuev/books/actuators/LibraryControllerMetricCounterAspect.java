package com.gitlab.sszuev.books.actuators;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * Created by @ssz on 28.03.2021.
 */
@Aspect
@Component
public class LibraryControllerMetricCounterAspect {
    private static final Logger LOGGER = LoggerFactory.getLogger(LibraryControllerMetricCounterAspect.class);
    private static final String LIBRARY_CONTROLLER_CLASS = "com.gitlab.sszuev.books.controller.LibraryController";
    private static final String METRIC_PREFIX = "library.";

    private final Counter listBooksCounter;
    private final Counter deleteBookCounter;
    private final Counter createBookCounter;
    private final Counter editBookCounter;

    public LibraryControllerMetricCounterAspect(MeterRegistry registry) {
        Objects.requireNonNull(registry);
        this.listBooksCounter = registry.counter(METRIC_PREFIX + "list");
        this.deleteBookCounter = registry.counter(METRIC_PREFIX + "delete");
        this.createBookCounter = registry.counter(METRIC_PREFIX + "create");
        this.editBookCounter = registry.counter(METRIC_PREFIX + "edit");
    }

    @AfterReturning("execution(* " + LIBRARY_CONTROLLER_CLASS + ".listBooks(..))")
    public void countListBooks(JoinPoint point) {
        LOGGER.info("Call {}", point);
        listBooksCounter.increment();
    }

    @AfterReturning("execution(* " + LIBRARY_CONTROLLER_CLASS + ".deleteBook(..))")
    public void countDelete(JoinPoint point) {
        LOGGER.info("Call {}", point);
        deleteBookCounter.increment();
    }

    @AfterReturning("execution(* " + LIBRARY_CONTROLLER_CLASS + ".saveBook(..))")
    public void countSaveBook(JoinPoint point) {
        LOGGER.info("Call {}", point);
        (point.getArgs()[0] == null ? createBookCounter : editBookCounter).increment();
    }
}
