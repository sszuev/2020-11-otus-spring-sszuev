package com.gitlab.sszuev.books.controller;

import com.gitlab.sszuev.books.dto.BookRecord;

import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by @ssz on 31.01.2021.
 */
class RecordUtils {

    public static Object[] toArray(BookRecord record) {
        Object[] res = new Object[4];
        res[0] = record.getId();
        res[1] = record.getTitle();
        res[2] = record.getGenre().getName();
        res[3] = getAuthorsAsString(record);
        return res;
    }

    public static String getAuthorsAsString(BookRecord record) {
        return collect(record.listAuthors().map(a -> a.getName()));
    }

    private static String collect(Stream<String> stream) {
        return stream.collect(Collectors.joining(", "));
    }

    public static Stream<String> split(String authors) {
        return Arrays.stream(authors.split(",\\s*"));
    }
}
