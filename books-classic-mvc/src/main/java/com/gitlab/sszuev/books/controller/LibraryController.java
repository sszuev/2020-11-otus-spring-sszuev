package com.gitlab.sszuev.books.controller;

import com.gitlab.sszuev.books.dto.BookRecord;
import com.gitlab.sszuev.books.dto.PagedResult;
import com.gitlab.sszuev.books.service.LibraryService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by @ssz on 31.01.2021.
 */
@Controller
public class LibraryController {
    private final LibraryService service;

    public LibraryController(LibraryService service) {
        this.service = Objects.requireNonNull(service);
    }

    @GetMapping(value = {"/", "/books"})
    public ModelAndView listBooks(@RequestParam(value = "page", required = false) Integer page) {
        int current = toCurrentPageNumber(page);
        PagedResult<BookRecord> res = service.getPage(current - 1);
        Map<String, Object> model = Map.of(
                "books", res.listContent().map(RecordUtils::toArray).collect(Collectors.toList()),
                "pages", IntStream.rangeClosed(1, res.getPageCount()).boxed().collect(Collectors.toList()),
                "page", current);
        return new ModelAndView("list", model);
    }

    @PostMapping(value = "/delete")
    public ModelAndView deleteBook(@RequestParam Long id,
                                   @RequestParam(value = "page", required = false) Integer page) {
        service.delete(id);
        return new ModelAndView("redirect:/books", toModel(page));
    }

    @GetMapping("/create")
    public ModelAndView createBook() {
        return new ModelAndView("edit", Map.of());
    }

    @GetMapping("/edit/{id}")
    public ModelAndView editBook(@PathVariable(value = "id") long id,
                                 @RequestParam(value = "page", required = false) Integer page) {
        BookRecord r = service.find(id).orElseThrow();
        Map<String, Object> model = toModel(page,
                r.getId(), r.getTitle(), r.getGenre().getName(), RecordUtils.getAuthorsAsString(r));
        return new ModelAndView("edit", model);
    }

    @PostMapping(value = {"/edit/{id}", "/new"})
    public ModelAndView saveBook(@PathVariable(value = "id", required = false) Long id,
                                 @RequestParam(value = "page", required = false) Integer page,
                                 @RequestParam("title") String title,
                                 @RequestParam("genre") String genre,
                                 @RequestParam("authors") String authors) {
        service.save(id, title, genre, RecordUtils.split(authors).collect(Collectors.toList()));
        return new ModelAndView("redirect:/books", toModel(page));
    }

    private Map<String, Object> toModel(Integer page) {
        return Map.of("page", toCurrentPageNumber(page));
    }

    private Map<String, Object> toModel(Integer page, Long id, String title, String genre, String authors) {
        return Map.of("page", toCurrentPageNumber(page), "id", id, "title", title, "genre", genre, "authors", authors);
    }

    private int toCurrentPageNumber(Integer page) {
        return page == null ? 1 : page < 1 ? 1 : page;
    }

}
