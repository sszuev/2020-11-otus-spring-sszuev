package com.gitlab.sszuev.books.dao;

import com.gitlab.sszuev.books.domain.Genre;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * Created by @ssz on 01.02.2021.
 */
public interface GenreRepository extends JpaRepository<Genre, Long> {

    /**
     * Finds a {@code Genre} by its name, which must be unique into database.
     *
     * @param name {@code String}, not {@code null}
     * @return an {@code Optional} around {@link Genre}
     */
    Optional<Genre> findByName(String name);
}
