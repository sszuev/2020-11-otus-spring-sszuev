package com.gitlab.sszuev.books.service;

import com.gitlab.sszuev.books.dto.BookRecord;
import com.gitlab.sszuev.books.dto.PagedResult;

import java.util.List;
import java.util.Optional;

/**
 * Created by @ssz on 31.01.2021.
 */
public interface LibraryService {

    /**
     * Gets a page of books.
     *
     * @param page non-negative {@code int}
     * @return a {@link PagedResult} with {@link BookRecord book}s
     */
    PagedResult<BookRecord> getPage(int page);

    /**
     * Finds a book-record by the given id.
     *
     * @param id of book-record, {@code long}
     * @return an {@code Optional} of {@link BookRecord book}s
     */
    Optional<BookRecord> find(long id);

    /**
     * Creates (if {@code id} is {@code null}) or updates a book-record with the specified parameters.
     *
     * @param id      {@code Long}
     * @param title   {@code String}, not {@code null}
     * @param genre   {@code String}, not {@code null}
     * @param authors {@code List} of {@code String}s, not {@code null}, not empty
     * @return {@link BookRecord}, never {@code null}
     */
    BookRecord save(Long id, String title, String genre, List<String> authors);

    /**
     * Deletes a book-record by the specified {@code id}.
     *
     * @param id of book-record, {@code long}
     */
    void delete(long id);
}
