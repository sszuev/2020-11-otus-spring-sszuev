package com.gitlab.sszuev.books;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by @ssz on 31.01.2021.
 */
@SpringBootApplication
@org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker
public class App {

    public static void main(String... args) {
        SpringApplication.run(App.class, args);
    }
}
