package com.gitlab.sszuev.books.service;

import com.gitlab.sszuev.books.domain.Book;
import com.gitlab.sszuev.books.domain.EntityFactory;
import com.gitlab.sszuev.books.dto.BookRecord;
import com.gitlab.sszuev.books.dto.EntityMapper;
import com.gitlab.sszuev.books.dto.PagedResult;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.support.PageableExecutionUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * An implementation of {@link LibraryService} with hystrix support.
 * Can't use a single impl since {@link HystrixCommand}
 * and {@link org.springframework.transaction.annotation.Transactional} are incompatible by default.
 * <p>
 * Created by @ssz on 19.04.2021.
 */
@Service
@Primary
public class LibraryServiceHystrixImpl implements LibraryService {
    private final LibraryServiceImpl delegate;
    private final EntityMapper mapper;

    public LibraryServiceHystrixImpl(LibraryServiceImpl base, EntityMapper mapper) {
        this.delegate = Objects.requireNonNull(base);
        this.mapper = Objects.requireNonNull(mapper);
    }

    @HystrixCommand(commandKey = "getPage", fallbackMethod = "getStubPage", ignoreExceptions = IllegalArgumentException.class)
    @Override
    public PagedResult<BookRecord> getPage(int page) {
        return delegate.getPage(page);
    }

    @SuppressWarnings("unused") // used by hystrix
    public PagedResult<BookRecord> getStubPage(int page) {
        Book res = EntityFactory.newBook(-1L, "UnknownBook", EntityFactory.newGenre(-1L, "UnknownGenre"),
                List.of(EntityFactory.newAuthor(-1L, "UnknownAuthor")));
        return mapper.toRecords(PageableExecutionUtils.getPage(List.of(res), PageRequest.of(page, 1), () -> 1));
    }

    @Override
    public Optional<BookRecord> find(long id) {
        return delegate.find(id);
    }

    @Override
    public BookRecord save(Long id, String title, String genre, List<String> authors) {
        return delegate.save(id, title, genre, authors);
    }

    @Override
    public void delete(long id) {
        delegate.delete(id);
    }
}
