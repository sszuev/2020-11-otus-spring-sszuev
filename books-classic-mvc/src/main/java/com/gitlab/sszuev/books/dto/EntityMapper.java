package com.gitlab.sszuev.books.dto;

import com.gitlab.sszuev.books.domain.Author;
import com.gitlab.sszuev.books.domain.Book;
import com.gitlab.sszuev.books.domain.Genre;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

/**
 * A helper to map JPA Entities to DTO Objects.
 * <p>
 * Created by @ssz on 30.12.2020.
 */
@Component
public class EntityMapper {

    public PagedResult<BookRecord> toRecords(Page<Book> books) {
        return new PagedResult<>(books.getTotalPages(), books.map(this::toRecord).getContent());
    }

    public BookRecord toRecord(Book book) {
        return new BookRecord(book.getID(), book.getTitle(), toRecord(book.getGenre()),
                book.getAuthors().stream().map(this::toRecord).collect(Collectors.toList()));
    }

    public GenreRecord toRecord(Genre genre) {
        return new GenreRecord(genre.getID(), genre.getName());
    }

    public AuthorRecord toRecord(Author author) {
        return new AuthorRecord(author.getID(), author.getName());
    }

}
