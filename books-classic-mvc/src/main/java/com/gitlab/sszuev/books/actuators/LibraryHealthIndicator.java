package com.gitlab.sszuev.books.actuators;

import com.gitlab.sszuev.books.dao.AuthorRepository;
import com.gitlab.sszuev.books.dao.BookRepository;
import com.gitlab.sszuev.books.dao.GenreRepository;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.jdbc.core.ConnectionCallback;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Objects;

/**
 * Created by @ssz on 28.03.2021.
 */
@Component
public class LibraryHealthIndicator implements HealthIndicator {
    private final BookRepository books;
    private final AuthorRepository authors;
    private final GenreRepository genres;
    private final JdbcTemplate template;

    public LibraryHealthIndicator(JdbcTemplate template, BookRepository books, AuthorRepository authors, GenreRepository genres) {
        this.template = Objects.requireNonNull(template);
        this.books = Objects.requireNonNull(books);
        this.authors = Objects.requireNonNull(authors);
        this.genres = Objects.requireNonNull(genres);
    }

    @Override
    public Health health() {
        if (!isConnectionValid()) {
            return Health.down().withDetail("validationQuery", "isValid()").build();
        }
        long books = this.books.count();
        Health.Builder res = books == 0 ? Health.down() : Health.up();
        return res.withDetails(Map.of("books", books, "genres", genres.count(), "authors", authors.count())).build();
    }

    private boolean isConnectionValid() {
        try {
            Boolean res = template.execute((ConnectionCallback<Boolean>) x -> x.isValid(0));
            return res != null ? res : false;
        } catch (Exception e) {
            return false;
        }
    }
}
