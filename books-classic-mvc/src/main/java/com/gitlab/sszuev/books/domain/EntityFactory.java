package com.gitlab.sszuev.books.domain;

import java.util.List;

/**
 * A helper-factory just to create domain entities.
 *
 * Created by @ssz on 31.01.2021.
 */
public class EntityFactory {

    public static Book newBook(Long id, String title, Genre genre, List<Author> authors) {
        Book res = new Book();
        res.setID(id);
        res.setTitle(title);
        res.setGenre(genre);
        res.setAuthors(authors);
        return res;
    }

    public static Author newAuthor(Long id, String author) {
        Author res = new Author();
        res.setID(id);
        res.setName(author);
        return res;
    }

    public static Genre newGenre(Long id, String genre) {
        Genre res = new Genre();
        res.setName(genre);
        res.setID(id);
        return res;
    }

}
