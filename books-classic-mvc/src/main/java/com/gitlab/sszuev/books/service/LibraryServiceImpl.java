package com.gitlab.sszuev.books.service;

import com.gitlab.sszuev.books.dao.AuthorRepository;
import com.gitlab.sszuev.books.dao.BookRepository;
import com.gitlab.sszuev.books.dao.GenreRepository;
import com.gitlab.sszuev.books.domain.Author;
import com.gitlab.sszuev.books.domain.EntityFactory;
import com.gitlab.sszuev.books.domain.Genre;
import com.gitlab.sszuev.books.dto.BookRecord;
import com.gitlab.sszuev.books.dto.EntityMapper;
import com.gitlab.sszuev.books.dto.PagedResult;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by @ssz on 31.01.2021.
 */
@Service
public class LibraryServiceImpl implements LibraryService {
    private final BookRepository bookRepository;
    private final AuthorRepository authorRepository;
    private final GenreRepository genreRepository;
    private final EntityMapper mapper;
    private final int pageSize;

    public LibraryServiceImpl(BookRepository bookRepository,
                              AuthorRepository authorRepository,
                              GenreRepository genreRepository,
                              EntityMapper entityMapper,
                              @Value("${app.web.page-size:5}") int pageSize) {
        this.bookRepository = Objects.requireNonNull(bookRepository);
        this.authorRepository = Objects.requireNonNull(authorRepository);
        this.genreRepository = Objects.requireNonNull(genreRepository);
        this.mapper = Objects.requireNonNull(entityMapper);
        this.pageSize = pageSize;
    }

    @Transactional(readOnly = true)
    @Override
    public PagedResult<BookRecord> getPage(int page) {
        if (page < 0) throw new IllegalArgumentException("Negative page number : " + page);
        PageRequest request = PageRequest.of(page, pageSize, Sort.by(Sort.Direction.DESC, "id"));
        return mapper.toRecords(bookRepository.fetchAll(request));
    }

    @Transactional(readOnly = true)
    @Override
    public Optional<BookRecord> find(long id) {
        return bookRepository.findById(id).map(mapper::toRecord);
    }

    @Transactional
    @Override
    public BookRecord save(Long id, String title, String genre, List<String> authors) {
        return mapper.toRecord(bookRepository.save(EntityFactory.newBook(id, title, fetchGenre(genre), fetchAuthors(authors))));
    }

    @Transactional
    @Override
    public void delete(long id) {
        bookRepository.deleteById(id);
    }

    private List<Author> fetchAuthors(List<String> authors) {
        // Note: right now there is no unique constraint on column AUTHORS.NAME !
        Map<String, List<Author>> found = authorRepository.streamByNameIn(authors)
                .collect(Collectors.groupingBy(Author::getName, Collectors.toList()));
        return authors.stream()
                .map(a -> found.computeIfAbsent(a, x -> List.of(EntityFactory.newAuthor(null, x))))
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    private Genre fetchGenre(String genre) {
        return genreRepository.findByName(genre).orElseGet(() -> EntityFactory.newGenre(null, genre));
    }

}
