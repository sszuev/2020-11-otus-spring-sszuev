FROM postgres:13.2

ENV POSTGRES_DB=devdb
ENV POSTGRES_USER=dev
ENV POSTGRES_PASSWORD=dev

COPY database/ps_schema.sql /docker-entrypoint-initdb.d/01_schema.sql
COPY database/ps_data.sql /docker-entrypoint-initdb.d/02_data.sql

EXPOSE 5432
CMD ["postgres"]