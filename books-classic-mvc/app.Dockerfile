FROM maven:3.8.1-jdk-11

ENV TARGET_DIR=/opt/project

RUN mkdir -p $TARGET_DIR
WORKDIR $TARGET_DIR
ADD ./pom.xml $TARGET_DIR
RUN mvn dependency:resolve
ADD ./src/ $TARGET_DIR/src
RUN echo "DO:::BUILD"
RUN mvn clean package

FROM openjdk:11.0.10-oracle

ENV APP_NAME=books-library
ENV TARGET_DIR=/opt/project
ENV JAR_PATH=${TARGET_DIR}/target/${APP_NAME}.jar
ENV JAVA_OPTS="-Dspring.profiles.active=docker"

RUN mkdir -p $TARGET_DIR
WORKDIR $TARGET_DIR
COPY --from=0 $JAR_PATH $JAR_PATH

EXPOSE 8080

RUN echo "DO:::RUN ${JAR_PATH}"
CMD java $JAVA_OPTS -jar ${JAR_PATH}