#### This is a home-work 'Spring MVC View'

A simple classic web application to manage information about library books.  
Once launched, the application must be available at the address http://localhost:8080.

###### For [OtusTeam](https://otus.ru).

##### Actuator links:

- http://localhost:8080/actuator/health/library
- http://localhost:8080/actuator/metrics/library.list
- http://localhost:8080/actuator/metrics/library.create
- http://localhost:8080/actuator/metrics/library.edit
- http://localhost:8080/actuator/metrics/library.delete
- http://localhost:8080/actuator/logfile

##### Requirements:

- Git
- Java **11**
- Maven **3+**

##### Build and run:
```
$ git clone git@gitlab.com:sszuev/2020-11-otus-spring-sszuev.git
$ cd 2020-11-otus-spring-sszuev/books-classic-mvc
$ mvn clean package
$ java -jar target/books-library.jar
```
_Note: the database (postgres) is required.  
Init script and example data is located in [database](database) dir_

##### Build and run using docker:
```
$ docker build -f db.Dockerfile -t books-classic-mvc-db .
$ docker build -f app.Dockerfile -t books-classic-mvc-app .
$ docker network create books-classic-mvc-net
$ docker run --network books-classic-mvc-net --name books-classic-mvc-db -d -p 5432:5432 books-classic-mvc-db
$ docker run --network books-classic-mvc-net -e JAVA_OPTS='-Dspring.profiles.active=docker -Dspring.datasource.url=jdbc:postgresql://books-classic-mvc-db:5432/devdb' -d -p 8080:8080 books-classic-mvc-app
```
##### Build and run using docker-compose:
```
$ docker-compose -f docker-compose.yml up -d
```